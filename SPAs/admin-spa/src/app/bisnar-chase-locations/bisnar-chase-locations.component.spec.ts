import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BisnarChaseLocationsComponent } from './bisnar-chase-locations.component';

describe('BisnarChaseLocationsComponent', () => {
  let component: BisnarChaseLocationsComponent;
  let fixture: ComponentFixture<BisnarChaseLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BisnarChaseLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BisnarChaseLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
