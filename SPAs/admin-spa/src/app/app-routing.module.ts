import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { FullTrelloComponent } from './full-trello/full-trello.component';
import { FullUserComponent } from './full-user/full-user.component';
import { FullGithubComponent } from './full-github/full-github.component';
import { BisnarChaseLocationsComponent } from './bisnar-chase-locations/bisnar-chase-locations.component';
import { EditWebsiteContentComponent } from './edit-website-content/edit-website-content.component';
import { WebServicesComponent } from './web-services/web-services.component';
import { BuildPipelineComponent } from './build-pipeline/build-pipeline.component';
import { TechnicalOperationsComponent } from './technical-operations/technical-operations.component';
import { WebsiteStructureComponent } from './website-structure/website-structure.component';
import { SignupPageComponent } from './auth/signup-page/signup-page.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { AuthGuard } from './auth/auth.guard';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';

const routes: Routes = [
  { path: '', component: HomeDashboardComponent, canActivate: [AuthGuard] },
  {
    path: 'sprint-info',
    component: FullTrelloComponent,
    canActivate: [AuthGuard]
  },
  { path: 'users/:id', component: FullUserComponent, canActivate: [AuthGuard] },
  {
    path: 'github-info',
    component: FullGithubComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bisnar-chase-locations',
    component: BisnarChaseLocationsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit-website-content',
    component: EditWebsiteContentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'web-services',
    component: WebServicesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'build-pipeline',
    component: BuildPipelineComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'technical-operations',
    component: TechnicalOperationsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'website-structure',
    component: WebsiteStructureComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'account',
    component: EditAccountComponent,
    canActivate: [AuthGuard]
  },
  { path: 'signup', component: SignupPageComponent },
  { path: 'login', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), NgBootstrapFormValidationModule],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
