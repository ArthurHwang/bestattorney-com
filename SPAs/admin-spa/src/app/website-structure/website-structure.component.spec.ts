import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteStructureComponent } from './website-structure.component';

describe('WebsiteStructureComponent', () => {
  let component: WebsiteStructureComponent;
  let fixture: ComponentFixture<WebsiteStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsiteStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
