import { Injectable, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataService implements OnInit {
  firstname: string;
  lastname: string;
  email: string;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.firstname = this.authService.firstname;
    this.lastname = this.authService.lastname;
    this.email = this.authService.email;
  }
}
