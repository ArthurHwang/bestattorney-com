import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthData } from '../auth/auth-data.model';
import { LoginData } from '../auth/login-data.model';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated = false;
  private token: string;
  private authStatusListener = new Subject<boolean>();
  private tokenTimer: any;
  public firstname: string;
  public lastname: string;
  public email: string;
  public id: number;

  constructor(private http: HttpClient, private router: Router) {}

  createUser(
    firstname: string,
    lastname: string,
    email: string,
    password: string
  ) {
    const authData: AuthData = {
      email: email,
      password: password,
      firstname: firstname,
      lastname: lastname
    };
    this.http.post(environment.signupRoute, authData).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      error => {
        console.log(error);
        this.authStatusListener.next(false);
      }
    );
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  loginUser(email: string, password: string) {
    const authData: LoginData = {
      email: email,
      password: password
    };
    this.http
      .post<{
        id: number;
        token: string;
        expiresIn: number;
        email: string;
        firstname: string;
        lastname: string;
      }>(environment.loginRoute, authData)
      .subscribe(
        response => {
          const token = response.token;
          this.token = token;
          if (token) {
            this.id = response.id;
            this.firstname = response.firstname;
            this.lastname = response.lastname;
            this.email = response.email;
            const expiresInDuration = response.expiresIn;
            this.setAuthTimer(expiresInDuration);
            this.isAuthenticated = true;
            this.authStatusListener.next(true);
            const now = new Date();
            const expirationDate = new Date(
              now.getTime() + expiresInDuration * 1000
            );
            this.saveAuthData(
              token,
              expirationDate,
              this.id,
              this.email,
              this.firstname,
              this.lastname
            );
            this.router.navigate(['/']);
          }
        },
        error => {
          console.log(error);
          this.authStatusListener.next(false);
        }
      );
  }

  editUser(
    firstname: string,
    lastname: string,
    email: string,
    password: string
  ) {
    const userData = {
      firstname: firstname,
      lastname: lastname,
      email: email,
      password: password
    };
    this.http
      .put<{
        msg: string;
        updatedUser: any;
      }>(environment.editRoute + `${this.id}/`, userData)
      .subscribe(
        response => {
          const token = response.updatedUser.token;
          this.token = token;
          if (token) {
            this.id = response.updatedUser.id;
            this.firstname = response.updatedUser.firstname;
            this.lastname = response.updatedUser.lastname;
            this.email = response.updatedUser.email;
            const expiresInDuration = response.updatedUser.expiresIn;
            this.setAuthTimer(expiresInDuration);
            this.isAuthenticated = true;
            this.authStatusListener.next(true);
            const now = new Date();
            const expirationDate = new Date(
              now.getTime() + expiresInDuration * 1000
            );
            this.saveAuthData(
              token,
              expirationDate,
              this.id,
              this.email,
              this.firstname,
              this.lastname
            );
            this.router.navigate(['/']);
          }
        },
        error => {
          console.log(error);
          this.authStatusListener.next(false);
        }
      );
  }

  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.firstname = authInformation.firstname;
      this.lastname = authInformation.lastname;
      this.id = authInformation.id;
      this.email = authInformation.email;
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/login']);
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(
    token: string,
    expirationDate: Date,
    id: number,
    email: string,
    firstname: string,
    lastname: string
  ) {
    localStorage.setItem('id', JSON.stringify(id));
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('email', email);
    localStorage.setItem('firstname', firstname);
    localStorage.setItem('lastname', lastname);
  }

  private clearAuthData() {
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('email');
    localStorage.removeItem('firstname');
    localStorage.removeItem('lastname');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const id = JSON.parse(localStorage.getItem('id'));
    const firstname = localStorage.getItem('firstname');
    const lastname = localStorage.getItem('lastname');
    const email = localStorage.getItem('email');

    if (!token && !expirationDate) {
      return;
    }

    return {
      token: token,
      expirationDate: new Date(expirationDate),
      id: id,
      email: email,
      firstname: firstname,
      lastname: lastname
    };
  }
}
