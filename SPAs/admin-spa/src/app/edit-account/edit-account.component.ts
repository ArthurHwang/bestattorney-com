import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {
  formGroup: FormGroup;
  firstname;
  lastname;
  email;

  constructor(public authService: AuthService) {}

  ngOnInit() {
    this.formGroup = new FormGroup({
      FirstName: new FormControl('', [
        // Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)
      ]),
      LastName: new FormControl('', [
        // Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)
      ]),
      Email: new FormControl('', [
        // Validators.required,
        Validators.pattern(
          /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        )
      ]),
      Password: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(20)
      ]),
      ConfirmPassword: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(20)
      ])
    });
    this.firstname = this.authService.firstname;
    this.lastname = this.authService.lastname;
    this.email = this.authService.email;
  }

  onEditAccount(formGroup: NgForm) {
    console.log(formGroup);
    if (
      formGroup.value.Password !== formGroup.value.ConfirmPassword ||
      formGroup.invalid
    ) {
      return;
    } else {
      this.authService.editUser(
        formGroup.value.FirstName,
        formGroup.value.LastName,
        formGroup.value.Email,
        formGroup.value.Password
      );
    }
  }

  resetForm() {
    this.formGroup.reset();
  }
}
