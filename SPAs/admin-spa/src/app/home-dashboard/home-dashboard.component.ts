import { AuthService } from './../services/auth.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { LoaderIconComponent } from '../loader-icon/loader-icon.component';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.scss']
})
export class HomeDashboardComponent implements OnInit {
  firstname: string;
  lastname: string;

  priorityCards: Array<any>;
  emergencyCards: Array<any>;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.firstname = this.authService.firstname;
    this.lastname = this.authService.lastname;
  }
}
