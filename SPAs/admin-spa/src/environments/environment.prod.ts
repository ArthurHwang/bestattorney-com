export const environment = {
  production: true,
  signupRoute: 'https://www.bestattorney.com/admin/api/signup/',
  loginRoute: 'https://www.bestattorney.com/admin/api/login/',
  editRoute: 'https://www.bestattorney.com/admin/api/user/'
};
