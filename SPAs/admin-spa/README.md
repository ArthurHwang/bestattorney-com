# Angular Administration Dashboard

## Technologies Used:

* Angular
* TypeScript
* Express
* Node.js
* bCrypt
* Sequelize ORM
* MySQL
* BootStrap

![Angular Dashboard](https://arthurhwang.dev/static/projects/bestattorney-admin/screenshot-1.png)
