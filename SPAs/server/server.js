#!/usr/bin/env nodejs

require('dotenv/config')

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const db = require('./models')
const apiRoutes = require('./app/routes/apiRoutes')
const proxy = require("express-http-proxy")
const PORT = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
  next()
})


app.use(express.static(__dirname)); // serve static files from cwd

function getIpFromReq (req) { // get the client's IP address
  var bareIP = ":" + ((req.connection.socket && req.connection.socket.remoteAddress)
    || req.headers["x-forwarded-for"] || req.connection.remoteAddress || "");

  console.log(`BAREIP MATCH: ${bareIP}`)
  return (bareIP.match(/:([^:]+)$/) || [])[1] || "127.0.0.1";
}

// proxying requests from /analytics to www.google-analytics.com.
app.use("/analytics", proxy("www.google-analytics.com", { 
    proxyReqPathResolver: function (req) {
      console.log(`Request URL: ${req.url}`)
      return req.url + (req.url.indexOf("?") === -1 ? "?" : "&")
        + "uip=" + encodeURIComponent(getIpFromReq(req));
    }
}));

apiRoutes(app)

db.sequelize
  .sync()
  .then(() => {
    app.listen(PORT, () => console.log(`************* LISTENING ON PORT ${PORT} *************`))
  })
  .catch(err => console.log(err, 'Something Went Wrong'))
