const db = require('../../models')
const jwt = require('jsonwebtoken')
const bCrypt = require('bcrypt')
const uuid = require('uuid/v5')

module.exports = app => {
  app.post('/signup/', (req, res) => {
    console.log(req.body)
    db.user
      .create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password
      })
      .then(result => res.status(201).json({ message: 'User Created', result: result }))
      .catch(err => {
        res.status(500).json({
          error: err
        })
      })
  })

  app.post('/login/', (req, res) => {
    console.log(req.body)
    let fetchedUser
    db.user
      .findOne({
        where: {
          email: req.body.email
        }
      })
      .then(user => {
        if (!user) {
          return res.status(401).json({
            message: 'Auth failed'
          })
        }
        fetchedUser = user
        console.log(fetchedUser)
        return bCrypt.compare(req.body.password, fetchedUser.password)
      })
      .then(result => {
        if (!result) {
          return res.status(401).json({
            message: 'Auth failed'
          })
        }
        const token = jwt.sign({ email: fetchedUser.email, userId: fetchedUser.id }, process.env.JWT_SECRET, { expiresIn: '1h' })
        res.status(200).json({
          token: token,
          id: fetchedUser.id,
          email: fetchedUser.email,
          firstname: fetchedUser.firstname,
          lastname: fetchedUser.lastname,
          expiresIn: 3600
        })
      })
      .catch(err => {
        console.log(err)
        return res.status(401).json({
          message: 'Auth failed',
          error: err
        })
      })
  })

  app.get('/user/:id/', (req, res) => {
    db.user
      .findOne({
        where: {
          id: req.params.id
        }
      })
      .then(user => res.send(user))
  })

  app.put('/user/:id/', (req, res) => {
    let oldPassword
    let isSamePassword

    db.user
      .findOne({
        where: {
          id: req.params.id
        }
      })
      .then(user => {
        oldPassword = user.password
        bCrypt.compare(req.body.password, oldPassword, (err, res) => {
          isSamePassword = res
        })
      })
      .then(() => {
        if (isSamePassword || req.body.password.length < 6) {
          console.log('FIRST BLOCK')
          return db.user.update(
            {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              email: req.body.email
            },
            { where: { id: req.params.id }, returning: true, plain: true }
          )
        } else {
          console.log('SECOND BLOCK')
          return db.user.update(
            {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              email: req.body.email,
              password: bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null)
            },
            { where: { id: req.params.id }, returning: true, plain: true }
          )
        }
      })
      .then(() => {
        return db.user.findOne({
          where: {
            id: req.params.id
          }
        })
      })
      .then(updatedUser => {
        const token = jwt.sign({ email: updatedUser.email, userId: updatedUser.id }, process.env.JWT_SECRET, { expiresIn: '1h' })
        res.status(200).json({
          msg: 'User credentials updated',
          updatedUser: {
            token: token,
            id: updatedUser.id,
            email: updatedUser.email,
            firstname: updatedUser.firstname,
            lastname: updatedUser.lastname,
            expiresIn: 3600
          }
        })
      })
      .catch(err => {
        console.log(err)
        return res.status(409).json({
          message: 'Cannot update user credentials',
          error: err
        })
      })
  })
}
