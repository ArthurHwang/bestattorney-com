const bCrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    firstname: {
      type: DataTypes.STRING,
      notEmpty: true
    },
    lastname: {
      type: DataTypes.TEXT,
      notEmpty: true
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  })

  User.prototype.validPassword = password => {
    return bCrypt.compareSync(password, this.password)
  }

  User.beforeCreate(function(user, options) {
    user.password = bCrypt.hashSync(user.password, bCrypt.genSaltSync(10), null)
  }) 

  return User
}
