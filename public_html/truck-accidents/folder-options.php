<?php
// Folder Options for /truck-accidents

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 1,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "truckaccident", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => " Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	// "sidebarLinks" => array(
 //    ),

	"sidebarContentTitle" => "Truck Accident Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'><li>If you or a loved one have suffered in a truck accident, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li></ul><p class='title-alt'>Related Pages</p>
	        <ul class='sidebar-buttons'>
	            <li><a href='https://www.bestattorney.com/truck-accidents/'>Truck Accidents</a></li>
	            <li><a href='https://www.bestattorney.com/truck-accidents/garbage-truck-accident-lawyers.html'>Garbage Truck Accidents</a></li>	
	  </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["Truck Driver Negligence", "/images/sb-video-mX8wMo8QTzo.jpg", "mX8wMo8QTzo"],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>