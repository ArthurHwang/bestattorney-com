<?php
// Folder Options for /premises-liability

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "premisesliability", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Premises Liability Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Assault" => "/premises-liability/assault.html",
        "School Bullying Injuries" => "/premises-liability/school-bullying-attorneys.html",
        "Legal Action Against Hotels" => "/premises-liability/legal-action-against-hotels.html",
       
        "Slip and Fall Accidents" => "/premises-liability/slip-and-fall-accidents.html",
        "Premises Liability Home" => "/premises-liability/",
    ),

	"sidebarContentTitle" => "Premises Liability Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'><li>If you or a loved one have suffered as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li></ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["How have you made the world a safer place?", "/images/sb-video-Gr6-H0TJgTE.jpg", "Gr6-H0TJgTE"],
		["Slip and Fall Attorneys in Orange County", "images/sb-video-qvkV_ShDjAE.jpg", "qvkV_ShDjAE"]
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>