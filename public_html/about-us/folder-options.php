<?php
// Folder Options for /about-us


$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"spanishPageEquivalent" => "sobre-nosotros/",


	// =====================================
	// Defining Sidebar Content
	// =====================================
	"sidebarLinksTitle" => "About Us",

	"sidebarLinks" => array(
		"Attorneys" => "/attorneys/",
		"No-Fee Guarantee" => "/about-us/no-fee-guarantee-lawyer.html",
		"Giving Back" => "/giving-back/",
		"Bisnar Chase Scholarship" => "/giving-back/branch-out-scholarship/",
	),

	"showSidebarContactForm" => false,

	"sidebarContentTitle" => "Our Attorneys",

	"sidebarContentHtml" => "
		<p class='subtitle'>Find out more about our staff:</p>
		<hr width='50%' style='margin-top: 0; margin-bottom: 0;' />
		<div class='centered'>
		<ul>
			<li><strong><a href='https://www.bestattorney.com/attorneys/john-bisnar.html'>John Bisnar</a></strong>,<br>
				Founding Partner</li>
			<li><strong><a href='https://www.bestattorney.com/attorneys/brian-chase.html'>Brian Chase</a></strong>,<br>
				Senior Partner</li>

			<li><strong><a href='https://www.bestattorney.com/attorneys/scott-ritsema.html'>Scott Ritsema</a></strong>,<br>
				Partner</li>

				<li><strong><a href='https://www.bestattorney.com/attorneys/gavin-long.html'>H. Gavin Long</a></strong>, <br>
				Partner</li>
					
			<li><strong><a href='https://www.bestattorney.com/attorneys/jerusalem-beligan.html'>Jerusalem Beligan</a></strong>,<br>
				Lawyer</li>
			<li><strong><a href='https://www.bestattorney.com/attorneys/steven-hilst.html'>Steven Hilst</a></strong>, <br>
				Lawyer</li>
			<li><strong><a href='https://www.bestattorney.com/attorneys/tom-antunovich.html'>Tom Antunovich</a></strong>, <br>
				Lawyer</li>
			<li><strong><a href='https://www.bestattorney.com/attorneys/ian-silvers.html'>Ian Silvers</a></strong>, <br>
				Lawyer</li>
			<li><strong><a href='https://www.bestattorney.com/attorneys/krist-biakanja.html'>Krist Biakanja</a></strong>, <br>
				Lawyer</li>
		
			<li><strong><a href='https://www.bestattorney.com/attorneys/legal-administrator-shannon-barker.html'>Shannon Barker</a></strong>,<br>
			Administrator</li>
		</ul>
		</div>
		<hr width='50%' style='margin-top: 0; margin-bottom: 0;' />
		<p align='center'><strong>Call us for a<br>free consultation<br>949-203-3814</strong></p>
	",

	"extraSidebar" => "",

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Bisnar Chase Brand Video', '/images/video-images/brian-brand-video.jpg', 'wmv1HKnhW7U'],
		['Brian Chase on American Law TV', '/images/featured-homepage-images/brian-american-law-television.jpg', 'jKy3wa5M2yM']
	], //array of video information that will show on the sidebar.

	"loadBlog" => false,

	"allowSidebarTruncate" => false, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
