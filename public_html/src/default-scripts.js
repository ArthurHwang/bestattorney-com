require('../js/scripts.js');
require('../js/lightbox.js');
require('../js/autotrack.js');
require('../js/togglebuttons.js');
require('../js/jcarousel.js');
require('../js/jcarousel-control.js');
require('../js/results-carousel.js');
require('../js/default-contact.js');
require('../js/modernizr.js');
require('../js/lazyload.js');

import '../css/style.css';
import '../css/bootstrap-grid.min.css';
import '../css/custom-icons.css';
import '../css/page-element-styles.css';
