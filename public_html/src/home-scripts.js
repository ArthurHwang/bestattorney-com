require('../js/scripts.js')
require('../js/home-scripts.js')
require('../js/lightbox.js')
require('../js/autotrack.js')
require('../js/togglebuttons.js')
require('../js/jcarousel.js')
require('../js/jcarousel-control.js')
require('../js/jcarousel-pagination.js')
require('../js/jcarousel-autoscroll.js')
require('../js/results-carousel.js')
require('../js/home-contact.js')
require('../js/modernizr.js')
require('../js/lazyload.js')
// require('../js/vanillaSlideshow.js')

import '../css/style.css'
import '../css/style-homepage.css'
// import '../css/style-test.css'
import '../css/bootstrap-grid.min.css'
import '../css/custom-icons.css'
import '../css/page-element-styles.css'
