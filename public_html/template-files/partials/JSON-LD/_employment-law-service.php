<?php

$options['jsonld'] .= <<<EOD
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Service",
    "serviceType": "Employment Law Legal Claims",
    "provider": {
        "@type": "Attorney",
        "name": "Bisnar Chase Personal Injury Attorneys",
        "image": "https://www.bestattorney.com/images/text-header-images/Bisnar-Chase-Staff-Photo-2017.jpg"
    },
     "priceRange": "Free Initial Consultation",
    "image": "{$options['featuredImage']}",
    "hasOfferCatalog": {
        "@type": "OfferCatalog",
        "name": "Employment Law",
        "itemListElement": [
            {
                "@type": "Offer",
                "itemOffered": {
                    "@type": "Service",
                    "name": "Lawsuits Against Employers"
                }
            },
            {
                "@type": "Offer",
                "itemOffered": {
                    "@type": "Service",
                    "name": "Employer Illegal Practices Lawsuits"
                }
            },
            {
                "@type": "Offer",
                "itemOffered": {
                    "@type": "Service",
                    "name": "Employment Law Legal Advice"
                }
            },
            {
                "@type": "Offer",
                "itemOffered": {
                    "@type": "Service",
                    "name": "Employment Law Settlements"
                }
            }
        ]
    }
}
</script>

<script type="application/ld+json">
{
 "@context": "https://schema.org/",
 "@type": "WebPage",
 "name": "Bisnar Chase Personal Injury Attorneys",
 "speakable":
 {
  "@type": "SpeakableSpecification",
  "xpath": [
    "/html/head/title",
    "/html/head/meta[@name='description']/@content"
    ]
  },
 "url": "https://www.bestattorney.com"
 }
</script>
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Attorney",
    "name": "Bisnar Chase Personal Injury Attorneys",
    "alternateName": "Bisnar Chase",
    "description": "Orange County Personal Injury Attorneys",
    "url": "https://www.bestattorney.com/",
    "logo": "https://www.bestattorney.com/images/2019-assets/bisnar-chase-logo.svg",
    "image": "https://www.bestattorney.com/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg",
    "telephone": "(949) 203-3814",
    "openingHours": "Mo,Tu,We,Th,Fr 08:30-17:00",
    "hasMap": "https://www.google.com/maps/place/Bisnar+Chase+Personal+Injury+Attorneys/@33.6620595,-117.8664064,15z/data=!4m5!3m4!1s0x0:0xae2eee17ae4e213e!8m2!3d33.6620595!4d-117.8664064",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "1301 Dove St #120",
        "addressLocality": "Newport Beach",
        "addressRegion": "CA",
        "postalCode": "92660",
        "addressCountry": "USA"
    },
    "sameAs": [
        "https://twitter.com/BisnarChase",
        "https://www.linkedin.com/company/bisnar-chase",
        "https://www.facebook.com/BisnarChase",
        "https://profiles.superlawyers.com/california-southern/newport-beach/lawfirm/bisnar-chase-personal-injury-attorneys/623df560-358e-4314-842f-ca5815a5635f.html",
        "https://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-newport-beach-4"
    ]
    }
</script>
<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "name": "Bisnar Chase Personal Injury Attorneys",
    "alternateName": "Bisnar Chase",
    "logo": "https://www.bestattorney.com/images/2019-assets/bisnar-chase-logo.svg",
    "image": "https://www.bestattorney.com/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "1301 Dove St #120",
        "addressLocality": "Newport Beach",
        "addressRegion": "CA",
        "postalCode": "92660",
        "addressCountry": "USA"
    },
    "description": "Orange County Personal Injury Attorneys",
    "url": "https://www.bestattorney.com/",
    "telephone": "(949) 203-3814"
    }
</script>
EOD;
