<!-- THIS FILE IS DEPRECATED, SEE header-contact.php -->
<div id="icons-bar">
    <a id="home-mobile-icon" class="icon-link nopc" href="https://www.bestattorney.com/"><i class="fa fa-home" aria-hidden="true"></i></a>
    <a id="call-mobile-icon" class="icon-link nopc" href="tel:<?php echo $options['callPhone']; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
    <!-- <a id="directions-mobile-icon" class="icon-link nopc" href="https://www.bestattorney.com/contact.html#cn-loc-list"><i class="fa fa-map-marker" aria-hidden="true"></i></a> -->
    <a id="search" class="icon-link" title="Search BestAttorney.com" data-dropdown="#search-form">
      <i class="fa fa-search" aria-hidden="true" style="font-size: 23px;"></i>
    </a>
    <a id="menu-link"  class="nopc icon-link" data-dropdown="#top-menu">
      <i class="fa fa-bars" aria-hidden="true" ></i>
    </a>
</div>
