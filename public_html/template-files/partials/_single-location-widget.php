<?php
//This is the older template that was in effect from June - August 2015, Gilbert replaced this with location-widget.php in late august.


$cityprefix = "http://www.city-data.com/city/";
//Get the content of the city-data page
$url = $cityprefix . str_replace(" ", "-", $city) . "-California.html";

// Determine the average amount of car accident injuries based on the population and the number of total accidents in the last 5 years in this city.
if ($populationscrape) {
	$avginjuries = round(($totalaccidents * 200) / (int) str_replace(",", "", $populationscrape));
} else {
	$populationscrape = "Population not Found.";
}
if ($avginjuries < 4) {
	$avgcolor = "rgb(164, 242, 168)";
	$avgtextcolor = "#1E7E13";
} else if ($avginjuries > 3 && $avginjuries < 6) {
	$avgcolor = "rgb(164, 242, 168)";
	$avgtextcolor = "#1E7E13";
} else if ($avginjuries > 5 && $avginjuries < 10) {
	$avgcolor = "rgb(232, 231, 99)";
	$avgtextcolor = "#D07011";
} else if ($avginjuries >= 10) {
	$avgcolor = "rgb(242, 164, 164)";
	$avgtextcolor = "#A30000";
}

?>

<style>
	#location-box p,
	#footnote-1,
	#footnote-2 {
		padding: 0 !important;
	}

	#city-average-injuries {
		background-color: <?php echo $avgcolor; ?>;
	}

	#city-average-injuries p {
		padding: 10px !important;
		text-align: center;
		margin: auto;
		color: <?php echo $avgtextcolor; ?>;
	}
</style>

<div id="location-box">
	<?php if ($topcontent) { ?>
		<div class="location-box-shadow">
			<div id="top-content">
				<?php echo $topcontent; ?>
				<div class="centered"><a href="<?php if ($addressshow == 'los-angeles') {
																					echo "https://www.bestattorney.com/los-angeles/contact.html";
																				} else {
																					echo "https://www.bestattorney.com/contact.html";
																				} ?>"><span class="phone-number fa-phone"></a></div>
			</div>
		</div>
	<?php } ?>

	<div id="location-box-1" class="row outline border-box no-gutter" style="margin: 0 0 20px">
		<div class="col-md-4 col-sm-4 border-box no-gutter">
			<div class="col-md-1 col-sm-0 border-box" style="font-size: 60px; padding: 14px 0 11px 5px"><i class="fa fa-map-marker"></i></div>
			<div class="col-md-11 col-sm-11 border-box no-gutter b-right">
				<div class="col-md-12 col-sm-12 border-box">
					<h2 class="header-blue"> City of <?php echo $city; ?></h2>
				</div>
				<div class="col-md-12 col-sm-12 border-box" style="padding-bottom: 11px;">
					<p>Population: <?php if ($populationscrape) {
														echo $populationscrape;
													} else {
														echo "Population Not Found";
													} ?></p>
				</div>
			</div>
		</div>

		<div class="col-md-8 col-sm-8 border-box">
			<div class="col-md-11 col-sm-12 border-box no-gutter">
				<p>Most Dangerous Intersection in <?php echo $city; ?>:<br>
					<?php
					if ($intersection1coordinates) {
						$intersection1url = ("https://www.google.com/maps/place/" . str_replace(", ", ",+", $intersection1coordinates));
					} else {
						$intersection1url = ("https://www.google.com/maps/place/" . str_replace(" ", "+", $intersection1) . "+" . $city);
					} ?>
					<span id="intersection"><a target="_blank" href="<?php echo $intersection1url; ?>"><?php echo $intersection1; ?></a></span> </p>
				<p>Last 5 Years: <?php echo $intersection1accidents; ?> Accidents | <?php echo $intersection1injuries; ?> Injuries | <?php echo $intersection1deaths; ?> Deaths
			</div>
			<div class="col-md-1 col-sm-0 border-box " style="font-size: 50px; padding: 19px 0 16px;">
				<i class="fa fa-car"></i>
			</div>
		</div>

	</div>

	<div id="location-box-2" class="row outline border-box no-gutter outline" style="margin: 0 0 20px">
		<div class="col-md-4 col-sm-4 border-box no-gutter">
			<div class="col-md-12 col-sm-12 border-box no-gutter">
				<div class="col-md-10 col-sm-8 border-box b-right " style="padding: 10px;">
					<p>Avg Annual Auto Injuries per 1,000:</p>
				</div>
				<div id="city-average-injuries" class="city-average-injuries col-md-2 col-sm-4 border-box no-gutter">
					<p class="header-red"><?php echo $avginjuries; ?></p>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 border-box b-top" style="padding: 4px 10px;">
				<div class="col-md-8 col-sm-8 border-box no-gutter">
					<p class="<?php
										if (strlen($city) > 15) {
											echo 'font-12';
										} ?>"><strong><?php echo $city; ?> Crime Index<span class="font-12">*</span></strong></p>
				</div>
				<div id="nearby-cities-div" class="col-md-4 col-sm-4 border-box font-14 no-gutter">
					<p style='padding:0;'>Nearby Cities:</p>
				</div>
				<div class="clearfix"></div>
				<div id="crime-index-number" class="col-md-8 col-sm-8 border-box font-24 no-gutter">
					<?php if ($crimeindex) {
						echo $crimeindex;
					} else {
						echo $crimeratescrape;
					} ?>
				</div>
				<div class="col-md-4 col-sm-4 border-box font-12 no-gutter " style="line-height:26px;">
					<?php //This code iterates through all of the nearby cities and prints out the city name with the city crime index.
					// If the city crime index doesn't exist, neither is printed out. If the city name is more than 11 characters, the font size is decreased so it doesn't mess up formatting.
					for ($i = 1; $i < 5; $i++) {
						if (${"city" . $i . "name"}) {
							echo "<div class='col-md-12 col-sm-12 col-xs-4 border-box no-gutter mobile-centered'><p style='padding:0;";
							if (strlen(${"city" . $i . "name"}) > 11) {
								echo ("font-size:10px;'>");
							} else {
								echo "'>";
							}
							echo (${"city" . $i . "name"} . ": <strong>" . ${"city" . $i . "index"} . "</strong></p></div>");
						}
					} ?>
				</div>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 border-box no-gutter b-left ">
			<div class="location-image col-md-6 col-sm-12 no-gutter border-box">
				<iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?zoom=14&key=AIzaSyBHvIxGvUNkk9zG7tYmKUCmv20mttRKz6Y&q=<?php if ($intersection1coordinates) {
																																																																																										echo str_replace(", ", ",+", $intersection1coordinates);
																																																																																									} else {
																																																																																										echo (str_replace("&", "and", str_replace(" ", "+", $intersection1) . "+" . str_replace(" ", "+", $city)));
																																																																																									} ?>">
				</iframe>

				<!--<img data-src="https://www.bestattorney.com/images/location-images/city-images/<?php echo str_replace(" ", "-", strtolower($city)); ?>-intersection-map.jpg">-->
			</div>
			<div class="location-image col-md-6 col-sm-12 no-gutter border-box">
				<img style="height:100%;" data-src="https://www.bestattorney.com/images/location-images/city-images/<?php echo str_replace(" ", "-", strtolower($city)); ?>-street-view.jpg" title="The intersection at <?php echo $intersection1; ?>" alt="<?php echo $intersection1; ?> intersection - street view image">
			</div>
		</div>
	</div>

	<div id="location-box-3" class=" row outline col-md-12 col-sm-12 border-box no-gutter outline" style="padding:0 0 10px 0; margin: 0">
		<div class="intersection-header col-md-6 col-sm-6 col-xs-6 border-box font-24 b-bottom" style="padding:10px; background-color: rgb(237, 232, 210);">
			<p>More Unsafe Intersections<span class="" style="font-size:14px;line-height:20px;">**</span></p>
		</div>
		<div class="intersection-header col-md-2 col-sm-2 col-xs-2 border-box font-24 b-bottom" style=" padding:10px; background-color: rgb(237, 232, 210);">
			<p>Accidents</p>
		</div>
		<div class="intersection-header col-md-2 col-sm-2 col-xs-2 border-box font-24 b-bottom" style="padding:10px;background-color: rgb(237, 232, 210);">
			<p>Injuries</p>
		</div>
		<div class="intersection-header col-md-2 col-sm-2 col-xs-2 border-box font-24 b-bottom" style=" padding:10px;background-color: rgb(237, 232, 210);">
			<p>Deaths</p>
		</div>
		<div class="clearfix"></div>
		<div class="intersection-names col-md-6 col-sm-6 col-xs-6 border-box">
			<strong><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/" . str_replace(" ", "+", $intersection2) . "+" . $city); ?>"><?php echo $intersection2; ?></a></strong>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection2accidents; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection2injuries; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection2deaths; ?>
		</div>
		<div class="clearfix"></div>
		<div class="intersection-names col-md-6 col-sm-6 col-xs-6 border-box">
			<strong><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/" . str_replace(" ", "+", $intersection3) . "+" . $city); ?>"><?php echo $intersection3; ?></a></strong>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection3accidents; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection3injuries; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box ">
			<?php echo $intersection3deaths; ?>
		</div>
		<div class="clearfix"></div>
		<div class="intersection-names col-md-6 col-sm-6 col-xs-6 border-box ">
			<strong><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/" . str_replace(" ", "+", $intersection4) . "+" . $city); ?>"><?php echo $intersection4; ?></a></strong>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection4accidents; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box">
			<?php echo $intersection4injuries; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 border-box ">
			<?php echo $intersection4deaths; ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<p class="font-9">* Crime index data taken from <a href="<?php echo $url; ?>">city-data.com.</a> The national average is 315.5. The crime index is calculated using the population and severity of crimes vs the frequency of crimes committed.</p>
<p id="footnote-2" class="font-9">**All intersection data is taken from the California Statewide Integrated Traffic Records System and is measured over the last 5 years of local and arterial street car accidents (non-highway). The data is not updated in real time. </p>


<p class="rc-title" style="padding-top:36px;text-align:left;"><?php echo $city; ?> Lawyers</p>
<div id="content" class="well well-lg" style="margin:24px 0 0 0;">
	<?php echo $content; ?>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/sidebar.php"); ?>