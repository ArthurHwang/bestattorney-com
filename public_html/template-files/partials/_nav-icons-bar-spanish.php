<div id="icons-bar">
    <a id="home-mobile-icon" href="https://www.bestattorney.com/abogados/" class="nopc"><i class="fa fa-home" aria-hidden="true"></i></a>
    <a id="call-mobile-icon" href="tel:9492033814" class="nopc"><i class="fa fa-phone" aria-hidden="true"></i></a>
    <a id="directions-mobile-icon" href="https://www.bestattorney.com/abogados/contactenos.html#cn-loc-list" class="nopc"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
    <a id="search" title="Search BestAttorney.com"></a>
    <a href="https://www.bestattorney.com/abogados/contactenos.html" id="consultation-link" class="nopc">CONSULTA GRATIS</a>
    <a id="menu-link"  class="nopc"><i class="fa fa-bars" aria-hidden="true"></i></a>
</div>