<div style="clear:both;"></div>
<hr />
<div id="helpful">Was This Page Helpful? <a id="helpful-button" class="helpful-click">Yes</a> | <a id="unhelpful-button" class="helpful-click">No</a></div>
<hr />
<div id="not-helpful" class="not-helpful display-none" >
  <form id="helpful-form" name="helpful-form" method="post" action="https://www.bestattorney.com/template-files/contact-files/helpful-form.php"> 
    <strong>Please let us know why this page was not helpful:</strong><br>
    <input class="helpful-radio" type="radio" name="unhelpful" value="The Page Did Not Answer My Questions">Did Not Answer My Questions</input><br>
    <input class="helpful-radio" type="radio" name="unhelpful" value="The Page Content Was Not What I Was Expecting">The Page Content Was Not What I Was Expecting</input><br>
    <input class="helpful-radio" type="radio" name="unhelpful" value="There Was Too Much Information On The Page">Too Much Information On The Page</input><br>
    <input class="helpful-radio" type="radio" name="unhelpful" value="There Was Too Little Information On The Page">Too Little Information On The Page</input><br>
    <input class="helpful-radio" type="radio" name="unhelpful" value="The Page Content Was Too Complicated">Page Content Was Too Complicated</input><br>
    <input style="visibility:hidden;" type="radio"  name="page" value="<?php echo $page; ?>" checked>
    <input id="no-fill" style="display:none;" type="text" name="no-fill" value="no-send">
    <strong>How Can We Make This Page Better?</strong> (Note: If you have any questions, please provide your email if you want us to respond to you)<br>
    <textarea id="helpful-textarea" rows="3" name="recommendations"></textarea>
    <span>Email: (optional):</span><input id="helpful-email" type="text" name="email"> </input>
    <button type="submit">Submit</button>
  </form>
</div>
<div id="form-messages"></div>
<div class="clear"></div>