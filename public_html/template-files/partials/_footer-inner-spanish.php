<div id="footer-inner">

    <p id="office-details" itemscope itemtype="http://schema.org/Attorney"><span itemprop="name">Bisnar Chase Personal Injury Attorneys</span><span class="nopc notablet"><br /></span> <span class="nomobile">&bull;</span> <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress"><?php echo $options["address"]["street"]; ?></span><span class="nopc notablet"><br /></span> <span itemprop="addressLocality"><?php echo $options["address"]["city"]; ?></span>, <span itemprop="addressRegion"><?php echo $options["address"]["state"]; ?></span> <span itemprop="postalCode"><?php echo $options["address"]["zip"]; ?></span></span> <span class="nopc"><br /></span><span class="notablet nomobile">&bull;</span> local: <span itemprop="telephone"><?php echo $options["phone"]; ?></span><span class="nopc notablet"><br /></span>
        <meta itemprop="image" content="https://www.bestattorney.com/images/text-header-images/Bisnar-Chase-Staff-Photo-2017.jpg">
        <?php if ($options["address"]["gmap"]) { ?>
        <span class="nomobile">&bull;</span> <a href="<?php echo $options["address"]["gmap"]; ?>" target="_blank">Direcciones</a>
        <?php

      } ?></p>

    <div id="social-media">
        <p class="title">Redes sociales</p>
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_social_media.php"); ?>
    </div>

    <div id="sitemap">

        <p class="title">Sitio web</p>

        <ul class="ul1">
            <li><a href="https://www.bestattorney.com/abogados/">Inicio</a></li>
            <li><a href="https://www.bestattorney.com/attorneys/john-bisnar.html">John Bisnar (Inglés)</a></li>
            <li><a href="https://www.bestattorney.com/attorneys/brian-chase.html">Brian Chase (Inglés)</a></li>
        </ul>

        <ul class="ul2">
            <li><a href="https://www.bestattorney.com/abogados/recursos/elegir-un-abogado.html">Elegir Un Abogado</a></li>
            <li><a href="https://www.bestattorney.com/abogados/lesiones-personales.html">Areas de Práctica</a></li>
        </ul>

        <ul class="ul3">
            <li><a href="https://www.bestattorney.com/abogados/resultos-destacados.html">Resultados</a></li>
            <li><a href="https://www.bestattorney.com/abogados/contactenos.html">Contactenos</a></li>
        </ul>
        <ul class="ul4">
            <li><a href="https://www.bestattorney.com/abogados/testimonios.html">Testimonios</a></li>
            <li><a href="https://www.bestattorney.com/blog/">Lea nuestro blog (Inglés)</a></li>
        </ul>
        <div class="clear"></div>

    </div>

    <div class="clear"></div>

    <div id="disclaimer">

        <p class="title">Copyright y Descargo de responsabilidad</p>

        <p><strong>Advertencia:</strong> La información legal presentada en este sitio no debe interpretarse como asesoramiento legal formal, ni la creación de una relación abogado-cliente. Cualquier resultado mostrado aqui depende de los hechos de ese caso y los resultados serán diferentes de un caso a otro.</p>

        <p><strong>Bisnar Chase </strong> sirve a toda California. Además, representamos a clientes en otros estados a través de nuestras asociaciones con firmas de abogados locales. A través de la firma local, seremos admitidos a ejercer leyes en su estado, pro hac vice, que significa "para esta ocasión especial." Cuando está en el interés de nuestros clientes, contamos con la firma de abogados local (sin costo adicional para nuestros clientes) para ayudarnos a comparecer ante la corte en asuntos de rutina y los procedimientos de investigación para perseguir más eficazmente la causa de nuestro cliente.</p>

        <p><strong>Copyright &copy; 1999-<?php echo (date('Y')); ?> Bisnar Chase Personal Injury Attorneys</strong> - Reservados todos los derechos.</p>

    </div>

</div> 