<li><a href="https://www.bestattorney.com/practice-areas.html" data-event-category="Button" data-event-action="Riverside Practice Areas Top Level Nav Click" data-event-label="<?php echo $noparamuri;?>">Practice Areas&nbsp;<div class="arrow-nav-right-big nopc"></div><div class="arrow-nav notablet nomobile"></div></a>

    <ul class="full-panel prac">

        <li>

            <p class="ptitle"><a href="https://www.bestattorney.com/practice-areas.html" data-event-category="Button" data-event-action="Riverside Practice Areas Nav Click" data-event-label="<?php echo $noparamuri;?>">Riverside Practice Areas<div class="arrow-nav-right-big nopc"></div></a></p>

            <div class="col1">
                <a href="https://www.bestattorney.com/riverside/auto-defect-lawyers.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Auto Defects Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="auto-defect-image"></div>Auto Defects<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/auto-accidents.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Car Accidents Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="car-accidents-image"></div>Car Accidents<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/truck-accidents.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Truck Accidents Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="truck-accident-image"></div>Truck Accidents<div class="clear"></div></a>

            </div>

            <div class="col2">

                <a href="https://www.bestattorney.com/riverside/dog-bites.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Dog Bites Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="dog-bite-image"></div>Dog Bite Injury<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/premises-liability.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Premises Liability Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="premises-liability-image"></div>Premises Liability<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/wrongful-death.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Wrongful Death Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="wrongful-death-image"></div>Wrongful Death<div class="clear"></div></a>

            </div>

            <div class="col3">

                

                <a href="https://www.bestattorney.com/riverside/pedestrian-accidents.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Pedestrian Accidents Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="pedestrian-accident-image"></div>Pedestrian Accidents<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/motorcycle-accidents.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Motorcycle Accidents Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="motorcycle-accident-image"></div>Motorcycle Accidents<div class="clear"></div></a>

                <a href="https://www.bestattorney.com/riverside/product-liability.html" class="prac-link" data-event-category="Button" data-event-action="Riverside Defective Products Nav Click" data-event-label="<?php echo $noparamuri;?>"><div class="practice-areas-images" id="defective-product-image"></div>Defective Product<div class="clear"></div></a>

            </div>

            <div class="col4"><a href="https://www.bestattorney.com/resources/injured-in-car-accident.html" data-event-category="Button" data-event-action="Accident Help Guide Nav Click" data-event-label="<?php echo $noparamuri;?>">Accident Help Guide<div class="arrow-nav"></div></a>

                <a href="https://www.bestattorney.com/case-results/" data-event-category="Button" data-event-action="Case Results Nav Click" data-event-label="<?php echo $noparamuri;?>">Case Results<div class="arrow-nav"></div></a>

                <a href="https://www.bestattorney.com/about-us/testimonials.html" data-event-category="Button" data-event-action="Client Reviews Nav Click" data-event-label="<?php echo $noparamuri;?>">Client Reviews<div class="arrow-nav"></div></a>

                <a href="https://www.bestattorney.com/about-us/no-fee-guarantee-lawyer.html" data-event-category="Button" data-event-action="No Fee Guarantee Nav Click" data-event-label="<?php echo $noparamuri;?>">No Fee Guarantee<div class="arrow-nav"></div></a>

                <a href="https://www.bestattorney.com/practice-areas.html" id="see-all-practice-areas" title="See All Practice Areas" data-event-category="Button" data-event-action="Riverside Practice Areas Nav Click (Yellow Button)" data-event-label="<?php echo $noparamuri;?>">See All Practice Areas</a>

            </div>

            <div class="clear"></div>



        </li>



    </ul>



</li>