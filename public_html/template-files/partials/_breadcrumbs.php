<?php 

	$openingbreadcrumb = '<div class="breadcrumb"itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
	$noslash = substr($page, 1); //Removes the first slash from the url

	if (strpos($noslash, "?")) {
		$noslash = strstr($noslash, "?", true); // bandaid for URL parameters that have a slash in them like the HTACCESS redirect UTM source & medium parameters
	}


	$folder1 = strstr($noslash, "/", true); //takes the string after the first slash and before the second slash, removes hypens, capitalizes each word. 
	$folder2 = strstr(substr($noslash, strlen($folder1)+1), "/", true);
	$folder3 = strstr(substr($noslash, strlen($folder1)+strlen($folder2)+2), "/", true);
	$folder4 = strstr(substr($noslash, strlen($folder1)+strlen($folder2)+strlen($folder3)+3), "/", true);

	$url1 = $folder1."/";
	$url2 = $folder1."/".$folder2."/";
	$url3 = $folder1."/".$folder2."/".$folder3."/";
	$url4 = $folder1."/".$folder2."/".$folder3."/".$folder4."/";

	for ($i = 1; $i <= 4; $i++) { //If The URL of the folder is the same as the current page we're on, we're dealing with the index file of the folder. We should not be linking to this. 
		if ("/".${"url".$i} != $noparamuri) {
			$addOne = $i + 1; //The folder url is not the same page, so we link the breadcrumb to the folder that this page is located underneath. 
			${"path".$i} = ($openingbreadcrumb.'<a href="https://www.bestattorney.com/'.${"url".$i}.'" itemprop="item"><span itemprop="name"><meta itemprop="position" content="'.$addOne.'" />'.ucwords(strtolower(str_replace("-", " ", ${"folder".$i}))).'</span></a></div>');
		}
		else { //This folder is the same as the page we're on. Do not link to the breadcrumb because it is the same page. This avoids a same page link and sets $isfolder to true, so we don't print out the file's h1 again - otherwise it is redundant. 
			$isfolder = true;
			${"path".$i} = ('<span>'.ucwords(strtolower(str_replace("-", " ", ${"folder".$i}))).'</span>');
		}
	}

?>

<div id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
<div class="breadcrumb"  itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
		<div id="home-crumb" ></div>
	<a href="https://www.bestattorney.com/" itemprop="item">
		<span itemprop="name">Home</span>
		<meta itemprop="position" content="1" />
	</a>
</div>
<?php
if ($folder1) {echo ($path1." ");}
if ($folder2) {echo ($path2." ");}
if ($folder3) {echo ($path3." ");}
if ($folder4) {echo ($path4." ");}?>

<script>
	<?php if ($isfolder == false) { // If this page is a folder, don't print out the h1. ?> 
		var h1Display = document.getElementsByTagName('h1')[0];
		var crumb = document.createElement('strong');
		crumb.textContent = h1Display.innerHTML;
		document.getElementById("breadcrumbs").append(crumb);		
	<?php } ?>
</script>
</div>

