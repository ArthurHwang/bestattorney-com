<div id="have-a-question">
    <div id="get-answers">
        <p class="title">Have a question that wasn't answered here?</p>
        <a id="call-us" href="tel:<?php echo $options["callPhone"]; ?>" class="brown-button <?php echo $options['location']; ?>">
	        <div id="footer-call-us-icons" >
			    <i class="fa fa-circle-thin" aria-hidden="true"></i>
			    <i class="fa fa-phone" aria-hidden="true" ></i>
		    </div>
		    <div>
				<p class="button-sub-title">Call Us!</p>
		        <p class="button-title"><?php echo $options["phone"]; ?></p>
		    </div>
		</a>
        <a id="fill-out-form" href="<?php echo $options['contactLink'];?>" class="contact-button">
		    <div id="footer-contact-icons" >
			    <i class="fa fa-file-text-o" aria-hidden="true"></i>
		    </div>
		    <div>
				<p class="button-sub-title">Fill Out Our</p>
		        <p class="button-title">Contact Form</p>
		    </div>
		</a>
    </div>
</div>