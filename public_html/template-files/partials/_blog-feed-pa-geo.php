<?php 

/* Short and sweet */

define('WP_USE_THEMES', false);


if ($environment == "production") {

	require($_SERVER['DOCUMENT_ROOT'].'/blog/wp-load.php');
	global $post;

	//If a category name was sent, use that to filter the results.

	if( !empty($options['pageSubject']) && $options['pageSubject'] != "") {
		$category;
		switch ($options['pageSubject']) {
			case "employmentlaw":
				$category = "Employment Law";
				break;
			case "dogbite":
				$category = "Dog Bites";
				break;
			case "accident":
				$category = "Car Accidents";
				break;
			case "autodefect":
				$category = "Auto Defects";
				break;
			case "productdefect":
				$category = "Defective Products";
				break;
			case "medicaldefect":
				$category = "Defective Medical Device";
				break;
			case "nursinghomeabuse":
				$category = "Nursing Home Abuse";
				break;	
			case "premisesliability":
				$category = "Premises Liability";
				break;	
			case "wrongfuldeath":
				$category = "Wrongful Death";
				break;	
			case "truckaccident":
				$category = "Truck Accident";
				break;
			case "motorcycleaccident":
				$category = "Motorcycle Accidents";
				break;
			case "bicycleaccident":
				$category = "Bicycle Accidents";
				break;
			default:
				$category = "Personal Injury";
				break;
		}

		$posts = get_posts('numberposts=4&order=DESC&orderby=date&category_name='.$category);
		$blogFeedHtml = "<p class='title'>News From Our Blog (".$category.")</p>
	                
					<ul>";
					
		
		foreach ($posts as $post) {

			setup_postdata($post); 
			$blogFeedHtml .= '<li>
				    <p class="date"><span>'.get_the_time('d').'</span><br>'.get_the_time('M').'</p>
				    <p class="post-title"><a href="'.get_permalink().'">'.get_the_title().'</a></p>
			    </li>';

		}



		$blogFeedHtml .= '</ul><div class="clear"></div>';
		// error_log("blogFeedHtml: ".$blogFeedHtml);
	} else {



		$posts = get_posts('numberposts=4&order=DESC&orderby=date');
		$blogFeedHtml = "<p class='title'>News From Our Blog</p>
	                
					<ul>";
		foreach ($posts as $post) {
		 	setup_postdata($post); 
			$blogFeedHtml .= '<li>
				    <p class="date"><span>'.get_the_time('d').'</span><br>'.get_the_time('M').'</p>
				    <p class="post-title"><a href="'.get_permalink().'">'.get_the_title().'</a></p>
			    </li>';
		} 
		$blogFeedHtml .= '</ul><div class="clear"></div>';
		// error_log("blogFeedHtml: ".$blogFeedHtml);

	}
	echo($blogFeedHtml);

}

?>