<?php
//This is the newer template for the location folders that gilbert created.

$cityprefix = "http://www.city-data.com/city/";
//Get the content of the city-data page
$url = $cityprefix . str_replace(" ", "-", $city) . "-California.html";

// Determine the average amount of car accident injuries based on the population and the number of total accidents in the last 5 years in this city.
if ($populationscrape) {
	$avginjuries = round(($totalaccidents*200)/(int)str_replace(",","",$populationscrape));
} else {
	$populationscrape = "Population not Found.";
}
if ($avginjuries < 4) {
	$avgcolor = "rgb(164, 242, 168)";
	$avgtextcolor = "#1E7E13";
}
else if ($avginjuries > 3 && $avginjuries < 6) {
	$avgcolor = "rgb(164, 242, 168)";
	$avgtextcolor = "#1E7E13";
}
else if ($avginjuries > 5 && $avginjuries < 10) {
	$avgcolor = "rgb(232, 231, 99)";
	$avgtextcolor = "#D07011";
}
else if ($avginjuries > 10) {
	$avgcolor = "rgb(242, 164, 164)";
	$avgtextcolor = "#A30000";
}

?>
<div class="dangerous-intersections">
	<p class="title">Most Dangerous Intersection in <?php echo $city; ?>**</p>
	<p class="sub-title big-title"><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection1)."+".$city);?>"><?php echo $intersection1; ?></a></p>
    <div class="flt3col">
        <p class="sub-title">Last 5 Years:</p>
        <ul>
        	<li><?php echo $intersection1accidents; ?> Accidents</li>
        	<li class="item-alt"><?php echo $intersection1injuries; ?> Injuries</li>
        	<li><?php echo $intersection1deaths; ?> Deaths</li>
        </ul>
    </div>

    <div class="flt3col">
        <p class="sub-title">City of <?php echo $city; ?></p>
        <ul>
        	<li>Population: <?php echo $populationscrape; ?></li>
        	<li class="item-alt">Avg Annual Auto Injuries per 1,000: <?php echo $avginjuries; ?></li>
        	<li><?php echo $city; ?> Crime Index* <?php if ($crimeindex) {echo $crimeindex;} else {echo $crimeratescrape;}?></li>
        </ul>
    </div>
    <div class="flt3col">
        <p class="sub-title">Nearby Cities Crime Indices*:</p>
        <ul>
		  <?php //This code iterates through all of the nearby cities and prints out the city name with the city crime index.
          // If the city crime index doesn't exist, neither is printed out. If the city name is more than 11 characters, the font size is decreased so it doesn't mess up formatting.
          for ($i=1;$i<5;$i++) {
		  	  if (($i==2) || ($i==4)) { $liecho = " class='item-alt'"; }
              if (${"city".$i."name"}) {
                  echo "<li".$liecho.">";
                  echo (${"city".$i."name"}.": <strong>".${"city".$i."index"}."</strong></li>" );
              }
			  $liecho = "";
          } ?>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="fltlt">
		<iframe class="gmap-loc" src="https://www.google.com/maps/embed/v1/place?zoom=14&key=AIzaSyDLj7JngsO7n25T0xFJl2UJ7pnMVgmQimk&q=<?php if ($intersection1coordinates) { echo str_replace(", ", ",+", $intersection1coordinates); } else { echo(str_replace("&", "and", str_replace(" ", "+", $intersection1)."+".str_replace(" ", "+", $city)));}?>"></iframe>
    </div>
    <div class="fltrt">
		<img class="imgstreet" data-src="/images/location-images/city-images/<?php echo str_replace(" ", "-", strtolower($city));?>-street-view.jpg" title="The intersection at <?php echo $intersection1;?>" alt="<?php echo($intersection1.' intersection - street view image'); ?>">
    </div>
    <div class="clear"></div>
    <p class="caption">* Crime index data taken from <a href="<?php echo $url;?>">city-data.com.</a> The national average is 315.5. The crime index is calculated using the population and severity of crimes vs the frequency of crimes committed.</p>
</div>
<div class="dangerous-intersections">
	<p class="title">More Unsafe Intersections in <?php echo $city; ?>**</p>
    <div class="flt3col">
        <p class="sub-title"><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection2)."+".$city);?>"><?php echo $intersection2; ?></a></p>
        <ul>
        	<li>Accidents: <?php echo $intersection2accidents; ?></li>
        	<li class="item-alt">Injuries: <?php echo $intersection2injuries; ?></li>
        	<li>Deaths: <?php echo $intersection2deaths; ?></li>
        </ul>
    </div>
    <div class="flt3col">
        <p class="sub-title"><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection3)."+".$city);?>"><?php echo $intersection3; ?></a></p>
        <ul>
        	<li>Accidents: <?php echo $intersection3accidents; ?></li>
        	<li class="item-alt">Injuries: <?php echo $intersection3injuries; ?></li>
        	<li>Deaths: <?php echo $intersection3deaths; ?></li>
        </ul>
    </div>
    <div class="flt3col">
        <p class="sub-title"><a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection4)."+".$city);?>"><?php echo $intersection4; ?></a></p>
        <ul>
        	<li>Accidents: <?php echo $intersection4accidents; ?></li>
        	<li class="item-alt">Injuries: <?php echo $intersection4injuries; ?></li>
        	<li>Deaths: <?php echo $intersection4deaths; ?></li>
        </ul>
    </div>
    <div class="clear"></div>
    <!-- <div class="flt3col">
    	<a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection2)."+".$city);?>"><img data-src="/images/location-images/city-images/<?php echo $mapimage2; ?>" class="img-mini-map" alt=""/></a>
    </div>
    <div class="flt3col">
    	<a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection3)."+".$city);?>"><img data-src="/images/location-images/city-images/<?php echo $mapimage3; ?>" class="img-mini-map" alt=""/></a>
    </div>
    <div class="flt3col">
    	<a target="_blank" href="<?php echo ("https://www.google.com/maps/place/".str_replace(" ", "+", $intersection4)."+".$city);?>"><img data-src="/images/location-images/city-images/<?php echo $mapimage4; ?>" class="img-mini-map" alt=""/></a>
    </div> -->
    <div class="clear"></div>
    <p class="caption">** All intersection data is taken from the California Statewide Integrated Traffic Records System and is measured over the last 5 years of local and arterial street car accidents (non-highway). The data is not updated in real time.</p>
</div>
