<div style="display: flex; align-items: center" id="awards-ratings-reviews" class="scroll-fade">
	<a aria-label="Daily Journal Top Lawyer 2020" alt="Daily Journal Top Lawyer 2020" style="background: none; height: 100%" href="https://www.dailyjournal.com" id="" target="_blank"><img alt="Daily Journal Top Lawyer 2020" width="170" src="/images/2020-assets/Daily Journal (1).png" /></a>
	<a aria-label="as seen on" href="http://abc7.com/former-oc-officer-files-lawsuit-against-ford-over-co-leak-in-patrol-cars/2274376/" id="as-seen-on" target="_blank"></a>
	<a aria-label="consumer attorneys" href="https://www.bestattorney.com/blog/chase-ritsema-attorney-award" id="consumer-attorneys" target="_blank"></a>
	<a aria-label="newsweek" href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html" id="newsweek" target="_blank"></a>
	<a aria-label="about us" href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html" id="time" target="_blank"></a>
	<a aria-label="as distinguished justice advocates on" href="https://distinguishedjusticeadvocates.com/" id="dja" target="_blank"></a>
	<a aria-label="abota" href="https://www.abota.org" id="abota" target="_blank"></a>
	<a aria-label="brian chase top 100 high stakes litigator" href="https://www.top100highstakeslitigators.com/listing/brian-d-chase/" id="am-top-100" target="_blank"></a>
</div>
<a class="scroll-fade" href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html" id="see-all-awards">See All Ratings And Awards <span class="small-right-arrow"></span></a>