<li><a href="https://www.bestattorney.com/practice-areas.html" data-event-category="Button" data-event-action="Practice Areas Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Practice Areas&nbsp;<div class="arrow-nav-right-big nopc"></div>
        <div class="arrow-nav notablet nomobile"></div>
    </a>

    <ul class="full-panel prac">
        <li>
            <p class="ptitle"><a href="https://www.bestattorney.com/practice-areas.html" data-event-category="Button" data-event-action="Practice Areas Nav Click (Header Text)" data-event-label="<?php echo $noparamuri; ?>">Practice Areas<div class="arrow-nav-right-big nopc"></div></a></p>
            <div class="col1">
                <a href="https://www.bestattorney.com/car-accidents/" class="prac-link" data-event-category="Button" data-event-action="Car Accidents Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="car-accidents-image"></div>Car Accidents<div class="clear"></div>
                </a>
                <!-- <a href="https://www.bestattorney.com/" class="prac-link" data-event-category="Button" data-event-action="Personal Injury Nav Click" data-event-label="<?php echo $noparamuri; ?>"><div class="practice-areas-images" id="personal-injury-image"></div>Personal Injury<div class="clear"></div></a> -->
                <a href="https://www.bestattorney.com/truck-accidents/" class="prac-link" data-event-category="Button" data-event-action="Truck Accidents Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="truck-accident-image"></div>Truck Accident<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/bicycle-accidents/" class="prac-link" data-event-category="Button" data-event-action="Bicycle Accident Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="bicycle-accident-image"></div>Bicycle Accidents<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/auto-defects/" class="prac-link" data-event-category="Button" data-event-action="Auto Defects Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="auto-defect-image"></div>Auto Defects<div class="clear"></div>
                </a>
            </div>
            <div class="col2">
                <a href="https://www.bestattorney.com/dog-bites/" class="prac-link" data-event-category="Button" data-event-action="Dog Bites Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="dog-bite-image"></div>Dog Bite Injury<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/premises-liability/" class="prac-link" data-event-category="Button" data-event-action="Premises Liability Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="premises-liability-image"></div>Premises Liability<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/wrongful-death/" class="prac-link" data-event-category="Button" data-event-action="Wrongful Death Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="wrongful-death-image"></div>Wrongful Death<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/class-action/" class="prac-link" data-event-category="Button" data-event-action="Pharmaceutical Litigation Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="pharmaceutical-litigation-image"></div>Class Action<div class="clear"></div>
                </a>
            </div>

            <div class="col3">
                <a href="https://www.bestattorney.com/employment-law/" class="prac-link" data-event-category="Button" data-event-action="Medical Devices Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="defective-medical-product-image"></div>Employment Law<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/defective-products/" class="prac-link" data-event-category="Button" data-event-action="Defective Products Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="defective-product-image"></div>Defective Products<div class="clear"></div>
                </a>
                <a href="https://www.bestattorney.com/head-injury/" class="prac-link" data-event-category="Button" data-event-action="Brain Injury Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                    <div class="practice-areas-images" id="brain-injury-image"></div>Brain Injury<div class="clear"></div>
                </a>
            </div>

            <div class="col4">
                <a href="https://www.bestattorney.com/resources/injured-in-car-accident.html" data-event-category="Button" data-event-action="Accident Help Guide Nav Click" data-event-label="<?php echo $noparamuri; ?>">Accident Help Guide<div class="arrow-nav"></div></a>
                <!-- <a href="https://www.bestattorney.com/case-results/" data-event-category="Button" data-event-action="Case Results Nav Click" data-event-label="<?php echo $noparamuri; ?>">Case Results<div class="arrow-nav"></div></a>
                <a href="https://www.bestattorney.com/about-us/testimonials.html" data-event-category="Button" data-event-action="Client Reviews Nav Click" data-event-label="<?php echo $noparamuri; ?>">Client Reviews<div class="arrow-nav"></div></a> -->
                <a href="https://www.bestattorney.com/about-us/no-fee-guarantee-lawyer.html" data-event-category="Button" data-event-action="No Fee Guarantee Nav Click" data-event-label="<?php echo $noparamuri; ?>">No Fee Guarantee<div class="arrow-nav"></div></a>
                <a href="https://www.bestattorney.com/practice-areas.html" id="see-all-practice-areas" title="See All Practice Areas" data-event-category="Button" data-event-action="Practice Areas Nav Click (Yellow Button)" data-event-label="<?php echo $noparamuri; ?>">See All Practice Areas</a>
            </div>

            <div class="clear"></div>
        </li>
    </ul>
</li>