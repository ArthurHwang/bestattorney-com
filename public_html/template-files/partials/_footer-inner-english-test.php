<div id="footer-inner">

    <p id="office-details" itemscope itemtype="http://schema.org/Attorney"><span itemprop="name">Bisnar Chase Personal Injury Attorneys</span><span class="nopc notablet"><br /></span> <span class="nomobile">&bull;</span> <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">
                <?php echo $options["address"]["street"]; ?></span><span class="nopc notablet"><br /></span> <span itemprop="addressLocality">
                <?php echo $options["address"]["city"]; ?></span>, <span itemprop="addressRegion">
                <?php echo $options["address"]["state"]; ?></span> <span itemprop="postalCode">
                <?php echo $options["address"]["zip"]; ?></span></span> <span class="nopc"><br /></span><span class="notablet nomobile">&bull;</span> local: <span itemprop="telephone">
            <?php echo $options["phone"]; ?></span><span class="nopc notablet"><br /></span>
        <meta itemprop="image" content="https://www.bestattorney.com/images/text-header-images/Bisnar-Chase-Staff-Photo-2017.jpg">
        <?php if ($options["address"]["gmap"]) { ?>
        <span class="nomobile">&bull;</span> <a href="<?php echo $options[" address"]["gmap"]; ?>" target="_blank">Get Directions</a>
        <?php

      } ?>
    </p>

    <div id="metros">
        <p class="title">Metros Served</p>
        <ul id="metros-inner">

            <li>
                <div>
                    <p><a href="https://www.bestattorney.com/orange-county/">Main Office</a></p>
                    <p>1301 Dove St, #120</p>
                    <p>Newport Beach, CA 92660</p>
                    <!-- <div id="oc-phone" class="phone-image"></div> -->
                </div>
            </li>
            <li>
                <div>
                    <p><a href="https://www.bestattorney.com/los-angeles/">Appointment Only</a></p>
                    <p>6701 Center Drive West, 14th Fl.</p>
                    <p>Los Angeles, CA 90045</p>
                    <!-- <div id="la-phone" class="phone-image"></div> -->
                </div>
            </li>
            <li>
                <div>
                    <p><a href="https://www.bestattorney.com/riverside/">Appointment Only</a></p>
                    <p>6809 Indiana Avenue #148</p>
                    <p>Riverside, CA 92506</p>
                    <!-- <div id="rv-phone" class="phone-image"></div> -->
                </div>
            </li>
            <li>
                <div>
                    <p><a href="https://www.bestattorney.com/san-bernardino/">Appointment Only</a></p>
                    <p>473 S Carnegie Dr #221</p>
                    <p>San Bernardino, CA 92408</p>
                    <!-- <div id="sb-phone" class="phone-image"></div> -->
                </div>
            </li>
        </ul>
    </div>

    <form id="newsletter" action="newsletter.php" autocomplete="off">
        <p class="title">Newsletter</p>
        <input id="nl-email" type="text" placeholder="Your email address">
        <input id="nl-button" type="submit" value="Subscribe Now">
        <div id="nl-validation"></div>
    </form>

    <div id="sitemap">
        <p class="title">Sitemap</p>
        <ul>
            <li><a href="https://www.bestattorney.com/">Home</a></li>
            <li><a href="https://www.bestattorney.com/about-us/">About Us</a></li>
            <li><a href="https://www.bestattorney.com/attorneys/john-bisnar.html">John Bisnar</a></li>
            <li><a href="https://www.bestattorney.com/attorneys/brian-chase.html">Brian Chase</a></li>
        </ul>
        <ul>
            <li><a href="https://www.bestattorney.com/giving-back/">Giving Back</a></li>
            <li><a href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html">Why Hire Us?</a></li>
            <li><a href="https://www.bestattorney.com/practice-areas.html">Practice Areas</a></li>
            <li><a href="https://www.bestattorney.com/about-us/testimonials.html">Reviews</a></li>
        </ul>
        <ul>
            <li><a href="https://www.bestattorney.com/press-releases/">Press</a></li>
            <li><a href="https://www.bestattorney.com/about-us/testimonials.html">Testimonials</a></li>
            <li><a href="https://www.bestattorney.com/case-results/">Case Results</a></li>
            <li><a href="<?php echo $options[" contactLink"]; ?> ">Contact Us</a></li>
        </ul>
        <ul>
            <li><a href="https://www.bestattorney.com/blog/">Read Our Blog</a></li>
            <li><a href="https://www.bestattorney.com/resources/">Resources</a></li>
            <li><a href="https://www.bestattorney.com/abogados/">En Espa&ntilde;ol</a></li>
            <li><a href="https://www.bestattorney.com/sitemap.html">Sitemap</a></li>
        </ul>
    </div>

    <div id="social-media">
        <p class="title">Connect</p>
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_social_media.php"); ?>
    </div>

    <style>
        .livChatFloatingButton {
            display: none !important;
        }
    </style>

    <div id="disclaimer">
        <p class="title">Copyright &amp; Disclaimer</p>
        <p><strong>Disclaimer:</strong> The legal information presented at this site should not be construed to be formal legal advice, nor the formation of an attorney-client relationship. Any results set forth here were dependent on the facts of that case and the results will differ from case to case. <strong>Bisnar Chase </strong> serves all of California. In addition, we represent clients in various other states through our affiliations with local law firms. Through the local firm we will be admitted to practice law in their state, pro hac vice. <a href="https://www.bestattorney.com/privacy-policy.html">Cookies &amp; Privacy Policy</a></p>
        <p><strong>Copyright &copy; 1999-
                <?php echo date("Y"); ?> Bisnar Chase Personal Injury Attorneys</strong> - All rights reserved. <br>
            Location:
            <?php echo $options["address"]["street"] . ", " . $options["address"]["city"] . ", " . $options["address"]["state"] . " " . $options['address']['zip']; ?>
        </p>
    </div>

</div> 