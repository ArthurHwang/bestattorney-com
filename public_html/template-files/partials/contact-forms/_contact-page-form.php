<div id="contact-form-insert">
    <form id="cn-form-wrap" method="POST">
        <div id="cn-form-title">Free Case Evaluation</div>
        <div id="cn-form-desc">Our staff will evaluate your case submission and respond in a timely manner.</div>
        <div id="cn-form-lt">
            <input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox">
            <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
            <input class="iform2" name="realname" placeholder="Name:" id="test-name">
            <input class="iform2" name="email" placeholder="E-mail address:">
            <input class="iform2" name="homephone" placeholder="Phone number:">
        </div>
        <div id="cn-form-rt">
            <textarea class="tform2" name="comment" rows="3" cols="5" placeholder="Describe Your Situation And List Any Questions:"></textarea>
        </div>
        <div class="clear"></div>

        <p id="contact-form-messages"><small>Submitting this form does not create an attorney-client relationship.</small></p>
        <input type="submit" name="submit" value="Send My Request" class="subform2" id="form-submit-button">
    </form>
</div>