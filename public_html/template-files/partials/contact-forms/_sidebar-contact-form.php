<div id="free-case-evaluation">
    <p class="title">Free Case Evaluation</p>
    <p class="subtext">Our staff will evaluate your case submission and respond in a timely manner.</p>
    <div id="form-wrap">
        <form id="case-evaluation-form">
            <p>
                <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
            </p>
            <p>
                <input class="form-hp" name="form-hp" type="checkbox" value="Have you received medical attention?">
                <input class="iform2" name="realname" placeholder="Name:">
            </p>
            <p>
                <input class=" iform2" name="email" placeholder="Email Address:">
            </p>
            <p>
                <input class="iform2" name="homephone" placeholder="Phone Number:" style="margin-bottom: 17px;">
            </p>
            <p>
                <textarea class="tform2" name="comment" rows="3" cols="5" placeholder="<?php echo $options['contactText']; ?>"></textarea>
            </p>
            <p id="contact-form-messages">
                <small>Submitting this form does not create an attorney-client relationship.</small>
            </p>
            <p>
                <input type="submit" name="submit" value="Send My Request" class="subform2">
            </p>
        </form>
    </div>
</div>