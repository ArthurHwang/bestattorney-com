<div id="consultation-form-wrap">
    <div id="consultation-form">
        <p class="title">Request Your Free Consultation</p>
        <p class="subtext">Our team is standing by to help. Call us at <strong>(800) 561-4887</strong> or complete this
            form to schedule a free consultation with us.</p>
        <form id="drop-evaluation-form" method="POST">
            <p><input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>"></p>
            <div id="formlt">
                <p><input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox"><input
                        class="iform3" name="realname" placeholder="Name:""></p><p><input class=" iform3" name="email"
                        placeholder="Email Address:"></p>
                <p><input class="iform3" name="homephone" placeholder="Phone Number:"></p>
                <p id="drop-form-messages"><small>Submitting this form does not create an attorney-client relationship.</small></p>
            </div>
            <div id="formrt">
                <p><textarea class="tform3" name="comment" rows="3" cols="5" placeholder="<?php echo $options['contactText']; ?>"></textarea></p>
                <p><input type="submit" name="submit" value="Send My Request" class="subform2"></p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>