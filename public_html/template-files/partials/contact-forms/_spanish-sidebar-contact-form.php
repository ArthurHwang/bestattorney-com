<div id="contactenos">
    <p class="title">Pongase en contacto<br>con nosotros</p>
    <p class="subtext"><strong>Evaluaci&oacute;n Gratuita de su Caso</strong> - Nuestro personal de tiempo completo
        est&aacute; listo para evaluar su caso y responder&aacute; de manera oportuna.</p>
    <div id="form-wrap">
        <form id="case-evaluation-form" class="espanol-contact">
            <p><input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>"></p>
            <p><input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox"><input
                    class="iform2" name="realname" placeholder="Nombre:"></p>
            <p><input class="iform2" name="email" placeholder="Correo El&eacute;ctronico:"></p>
            <p><input class="iform2" name="homephone" placeholder="Tel&eacute;fono:"></p>
            <p><textarea class="tform2" name="comment" rows="3" cols="5" placeholder="Detalles:"></textarea></p>
            <p id="contact-form-messages"><small>El envío de este formulario no crea una relación abogado-cliente.</small></p>
            <p><input type="submit" name="submit" value="Recibe Ayuda Ahora" class="subform2"></p>
        </form>
    </div>
</div>