<div id="contact-form-wrap">
    <h4 class="free-case" id="free-case-evaluation" style="color:white;text-align:center;">Free Case Evaluation!</h4>
    <form id="case-evaluation-form" method="POST">
        <p>
            <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
        </p>
        <div class="pc-flex tablet-flex">
            <input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox">
            <input class="iform2" name="realname" id="test-name" placeholder="Name:">
            <input class="iform2" name="email" placeholder="E-mail address:">
            <input class="iform2" name="homephone" placeholder="Phone Number:">
        </div>
        <p>
            <textarea class="tform2" name="comment" rows="3" cols="5" placeholder="Describe Your Situation And List Any Questions:"></textarea>
        </p>
        <p id="contact-form-messages">
            <small>Submitting this form does not create an attorney-client relationship.</small>
        </p>
        <p>
            <input type="submit" name="submit" value="Send My Request" class="subform2" id="form-submit-button">
        </p>
    </form>
</div>