<div id="contact-form-insert-mvc">
    <form id="case-evaluation-form" method="POST">
        <input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox">
        <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
        <div class="flex">
            <input class="iform2" name="realname" placeholder="Name:" id="test-name">
            <input class="iform2" name="email" placeholder="E-mail address:">
            <input class="iform2" name="homephone" placeholder="Phone number:">
        </div>
        <textarea class="tform2" name="comment" rows="3" cols="5" placeholder="<?php echo $options['contactText']; ?>">
        </textarea>
        <p id="contact-form-messages"><small>Submitting this form does not create an attorney-client relationship.</small></p>
        <input type="submit" name="submit" value="Send My Request" class="subform2" id="form-submit-button">
    </form>
</div>