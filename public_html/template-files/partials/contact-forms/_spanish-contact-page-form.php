<div id="contact-form-insert-espanol">
    <form id="cn-form-wrap" class="espanol-contact" method="POST">
        <div id="cn-form-title">Pongase en contacto con nosotros</div>
        <div id="cn-form-desc">
            <strong>Evaluación Gratuita de su Caso</strong> - Nuestro personal de tiempo completo está listo para evaluar su caso y responderá de manera oportuna.
        </div>
        <div id="cn-form-lt">
            <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
            <input class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox">
            <input class="iform2" name="realname" id="test-name" placeholder="Nombre:">
            <input class="iform2" name="email" placeholder="Correo Eléctronico:">
            <input class="iform2" name="homephone" placeholder="Teléfono:">
        </div>
        <div id="cn-form-rt">
            <textarea class="tform2" name="comment" rows="3" cols="5" placeholder="Detalles:"></textarea>
        </div>
        <div class="clear"></div>
        <p id="contact-form-messages">
            <small>El envío de este formulario no crea una relación abogado-cliente.</small>
        </p>
        <input type="submit" name="submit" value="Recibe Ayuda Ahora" class="subform2" id="form-submit-button">
    </form>
</div>