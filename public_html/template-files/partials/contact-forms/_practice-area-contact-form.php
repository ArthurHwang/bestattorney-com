<div id="contact-form-pa">
    <form id="pa-case-evaluation-form">
        <input type="hidden" name="noparamuri" value="<?php echo $noparamuri; ?>">
        <div id="pa-form-left">
            <input aria-label="Have you received medical attention?" class="form-hp" name="form-hp" value="Have you received medical attention?" type="checkbox">
            <input aria-label="Name"  class="iform3" name="realname" placeholder="Name:">
            <input aria-label="Phone"  class="iform3" name="homephone" placeholder="Phone:">
            <input aria-label="Email"  class="iform3" name="email" placeholder="Email:">
        </div>
        <textarea aria-label="Brief Description of your personal injury" class="tform3" name="comment" rows="3" cols="5" placeholder="<?php echo $options['contactText']; ?>"></textarea>
        <input type="submit" class="subform2" value="Send My Request" />
    </form>
    <p id="pa-contact-form-messages">
        <small>Submitting this form does not create an attorney-client relationship.</small>
    </p>
</div>