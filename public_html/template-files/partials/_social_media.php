<div id="social-container">
	<a href="https://www.google.com/search?q=bisnarchase&rlz=1C1GCEU_enUS842US842&oq=bisnarchase&aqs=chrome..69i57j69i60l5.3310j0j7&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,," class="plus" title="Find us on Google Plus" target="_blank"></a>
	<a href="https://www.facebook.com/california.attorney" title="Follow us on Facebook" class="facebook" target="_blank"></a>
	<a href="https://twitter.com/bisnarchase" class="twitter" title="Follow us on Twitter" target="_blank"></a>
	<a href="https://www.youtube.com/user/BisnarChaseAttorneys" class="youtube" title="View our YouTube Videos" target="_blank"></a>
	<a href="https://www.linkedin.com/company/bisnar-chase" class="linkedin" title="Follow us on LinkedIn" target="_blank"></a>
	<a href="https://www.avvo.com/attorneys/92660-ca-john-bisnar-211889.html" class="avvo" title="See John Bisnar on Avvo" target="_blank"></a>
	<a href="https://lawyers.justia.com/lawyer/brian-doster-chase-145784" class="justia" title="See Brian Chase on Justia" target="_blank"></a>
	<a href="https://www.dandb.com/verified/business/574414421/" class="db" title="Visit us on Dun & Bradstreet" target="_blank"></a>
	<a href="https://www.pinterest.com/bisnarchase/" class="pinterest" title="Visit Our Pinterest" target="_blank"></a>
	<a href="https://www.instagram.com/bisnarchase/" class="instagram" title="Visit us on Instagram" target="_blank"></a>
</div>