<style>
    body {
        /* background-color: #e6dec3; */
    }

    #title-head {
        width: auto;
        background: #ffffff;
        min-height: 124px;
    }

    #title-head-inner {
        margin-left: 114px;
    }

    #navbar {
        width: 100%;
        height: 53px;
        position: absolute;
        background: #222533;
        z-index: 1049;
    }

    #navbar-inner {
        height: 38px;
        margin-left: 100px;
        font-size: 12px;
        box-sizing: border-box;
    }

    #icons-bar {
        position: relative;
        display: flex;
        justify-content: space-between;
        align-content: center;
        padding: 1px 10px;
        z-index: 500;
        width: 100%;
        box-sizing: border-box;
        line-height: 30px;
    }

    ul#menu li {
        float: left;
        line-height: 54px;
        text-transform: uppercase;
    }

    ul#menu li ul {
        position: absolute;
        display: block;
        width: 215px;
        left: -700em;
        /* using left instead of display to hide menus because display: none isn't read by screen readers */
        padding: 35px 40px 40px 40px;
        margin: 0px 0px 0px 0px;
        z-index: 1000;
        background: #fff;
        border-bottom: none;
        font-size: 12px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, sans-serif;
    }

    #attorneys-mobile {
        display: none;
    }

    #myModal {
        /* display: flex;
        align-items: center; */
    }

    #myModal .modal-content {
        background-color: #ec7d29;
    }

    #myModal .modal-body {
        padding-bottom: 10px;
    }

    #myModal .modal-header {
        /* padding: 0; */
        /* padding-top: 5px; */
        /* padding-bottom: 0; */
        padding: 5px 5px;
    }

    @media only screen and (min-width: 641px) and (max-width: 1100px) {
        #attorneys-mobile {
            display: block;
        }
    }

    @media only screen and (min-width: 1px) and (max-width: 640px) {
        #attorneys-mobile {
            display: block;
        }

        #myModal {
            top: 90px;
        }

        #navbar {
            background: #fff !important;
            border-bottom: 5px solid #222533;
            height: 84px !important;
            position: initial;
        }
    }

    <?php if (isset($options['headerImageClass'])) {
        echo ("

       #template-default #content-color-fill {
    height: 450.4px !important;
    background: black !important;
  }
#centered-content-box, #content-box {

            background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg.jpg') no-repeat center top !important;
        }

        @media only screen and (min-width:641px) and (max-width:1100px) {
            #centered-content-box, #content-box {
                background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg-tablet.jpg') no-repeat center top !important;
            }
        }

        @media only screen and (min-width:1px) and (max-width:640px) {
            #centered-content-box, #content-box {
                background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg-mobile.jpg') no-repeat center top !important;
            }
        }

        ");
    }

    ?><?php if ($options['headerImageClass'] == "testimonials") {
            echo ("

       #template-default #content-color-fill {
    height: 613px !important;
    background: #CCC !important;
  }




        ");
        }

        ?>
</style>

<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/site-options/meta-options.php");

/***********************************************
 Extra Page Options Set Here For The Whole Site.
 ***********************************************/

include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/site-options/sitewide-options.php");

/***********************************************
 All Header HTML & scripts
 ***********************************************/
?>

<!-- Base Element that is used by MaxCDN to reset all relative paths to the CDN -->
<?php if ($environment == "development") { ?>
    <base href="" />
<?php

} else { ?>
    <base href="https://https-webfiles-bisnarchaseperso.netdna-ssl.com/" />
<?php

} ?>

<!-- Normal Meta Info -->
<meta charset="UTF-8">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
<meta name="google-site-verification" content="AxlGywVV4zElv2U8uVF9nK59Gxg9Ug4_kNrmdQqQHyg" />
<?php
if (isset($options['noIndex']) && $options['noIndex'] == true) { ?>
    <meta name="robots" content="noindex" />
<?php

}
if (isset($options['searchWeight'])) { ?>
    <meta name="search-weight" content="<?php echo $options['searchWeight']; ?>" />
<?php

}
if (!$options['isBlog']) { ?>


    <meta property="og:image" content="https://www.bestattorney.com/images/carousel/brian-chase-john-bisnar-small.webp" />
    <meta property="og:image:secure_url" content="https://www.bestattorney.com/images/carousel/brian-chase-john-bisnar-small.webp" />
    <meta property="og:image:type" content="image/webp" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
    <meta property="og:type" content="website" />
    <meta property="og:image:alt" content="injury attorneys in california" />
    <meta property="fb:app_id" content="2249347635315765">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@bisnarchase">
    <meta name="twitter:image" content="https://www.bestattorney.com/images/carousel/brian-chase-john-bisnar-small.webp">
    <meta name="twitter:image:alt" content="California Personal Injury Attorneys of Bisnar Chase">
    <meta name="twitter:creator" content="@bisnarchase" />
    <meta property="og:url" content="<?php echo $options['canonical']; ?>" />
    <?php if (isset($options['socialTitle'])) { ?>
        <meta property="og:title" content="<?php echo $options['socialTitle']; ?>" />
        <meta property="twitter:title" content="<?php echo $options['socialTitle']; ?>" />
    <?php
    }
    if (isset($options['socialDescription'])) { ?>
        <meta property="og:description" content="<?php echo $options['socialDescription']; ?>" />
        <meta property="twitter:description" content="<?php echo $options['socialDescription']; ?>" />
<?php
    }
} ?>

<meta name="lang" content="en" />

<link rel="canonical" href="<?php echo $options['canonical']; ?>">



<!-- Custom Ad-block bypass GA -->

<!-- <script>
(function(i,s,o,r){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date()})(window,document,'script','ga');
  ga('create', 'UA-44427913-2', 'auto');
  ga('send', 'pageview');
</script>
<script src="/analytics.js" async></script> -->


<!-- Google Tag Manager -->
<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WK3JJ3');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44427913-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-44427913-2', {
        'optimize_id': 'GTM-TQQVDKB'
    });
</script>

<?php

/***********************************************
 Custom Header Pages.
 ***********************************************/
if ($options['isSpanish']) {
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-spanish.php");
} else if ($options['isHome']) {
    $noparamuri = "/";
    $options['canonical'] = 'https://www.bestattorney.com/';
    $options['fullWidth'] = true;
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-home.php");
} else if ($options['isBlog']) {
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-blog.php");
} else if ($options['isContact']) {
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-default.php");
} else if ($options['isGeo'] || $options['isPa']) {
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-pa-geo.php");
} else {
    include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-default.php");
}

?>