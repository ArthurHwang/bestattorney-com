<html lang="es">
<script>
  const pageSubject = "<?php echo $options['pageSubject']; ?>";
  const noparamuri = "<?php echo $noparamuri; ?>";
</script>

<style>
  @media only screen and (min-width: 641px) and (max-width: 1100px) {
    a.free-case-review-button:after {
      margin: 4px 36px 0 0 !important;
    }

    #call-text {
      display: none !important;
    }

    #no-win-no-fee {
      display: none !important;
    }

    #menu {
      background: #fff !important;
    }


  }


  @media only screen and (min-width: 1px) and (max-width: 640px) {

    #header-bar-right {
      /* bottom: 10%;
         */
      display: none !important;
    }

    html body div#title-head-inner {
      height: initial !important;
    }

    html body div#title-head {
      height: initial !important;
    }


    p.title {
      padding-top: 10px !important;
    }

    #template-espanol div#our-practice-areas {
      margin-bottom: -57px !important;
    }

    h2#home-mobile-title {
      padding-top: 30px !important;
    }

    #featured-mobile #experienced a.contact-button {
      margin-top: 155px;
    }

    #navbar #navbar-inner .top-menu-toggled {
      height: 381px !important;
    }

    #content-box #mobile-call-header-button {
      background: 0 0;
      border-top: 1px solid #fff;
      border-bottom: 1px solid #fff;
      box-sizing: border-box;
      height: 55px;
      width: 52%;
      border-radius: initial;
      /* margin-top: 238px; */
    }

    #content-box #centered-content-box #header-over-image {
      height: 93px !important;
    }

    #breadcrumbs {
      margin-top: 20px !important;
    }

    #free-consultations a.free-case-review-button:after {
      margin: 4px 36px 0 0 !important;
    }
  }
</style>

<!-- <meta property="og:image" content="https://www.bestattorney.com/images/carousel/brian-chase-john-bisnar-small.webp" />
<meta property="og:image:secure_url" content="https://www.bestattorney.com/images/carousel/brian-chase-john-bisnar-small.webp" />
<meta property="og:image:type" content="image/webp" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="300" />
<meta property="og:type" content="website" />
<meta property="og:image:alt" content="injury attorneys in california" />
<meta property="fb:app_id" content="2249347635315765"> -->


</head>

<body id="template-espanol" lang="es-MX">

  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>



  <!-- Main menu -->
  <div id="title-head">
    <div id="title-head-inner">
      <a href="https://www.bestattorney.com/" id="home-link" title="Bisnar|Chase - California Personal Injury Attorneys"></a>

      <!-- <a href="tel:<?php echo $options['callPhone']; ?>" id="contact-link" class="cr_image <?php echo $options['location']; ?>" title="Call Today For Your Free Consultation!"></a> -->
      <div id="header-bar-right">
        <a href="tel:<?php echo $options['callPhone']; ?>" id="contact-link-text" class="" title="Call Today For Your Free Consultation!">
          <span id="call-text">Llámenos Para Ayuda!</span>
          <span class="phone-number fa-phone">
            <?php echo $options['phone']; ?>
          </span>
        </a>
        <a aria-label="Better Business Bureau" id="topbbb" href="https://www.bbb.org/sdoc/business-reviews/lawyers/bisnar-chase-personal-injury-attorneys-in-newport-beach-ca-100046710" target="_blank"></a>
        <!-- <a href="https://www.bestattorney.com/abogados/recursos/elegir-un-abogado.html" id="no-win-no-fee" class="nomobile">Si no Ganamos Usted no Necesita a Pagar</a> -->
      </div>
      <div class="clear"></div>
    </div>
  </div>

  <nav id="navbar">
    <div id="navbar-inner">

      <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_nav-icons-bar-general.php"); ?>
      <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/nav-files/nav-spanish.php"); ?>

      <div class="clear"></div>
      <div id="search-form">
        <form action="https://www.bestattorney.com/search.html" method="get">
          <p><input type="text" id="search-box" name="q" placeholder="Escriba y Presione Enter" size="30" class="searchinput" /></p>
        </form>
      </div>
    </div>
  </nav>

  <div class="clear"></div>

  <?php if ($noparamuri != "/abogados/") { ?>
    <div class="clear"></div>
    <div id="content-color-fill"></div>
    <div id="content-box">
      <div id="centered-content-box">
        <div id="header-over-image">
          <div id="mobile-call-header" class="nopc">
            <a id="mobile-call-header-button" href="tel:<?php echo $options['callPhone']; ?>" class="contact-button">
              <p class="button-title fa-phone">
                Llámenos Hoy
              </p>
              <p class="button-sub-title">
                y solicite una consulta gratis
              </p>
            </a>
          </div>
          <div id="free-consultations">
            <div id="consultation-form-wrap-espanol"></div>
            <a href="https://www.bestattorney.com/abogados/contactenos.html" id="free-case-review-button-mobile" class="nopc free-case-review-button">Recibe su revisión gratuita de su caso</a>

          </div>
        </div>
        <div id="<?php if ($options['fullWidth']) {
                    echo " full-width";
                  } else {
                    echo "content";
                  } ?>">
        <?php

      } ?>