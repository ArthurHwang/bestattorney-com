<script>
    const pageSubject = "accident";
    const noparamuri = "<?php echo $noparamuri; ?>";
</script>



<style>

    html body div#title-head-inner {
        height: initial !important;
      }
      html body div#title-head {
        height: initial !important;
      }


    #home-mobile-title, #experienced>p  {
    display:none;
}

p.subtitle {
    padding: 150px 60px 0px 0px;
    margin: 0px 0px 0px 0px;
    font-size: 15px;
    color: #bca67a;
    text-transform: uppercase;
    line-height: 24px;
    font-weight: bold;
}


p.pane-text {
    padding: 21px 60px 0px 430px;
    margin: 0px 0px 0px 0px;
    font-size: 14px;
    color: #ffffff;
    line-height: 24px;
}

@media only screen and (min-width: 640px) {
  #featured-wrap {
    height: 614px;
    border-bottom: 3px solid #172633;
    overflow: hidden;
  }

  #featured {
    width: 1101px;
    height: 573px;
    margin: 0px auto 0px auto;
    padding: 82px 0px 0px 0px;
    /*background: url("/images/featured-bg.jpg") no-repeat center top;*/
    border-bottom: 3px solid #172633;
    text-align: right;
  }

  .webp #tab-1 {
    background: url(/images/pane-1-bg.webp) left top no-repeat;
  }
  .no-webp #tab-1 {
    background: url(/images/pane-1-bg.jpg) left top no-repeat;
  }
}
@media only screen and (max-width: 639px) {
    #home-mobile-title, #experienced>p  {
        display:block;
    }
  #featured-mobile-wrap {
    height: 388px;
    /*background: #2b180a url(/images/experience.jpg) no-repeat center top;*/
    background: #5d4a3c;
    background: -moz-linear-gradient(-45deg, #5d4a3c 5%, #5b4642 16%, #39251a 31%, #3f3923 51%, #3f3223 57%, #4f3025 71%, #514642 79%, #3f3223 87%, #261710 99%);
    background: -webkit-linear-gradient(-45deg, #5d4a3c 5%,#5b4642 16%,#39251a 31%,#3f3923 51%,#3f3223 57%,#4f3025 71%,#514642 79%,#3f3223 87%,#261710 99%);
    background: linear-gradient(135deg, #5d4a3c 5%,#5b4642 16%,#39251a 31%,#3f3923 51%,#3f3223 57%,#4f3025 71%,#514642 79%,#3f3223 87%,#261710 99%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5d4a3c', endColorstr='#261710',GradientType=1 );
    background-position: center top;
    border-bottom: 3px solid #172633;
    background-position:center;
  }

  #attorney-pic-div {
    height: 263px;
    width: 320px;
    z-index: 1;
    position: relative;
  }
}

</style>

</head>

<body id="template-home" lang="en-US">

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- Main menu -->
    <div id="title-head">
        <div id="title-head-inner">
            <a href="https://www.bestattorney.com/" id="home-link" class="" title="Bisnar|Chase - California Personal Injury Attorneys"></a>
            <!-- <div id="header-bar-right">
            <a href="tel:<?php echo $options['callPhone']; ?>" id="contact-link-text" class="" title="Call Today For Your Free Consultation!">
                <span id="call-text">Call now for Help!</span>
                <span class="phone-number fa-phone">
                    <?php echo $options['phone']; ?>
                </span>
            </a>
            <a id="topbbb"  href="https://www.bbb.org/sdoc/business-reviews/lawyers/bisnar-chase-personal-injury-attorneys-in-newport-beach-ca-100046710/" target="_blank"></a>
            <a href="https://www.bestattorney.com/abogados/" id="espanol">En Español</a>
            <a href="https://www.bestattorney.com/about-us/no-fee-guarantee-lawyer.html" id="no-win-no-fee" class="nomobile">No Win, No Fee - Guarantee</a>
        </div> -->
        </div>
    </div>

    <nav id="navbar">
        <div id="navbar-inner">

            <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_nav-icons-bar-general.php"); ?>
            <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/nav-files/nav-general.php"); ?>

            <div class="clear"></div>
            <div id="search-form">
                <form action="https://www.bestattorney.com/search.html" method="get">
                    <p><input type="text" id="search-box" name="q" placeholder="type and hit enter" size="30" class="searchinput"></p>
                </form>
            </div>
        </div>
    </nav>

    <div class="clear"></div> 