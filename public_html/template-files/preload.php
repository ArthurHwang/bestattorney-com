<?php 
session_start();

/***********************************************
 Sets the environment to start
***********************************************/
include_once($_SERVER['DOCUMENT_ROOT']."/../environment.php");

/***********************************************
 Page Functions for all pages.
***********************************************/
include_once($_SERVER['DOCUMENT_ROOT']."/template-files/functions.php");


$domain = "www.bestattorney.com"; 

$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "No Referrer";

$fullUrl = 'https://www.bestattorney.com'.$_SERVER['REQUEST_URI'];
$host = $_SERVER['HTTP_HOST'];

if (($host == $domain) && !empty($_SESSION['pagebeforelast']) && ($_SESSION['pagebeforelast'] != $referrer) && !empty($_SESSION['pagelast']) &&($_SESSION['pagelast'] != $fullUrl)) {

	if (!isset($_SESSION['route'])) { 
		if ($referrer != '') { $_SESSION['route'] = $referrer.',';
	} 

	if ($fullUrl != '') {
		$_SESSION['route'] .= $fullUrl.','; }
	} else { 
		if (($referrer != $_SESSION['pagelast']) && ($referrer != '')) { 
			$_SESSION['route'] .= $referrer.',';
		} 
		if (($referrer != $fullUrl) && ($fullUrl != '')) { 
			$_SESSION['route'] .= $fullUrl.',';
		}
	}
}

if (!empty($_SESSION['pagebeforelast'])) 
	$_SESSION['pagebeforelast'] = $_SESSION['pagelast'];
if (!empty($_SESSION['pagelast']))
	$_SESSION['pagelast'] = $fullUrl;


/** 
 * Custom Error Function
 * If the error is an E_WARNING or E_NOTICE, trigger this function instead of the default.
 * 
 */
function custom_error_reporting($error_number, $error_message, $error_file, $error_line) {
	if ($error_number != 8) {
		error_log("Error: [$error_number] $error_message in $error_file on line $error_line");
		header("x-php-error: #$error_number | On Line $error_line || $error_message");
	}
}

// Only set custom error reporting if testing is happening from CircleCI
if (isset($_GET['test']) && $_GET['test']=="true") {
	set_error_handler("custom_error_reporting");
}