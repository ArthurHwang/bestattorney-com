<?php include "./instructional-header.php"; ?>
    
    <body>
        <main class="wrapper">
            <section>
                <h1>The Build Pipeline</h1>
                <p>The build pipeline is a relatively new way of developing our website and then publishing it online. You can learn the steps that we go through in order to get our website changes live, and how each step works along the way.</p>
                <p>Prior to implementing this stuff, all we did was open a file on the server, edit it, and then save it. To check our work we'd just visit the page live and see if our changes worked. This is not a good way to do things - it means that the website can be broken really easily.</p>
                <p>We've now rewired our workflow to the following process: <strong>If you don't read anything else in this document, at least read this list.</strong></p>
                <ol>
                    <li><strong>We work on our files on a local copy of the website.</strong> We check that our files are working by visiting <code>http://localhost</code></li><br>
                    <li><strong>We run webpack to bundle our CSS and Javascript code together,</strong> so that we combine CSS files and JS files and minify all the content.</li><br>
                    <li><strong>We use Git to commit (save) our changes and push our recently changed files to Github.</strong> At this point, everyone can pull those changes down to their local website copy on their computer.</li><br>
                    <li><strong>Git Hooks (an automated github feature) tells our server: "There are new changes, pull them to your server!"</strong> At this point, the changes are pulled and go live on https://www.bestattorney.com</li><br>
                    <li><strong>CircleCI also integrates with github.</strong> Whenever a new commit is pushed, CircleCI runs "Continuous Integration" software and Jasmine Tests, to check for 404s and PHP errors</li><br>
                </ol>
            </section>
            <hr>
            <section>
                <h2>Part 1. The Local Environment</h2>
                <p>In order to check that our changes to the website are correct, we want to make sure that our local environment is as close to the server environment as possible. That being said, we've installed <span class="tippy" title="Apache: The web server program that accepts a URL and then spits out a file as a 'webpage'">Apache</span> and <span class="tippy" title="PHP: The programming language that allows us to have dynamic content on our website">PHP</span> on everyone's computer, which matches the <span class="tippy" title="Environment means the software and programs that we're running on our server to produce our website. Naturally many things will be different, eg. Our server is on Linux and our computers are on Windows.">environment</span> on our server.</p>
                <h4>Relevant Files:</h4>
                <table class="relevant-files">
                    <tbody>
                        <tr>
                            <td>~/environment.php</td>
                            <td>The environment.php file is different between the live site and the dev site - it tells the website how to act if we're live or if we're in dev. For example, the blog is not loaded on the dev site because we'd have to query our server to get blog posts and that's a pain.</td>
                        </tr>
                        <tr>
                            <td>~/development-scripts.php</td>
                            <td>This file currently only has 1 piece of javascript, but it just redirects every link we click in localhost back to the localhost domain, instead of following through to bestattorney.com. That would be confusing. </td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr>
            <section>
                <h2>Part 2. Webpack</h2>
                <p>Webpack is a piece of javascript software called a "module bundler," meaning that it takes resources that we need to use on our website and combines them all together so that we need to include 1 file on our website, while making modifications to that file during the bundling process.</p>
                <h4>Here are the things that Webpack does when we run it:</h4>
                <ul>
                    <li>Looks through each file in the <code>~/public_html/src/</code> folder to see what files we're combining</li>
                    <li>Takes each of the imported resources in those files and combines them into one CSS and one JS file while doing the following:
                        <ul>
                            <li><span class="tippy" title="Minifying just squishes code together and eliminates unnecessary code automatically. It makes the final file-size much smaller without affecting the code." >Minifying</span> all content so that the final file is smaller.</li>
                            <li>Updating the Javascript to work on older browsers (<span class="tippy" title="Babel is another piece of javascript software called a 'transpiler' - it takes code from newer versions of javascript and re-writes it to older versions of JS, so you can write JS quickly and efficiently, but it will still work on older browsers.">Babel</span>)</li>
                            <li>Extracts a <code>common</code> CSS and Javascript file that contains code for the whole website, so it can be loaded on every single page. This file will be cached across the whole site so you only need to load it once. Everything that is not a part of this file gets separated into it's own 'template-specific' CSS and JS file. </li>
                        </ul>
                    </li>
                    <li>Creates a 'source map' -  a file that helps programmers debug code that is minified (since it's a lot harder to read when it's minified.)</li>
                    <li>Spits out one CSS file and one JS file for each template of the site, in addition to one common CSS and one common JS file to be loaded across the whole site.</li>
                </ul>

                <h4>Therefore, pay attention:</h4>

                <p><strong>You can no longer change files in the <code>/css</code> and <code>/js</code> folders and expect the website to change. You must make your changes and then run the webpack script.</strong></p>
                <p>Currently, only Ryan and Myself (Matt) have webpack installed on our computers because we don't expect anyone else to be making CSS and JS changes. If you ever decide to make an effort to understand either of those, you can ask Ryan to install a few things on your computer (<span class="tippy" title="The software that allows Javascript to run on a server and not on a user's browser. Every piece of javascript that is not in the public_html folder is run by Node.">Node</span>, Node Package Manager, Babel, Webpack, etc.) </p>
                <p>Or if you need CSS changed, you can make the changes, push those changes up to github, and ask someone to pull those changes and run webpack for you.</p>

                <h4>Relevant Files: </h4>
                <p>Note - you may not have these files on your computer but they are on the server.</p>
                <table class="relevant-files">
                    <tbody>
                        <tr>
                            <td>~/node_modules</td>
                            <td>This folder has a bunch of Node plugins and packages that we install and use right out of the box (webpack, babel, etc.) As such, we don't need to touch this folder ever.</td>
                        </tr>
                        <tr>
                            <td>~/build_scripts</td>
                            <td>This folder has files that are used to configure Webpack and tell it what packages it needs to use when it needs to run. It also doesn't need to be touched.</td>
                        </tr>
                        <tr>
                            <td>~/package.json</td>
                            <td>This file is used to configure Node - it notates all of the packages that we've installed, and the shortcut commands that we can use to run those packages. (eg. The command: <code>npm run prod</code> when typed into a terminal will run webpack and bundle our CSS and JS) </td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr>
            <section>
                <h2>Part 3. Git/Github</h2>
                <p>Github is a version control tool that we use to save every change that we've ever made, and if we need to we can revert our changes immediately.</p>
                <p>Here's how it works</p>
                <ul>
                    <li>We make changes on our local copy</li>
                    <li>We "commit", or save that work - essentially taking a git 'snapshot' of what our website looks like when we finished editing</li>
                    <li>We "push" our work to the github repository online</li>
                    <li>Other people "pull" our changes down to their local copy, and everyone's work is synced and saved.</li>
                    <li>If others are working on the same file that we are at the same time, git tells us what kinds of changes we've made that conflict, and leaves it up to us to decide what we want to keep.</li>
                </ul>
                <p>For more information, read the <a href="https://www.bestattorney.com/template-files/instructional-files/quick-guide-github.html">Quick Github Guide</a>, and if you need help with a situation, you can read the <a href="https://www.bestattorney.com/template-files/instructional-files/git-help.html">Github Help Document</a>.</p>

                <h4>Relevant Files: </h4>
                <table class="relevant-files">
                    <tbody>
                        <tr>
                            <td>~/.git</td>
                            <td>This folder contains all of the git information - configuration settings, hooks for doing things with your code before or after committing, etc. You don't usually have to go into this folder. Also, it's a hidden folder (starts with a <code>.</code>) so you might not even see it.</td>
                        </tr>
                        <tr>
                            <td>~/.git/hooks/commit-msg</td>
                            <td>This file is a script that is run every time we make a commit. If the files that we're committing are not HTML or PHP, then we prepend a message to the commit message saying <code>[skip ci]</code>, which skips any tests run on these files by CircleCI - since we don't have unlimited CircleCI running time for free.</td>
                        </tr>
                        <tr>
                            <td>~/.gitignore</td>
                            <td>This file has a list of all of the files that git should ignore and NOT push up to github. This is important because sometimes we want files to be different between our local installation and the live one. (eg. environment.php, or files with logins and passwords in them - we don't want those to exist in our repo.)</td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr>
            <section>
                <h2>Part 4. Git Hooks/Pushing Live</h2>
                <p>This is how the website goes live. When our Github repository (https://github.com/bestattorney/bestattorney.com) receives a pushed commit, it will send a message to a file on our website. You can see the options for this message, or 'hook' here: https://github.com/bestattorney/bestattorney.com/settings/hooks</p>
                <p>Here's what that file does:</p>
                <ul>
                    <li>Checks to see if the password given by github is correct, and that it's being accessed by a POST request</li>
                    <li>runs a git command, <code>git pull</code>. This updates the site with all changes from github.</li>
                    <li>Checks to see if there are any errors from that operation. If there are it emails me (Matt) and displays an error in the PHP error log on the server.</li>
                </ul>
                <h4>Relevant Files: </h4>
                <table class="relevant-files">
                    <tbody>
                        <tr>
                            <td>~/public_html/template-files/scripts/deploy2.php</td>
                            <td>This file will not be in your local repository, and the way it is set up is fairly breakable, so please don't touch.</td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr>
            <section>
                <h2>Part 5. CircleCI Testing</h2>
                <p>CircleCI is a Continuous Integration tool - meaning that it will automate building tasks and tests for you that can be done prior to you setting changes live to the website.</p>
                <p>Every time we push a commit to github, github sends a message to CircleCI saying: "I have a new commit. Copy all of my repository's files over to your server, and run any commands that I tell you to run."</p>
                <p>CircleCI only does one thing for us currently, which is to run <span class="tippy" title="Jasmine: A javascript testing framework that allows very easy tests to be created. We have it installed in our repository and CircleCI automatically runs the tests we tell it to.">Jasmine</span> tests on the files that are the most recently changed. <span class="tippy" title="In a bigger professional environment, CircleCI may do the following: Run Webpack to compile CSS/JS, or Compile SASS/LESS to CSS and Typescript/Coffeescript to JS, upload files to a development server where developers can check their work, run unit tests on that development server, and then push those changes live if those tests pass.">CircleCI is definitely more capable of that however.</span> Here's how it does it:</p>
                <ul>
                    <li>Pulls our github repository to the CircleCI server</li>
                    <li>CircleCI creates a copy of our environment on it's servers, from the instructions we give it (~/circle.yml)
                        <ul>
                            <li>Use Node version 8.2.0</li>
                            <li>Install the Node package "request", which is used to make http requests during testing to check for 404s and PHP errors.</li>
                            <li>Install the Node package "Jasmine" - the javascript testing framework.</li>
                        </ul>
                    </li>
                    <li>Get a log of the files that were changed in the last 2 git commits, and save them to a file <code>/spec/helpers/git-changes.txt</code></li>
                    <li>CircleCI starts the jasmine test, which first runs the file <code>~/spec/helpers/ReadGitChanges.js</code> and gets a list of changed (not deleted) HTML files to test for.</li>
                    <li>Tests that each changed file loads properly, (sends a 200 OK HTTP Header) and doesn't have any PHP errors</li>
                    <li>If there are errors, the build fails and an email is sent out. Otherwise, the build passes and nothing else happens.</li>
                </ul>
                <h4>Relevant Files: </h4>
                <table class="relevant-files">
                    <tbody>
                        <tr>
                            <td>~/circle.yml</td>
                            <td>The configuration file for CircleCI. This file gives instructions for what software the build should be using and how to run the tests.</td>
                        </tr>
                        <tr>
                            <td>~/spec</td>
                            <td>A spec folder refers to testing. All files required for jasmine tests are located here.</td>
                        </tr>
                        <tr>
                            <td>~/spec/helpers/ReadGitChanges.js</td>
                            <td>The file that reads the list of changed files, and compiles it into an array to be read by the Jasmine tests</td>
                        </tr>
                        <tr>
                            <td>~/spec/HttpRequestsSpec.js</td>
                            <td>The file that contains the actual tests - it makes the HTTP requests, and gets back the Status Code (should be 200 or the test will fail) and if there is a header in the response named <code>x-php-error</code>, in which case there is a PHP error and the test will fail.</td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr>
        </main>
    </body>
    <script>
            tippy('.tippy', {
                animation: 'fade',
                arrow: true,
                size: 'large',
                maxWidth: "400px",
            });
    </script>
    <style>
        .tippy {
            color: #4b7eb5;
            font-weight: 600;
            cursor:pointer;
        }
        .tippy-tooltip[data-size=large] {
            font-size:1.6rem;
        }
    </style>
</html>