#################################################################
HOW TO EDIT THE PAGE OPTIONS ON EVERY SINGLE PAGE OF THE WEBSITE. 
#################################################################

	Hi, my name is Matt, good to meet you. I'll be walking you through your WONDERFUL and EXCITING journey of understanding the bestattorney website today.
	I'd suggest reading the introduction once and then after that, just skip to the section you need to edit in the Table of Contents. 

#################
TABLE OF CONTENTS
#################

	1. INTRODUCTION - Line 32
		A. The PHP Options Object - Line 36
		B. The Folder Options File - Line 65
		C. Adding different options for 1 page. - Line 77
	2. LIST OF ALL PAGE OPTIONS - Line 102
		A. Page Type - Line 107
		B. Page Meta Info - Line 117
		C. Template Content - Line 131
		D. Sidebar Content - Line 164
		E. Page Layout - Line 237
		F. Page Resources - Line 246
	3. THE HEADER - Line 256
	4. THE SIDEBAR - Line 280
	5. THE FOOTER - Line 295
	6. META-OPTIONS - Line 311

###############
1. INTRODUCTION
###############


####  A. The PHP Options Object  ####


	With the advent of a new back end framework created in PHP, we now have a much easier way to customize options for each page individually. 
	Let me show you how it works. 

	The following is PHP code. You don't have to understand how it works, you just have to learn how to recognize it. 

	<?php      // The "<?php" signifies that everything following is considered php code. If anything in here is not correct (missing comma, missing semi-colon, extra characters, etc.) the webpage will fail.
		
		// This is a comment. Anything following 2 slashes ("//") will not be considered as code and you can write anything you want on the same line. 

		$options['location'] = "los-angeles";  // This is one option, set inside an object that holds ALL the options ($options).
		// This particular option is setting the location for the page, which means we can do something like this: 

		if ($options['location'] == "los-angeles") {
			// Set our contact link to <a href="https://www.bestattorney.com/los-angeles/contact.html">
		} else {
			// Set our contact link to <a href="https://www.bestattorney.com/contact.html">
		}

		// In plain english, this is saying "If this page is a Los Angeles page, the contact link should go to the los angeles contact page. Otherwise, it should go to the regular contact page. "
		// Setting these page options before the page is actually loaded makes it really easy to make adjustments to each individual page, or entire folders at a time. 

	?>  

	The "?>" signifies that the PHP code is ending, and anything after this is treated as normal HTML.


####  B. The Folder Options File  ####


	A folder-options.php file is included in every folder of the site, and every file should include one of these at the beginning. 
	The folder options file CREATES the OPTIONS object, and sets some defaults. You can change these for different folders, if you need to. 

	For example, you can set the $options['location'] to 'riverside' inside the riverside folder, or set it to 'san-bernardino' in the san bernardino folder. 
	you can set the $options['isSpanish'] to true inside the /abogados/ folder. (NOTE: true AND false DO NOT REQUIRE QUOTATIONS AROUND THEM. eg. $options['isHome'] = false;) 

	Every page on the site should load their folder-options.php file from their directory (unless they're loading /template-files/site-options/default-folder-options.php), and therefore you have a set of options that is set correctly for every page in that folder. 


####  C. Adding different options for 1 page. ####


	If you open most pages on the site, you should see the following code: 

		<?php include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php");
		include("folder-options.php");

		// ADD ALL OTHER PAGE OPTIONS HERE!

		?>

	You'll notice that the folder options file is included (after another file which you shouldn't worry about - it's responsible for tracking users as they go through our site and then showing us what pages they visited on the contact form.)
	Underneath the comment section that says "ADD ALL OTHER PAGE OPTIONS HERE!", you have the chance to override any options that have been set by the folder options. 

	FOR EXAMPLE: in the about-us folder, every page has a sidebar, EXCEPT the testimonials page.
	If we didn't change the page options on /about-us/testimonial.html to show that it was a fullWidth page, the page would have a sidebar and the design would be messed up. 
	Therefore, we have to add an option after the folder-options.php line, that says:

		$options['fullWidth'] = true;

	This will overwrite the setting of   `$options['fullWidth'] = false;`  that was set in the folder-options.php file. 
	For any other options that you'll need to overwrite on a page by page basis, you can do that here. 

###########################
2. LIST OF ALL PAGE OPTIONS
###########################

Here I will list all of the page options and the effects of changing them. 

####  A. Page Type  ####
	These page options are here to primarily decide what header template we load. (header-home.php gets loaded if $options['isHome'] is set to true.) 

	$options['isHome']
	$options['isBlog']
	$options['isGeo']
	$options['isPa']
	$options['isSpanish']
	$options['isContact']
	
####  B. Page Meta Info  ####
	These options define the page meta information. 

	$options['canonical']
		Just add the canonical link as the option. 
	$options['noindex']
		Add true to noindex a page. 
	$options['ogTitle']
		Determines the Title that is shown when this page is shared on Facebook
	$options['ogDescription']
		Determines the description that is shown when this page is shared on Facebook
	$options['ogImage']
		Determines the image that is shown when this page is shared on Facebook
	$options['searchWeight']
		A number from 1-5, determines how high up a page shows in the site search. 
		Note, for this to take effect the page has to be submitted to the index when this option is set. Changing the option by itself does nothing. See more: https://www.bestattorney.com/template-files/scripts/add-page-to-index.php 
	$options['featuredImage']
		A full url ("https://www.bestattorney.com/images/....") that will populate JSON-LD schema, hopefully leading to being able to see thumbnails in mobile SERPs in the future.


####  C. Template Content  ####
	These options define some of the page content. 

	$options['pageSubject']
		pageSubject sets a few different things like the message on the contact form. There are several different words you can use to set this, depending on the subject of the page you're on:
			"personalinjury"
			"dogbite"
			"accident"
			"autodefect"
			"productdefect"
			"medicaldefect"
			"drugdefect"
			"premisesliability"
			"truckaccident"
			"pedestrianaccident"
			"motorcycleaccident"
			"bicycleaccident"
			"employmentlaw"
			"wrongfuldeath"

	$options['location']
		Changing this changes the phone numbers, contact links, nav options, and addresses shown on the page. The options are: 
			"orange-county"
			"los-angeles"
			"riverside"
			"san-bernardino"

	$options['spanishPageEquivalent']
		If you have a specific spanish page that this page should link to, write the RELATIVE TO THE ABOGADOS PATH url here. (ie. if you want it to go to "https://www.bestattorney.com/abogados/lesiones-personales.html", set the option to "lesiones-personales.html")

	$options['noLiveChat']
		Set this option to true and the live chat will not load on the site. 

####  D. Sidebar Content  ####
	These all change something on the sidebar.

	$options['showSidebarContactForm']
		true or false if you want the sidebar contact form to show. 

	$options['sidebarLinksTitle']
		The title you want to show above the sidebar links. Usually "____ injury information"
	$options['sidebarLinks'] :
		The links you want to show as sidebar links. Must be in a format like so:

			[
		        "Bicycle Accidents" => "/laguna-niguel/bicycle-accidents.html",
		        "Brain Injury" => "/laguna-niguel/brain-injury.html",
		        "Bus Accidents" => "/laguna-niguel/bus-accidents.html",
		        "Car Accidents" => "/laguna-niguel/car-accidents.html",
		        "Dog Bites" => "/laguna-niguel/dog-bites.html"
		    ]

		If you don't want to show sidebar links, comment this option out, like so:

			// "sidebarLinks" = []
		or
			// "sidebarLinks" = array()
		etc.

	$options['sidebarContentTitle']
		This title shows up in the blue box on the sidebar. 
	$options['sidebarContentHtml']
		This content shows up in the blue box on the sidebar. You can and should write HTML code in here. 
	$options['extraSidebar']
		If you want to put any extra HTML in the sidebar, add this here. This is used on Brian's profile where he has extra stuff to show. 
	$options['caseResultsArray']
		This sets the type of results that you'll get in the case results widget. To set it properly, set the option as an array like so: 

			[ type, string, additional]

		type:
			is the type of results you want to get. (Location, Category, Search, or Top)
			eg.   `$options['caseResultsArray'] = ["top"];`   gives you the top 10 results. 
		string: 
			is the information you give to describe the type. 
			eg.   `$options['caseResultsArray'] = ["location", 'los-angeles'];`   gives you the top 10 results in Los Angeles.
		additional:
			is an array with additional case results that you want to add to the end of the first set of results. 
			eg.   `$options['caseResultsArray'] = ["location", 'bakersfield', ["category", "auto"]];`   gives you up to 10 results in bakersfield, but if there are fewer than 10 bakersfield results, the rest of the results will be filled with auto accident results. 

		read function.php for more info on this. 

	$options['rewriteCaseResults']
		true or false, depending on if you want to change the case results based on pageSubject.  

	$options['reviewsArray']
		This sets all of the reviews on the sidebar. The easiest way to do it is to set it to have 2 or 3 random reviews, like so: 

			$options["reviewsArray"] = [2];

		This sets 2 random reviews. 

	$options['videosArray']
		An array of arrays, each containing video information, like so: 
			$options['videosArray'] = [
				['Bisnar Chase Brand Video', '/images/video-images/brian-brand-video-full.jpg', 'wmv1HKnhW7U'],
				['Brian Chase on American Law TV', '/images/featured-homepage-images/brian-american-law-television.jpg', 'BhVPzkYsLuE']
			];

		This will print out 2 videos on the sidebar - "Bisnar chase brand video" and "Brian chase on ALTV"
		The first parameter of each video array is the video title. 
		The second parameter is the relative url to the video thumbnail.
		The third parameter is the 11-character youtube video id. You can get this from the youtube url.

	$options['loadBlog']
		true or false, show the blog on the sidebar. 
	$options['allowSidebarTruncate']
		true or false, allow a function to remove some sidebar elements if it stretches way past the end of the content.

	$options['contactText'] = "...text here";
		This text will go into the textarea box as a placeholder text (ie. describe your injuries/accident, etc.) 

	$options['formTitle']; = "...text here";
		This text will go across the top of the PA-GEO template as the title for the form. 


####  E. Page Layout  ####		

	$options['headerImageClass']
		Add a class to the header-image so you can add a different image for the header. See /about-us/testimonials or /referrals.html
	$options['fullWidth']
		true or false depending on if you want a full-width page with no sidebar. 
	$options['showFooterVideos']
		shows the 'wooden' section underneath the content with all the john bisnar videos. Default off cause no one wants to see that.  
	
####  F. Page Resources  ####		
	Load specific resources for the whole folder. Should usually stay default. 

	$options['loadJquery']
	$options['loadJqueryUi']
	$options["loadLegacyBootstrap"]
	$options['loadFontAwesome']


###############
3. THE HEADER
###############

	There are 6 Templates for the BestAttorney.com site. They are as follows: 

		- Home - Only the Normal Homepage and the Spanish Homepage are loaded by this template.
		- Default - All normal pages (About Us, Case Results, Etc.)
		- PA-GEO - All Practice Area or Location Pages.
		- Blog - All Blog Pages
		- Contact - All Contact Pages
		- Spanish - All Spanish pages

	Each of these templates have their own header file which differ slightly. 
	For example:
		- The Contact page templates don't have a dropdown contact form on them in tablet view (because that wouldn't make sense.)
		- The Spanish page templates don't have a "En Español" button at the top (because that wouldn't make sense)
		- The Blog pages load the Wordpress instance so that posts can be retrieved from the database.
		- etc.

	Every page on the site loads /template-files/header.php, and all of the website info that is necessary for every page is loaded there (Google analytics info, meta-options.php, functions.php, Meta info for canonical, facebook open graph, etc.), and then the options object is used to determine what header template should be loaded from there. 

	If you need to edit something that you believe is determined in the header, you can open /template-files/header.php and try to read through the code, following any `include` statements you encounter to find your code. 

###############
4. THE SIDEBAR
###############

	The Sidebar is loaded within the /template-files/side-footer.php. This file looks at all of the sidebar options discussed in section 2-D and spits out sidebar content depending on the options set within folder options or at the top of the page. 

	Some things to note: 

		- The blog has its own sidebar, since it is requires some PHP that is specific to the Wordpress instance. 
		- The `fullWidth` option must be set to false in order for the sidebar to show. 
		- The sidebar.php file is also loaded on spanish pages by /template-files/side-footer-spanish.php

	I have no idea what else to put here so if anyone needs to know anything else, figure it out yourself. 
	Or alternatively, just ask me, and I'll add it here. 

###############
5. THE FOOTER
###############

	The footer is loaded by the /template-files/side-footer.php file (or the /template-files/side-footer-spanish.php file on the spanish pages)
	It contains the ending divs for the content section, as well as including a few other sections: 

		-  The "Was this page helpful?" Section - this goes right above the end content div. 
		-  The included sidebar.php file
		-  The awards/mentions section (Flexbox of logos where we've won awards or been mentioned)
		-  "Have a question that wasn't answered here?" Section
		-  Footer Videos section - these were videos from John and Brian answering questions about themselves. These are turned on by setting the $options['showFooterVideos'] option to true, but by default it is off and I'm not even sure if it would work if it were on. 
		-  Actual Footer area (contains footer links, social media icons, links to locations, etc.)
		-  Included Scripts (jquery, jqueryUi, CSS and JS, Bootstrap and Fonts.)
		-  JSON+LD (schema markup )

###############
6. META-OPTIONS
###############

	The /template-files/meta-options.php file is included on every page to automatically set options that can be echoed onto the page. 
	For example, the meta-options file uses the $options['location'] option to set the $options['callPhone'] to be "9492933814" if the page is an "orange-county" page, or to be "3232384683" if the page is a "los-angeles" page. The address items are also set in order to echo out the address correctly on every page.

	These are all of the options being set on the meta-options page:

		- $page, $thisdir, and $noparamuri. These are not in the $options object, but are determined at the start of every page using the URL, and are used whenever the HTML or PHP needs access to the current URL. You will most likely not need to know about or use these. $noparamuri strips out any string parameters from the url and stores it. 
		- Address and phone options set depending on location. (eg. $options['gmap'] equals a link to a google map of the location's office)
		- Set default case results depending on pagesubject (eg. Case results on dog bite pages will show dog bite case results) These default case results can be rewritten later.
		- Set Spanish Page Equivalent depending on pagesubject.
		- Set form title depending on pagesubject (on PA-GEO pages only. Eg. "Do I have a case for my dog bite injury?")
		- Load jQuery and jQueryUI if $options['showFooterVideos'] is true. (to make sure it works.)
		- Set canonical depending on $noparamuri