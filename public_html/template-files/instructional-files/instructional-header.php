

<html>
<head>
    <link rel="stylesheet" href="https://unpkg.com/sakura.css/css/sakura-dark.css" type="text/css">
    <link rel="stylesheet" href="/css/custom-icons.css" type="text/css">
    <script src="https://unpkg.com/tippy.js@2.2.3/dist/tippy.all.min.js"></script>
    <meta name="robots" content="noindex, nofollow" />
    <style>
        a {
            color: #74afec !important;
        }
        section {
            padding: 15px 0px;
        }
        strong {
            color: #f7f46c;
        }
        code {
            color:#e23434;
        }

        ul, ol {
        list-style: none; /* Remove default bullets */
        }

        li::before  {
        content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
        color: #e23434; /* Change the color */
        font-weight: bold; /* If you want it to be bold */
        display: inline-block; /* Needed to add space between the bullet and the text */ 
        width: 1.5em; /* Also needed for space (tweak if needed) */
        margin-left: -1.5em; /* Also needed for space (tweak if needed) */
        }
        ol {
            counter-reset:li;
        }
        ol > li {
            counter-increment: li;
        }
        ol > li::before {
            color: #1c91d8;
            content: counter(li);
            font-size: 26px;
        }
    </style>
</head>
<a href="https://www.bestattorney.com/template-files/instructional-files/"><small>Go Back to Instructional Files</small></a>