<?php include "./instructional-header.php"; ?>
<body>
    <h2>Instructional Files:</h2>

    <ul>
        <li><a href="/template-files/instructional-files/add-images-to-website.html">Add Images To Website</a></li>
        <li><a href="/template-files/instructional-files/contact-forms.html">Contact Forms</a></li>
        <li><a href="/template-files/instructional-files/google-pagespeed.html">Google Pagespeed</a></li>
        <li><a href="/template-files/instructional-files/git-help.html">Git Help</a></li>
        <li><a href="/template-files/instructional-files/quick-guide-github.html">Quick Guide To Github</a></li>
        <li><a href="/template-files/instructional-files/the-build-pipeline.html">The Build Pipeline</a></li>
        <li><a href="/template-files/instructional-files/texting-and-IFTTT.html">Texting and IFTTT</a></li>
        <li><a href="/template-files/instructional-files/images-lazyloading.html">Images and Lazy-loading</a></li>
        <li><a href="/template-files/instructional-files/algolia.html">Algolia Search Service</a></li>
        <li><a href="/template-files/instructional-files/how-to-edit-page-options.txt">How to edit page options</a></li>
    </ul>
</body>
</html>