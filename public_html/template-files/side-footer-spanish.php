<?php if (!$options['isHome']) { ?>
	</div> <!-- End #content div -->

	<?php if (!$options['fullWidth']) {
		include($_SERVER['DOCUMENT_ROOT'] . "/template-files/sidebar.php");
	} ?>

	<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_awards.php"); ?>

	<?php if (!$options["isContact"]) { ?>
		<div id="have-a-question">
			<div id="get-answers">
				<p class="title">¿más preguntas? ¡Llámenos!</p>
				<a id="call-us" href="tel:<?php echo $options["callPhone"]; ?>" class="brown-button <?php echo $options['location']; ?>">
					<div id="footer-call-us-icons">
						<i class="fa fa-circle-thin" aria-hidden="true"></i>
						<i class="fa fa-phone" aria-hidden="true"></i>
					</div>
					<div>
						<p class="button-sub-title">Llámenos Hoy</p>
						<p class="button-title"><?php echo $options["phone"]; ?></p>
					</div>
				</a>
				<a id="fill-out-form" href="<?php echo $options['contactLink']; ?>" class="contact-button">
					<div id="footer-contact-icons">
						<i class="fa fa-file-text-o" aria-hidden="true"></i>
					</div>
					<div>
						<p class="button-sub-title">Evaluation Caso</p>
						<p class="button-title">Gratuita</p>
					</div>
				</a>
			</div>
		</div>
	<?php } ?>

<?php } ?>
<footer id="footer">

	<div id="footer-inner">

		<p id="office-details" itemscope itemtype="http://schema.org/Attorney">
			<span itemprop="name">Bisnar Chase Personal Injury Attorneys</span>
			<img itemprop="image" style="display:none;" src="https://www.bestattorney.com/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg" alt="Brian Chase and John Bisnar" />
			<span class="nopc notablet"><br /></span>
			<span class="nomobile">&bull;</span>
			<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress"><?php echo $options["address"]["street"]; ?></span>
				<span class="nopc notablet"><br /></span>
				<span itemprop="addressLocality"><?php echo $options["address"]["city"]; ?></span>,
				<span itemprop="addressRegion"><?php echo $options["address"]["state"]; ?></span>

				<span itemprop="postalCode"><?php echo $options["address"]["zip"]; ?></span>
			</span>

			<span class="nopc"><br /></span><span class="notablet nomobile">&bull;</span> local: <span itemprop="telephone"><?php echo $options["phone"]; ?></span><span class="nopc notablet"><br /></span>
			<?php if ($options["address"]["gmap"]) { ?>
				<span class="nomobile">&bull;</span> <a href="<?php echo $options["address"]["gmap"]; ?>" target="_blank">Direcciones</a>
			<?php } ?></p>

		<div id="social-media">
			<p class="title">Redes sociales</p>
			<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_social_media.php"); ?>
		</div>

		<div id="sitemap">

			<p class="title">Sitio web</p>

			<ul class="ul1">

				<li><a href="https://www.bestattorney.com/abogados/">Inicio</a></li>

				<li><a href="https://www.bestattorney.com/attorneys/john-bisnar.html">John Bisnar (Inglés)</a></li>

				<li><a href="https://www.bestattorney.com/attorneys/brian-chase.html">Brian Chase (Inglés)</a></li>

			</ul>

			<ul class="ul2">

				<li><a href="https://www.bestattorney.com/abogados/recursos/elegir-un-abogado.html">Elegir Un Abogado</a></li>

				<li><a href="https://www.bestattorney.com/abogados/lesiones-personales.html">Areas de Práctica</a></li>

			</ul>

			<ul class="ul3">

				<li><a href="https://www.bestattorney.com/abogados/resultos-destacados.html">Resultados</a></li>

				<li><a href="https://www.bestattorney.com/abogados/contactenos.html">Contactenos</a></li>

			</ul>

			<ul class="ul4">

				<li><a href="https://www.bestattorney.com/abogados/testimonios.html">Testimonios</a></li>

				<li><a href="https://www.bestattorney.com/blog/">Lea nuestro blog (Inglés)</a></li>

			</ul>

			<div class="clear"></div>

		</div>

		<div class="clear"></div>

		<div id="disclaimer">

			<p class="title">Copyright y Descargo de responsabilidad</p>

			<p><strong>Advertencia:</strong> La información legal presentada en este sitio no debe interpretarse como asesoramiento legal formal, ni la creación de una relación abogado-cliente. Cualquier resultado mostrado aqui depende de los hechos de ese caso y los resultados serán diferentes de un caso a otro.</p>

			<p><strong>Bisnar Chase </strong> sirve a toda California. Además, representamos a clientes en otros estados a través de nuestras asociaciones con firmas de abogados locales. A través de la firma local, seremos admitidos a ejercer leyes en su estado, pro hac vice, que significa "para esta ocasión especial." Cuando está en el interés de nuestros clientes, contamos con la firma de abogados local (sin costo adicional para nuestros clientes) para ayudarnos a comparecer ante la corte en asuntos de rutina y los procedimientos de investigación para perseguir más eficazmente la causa de nuestro cliente.</p>

			<p><strong>Copyright &copy; 1999-<?php echo (date('Y')); ?> Bisnar Chase Personal Injury Attorneys</strong> - Reservados todos los derechos.</p>

		</div>

	</div>

	<span id="scripts">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<?php if ($options['isHome']) { ?>

			<?php if ($options["loadLegacyBootstrap"]) { ?>
				<link rel="stylesheet" href="/css/bootstrap.min.css">
				<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
			<?php } else { ?>
				<link rel="stylesheet" href="/css/bootstrap-grid.min.css">
			<?php } ?>

			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/common-styles.css'); ?>" type="text/css" media="all" />
			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/home-styles.css'); ?>" type="text/css" media="all" />
			<script src="<?php echo auto_version("/dist/js/common-scripts.js"); ?>"></script>
			<script src="<?php echo auto_version("/dist/js/home-scripts.js"); ?>"></script>

		<?php } else { ?>
			<!-- If this is not the spanish homepage -->

			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/common-styles.css'); ?>" type="text/css" media="all" />
			<!-- <link rel="stylesheet" href="<?php echo auto_version('/dist/css/default-styles.css'); ?>" type="text/css" media="all"/> -->
			<script src="<?php echo auto_version("/dist/js/common-scripts.js"); ?>"></script>
			<script src="<?php echo auto_version("/dist/js/default-scripts.js"); ?>"></script>
		<?php }

		// THIS IS ADDING THE FEATURED IMAGE INTO JSON-LD TO TRY TO GET THAT STUPID THUMBNAIL INTO GOOGLE MOBILE SERPS
		if (!isset($options['featuredImage'])) {
			$options['featuredImage'] = "https://www.bestattorney.com/images/featured-homepage-images/case-value.jpg";
		}

		echo $options['jsonld']; ?>

		<script>
			let truncate = false;
			<?php if ($options['allowSidebarTruncate'] && !$options['fullWidth']) { ?>
				truncate = true;
			<?php } ?>
		</script>

		<?php if ($environment == "development") {
			include_once($_SERVER['DOCUMENT_ROOT'] . "/../development-scripts.php");
		}

		if (!isset($_GET['test']) && !$options['noLiveChat']) { ?>
			<!-- Start Of NGage -->
			<!-- <div id="nGageLH" style="visibility:hidden; display: block; padding: 0; position: fixed; right: 0px; bottom: 0px; z-index: 5000;"></div>
			<script type="text/javascript" src="https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=88-8-25-99-227-118-219-81" async="async"></script> -->
			<!-- End Of NGage -->
			<!-- Start Of NGage -->
			<div id="nGageLH" style="visibility:hidden; display: block; padding: 0; position: fixed; right: 0px; bottom: 0px; z-index: 5000;"></div>
			<script type="text/javascript">
				function loadLiveChat() {
					var element = document.createElement("script");
					element.src = "https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=88-8-25-99-227-118-219-81";
					// setTimeout(()=>document.body.appendChild(element),1000);
					document.body.appendChild(element)
				}
				if (window.addEventListener) window.addEventListener("load", loadLiveChat, false);
				else if (window.attachEvent) window.attachEvent("onload", loadLiveChat);
				else window.onload = loadLiveChat;
			</script>
			<!-- End Of NGage -->
		<?php } ?>

	</span>

</footer>
</body>

</html>