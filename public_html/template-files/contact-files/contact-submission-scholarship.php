<?php
include $_SERVER['DOCUMENT_ROOT']."/template-files/contact-files/contact-submission.php";

class ContactSubmissionScholarship extends ContactSubmission {



    protected function setAllVariables() {
        $this->name = strip_tags(trim($_POST['name']));
        $this->school = strip_tags(trim($_POST['school']));
        $this->grade = strip_tags(trim($_POST['grade']));
        $this->phoneNumber = strip_tags(trim($_POST['phone']));
        $this->emailscholarship = trim($_POST['emailscholarship']);

        $this->q1 =  trim($_POST['q1']);
        $this->q2 =  trim($_POST['q2']);
        $this->q3 =  trim($_POST['q3']);
        $this->q4 =  trim($_POST['q4']);
        $this->q5 =  trim($_POST['q5']);
        $this->terms = trim($_POST['terms']);

        $essay = "Introduction: <br>";
        $essay .= $this->q1." <br><br>";
        $essay .= "Why are you committed to education? <br>";
        $essay .= $this->q2." <br><br>";
        $essay .= "What are the struggles your community goes through? <br>";
        $essay .= $this->q3." <br><br>";
        $essay .= "What is your plan to solve or deal with these issues in your community? <br>";
        $essay .= $this->q4." <br><br>n";
        $essay .= "How will your continued education help you make your plan a reality? <br>";
        $essay .= $this->q5." <br>";
        $this->message = $essay;

        $this->honeypot = $_POST['form-hp'];

        $this->validPhone = $this->validatePhone();
        // $this->validEmail = $this->validateEmail() ? true : false; // Theres a reason for this i swear.

        $this->ip = isset($_SERVER['HTTP_X_SUCURI_CLIENTIP']) ? $_SERVER['HTTP_X_SUCURI_CLIENTIP'] : $_SERVER['REMOTE_ADDR'];
        $this->page = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_POST['noparamuri'];
        $this->referrer = $_SERVER['HTTP_REFERER'];
        $this->userAgent = $_SERVER['HTTP_USER_AGENT'];

        $this->autoResponderFailMessage = "Error: An Autoresponder email to: ".$this->emailscholarship." was unable to be sent but we have recived your schlarship submission.";
        $this->autoResponderEmailFailMessage = "Error: An Autoresponder email to: ".$this->emailscholarship." was unable to be sent. Please try again.";
    }

    public function checkValidations() {
        if ($this->terms != "true") {
            echo("this->terms is equal to: ".$this->terms);
		    $this->errorOut("HTTP/1.0 403 Forbidden", "Please Read the Terms and Conditions and Check the Box!");
        }

        if ($this->honeypot != "") {
            $this->invalidEmail("Honeypot form field was full");
            error_log("=============");
            error_log("Honeypot should be empty but is: ".$this->honeypot);
            error_log("=============");
            $this->errorOut("HTTP/1.0 200 OK", "Scholarship Submitted!");
        }

        // if (!$this->validEmail && !$this->validPhone) {
        //     $this->invalidEmail("This submission has an invalid phone and email");
        //     error_log("=============");
        //     error_log("Valid Phone or email not set: ");
        //     error_log("=============");
        //     $this->errorOut("HTTP/1.0 400 Bad Request", $this->invalidPhoneEmailMessage);
        // }

        if ($this->name == "") {
            $this->invalidEmail("This submission contained no name");
            error_log("=============");
            error_log("Name not set: ");
            error_log("=============");
            $this->errorOut("HTTP/1.0 400 Bad Request", $this->noNameMessage);
        }

        if( $this->q1 == "" ||
            $this->q2 == "" ||
            $this->q3 == "" ||
            $this->q4 == "" ||
            $this->q5 == "" ) {
                $this->invalidEmail("This submission is not complete");
                error_log("=============");
                error_log("Scholarship Fields not all filled out.");
                error_log("=============");
                $this->errorOut("HTTP/1.0 400 Bad Request", "Please answer all scholarship questions!");
        }
    }

    public function prepareIntakeMessage() {

        $this->intakeSubject = "Scholarship Submission from: ".$this->name." - Due 12/15/18";
        $this->intakeMessageHtml = "<html style='padding:20px;'>
            <div style='display:flex;'>
                <img src='https://https-webfiles-bisnarchaseperso.netdna-ssl.com/images/logo.png'>
            </div>
            <div style='width:100%;padding:40px;box-sizing:border-box;font-family:Verdana,sans-serif;color:#333;'>
                <h1 style='padding:10px;'>Scholarship Submission:</h1>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
                <table style='border-collapse:collapse;'>
                    <tbody>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;width:20%;padding:10px;'>Name:</td>
                            <td style='width:80%;padding:10px;'>".$this->name."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Phone Number:</td>
                            <td style='padding:10px;'>".$this->phoneNumber."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Email Address:</td>
                            <td style='padding:10px;'>".$this->emailscholarship."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>School:</td>
                            <td style='padding:10px;'>".$this->school."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Grade:</td>
                            <td style='padding:10px;'>".$this->grade."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Scholarship Submission:</td>
                            <td style='padding:10px 50px 10px 10px;'>".$this->message."</td>
                        </tr>
                    </tbody>
                </table>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
            </div>
        </html>";
    }

    public function prepareAutoResponseMessage() {
        $this->autoResponderSubject = "Thanks for Submitting Your Scholarship - Bisnar Chase";
        $this->autoResponderMessageText = "
Dear ".$this->name.",
Thank you for submitting your scholarship application to us! We will review all of our applicants and choose the best one at the end of the scholarship period. When we choose a winner, we will use the email or phone number to contact our winner. Please ensure that the information you provided is correct.<br><br>"
.$this->intakeMessageHtml.
"Thank you, have a great day!\n\n
- Bisnar Chase Personal Injury Attorneys\n
www.bestattorney.com\n
    ";
    }

    public function sendAutoResponseEmail() {
        $responseEmail = new SendGrid\Email();

        try {
		    $responseEmail->
		    setFrom( "inquiry@bestattorney.com" )->
		    addTo($this->emailscholarship)->
		    setSubject($this->autoResponderSubject)->
		    setHtml($this->autoResponderMessageText);

		    $response2 = $this->sendGrid->send($responseEmail);

		    if (!$response2) {
		        throw new Exception("Did not receive response.");
		    } else if ($response2->message && $response2->message == "error") {
		        throw new Exception("Received error: ".join(", ", $response2->errors));
		    } else {
		    	return true;
		    }
		} catch ( Exception $e ) {
            error_log("Error sending email from: responseEmail");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->inquirySent ? $this->autoResponderFailMessage : $this->autoResponderEmailFailMessage);
        }

    }

    protected function invalidEmail($invalidReason = "(error: No reason given for invalid scholarship)") {
        require_once $_SERVER['DOCUMENT_ROOT']."/template-files/contact-files/invalid-contact-submission.php";
        try {
            $message = "
            School: ".$this->school."\n
            Grade: ".$this->grade."\n
            Submission: \n".$this->message;

            new InvalidContactSubmission($this->ip, $this->name, $this->phoneNumber, $this->emailscholarship, $message, $this->page, $invalidReason, $this->referrer, $this->userAgent, $this->isSpam);
        } catch(Exception $e) {
            error_log("Error Instantiating an InvalidContactSubmission object in /contact-submission.php on line 58: $e");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->invalidEmailMessage);
        }
    }
}