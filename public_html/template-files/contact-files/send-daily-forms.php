<?php

require $_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';

$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();

try {

	$link = new PDO('mysql:dbname=bestatto_contacterr;host=localhost;', 'bestatto_ctcterr', 'BC1301dove');
	$link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$query = "SELECT * FROM emails WHERE created < CURDATE() && created >= (CURDATE() - INTERVAL 1 DAY)";
	$getSubmissions = $link->prepare($query);

	if ($getSubmissions -> execute()) {
		$result = $getSubmissions->fetchAll(PDO::FETCH_ASSOC);
	} else {
		throw new Exception('Could not get yesterdays emails in database.');
	}

} catch(PDOException $e) {
	throw new Exception($e);
}

$numEmails = count($result);
$numIPs = uniqueIPs($result);

if ($numEmails == 0) {
	exit;
}

$messageHtml = "<h1>Good Morning, Wonderful Intake Team!</h1>
				
			 	<p>We had $numEmails invalid contact form attempts yesterday, from $numIPs IP address(es).</p><br>";

$emailIndex = 1;
$formerIP = "";
$currentIP = "";

foreach($result as $email) {
	$formerIP = $currentIP;
	$currentIP = $email['ipAddress'];
	if ($formerIP != $currentIP) {
		$messageHtml .= "<br><div class='big-line'>############################################################</div>
					  	<h2>Group $emailIndex: ".$email['ipAddress']."</h2>";
		$emailIndex++;
	} else {
		$messageHtml .= "<div class='small-line'>____________________________________________________</div><br>";
	}

	$messageHtml .= "<div>"
					  . "<p><strong>Name:</strong> ".$email['name']." </p>"
					  . "<p><strong>Phone:</strong> ".$email['phone']." </p>"
					  . "<p><strong>Email:</strong> ".$email['email']." </p>"
					  . "<p><strong>Body:</strong> ".$email['body']." </p>"
					  . "<p><strong>Sent From Page:</strong> ".$email['page']." <strong>at</strong> ".$email['created']."</p>";

	if ($email['is_spam'] == 1) {
		$queryString="isSpam=false&blog=https%3A%2F%2Fwww.bestattorney.com%2F&user_ip=".urlencode($email['ipAddress'])."&user_agent=".urlencode($email['user_agent'])."&referrer=".urlencode($email['referrer'])."&permalink=".urlencode($email['page'])."&comment_type=contact-form&comment_author=".urlencode($email['name'])."&comment_author_email=".urlencode($email['email'])."&comment_content=".urlencode($email['body'])."&blog_lang=en";
		$messageHtml .= "<br><h3 style='color:red;'>This email was blocked by our spam filter. Does this seem like a real email from a real client? <a href='https://www.bestattorney.com/template-files/contact-files/report-spam.php?".$queryString."'>Report It!</a></h3>";
	} else {
		$messageHtml .= "<br><p>This email is invalid for the following reason: ".$email['invalid_reason']." </p>";
	}

	$messageHtml .=	"</div>";
}

$messageHtml .= "<br><div class='big-line'>############################################################</div><br><p>This message will be sent out at 9am every morning. If you notice that any of these are NOT spam, and we have not yet contacted this user, please try to use the available information to contact them.</p>
				<style>
					.big-line {
						padding: 20px;
					}

					.small-line {
						padding: 5px;
					}

					p {
						margin: 0;
					}
				</style>";

// $headers = "MIME-Version: 1.0" . "\r\n";
// $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// mail("mcheah@bisnarchase.com", "Invalid thing", $messageHtml, $headers);
try {
    $mail->
    setFrom( "inquiry@bisnarchase.com" )->
    setReplyTo("inquiry@bisnarchase.com")->
    setFromName("Marketing Team")->
    addTo("inquiry@bisnarchase.com")->
    setSubject("Invalid Contact Forms Filled Out Yesterday")->
    setHtml($messageHtml);
    
    $response = $sendgrid->send($mail);

if (!$response) {
    throw new Exception("Did not receive response.");
} else if ($response->message && $response->message == "error") {
    throw new Exception("Received error: ".join(", ", $response->errors));
} 
} catch ( Exception $e ) {
    var_export($e);
    exit;
}

function uniqueIPs($emails) {
	$unique = [];
	foreach ($emails as $email) {
		if (!in_array($email['ipAddress'], $unique)) {
			array_push($unique, $email['ipAddress']);
		}
	}
	return count($unique);
}

echo $messageHtml;

?>