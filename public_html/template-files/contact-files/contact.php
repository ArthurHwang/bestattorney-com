<?php

include('./contact-submission-default.php');

$contact = new ContactSubmissionDefault();

$contact->checkValidations();
$contact->checkForSpam();
$contact->setUpSendgrid();
$contact->prepareIntakeMessage();
$contact->sendIntakeEmail();
$contact->prepareAutoResponseMessage();
$contact->sendAutoResponseEmail();

if ($contact->inquirySent) {
  header("HTTP/1.0 200 OK");
  redirect('https://www.bestattorney.com/thank-you-contact.html');
  // exit("Thank You, Your Contact Form Has Been Sent!");
}

?>