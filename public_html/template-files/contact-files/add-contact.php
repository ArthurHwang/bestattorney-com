<?php

include $_SERVER['DOCUMENT_ROOT']."/../misc/constant-contact-creds.php";

$ccUrl = 'https://api.constantcontact.com/v2/contacts?action_by=ACTION_BY_OWNER&api_key='.$ccapikey;

if (isset($_POST['email'])) {
    $newEmail = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);

    $curl = curl_init();

    // write email to log file with timestamp
    date_default_timezone_set('America/Los_Angeles');
    $timestamp = date("Y-m-d h:i:sA");
    $logtext = $timestamp." ".$newEmail."\r\n";
    $logfile = fopen($_SERVER['DOCUMENT_ROOT']."/../misc/newsletter_log.txt", "a") or die("Unable to open file!");
    fwrite($logfile, $logtext);

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $ccUrl,
        CURLOPT_POST => 1,
        CURLOPT_FAILONERROR => 1,
        CURLOPT_POSTFIELDS => json_encode(array(
            "lists" => array(
                array(
                    "id" => "1545875658"
                )
            ),
            "email_addresses" => array(
                array(
                    "email_address" => $newEmail
                )
            )
        )),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$ccapitoken,
            "Content-Type: application/json"
        )
    ));

    $result = curl_exec($curl);
    $response = array();

    if (isset($result['email_addresses'][0])) {
        $response['text'] = "Email Address Added To Newsletter";
    }

    if(curl_error($curl)) {
        $response['errorMessage'] = curl_error($curl);
        $response['errorCode'] = curl_errno($curl);
    }


    $response['statusCode'] = curl_getinfo($curl)['http_code'];

    curl_close($curl);
    echo(json_encode($response));
} else {
    header("HTTP/1.1 403 Forbidden");
    echo "Error: Please Stop It Now.";
}
