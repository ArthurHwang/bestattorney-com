<?php

include('./contact-submission-spanish.php');

$contact = new ContactSubmissionSpanish();

$contact->setSpanishMessages();
$contact->checkValidations();
$contact->checkForSpam();
$contact->setUpSendgrid();
$contact->prepareIntakeMessage();
$contact->sendIntakeEmail();
$contact->prepareAutoResponseMessage();
$contact->sendAutoResponseEmail();

if ($contact->inquirySent) {
	header("HTTP/1.0 200 OK");
	exit("Gracias, su mensaje fue enviado");
}