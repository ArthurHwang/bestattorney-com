<?php

include "./contact-submission.php";

/**
 * This class is used to submit custom contact forms that are not the same as the other forms. 
 * Off the top of my head, I believe there are only 2 forms like this:
 * 1. The Attorney referral form /referrals/
 * 2. The Timeline contact form /resources/case-timeline.html
 */
class ContactSubmissionCustom extends ContactSubmission {
    
    private $intakeAddendum;
    private $autoResponderAddendum;

    public function setIntakeAddendum($intakeHtml) {
        if (is_string($intakeHtml)) {
            $this->intakeAddendum = $intakeHtml;
            return true;
        } else if (is_callable($intakeHtml)) {
            $this->intakeAddendum = $intakeHtml();
            return true;
        } else {
            $this->errorOut("HTTP/1.0 400 Bad Request", "An argument was supplied (\$intakeHtml) that was neither a string nor a function.");
            return false;
        }
    }

    public function setAutoResponderAddendum($autoResponderText) {
        if (is_string($autoResponderText)) {
            $this->autoResponderAddendum = $autoResponderText;
            return true;
        } else if (is_callable($autoResponderText)) {
            $this->autoResponderAddendum = $autoResponderText();
            return true;
        } else {
            $this->errorOut("HTTP/1.0 400 Bad Request", "An argument was supplied (\$autoResponderText) that was neither a string nor a function.");
            return false;
        }
    }

    public function prepDefaultIntakeMessage($keyword = "") {

        if (!isset($this->intakeAddendum)) {
            $this->errorOut("HTTP/1.0 400 Bad Request", "\$this->intakeAddendum is not set. Please call setIntakeAddendum() or use the ContactSubmissionDefault class instead.");
        }

        $this->intakeSubject = "BestAttorney.com $keyword Case Evaluation Request - From:".$this->name;
        $this->intakeMessageHtml = "<html style='padding:20px;'>
        	<div style='display:flex;'>
            	<img src='https://https-webfiles-bisnarchaseperso.netdna-ssl.com/images/logo.png'>
            </div>
            <div style='width:100%;padding:40px;box-sizing:border-box;font-family:Verdana,sans-serif;color:#333;'>
                <h1 style='padding:10px;'>$keyword Contact Form Submission:</h1>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
                <table style='border-collapse:collapse;'>
                    <tbody>
                        <tr style='border-bottom:1px solid #222222;'>   
                            <td style='font-weight:bold;width:20%;padding:10px;'>Name:</td>
                            <td style='width:80%;padding:10px;'>".$this->name."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>   
                            <td style='font-weight:bold;padding:10px;'>Phone Number:</td>
                            <td style='padding:10px;'>".$this->phoneNumber."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>   
                            <td style='font-weight:bold;padding:10px;'>Email Address:</td>
                            <td style='padding:10px;'>".$this->email."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>   
                            <td style='font-weight:bold;padding:10px;'>Message:</td>
                            <td style='padding:10px;'>".$this->message."</td>
                        </tr>
                        ".$this->intakeAddendum."
                    </tbody>
                </table>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
                <p style='font-size:12px;'>This message was sent from the following page: </p>
                <p style='font-size:16px;font-weight:bold;'><a href='".$this->page."'>".$this->page."</a></p>

                <p style='font-size:12px;'>User's activity prior to submitting this contact form: </p>
                <ul>";
        for($i = 0; $i < count($this->historyArray); $i++) {
            $this->intakeMessageHtml .= "<li>".$this->historyArray[i]."</li>";
        }
        $this->intakeMessageHtml .= "</ul>
                <p style='font-size:12px;'>Is this email spam? <a href='https://www.bestattorney.com/template-files/contact-files/report-spam.php?isSpam=true&".$this->contactDataString."'>Report It!</a>
                <p><small>Note: Only use this button if it is actually spam. If it is a real person submitting a form with a case we don't take (towing, funeral home negligence, etc.) do not click this button.</small></p>
            </div>
        </html>";
    }

    public function prepDefaultAutoResponderMessage() {
        if (!isset($this->autoResponderAddendum)) {
            $this->errorOut("HTTP/1.0 400 Bad Request", "\$this->autoResponderAddendum is not set. Please call setAutoResponderAddendum() or use the ContactSubmissionDefault class instead.");
        }
        $emailBody = "Name: ".$this->name."\n
Homephone: ".$this->phone."\n
Email: ".$this->email."\n\n
Brief description of accident/injury:\n".$this->message."\n
".$this->autoResponderAddendum."\n\n";

        $this->autoResponderSubject = "(Bisnar Chase) Thanks for Contacting Us!";
        $this->autoResponderMessageText = "Dear ".$this->name.",\n\nYour inquiry has been received as copied below. FOR IMMEDIATE ASSISTANCE, call us at 949-203-3814.\nYou will receive a telephone call between 8:30 a.m. and 4:30 p.m. on business days. We are available from 8:30 a.m. to 8:30 p.m., seven days a week (excluding major holidays) for a new client inquiry. In emergencies, we are always available.\n\nThank you for your inquiry. We are here to assist you.\nBISNAR CHASE\nwww.bestattorney.com\n949-203-3814\n\n\n____________________  Information you provided: ______________________\n\n".$emailBody."\n_______________________________________________________________";
    }

    public function setIntake($subject, $message) {
        $this->intakeSubject = $subject;
        $this->intakeMessageHtml = $message;
    }

    public function setAutoResponder($subject, $message) {
        $this->autoResponderSubject = $subject;
        $this->autoResponderMessageText = $message;
    }
}