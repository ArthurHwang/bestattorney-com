<?php

include("./contact-submission-custom.php");

$contact = new ContactSubmissionCustom();

// Do Basic Setup of the contact object.
$contact->checkValidations();
$contact->checkForSpam();
$contact->setUpSendgrid();

// Fill in info from referral forms (inaccessible because they're protected. Should they be public? Who knoes.)

$firmName = $_POST['realname'];
$phone = $_POST['homephone'];
$email = $_POST['email'];
$message = $_POST['comment'];
$page = $_POST['noparamuri'];
$contactDataString = $contact->getContactDataString();

$intakeSubject = "BestAttorney.com Case Referral Request - From: $firmName";
$intakeMessage = "<html style='padding:20px;'>
	<div style='display:flex;'>
		<img src='https://https-webfiles-bisnarchaseperso.netdna-ssl.com/images/logo.png'>
	</div>
	<div style='width:100%;padding:40px;box-sizing:border-box;font-family:Verdana,sans-serif;color:#333;'>
		<h1 style='padding:10px;'>Case Referral:</h1>
		<div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
		<table style='border-collapse:collapse;'>
			<tbody>
				<tr style='border-bottom:1px solid #222222;'>   
					<td style='font-weight:bold;width:20%;padding:10px;'>Law Firm Name:</td>
					<td style='width:80%;padding:10px;'>$firmName</td>
				</tr>
				<tr style='border-bottom:1px solid #222222;'>   
					<td style='font-weight:bold;padding:10px;'>Phone Number:</td>
					<td style='padding:10px;'>$phone</td>
				</tr>
				<tr style='border-bottom:1px solid #222222;'>   
					<td style='font-weight:bold;padding:10px;'>Email Address:</td>
					<td style='padding:10px;'>$email</td>
				</tr>
				<tr style='border-bottom:1px solid #222222;'>   
					<td style='font-weight:bold;padding:10px;'>Description of Client and Case:</td>
					<td style='padding:10px;'>$message</td>
				</tr>
			</tbody>
		</table>
		<div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
		<p style='font-size:12px;'>This message was sent from the following page: </p>
		<p style='font-size:16px;font-weight:bold;'><a href='$page'>$page</a></p>

		<p style='font-size:12px;'>Is this email spam? <a href='https://www.bestattorney.com/template-files/contact-files/report-spam.php?isSpam=true&$contactDataString'>Report It!</a>
		<p><small>Note: Only use this button if it is actually spam. If it is a real person submitting a form with a case we don't take (towing, funeral home negligence, etc.) do not click this button.</small></p>
	</div>
</html>";

$emailBody = "Law Firm Name: $firmName \nPhone: $phone\nEmail: $email\n\nDescription of Case Referred:\n $message\n";
$autoResponderSubject = "(Bisnar Chase) Thanks for Contacting Us!";
$autoResponderMessage = "$firmName,\n\nYour referral inquiry has been received as copied below.\n\nYou will receive a telephone call between 8:30 a.m. and 4:30 p.m. on business days. \n\nThank you for thinking of Bisnar Chase with regards to your case referral.\nBISNAR CHASE\nwww.bestattorney.com\n949-203-3814\n\n\n____________________  Information you provided: ______________________\n\n".$emailBody."\n\n________________________________________________________________";


$contact->setIntake($intakeSubject, $intakeMessage);
$contact->sendIntakeEmail();

$contact->setAutoResponder($autoResponderSubject, $autoResponderMessage);
$contact->sendAutoResponseEmail();

if ($contact->inquirySent) {
	header("HTTP/1.0 200 OK");
	exit("Thank You, Your Referral Request Has Been Sent!");
}

