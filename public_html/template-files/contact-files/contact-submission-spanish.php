<?php

include "./contact-submission-default.php";

class ContactSubmissionSpanish extends ContactSubmissionDefault {

    protected $invalidEmailMessage = "Error: su correo electrónico no fue enviado";
    protected $autoResponderFailMessage;
    protected $autoResponderEmailFailMessage;
    protected $isSpamMessage = "Gracias.";
    protected $invalidPhoneEmailMessage = "Por favor introduce un correo electrónico o número de teléfono válido";
    protected $noNameMessage = "Por favor escriba su nombre!";
    protected $unauthorizedMessage = "Hubo un problema con el envío, por favor intente de nuevo.";

    public function setSpanishMessages() {
        $this->invalidEmailMessage = "Error: su correo electrónico no fue enviado";
        $this->isSpamMessage = "Gracias.";
        $this->invalidPhoneEmailMessage = "Por favor introduce un correo electrónico o número de teléfono válido";
        $this->noNameMessage = "Por favor escriba su nombre!";
        $this->unauthorizedMessage = "Hubo un problema con el envío, por favor intente de nuevo.";
        $this->autoResponderFailMessage = "La respuesta ".$this->email." no se pudo enviar. Pero hemos recibido su mensaje.";
        $this->autoResponderEmailFailMessage = "La respuesta ".$this->email." no se pudo enviar. Por favor intentalo mas tarde otra vez.";
    }

    public function prepareAutoResponseMessage() {
        $this->intakeSubject = "(Bisnar Chase) Gracias por contactar con nosotros!";
        $emailBody = "Nombre: ".$this->name."\nTeléfono: ".$this->phone."\nCorreo Electronico: ".$this->email."\n\nBreve descripción de Accidentes de Incidentes:\n ".$this->message."\n";
        $this->intakeMessageHtml = $this->name.",\n\nSu mensaje ha sido recibido como copiado a continuación.\n\nPara asistencia inmediata, llámenos al 949-203-3814.\n\nUsted recibirá una llamada telefónica entre las 8:30 am y las 4:30 pm en días laborables. Estamos disponibles de 8:30 am a 8:30 pm, los siete días a la semana (excluyendo los días festivos) para una nueva consulta del cliente. En situaciones de emergencia, siempre estamos disponibles.\n\nSi usted ha sido lesionado en un accidente de coche, por favor visite https://www.bestattorney.com/resources/injured-in-car-accident.html.\n\nSi necesita información sobre un defecto del producto, por favor visite https://www.bestattorney.com/defective-products/\n\nGracias.\nBISNAR CHASE\nwww.bestattorney.com\n949-203-3814\n\n\n____________________  Informacion que usted ha provisto: ______________________\n".$emailBody."______________________________________________________________________";
    }
    
    public function sendAutoResponseEmail() {
        $responseEmail = new SendGrid\Email();
        
        try {
		    $responseEmail->
		    setFrom( "inquiry@bestattorney.com" )->
		    addTo($this->email)->
		    setSubject($this->intakeSubject)->
		    setText($this->intakeMessageHtml);
		    
		    $response2 = $this->sendGrid->send($responseEmail);

		    if (!$response2) {
		        throw new Exception("No recibí la correspondencia");
		    } else if ($response2->message && $response2->message == "error") {
		        throw new Exception("Error Recibido: ".join(", ", $response2->errors));
		    } else {
		    	return true;
		    }
		} catch ( Exception $e ) {
            error_log("Error sending email from: responseEmail");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->inquirySent ? $this->autoResponderFailMessage : $this->autoResponderEmailFailMessage);
        }
    }
}