<?php 

class InvalidContactSubmission {

	function __construct($ip, $name, $phone, $email, $body, $page, $invalidReason, $referrer = "", $userAgent = "", $isSpam = false) {
		$page = $page ?? "Page not Found";
		$name = $name ?? "No name given";
		$isSpam = $isSpam == true ? 1 : 0;
		
		//Sets up the database link.
		try {
			$link = new PDO('mysql:dbname=bestatto_contacterr;host=localhost;', 'bestatto_ctcterr', 'BC1301dove');
			$link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$query = "INSERT INTO `emails` (`ipAddress`, `name`, `phone`, `email`, `body`, `page`, `referrer`, `user_agent`, `is_spam`, `invalid_reason`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			$storeSubmission = $link->prepare($query);

			if ($storeSubmission -> execute([$ip, $name, $phone, $email, $body, $page, $referrer, $userAgent, $isSpam, $invalidReason]) == false) {
				throw new Exception('Could not store invalid contact email in database.');
			}

		} catch(PDOException $e) {
			include($_SERVER['DOCUMENT_ROOT']."/../environment.php");
			if ($environment != "development") {
				throw new Exception($e);
			}
		}

	}

	private function prepareQuery($ip, $name, $phone, $email, $body, $page) {
		$query = "INSERT INTO `emails` (`ipAddress`, `name`, `phone`, `email`, `body`, `page`) VALUES (:ip, :name, :phone, :email, :body, :page);";
		$storeSubmission = $link->prepare($query);
		$storeSubmission -> bind(':ip', $ip);
		$storeSubmission -> bind(':name', $name);
		$storeSubmission -> bind(':phone', $phone);
		$storeSubmission -> bind(':email', $email);
		$storeSubmission -> bind(':body', $body);
		$storeSubmission -> bind(':page', $page);
	}

	private function sendQuery() {
		if ($storeSubmission -> execute() == false) {
			throw new Exception('Could not store invalid contact email in database.');
		}
	}
}
?>