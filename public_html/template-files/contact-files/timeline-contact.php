<?php

include("./contact-submission-custom.php");

$injuries = json_decode($_POST['injuryData']);
$injuryText = "";
foreach ($injuries as $key => $value) {
	switch ($key) {
		case "1":
			$injuryText .= "Hospitalization, ";
			break;
		case "2":
			$injuryText .= "Surgery, ";
			break;
		case "3":
			$injuryText .= "Physical Therapy, ";
			break;
		case "4":
			$injuryText .= "Medication, ";
			break;
		case "5":
			$injuryText .= "Specialist Visits";
			break;	
	}
}

$intakeHtml = "<tr style='border-bottom:1px solid #222222;'>   
				<td style='font-weight:bold;padding:10px;'>Summary of Injuries (From Timeline):</td>
				<td style='padding:10px;'>".$injuryText."</td>
			</tr>";
$autoResponderText = "Summary of Injuries (From Timeline): ".$injuryText;


$contact = new ContactSubmissionCustom();

$contact->checkValidations();
$contact->checkForSpam();
$contact->setUpSendgrid();


$contact->setIntakeAddendum($intakeHtml);
$contact->prepDefaultIntakeMessage("Timeline");
$contact->sendIntakeEmail();


$contact->setAutoResponderAddendum($autoResponderText);
$contact->prepDefaultAutoResponderMessage(); 
$contact->sendAutoResponseEmail();

if ($contact->inquirySent) {
	header("HTTP/1.0 200 OK");
	exit("Thank You, Your Referral Request Has Been Sent!");
}