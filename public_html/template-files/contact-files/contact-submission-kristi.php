<?php
class ContactSubmission {

    public $inquirySent = false;
    public $isSpam = false;

    protected $name;
    protected $phoneNumber;
    protected $email;
    protected $message;

    protected $honeypot;
    protected $validPhone;
    protected $validEmail;
    protected $contactDataString;
    protected $historyArray;
    protected $sendGrid;

    protected $intakeSubject;
    protected $intakeMessageHtml;
    protected $autoResponderSubject;
    protected $autoResponderMessageText;

    protected $ip;
    protected $page;
    protected $referrer;
    protected $userAgent;

    protected $invalidEmailMessage = "Error: Your email was unable to be sent.";
    protected $autoResponderFailMessage;
    protected $autoResponderEmailFailMessage;
    protected $isSpamMessage = "Thank You, We got your info.";
    protected $invalidPhoneEmailMessage = "Please provide a valid email or phone number.";
    protected $noNameMessage = "Please Provide a valid name";
    protected $unauthorizedMessage = "Error: You are not authorized to access this service.";

    public function __construct() {
        if ($_SERVER["REQUEST_METHOD"] != "POST") $this->errorOut("HTTP/1.0 403 Forbidden", $this->unauthorizedMessage);
        $this->setAllVariables();
    }

    public function sendIntakeEmail() {
        $intakeEmail = new SendGrid\Email();

        try {
		    $intakeEmail->
		    setFrom( "kfeathers@bisnarchase.com" )->
		    setReplyTo($this->email)->
		    setFromName($this->name)->
		    addTo( "kfeathers@bisnarchase.com" )->
            setSubject($this->intakeSubject)->
            addHeader('Content-Type', 'text/html')->
		    setHtml($this->intakeMessageHtml);

		    $response = $this->sendGrid->send($intakeEmail);

            if (!$response) {
                throw new Exception("Did not receive response.");
            } else if ($response->message && $response->message == "error") {
                throw new Exception("Received error: ".join(", ", $response->errors));
            } else {
                $this->inquirySent = true;
                return true;
            }
		} catch ( Exception $e ) {
            error_log("Error sending email from: sendIntakeEmail");
            $this->invalidEmail("Error Sending Email from sendIntakeEmail(): $e");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->invalidEmailMessage);
        }
    }

    public function sendAutoResponseEmail() {
        $responseEmail = new SendGrid\Email();

        try {
		    $responseEmail->
		    setFrom( "kfeathers@bisnarchase.com" )->
		    addTo($this->email)->
		    setSubject($this->autoResponderSubject)->
		    setText($this->autoResponderMessageText);

		    $response2 = $this->sendGrid->send($responseEmail);

		    if (!$response2) {
		        throw new Exception("Did not receive response.");
		    } else if ($response2->message && $response2->message == "error") {
		        throw new Exception("Received error: ".join(", ", $response2->errors));
		    } else {
		    	return true;
		    }
		} catch ( Exception $e ) {
            error_log("Error sending email from: responseEmail");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->inquirySent ? $this->autoResponderFailMessage : $this->autoResponderEmailFailMessage);
        }

    }

    public function checkValidations() {
        if ($this->honeypot != "") {
            $this->invalidEmail("Honeypot form field was full");
            error_log("=============");
            error_log("Honeypot should be empty but is: ".$this->honeypot);
            error_log("=============");
            $this->errorOut("HTTP/1.0 200 OK", $this->isSpamMessage);
        }

        if (!$this->validEmail && !$this->validPhone) {
            $this->invalidEmail("This submission has an invalid phone and email");
            error_log("=============");
            error_log("Valid Phone or email not set: ");
            error_log("=============");
            $this->errorOut("HTTP/1.0 400 Bad Request", $this->invalidPhoneEmailMessage);
        }

        if ($this->name == "") {
            $this->invalidEmail("This submission contained no name");
            error_log("=============");
            error_log("Name not set: ");
            error_log("=============");
            $this->errorOut("HTTP/1.0 400 Bad Request", $this->noNameMessage);
        }
    }

    public function checkForSpam() {
        require_once $_SERVER['DOCUMENT_ROOT'].'/template-files/contact-files/contact-spam-checker.php';
        $spamCheck = new ContactSpamChecker($this->ip, $this->userAgent, $this->referrer, $this->page, $this->name,  $this->email, $this->message);
        $this->contactDataString = $spamCheck->urlDataString;

        $this->isSpam = $spamCheck->checkSpam();
        if ($this->isSpam === 'true') {
            error_log("=============");
            error_log("This is spam!");
            error_log("=============");
            $this->invalidEmail("This submission was marked as spam");
            $this->errorOut("HTTP/1.0 200 OK", $this->isSpamMessage);
        }
    }

    public function getContactDataString() {
        return $this->contactDataString;
    }

    public function setUpSendgrid() {
        require_once $_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php';
        require_once $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';
        include_once($_SERVER['DOCUMENT_ROOT']."/template-files/keycheck.inc.php");
        $this->historyArray = $historyArray;
        $this->sendGrid = new SendGrid($sg_username, $sg_password);
    }

    protected function setAllVariables() {
        $this->name = strip_tags(trim($_POST['realname']));
        $this->phoneNumber = strip_tags(trim($_POST['homephone']));
        $this->email = filter_var(strip_tags(trim($_POST['email'])), FILTER_SANITIZE_EMAIL);
        $this->message =  trim($_POST['comment']);

        $this->honeypot = $_POST['form-hp'];
        $this->validPhone = $this->validatePhone();
        $this->validEmail = $this->validateEmail() ? true : false; // Theres a reason for this i swear.

        $this->ip = isset($_SERVER['HTTP_X_SUCURI_CLIENTIP']) ? $_SERVER['HTTP_X_SUCURI_CLIENTIP'] : $_SERVER['REMOTE_ADDR'];
        $this->page = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_POST['noparamuri'];
        $this->referrer = $_SERVER['HTTP_REFERER'];
        $this->userAgent = $_SERVER['HTTP_USER_AGENT'];

        $this->autoResponderFailMessage = "Error: An Autoresponder email to: ".$this->email." was unable to be sent but we have recived your contact form message.";
        $this->autoResponderEmailFailMessage = "Error: An Autoresponder email to: ".$this->email." was unable to be sent. Please try again.";
    }

    protected function validatePhone() {
        $phonevalidate = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
        return (preg_match($phonevalidate, $this->phoneNumber) == 1) ? true : false;
    }

    protected function validateEmail() {
        return filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }

    protected function invalidEmail($invalidReason = "(error: No reason given for invalid email)") {
        require_once $_SERVER['DOCUMENT_ROOT']."/template-files/contact-files/invalid-contact-submission.php";
        try {
            new InvalidContactSubmission($this->ip, $this->name, $this->phoneNumber, $this->email, $this->message, $this->page, $invalidReason, $this->referrer, $this->userAgent, $this->isSpam);
        } catch(Exception $e) {
            error_log("Error Instantiating an InvalidContactSubmission object in /contact-submission.php on line 58: $e");
            $this->errorOut("HTTP/1.0 500 Internal Server Error", $this->invalidEmailMessage);
        }
    }

    protected function errorOut($headerInfo, $message) {
        error_log("ContactSubmission->errorOut() called: $message");
        header($headerInfo);
        exit($message);
    }
}
