<?php

	require($_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php');
	require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';


	/* CREATE THE SENDGRID MAIL OBJECT
	====================================================*/
	$sendgrid = new SendGrid( $sg_username, $sg_password );
	$mail = new SendGrid\Email();
	

	// Only process POST reqeusts.
    if (($_SERVER["REQUEST_METHOD"] == "POST") && ($_POST['no-fill'] == "")) {
        // Get the form fields and remove whitespace.
        $unhelpful = $_POST['unhelpful'];
        $recommendations = trim($_POST['recommendations']);
		$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
		$page = $_POST['page'];
		// Check that data was sent to the mailer.
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            //header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request"); 
            header("HTTP/1.0 400 Bad Request");
            echo "Oops! If you want us to email you back, please provide a valid email!";
            exit;
    	}

    	if (!$_POST['unhelpful'] && !$_POST['recommendations']) {
    		header("HTTP/1.0 400 Bad Request");
			echo 'Please select a reason for the page being unhelpful or write a question in the text box.';
			exit;
    	} 

		function clean_string($string) {
			$bad = array("content-type","bcc:","to:","cc:","href");
			return str_replace($bad,"",$string);
		} 
	
		
		$email_message = "A new website recommendation and/or question has been submitted from BestAttorney.com.\n\n";
		$email_message .= "This page (". $page .") was not helpful because: ".clean_string($unhelpful)."\n\n";
		$email_message .= "Other Comments from the User: ".clean_string($recommendations)."\n\n";
		
		if (empty($email)) {
			$email = "marketing@bestattorney.com";
			$email_message .= "The user did not submit an email address. \n\n";
		} else {
			$email_message .= "This Form Was Submitted by: ".clean_string($email)."\n\n";
		}

		try {
		    $mail->
		    setFrom($email)->
		    setReplyTo($email)->
		    setFromName($email)->
		    addTo( "marketing@bestattorney.com" )->
		    setSubject("Website Recommendation/Question - Form Submission")->
		    setText($email_message);
		    
		    $response = $sendgrid->send($mail);

		    if (!$response) {
		        throw new Exception("Did not receive response.");
		    } else if ($response->message && $response->message == "error") {
		        throw new Exception("Received error: ".join(", ", $response->errors));
		    } else {
		    	header("HTTP/1.0 200 OK");
            	exit("Thank you for giving us your recommendation! We will respond to any questions you had in a timely manner.");
		    }

		} catch ( Exception $e ) {
			var_export($e);
			exit;
		}

		/*$headers = 'From: '.$email."\r\n".
		'Reply-To: '.$email."\r\n" . 
		'X-Mailer: PHP/' . phpversion(); */

		/* @mail($email_to, $email_subject, $email_message, $headers); 
		echo 'Thank you for giving us your recommendations! We will respond to any questions you had in a timely manner.'; */

		// Send the email.
        /*if (mail($email_to, $email_subject, $email_message, $headers)) {
            // Set a 200 (okay) response code.
            header("HTTP/1.0 200 OK");
            echo "Thank you for giving us your recommendations! We will respond to any questions you had in a timely manner.";
            exit;
        } else {
            // Set a 500 (internal server error) response code.
            header("HTTP/1.0 500 Internal Server Error");
            echo "Oops! Something went wrong and we couldn't send your message.";
        }*/

	} else {
        // Not a POST request, set a 403 (forbidden) response code.
        header("HTTP/1.0 403 Forbidden");
        echo "There was a problem with your submission, please try again.";
    }

?>
