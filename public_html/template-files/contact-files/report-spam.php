<?php

$user_ip = $_GET['user_ip'];
$user_agent = $_GET['user_agent'];
$referrer = $_GET['referrer'];
$permalink = $_GET['permalink'];
$comment_author = $_GET['comment_author'];
$comment_author_email = $_GET['comment_author_email'];
$comment_content = $_GET['comment_content'];

$isSpam = $_GET['isSpam'];


include('./contact-spam-checker.php');

$spam = new ContactSpamChecker($user_ip, $user_agent, $referrer, $permalink, $comment_author, $comment_author_email, $comment_content);

if (!isset($isSpam)) {
    echo "This URL is incorrect. Please contact your webmaster to properly submit spam for review.";
}
if ($isSpam === "true") {
    $response = $spam->reportSpam();
    error_log("========== Someone has manually reported an email as spam =============");
} else if ($isSpam === "false") {
    $response = $spam->reportHam();
    error_log("========== Someone has manually reported an email as ham =============");
}
echo $response ? ($isSpam === "true" ? "Spam" : "Ham")." Reported Successfully" : ($isSpam === "true" ? "Spam" : "Ham")." was not reported successfully. Check error_log to see what went wrong."; 

?>

