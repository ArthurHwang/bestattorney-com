<?php

include "./contact-submission-kristi.php";

/**
 * This class is used to submit the default contact forms. Most contact forms on this site will initialize this class.
 */
class ContactSubmissionClassAction extends ContactSubmission {

    public function prepareIntakeMessage() {
        $this->intakeSubject = "BestAttorney.com Class Action Request - From:".$this->name;
        $this->intakeMessageHtml = "<html style='padding:20px;'>
        	<div style='display:flex;'>
            	<img src='https://https-webfiles-bisnarchaseperso.netdna-ssl.com/images/logo.png'>
            </div>
            <div style='width:100%;padding:40px;box-sizing:border-box;font-family:Verdana,sans-serif;color:#333;'>
                <h1 style='padding:10px;'>Class Action Form Submission:</h1>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
                <table style='border-collapse:collapse;'>
                    <tbody>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;width:20%;padding:10px;'>Name:</td>
                            <td style='width:80%;padding:10px;'>".$this->name."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Phone Number:</td>
                            <td style='padding:10px;'>".$this->phoneNumber."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Email Address:</td>
                            <td style='padding:10px;'>".$this->email."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>IP Address:</td>
                            <td style='padding:10px;'>".$this->ip."</td>
                        </tr>
                        <tr style='border-bottom:1px solid #222222;'>
                            <td style='font-weight:bold;padding:10px;'>Message:</td>
                            <td style='padding:10px 50px 10px 10px;'>".$this->message."</td>
                        </tr>
                    </tbody>
                </table>
                <div style='background-color:#333;line-height:2px;font-size:2px;'><span>&nbsp;</span></div>
                <p style='font-size:12px;'>This message was sent from the following page: </p>
                <p style='font-size:16px;font-weight:bold;'><a href='https://www.bestattorney.com/employment-law/employment-law-cases.html'></a></a></p>

                <p style='font-size:12px;'>User's activity prior to submitting this contact form: </p>
                <ul>";
        for($i = 0; $i < count($this->historyArray); $i++) {
            $this->intakeMessageHtml .= "<li>".$this->historyArray[i]."</li>";
        }
        $this->intakeMessageHtml .= "</ul>
                <p style='font-size:12px;'>Is this email spam? <a href='https://www.bestattorney.com/template-files/contact-files/report-spam.php?isSpam=true&".$this->contactDataString."'>Report It!</a>
                <p><small>Note: Only use this button if it is actually spam. If it is a real person submitting a form with a case we don't take (towing, funeral home negligence, etc.) do not click this button.</small></p>
            </div>
        </html>";
    }

    public function prepareAutoResponseMessage() {
        $emailBody = "Name: ".$this->name."\nHomephone: ".$this->phone."\nEmail: ".$this->email."\nIP Address: ".$this->ip."\n\nBrief description of accident/injury:\n ".$this->message."\n";

        $this->autoResponderSubject = "(Bisnar Chase) Thanks for Contacting Us!";
        $this->autoResponderMessageText = "Dear ".$this->name.",\n\nYour inquiry has been received as copied below. FOR IMMEDIATE ASSISTANCE, call us at 949-203-3814.\nYou will receive a telephone call between 8:30 a.m. and 4:30 p.m. on business days. We are available from 8:30 a.m. to 8:30 p.m., seven days a week (excluding major holidays) for a new client inquiry. In emergencies, we are always available.\n\nThank you for your inquiry. We are here to assist you.\nBISNAR CHASE\nwww.bestattorney.com\n949-203-3814\n\n\n____________________  Information you provided: ______________________\n\n".$emailBody."_______________________________________________________________";
    }

}