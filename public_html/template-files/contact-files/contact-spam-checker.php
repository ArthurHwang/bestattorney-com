<?php

class ContactSpamChecker {
    public $commentIsSpam;
    public $urlDataString;

    const BLOG = "https%3A%2F%2Fwww.bestattorney.com%2F";
    const COMMENT_TYPE = "contact-form";
    const BLOG_LANG = "en";

    private $ip;
    private $user_agent;
    private $referrer;
    private $permalink;
    private $comment_author;
    private $comment_author_email;
    private $comment_content;

    private $dataString;


    public function __construct($ip, $user_agent, $referrer, $permalink, $comment_author, $comment_author_email, $comment_content) {

        $this->ip = $ip;
        $this->user_agent = $user_agent;
        $this->referrer = $referrer;
        $this->permalink = $permalink;
        $this->comment_author = $comment_author;
        $this->comment_author_email = $comment_author_email;
        $this->comment_content = $comment_content;

        $this->calculateDataString();
    }

    public function checkSpam() {
        $url = "https://3c3c2020c32e.rest.akismet.com/1.1/comment-check";
        $response = $this->makeCurlRequest($url);

        $this->commentIsSpam = $response;
        return ($response);
    }

    public function reportSpam() {
        // Send this to akismet and tell them that it's Spam, use it to refine the algorithm.
        $url = "https://3c3c2020c32e.rest.akismet.com/1.1/submit-spam";
        return $this->makeCurlRequest($url);
    }

    public function reportHam() {
        // Send this to akismet and tell them that this was marked as spam, but it's actually not.
        $url = "https://3c3c2020c32e.rest.akismet.com/1.1/submit-ham";
        return $this->makeCurlRequest($url);
    }

    private function calculateDataString() {
        $a = "blog=".self::BLOG;
        $a .= "&user_ip=".$this->ip;
        $a .= "&user_agent=".$this->user_agent;
        $a .= "&referrer=".$this->referrer;
        $a .= "&permalink=".$this->permalink;
        $a .= "&comment_type=".self::COMMENT_TYPE;
        $a .= "&comment_author=".$this->comment_author;
        $a .= "&comment_author_email=".$this->comment_author_email;
        $a .= "&comment_content=".$this->comment_content;
        $a .= "&blog_lang=".self::BLOG_LANG;

        // error_log("Test DataString: $a");

        $this->dataString = $a;
        $this->urlDataString = urlencode($a);
    }

    private function makeCurlRequest($url) {
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->dataString);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($curl, CURLOPT_HEADERFUNCTION, "checkHeaders");

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;

        // function checkHeaders($curl, $header_line) {
        //     error_log("returning headers: ".$header_line);
        // }
    }
}
