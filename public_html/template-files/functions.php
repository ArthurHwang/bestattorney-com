<?php
 // PHP file to be included in the top of the header pages so any function can be used on any page.

/**
Debug
*/

//Normal Debug Function. Takes a Variable and a string of a variable Name.
if (!function_exists('debug')) {
  function debug($variable, $variableName = "thisVariable")
  {
    echo ("<script>console.log('");
    echo ("$" . $variableName . " Is Equal To: " . $variable . "');");
    echo ("</script>");
  }
}

/**
Auto_Versioning (to allow newly changed versions to get past browser cache)
*/

if (!function_exists('auto_version')) {
  function auto_version($file)
  {
    if (strpos($file, "/") !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file)) {
      return $file;
    }

    $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
    return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
  }
}

/**
Reviews
*/

// Function to add reviews on any sidebar.

// Logic: If the second parameter is given an array, use the ids in the array to populate the reviews section - as many reviews as there are IDs in the array.
// If the third parameter is filled out with an array, populate $chosenReviews with all of the reviews that match those specific categories. If there are more reviews in $chosenArray, figure out
// how many more there are than the number of results we want, and remove them randomly.
// If no arrays are provided as parameters, just put in the number of reviews you want as the only argument and that number will appear randomly.
if (!function_exists('getReviews')) { // This is a bandaid for the errors in the error_log. We'll see if this fixes it but in the future we should make sure functions is only included once
  function getReviews($numberOfResults, $reviewIds = array(), $categories = array())
  {

    include($_SERVER['DOCUMENT_ROOT'] . "/reviews/reviews-database.php");

    //The array that we're going to store the chosen reviews in so we can print them out.

    $chosenReviews = array();

    if (count($reviewIds) != 0) {

      foreach ($reviews as $review) {
        if (in_array($review['id'], $reviewIds)) {
          array_push($chosenReviews, $review);
        }
      }
    } else if (count($categories) != 0) {



      foreach ($reviews as $review) {

        foreach ($review['caseType'] as $caseType) {
          if (in_array($caseType, $categories)) {
            array_push($chosenReviews, $review);
            break;
          }
        }
      }

      //If We are asking for fewer results than are in the chosen reviews array, we determine how many excess reviews we have in the $chosenReviews array.

      //For every review in excess, we choose a random review and unset it from the array.

      if ($numberOfResults < count($chosenReviews)) {

        $excess = count($chosenReviews) - $numberOfResults;

        for ($i = $excess; $i > 0; $i--) {

          unset($chosenReviews[rand(0, count($chosenReviews) - 1)]);

          $chosenReviews = array_values($chosenReviews);
        }
      }
    } else if ($numberOfResults > 0) {

      // Get a number of random results, store it in an array

      $randomIds = array_rand($reviews, $numberOfResults);

      // Call this function on itself with $randomIds as the array.

      getReviews(0, $randomIds);

      return;
    } else {

      echo ("Please Provide Arguments for this function! $numberOfResults (int), $reviewIds (array), $categories (array)");

      return;
    }



    $rating = 0;

    $ratingLimit = 0;



    $reviewsHtml = "";

    $reviewsHtml .= '<div id="sb-reviews">
		<p class="title">Client Reviews of Bisnar Chase</p>';



    /**
			THIS IS THE OLD REVIEWS FOR EACH ITEM.
		*/
    /*foreach($chosenReviews as $review) {


			$rating += $review['stars'];

			$ratingLimit += 5;



			$excerpts = $review['reviewExcerpts'];

			$reviewsHtml .= "<div typeof='schema:Review'>";

			$reviewsHtml .= '<div class="schema_review_name" property="schema:itemReviewed" typeof="schema:Thing">
								<meta property="schema:name" value="Bisnar Chase Personal Injury Attorneys">
								<meta property="schema:url" value="https://www.google.com/search?q=bisnar+chase+personal+injury+attorneys">
							</div>';

			$reviewsHtml .= "<p class=\"quote\">“".$excerpts[rand(0, count($excerpts)-1)]."” <a tabindex='0' role='button' data-toggle='popover-review' data-trigger='focus' data-placement='top' data-html='true' title='' data-content='";

			//$reviewsHtml .= addslashes($review['fullReview']);

			$reviewsHtml .= $review['fullReview'];

			$reviewsHtml .= "' data-original-title='Full Review: ".$review['reviewer']."'>(full review)</a></p>";

			$reviewsHtml .= "<meta property='schema:reviewBody' value='".$review['fullReview']."'>";

			$reviewsHtml .= "<img src=\"".$review['thumbnailPath']."\" class=\"imgcircle\">";

			$reviewsHtml .= "<p class=\"author\" property='schema:author' typeof='schema:Person'>by <span property='schema:name'>".$review['reviewer']."</span></p>";

			$reviewsHtml .= "<p class=\"reviewed\">reviewed at ";



			switch($review['reviewedAt']) {

				case "Google+":

					$reviewsHtml .= "<a href='https://plus.google.com/+Bestattorney/' target='_blank'>Google+</a></p>";

					break;

				case "Yelp":

					$reviewsHtml .= "<a href='http://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-newport-beach-4' target='_blank'>Yelp</a></p>";

					break;

				case "Yahoo!":

					$reviewsHtml .= "<a href='https://local.yahoo.com/info-45552418-bisnar-chase-personal-injury-attorneys-newport-beach' target='_blank'>Yahoo!</a></p>";

					break;

				case "City Search":

					$reviewsHtml .= "<a href='http://www.citysearch.com/profile/607714/newport_beach_ca/bisnar_chase_personal_injury.html' target='_blank'>City Search</a></p>";

					break;

				case "Super Pages":

					$reviewsHtml .= "<a href='http://www.superpages.com/bp/Newport-Beach-CA/Bisnar-Chase-Personal-Injury-Attorneys-L2050662459.htm' target='_blank'>Super Pages</a></p>";

					break;

				case "Facebook":

					$reviewsHtml .= "<a href='https://www.facebook.com/california.attorney/' target='_blank'>Facebook</a></p>";

					break;

				default:

					$reviewsHtml .= "an online directory</p>";

					break;

			}*/

    /**
					THIS IS THE NEW REVIEWS FOR EACH ITEM - SHOWS ONLY ONE REVIEW AND IT SHOWS THE FULL REVIEW WITHOUT NEEDING THE POPOVER.
				*/

    foreach ($chosenReviews as $review) {

      $rating += $review['stars'];

      $ratingLimit += 5;



      $excerpts = $review['reviewExcerpts'];

      $reviewsHtml .= "<div class='single-review' >";

      // $reviewsHtml .= "<meta itemprop='itemReviewed'";

      $reviewsHtml .= "<p class=\"quote\">“" . $review['fullReview'] . "”</p>";

      // $reviewsHtml .= "<img src=\"".$review['thumbnailPath']."\" class=\"imgcircle\">";

      $reviewsHtml .= "<img alt='Client Ratings & Reviews' data-src='" . $review['thumbnailPath'] . "' class='imgcircle'>";

      $reviewsHtml .= "<p class=\"author\">by " . $review['reviewer'] . "</p>";

      $reviewsHtml .= "<p class=\"reviewed\">reviewed at ";



      switch ($review['reviewedAt']) {

        case "Google":

          $reviewsHtml .= "<a href='https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,' target='_blank' >Google</a>";

          break;

        case "Yelp":

          $reviewsHtml .= "<a href='http://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-newport-beach-4' target='_blank' >Yelp</a>";

          break;

        case "Yahoo!":

          $reviewsHtml .= "<a href='https://local.yahoo.com/info-45552418-bisnar-chase-personal-injury-attorneys-newport-beach' target='_blank' >Yahoo!</a>";

          break;

        case "City Search":

          $reviewsHtml .= "<a href='http://www.citysearch.com/profile/607714/newport_beach_ca/bisnar_chase_personal_injury.html' target='_blank' >City Search</a>";

          break;

        case "Super Pages":

          $reviewsHtml .= "<a href='http://www.superpages.com/bp/Newport-Beach-CA/Bisnar-Chase-Personal-Injury-Attorneys-L2050662459.htm' target='_blank' >Super Pages</a>";

          break;

        case "Facebook":

          $reviewsHtml .= "<a href='https://www.facebook.com/california.attorney/' target='_blank'>Facebook</a>";

          break;

        default:

          $reviewsHtml .= "an online directory";

          break;
      }

      if ($review['dateReviewed']) {
        $reviewsHtml .= " on " . $review['dateReviewed'][1];
      }

      $reviewsHtml .= "</p><div class='imgreviewstars'>";

      for ($i = 1; $i <= $review['stars']; $i++) {

        $reviewsHtml .= "<i class='review-star fa fa-star'></i>";
      }
      // $reviewsHtml .= "<meta itemprop='worstRating' content='1' />";
      $reviewsHtml .= "  (<span>" . $review['stars'] . "</span>/<span>5</span>)";

      $reviewsHtml .= "</div></div><div class='review-divider'></div>";
    }



    $reviewsHtml .= '</div>';

    //For use in adding the markup afterwards. This method did not trigger review star rich snippets in SERPS
    $reviewsHtml .= '<script type="application/ld+json">

		{
	  	"@context": "http://schema.org",
	  	"@type": "Attorney",
		"name": "Bisnar Chase Personal Injury Attorneys",
    "image": "https://www.bestattorney.com/images/text-header-images/Bisnar-Chase-Staff-Photo-2017.jpg",
    "address": "1301 Dove St, #120 Newport Beach, CA 92660",
    "telephone": "(949) 203-3814",
    "priceRange": "Free Initial Consultation",
	    "aggregateRating": {
	    "@type": "AggregateRating",
	    "ratingValue": "' . ($rating / count($chosenReviews)) . '",
	    "reviewCount": "' . count($chosenReviews) . '"
	 	 },
	    "review": [';

    $i = 1;

    foreach ($chosenReviews as $review) {
      $reviewsHtml .= '{
				"@type": "Review",
				"author": "' . $review['reviewer'] . '",
				"description": "' . addslashes($review['fullReview']) . '",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "' . $review['stars'] . '",
					"worstRating": "1"
				}
		    }';

      if ($i != count($chosenReviews)) {
        $reviewsHtml .= ',';
      }

      $i++;
    }

    $reviewsHtml .= '
		  ]
		}
		</script>';

    echo $reviewsHtml;
  }
}

/**
Case Results
*/

//
//Function to customize the case results widget on the sidebar
//

//This is called by usort later in getCaseResults().
if (!function_exists('compareByName')) {
  function compareByName($a, $b)
  {
    return $b["awardint"] - $a["awardint"];
  }
}
/*

Parameters I need to include here:

$type = 'location' || 'category' || 'search' || 'top' || specific IDs (eg. array(4,6,1,51,42,32)
$resultString = '$linkshow' || '$linkshow' || '$searchString' || ''

'category' can only be: "auto", "motorcycle", "auto-defect", "premises", "pedestrian", "other"

*/

//Array to store all of the stored case IDs, so that getCaseResults() can check it and not add duplicates.
$resultIds = array();

if (!function_exists('printCaseResults')) {
  function printCaseResults($type = "top", $string = "", $additional = "")
  {

    //Declares this global so it can be used in getCaseResults() and then resets the array.
    global $resultIds;
    $resultIds = array();



    $titleCaseString = ucwords(str_replace("-", " ", $string));
    //Quick bandaid to fix the issue if $titleCaseString is empty, then there will be an extra space in "Top  Case Results"
    if ($titleCaseString != "") {
      $titleCaseString .= " ";
    }
    if ($type == "search") {
      $titleCaseString = "";
    }

    $titleCaseString2 = ucwords(str_replace("-", " ", $additional[1]));
    if ($titleCaseString2 != "") {
      $titleCaseString2 .= " ";
    }
    if ($additional[0] == "search") {
      $titleCaseString2 = "";
    }

    $results1 = getCaseResults($type, $string);

    if (count($resultIds) < 10 && is_array($additional)) {
      $results2 = getCaseResults($additional[0], $additional[1]);
    }

    if (count($resultIds) < 10) {
      $results3 = getCaseResults('top');
    }

    $resultsHtml = "";
    $resultsHtml .= '
					<div id="results-in-bg">
					    <div id="results" class="geo-results" data-src="/images/top-case-results.jpg" style="margin-right: 30px;" >
					        <a href="https://www.bestattorney.com/case-results/" id="all-results" title="Top Case Results">Top ' . $titleCaseString . 'Case Results</a>
					        <div id="results-wrap" data-jcarousel="true" data-jcarouselautoscroll="true">
								<ul id="results-wrap-ul" style="left: 0px; top: 0;">';

    $resultIndex = 1;
    // $results1
    foreach ($results1 as $case) {
      if ($case['id'] == 1) {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/attorneys/brian-chase.html"';
      } else if ($case['id'] == 58 || $case['id'] == 59) {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/case-results/"';
      } else if ($case['id]'] == 56) {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/attorneys/brian-chase.html"';
      } else {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/case-results/?id=' . $case['id'] . '&amp;expand=true"';
      }


      if ($type == 'location') {
        $resultsHtml .= 'class="location-description"><span>' . $case['award'] . '</span><br>' . $case['descriptor'] . ': <span class="result-highlight" style="font-size: 12px;color: #B97F35">' . $case['chosenLocation'] . '</span></a></li>';
      } else {
        $resultsHtml .= '><span>' . $case['award'] . '</span><br>' . $case['type'] . '</a></li>';
      }

      $resultIndex++;
    }

    // $results2
    if ($resultIndex < 10 && is_array($results2)) {
      if ($additional[1] != "search") {
        /*$resultsHtml .= '<li class="results-divider">
								<div class="results-divider-text">Top '.$titleCaseString2.'Results:</div>
								<div class="results-divider-line"></div></li>';*/ }

      foreach ($results2 as $case) {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/case-results/?id=' . $case['id'] . '&amp;expand=true"';

        if ($additional[0] == 'location') {
          $resultsHtml .= 'class="location-description"><span>' . $case['award'] . '</span><br>' . $case['descriptor'] . ': <span class="result-highlight" style="font-size: 12px;color: #B97F35">' . $case['chosenLocation'] . '</span></a></li>';
        } else {
          $resultsHtml .= '><span>' . $case['award'] . '</span><br>' . $case['type'] . '</a></li>';
        }

        $resultIndex++;
      }
    }

    // $results3
    if ($resultIndex < 10) {
      $resultsHtml .= '<li class="results-divider">
								<div class="results-divider-text">Other Top Results:</div>
								<div class="results-divider-line"></div></li>';
      foreach ($results3 as $case) {
        $resultsHtml .= '<li id="result-' . $resultIndex . '"><div class="result-index">' . $resultIndex . '</div><a href="https://www.bestattorney.com/case-results/?id=' . $case['id'] . '&amp;expand=true"><span>' . $case['award'] . '</span><br>' . $case['type'] . '</a></li>';
        $resultIndex++;
      }
    }

    $resultsHtml .= '</ul>
					</div>
			        <a id="results-prev" class="mycarousel-prev2 results-carousel-button" data-jcarouselcontrol="true"><span class="small-up-arrow-blue"></span></a>
			        <a id="results-next" class="mycarousel-next2 results-carousel-button" data-jcarouselcontrol="true"><span class="small-down-arrow-blue"></span></a>
			        <div class="clear"></div>
			        <a href="https://www.bestattorney.com/case-results/" id="all-case-results" title="All Case Results">ALL CASE RESULTS&nbsp;<span class="small-right-arrow-orange"></span></a>
			    </div>
			    <div class="clear"></div>
			</div>';

    echo $resultsHtml;
  }
}

if (!function_exists('getCaseResults')) {
  function getCaseResults($type, $string = "")
  {

    global $resultIds;
    $resultsArray = array();

    // First, Get all the cases in the database. The resulting variable is $caseresults
    include($_SERVER['DOCUMENT_ROOT'] . "/case-results/case-database.php");
    //Sets up the caseresults to be ordered by reward amount.
    usort($caseresults, 'compareByName');

    switch ($type) {

      case "location":

        //Gets the city from string, removes the hyphen and capitalizes each first letter.
        $titleCaseString = ucwords(str_replace("-", " ", $string));

        foreach ($caseresults as $case) {

          if (count($resultIds) >= 10) {
            break;
          }

          foreach ($case['relatedLocations'] as $description => $location) {
            if (is_array($location)) {
              foreach ($location as $multidescription => $multiLocation) {
                if ($multiLocation == $titleCaseString && !in_array($case['id'], $resultIds)) {
 if ($case['awardint'] > 650000) {         $case['descriptor'] = $multidescription;
                  $case['chosenLocation'] = $location;
                  array_push($resultsArray, $case);
                  array_push($resultIds, $case['id']);
                  break 2;}
         
                }
              }
            } else {
              if ($location == $titleCaseString && !in_array($case['id'], $resultIds)) {
if ($case['awardint'] > 650000) {       $case['descriptor'] = $description;
                $case['chosenLocation'] = $location;
                array_push($resultsArray, $case);
                array_push($resultIds, $case['id']);
                break;}
           
              }
            }
          }
        }
        break;

      case "category":

        foreach ($caseresults as $case) {

          if (count($resultIds) >= 10) {
            break;
          }

          foreach ($case['category'] as $category) {

        
            if ($category == $string && !in_array($case['id'], $resultIds)) {


                  if ($case['awardint'] > 650000) {
                   array_push($resultsArray, $case);
              array_push($resultIds, $case['id']);
              break;
            }

         
            }
          }
        }
        break;

      case "search":
        //return "Search Function does not work at this time.";
        $queryArray = explode(" ", $string);
        $regexQuery = "/(";

        $queryIndex = 0;
        foreach ($queryArray as $query) {
          if ($queryIndex == 0) {
            $regexQuery .= " " . $query . " ";
          } else {
            $regexQuery .= "| " . $query . " ";
          }
          $queryIndex++;
        }
        $regexQuery .= ")/";

        foreach ($caseresults as $case) {

          if (count($resultIds) >= 10) {
            break;
          }

          $foundValues = preg_grep($regexQuery, $case);
          if (is_array($foundValues) && count($foundValues) > 0 && !in_array($case['id'], $resultIds)) {

               if ($case['awardint'] > 650000) {          
                 array_push($resultsArray, $case);
            array_push($resultIds, $case['id']);}
 
          }
        }

        break;
      case "top":
        foreach ($caseresults as $case) {

          if (count($resultIds) >= 10) {
            break;
          }

          if (!in_array($case['id'], $resultIds)) {

               if ($case['awardint'] > 650000) {                   array_push($resultsArray, $case);
            array_push($resultIds, $case['id']); }

    
 }
      
        }
        break;

      default:
        echo ("<br>printCaseResults() was given an invalid argument<br>");
        break;
    }

    return $resultsArray;
  }
}

if (!function_exists('subfooterLinks')) {
  function subfooterLinks($links, $noparamuri, $sidebarlinks)
  { // Takes the array of each of the location sidebar links and prints them out individually as list items, only if the listed link does not go to the current page.

    foreach ($links as $key => $value) {
      if ($noparamuri != $value) {
        echo ('<li><a href="https://www.bestattorney.com' . $value . '">' . $key . '</a></li>');
      }
    }

    if ($sidebarlinks) {
      echo ('
	                <li class="separator"></li>
	                <li><a href="https://www.bestattorney.com/about-us/testimonials.html">Testimonials & Reviews</a></li>
	                <li><a href="https://www.bestattorney.com/case-results/">Case Results & Success Stories</a></li>
	                <li><a href="https://www.bestattorney.com/about-us/">The Bisnar Chase Difference</a></li>
	            </ul>
	            <div class="clear"></div>
	        </div>
	        ');
    }
  }
}
