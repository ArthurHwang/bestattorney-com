<div id="top-menu">
    <ul id="menu" style="list-style:none;">
        <li id="menu-home"><a href="https://www.bestattorney.com/">Home<div class="arrow-nav-right-big nopc"></div></a>
        <li>
            <a href="https://www.bestattorney.com/about-us/" data-event-category="Button" data-event-action="About Us Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">About Us&nbsp;<div class="arrow-nav notablet nomobile"></div>
                <div class="arrow-nav-right-big nopc"></div>
            </a>
            <ul class="full-panel atto">
                <li>
                    <p class="ptitle"><a href="https://www.bestattorney.com/attorneys/" data-event-category="Button" data-event-action="Attorneys Nav Click" data-event-label="<?php echo $noparamuri; ?>">Lawyers<div class="arrow-nav-right-big nopc"></div></a></p>

                    <div class="col1">
                        <a href="https://www.bestattorney.com/attorneys/john-bisnar.html" class="atto-link" data-event-category="Button" data-event-action="John Bisnar Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="bisnar-atto-image"></div>
                            <span>John Bisnar</span><br /><strong>Founding Partner</strong>
                            <div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/attorneys/brian-chase.html" class="atto-link" data-event-category="Button" data-event-action="Brian Chase Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="chase-atto-image"></div>
                            <span>Brian Chase</span><br /><strong>Senior Partner</strong>
                            <div class="clear"></div>
                        </a>
                        <div class="clear"></div>
                        <a href="https://www.bestattorney.com/attorneys/scott-ritsema.html" class="atto-link" data-event-category="Button" data-event-action="Scott Ritsema Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="ritsema-atto-image"></div>
                            <span>Scott Ritsema</span><br /><strong>Partner</strong>
                            <div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/attorneys/gavin-long.html" class="atto-link" data-event-category="Button" data-event-action="Gavin Long Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="long-atto-image"></div>
                            <span>H. Gavin Long</span><br /><strong>Partner</strong>
                            <!-- <div class="clear"></div> -->
                        </a>

                    </div>
                    <div class="col2">

                        <a href="https://www.bestattorney.com/attorneys/jerusalem-beligan.html" class="atto-link" data-event-category="Button" data-event-action="Jerusalem Beligan Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="beligan-atto-image"></div>
                            <span>Jerusalem Beligan</span><br />Lawyer<div class="clear"></div>
                        </a>

                        <a href="https://www.bestattorney.com/attorneys/steven-hilst.html" class="atto-link" data-event-category="Button" data-event-action="Steven Hilst Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="hilst-atto-image"></div>
                            <span>Steven Hilst</span><br />Lawyer<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/attorneys/tom-antunovich.html" class="atto-link" data-event-category="Button" data-event-action="Tom Antunovich Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="antunovich-atto-image"></div>
                            <span>Tom Antunovich</span><br>Lawyer<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/attorneys/ian-silvers.html" class="atto-link" data-event-category="Button" data-event-action="Ian Silvers Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="silvers-atto-image"></div>
                            <span>Ian Silvers</span><br>Lawyer<div class="clear"></div>
                        </a>
                    </div>

                    <div class="col3">

                        <a href="https://www.bestattorney.com/attorneys/krist-biakanja.html" class="atto-link" data-event-category="Button" data-event-action="Krist Biakanja Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="biankanja-atto-image"></div>
                            <span>Krist Biakanja</span><br>Lawyer<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/attorneys/legal-administrator-shannon-barker.html" class="atto-link" data-event-category="Button" data-event-action="Shannon Barker Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="barker-atto-image"></div>
                            <span id="">Shannon Barker</span><br>Administrator
                            <div class="clear"></div>
                        </a>
                        <!-- <a href="https://www.bestattorney.com/attorneys/jordan-southwick.html" class="atto-link" data-event-category="Button" data-event-action="Jordan Southwick Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="southwick-atto-image"></div>
                            <span>Jordan Southwick</span><br>Lawyer<div class="clear"></div>
                        </a> -->
                    </div>

                    <div class="col4">
                        <a href="https://www.bestattorney.com/about-us/" data-event-category="Button" data-event-action="About Bisnar Chase Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            About Bisnar Chase<div class="arrow-nav"></div>
                        </a>
                        <a href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html" data-event-category="Button" data-event-action="Why Hire Us Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            Why Hire Us?<div class="arrow-nav"></div>
                        </a>
                        <a href="https://www.bestattorney.com/press-releases/" data-event-category="Button" data-event-action="Press Releases Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            Press Releases<div class="arrow-nav"></div>
                        </a>
                        <a href="https://www.bestattorney.com/giving-back/" data-event-category="Button" data-event-action="Giving Back Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            Giving Back<div class="arrow-nav"></div>
                        </a>
                        <a href="https://www.bestattorney.com/blog/" data-event-category="Button" data-event-action="Read Our Blog Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            Read Our Blog<div class="arrow-nav"></div>
                        </a>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
        </li>


        <li id="attorneys-mobile">
            <a href="https://www.bestattorney.com/attorneys/" data-event-category="Button" data-event-action="Results Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Attorneys<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
        </li>

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_nav-pa-" . $options['nav'] . ".php"); ?>




        <li>
            <a href="https://www.bestattorney.com/case-results/" data-event-category="Button" data-event-action="Results Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Results<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
            <ul>
                <li class="title"><a href="https://www.bestattorney.com/case-results/" data-event-category="Button" data-event-action="Results Nav Click (Header Text)" data-event-label="<?php echo $noparamuri; ?>">Results</a></li>
                <li><a href="https://www.bestattorney.com/case-results/" data-event-category="Button" data-event-action="Results Nav Click (Nav Element)" data-event-label="<?php echo $noparamuri; ?>">Case Results<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/about-us/testimonials.html" data-event-category="Button" data-event-action="Reviews Nav Click" data-event-label="<?php echo $noparamuri; ?>">Client Reviews<div class="arrow-nav"></div></a></li>
            </ul>
        </li>

        <li>
            <a href="https://www.bestattorney.com/resources/" data-event-category="Button" data-event-action="Resources Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Resources&nbsp;<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
            <ul>
                <li class="title"><a href="https://www.bestattorney.com/resources/" data-event-category="Button" data-event-action="Resources Nav Click (Header Text)" data-event-label="<?php echo $noparamuri; ?>">Resources</a></li>
                <li><a href="https://www.bestattorney.com/resources/" data-event-category="Button" data-event-action="Resources Nav Click (Nav Element)" data-event-label="<?php echo $noparamuri; ?>">Helpful Resources<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/faqs/" data-event-category="Button" data-event-action="FAQs Nav Click" data-event-label="<?php echo $noparamuri; ?>">Frequently asked Questions<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/resources/car-accident-statistics.html" data-event-category="Button" data-event-action="Car Accident Statistics Nav Click" data-event-label="<?php echo $noparamuri; ?>">Car Accident Statistics<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/resources/book-order-form.html" data-event-category="Button" data-event-action="Books by Bisnar Chase Nav Click" data-event-label="<?php echo $noparamuri; ?>">Books by Bisnar Chase<div class="arrow-nav"></div></a></li>
                <!-- <li><a href="https://www.bestattorney.com/giving-back/client-appreciation.html" data-event-category="Button" data-event-action="Loyalty Program Nav Click" data-event-label="<?php echo $noparamuri; ?>">Loyalty Program<div class="arrow-nav"></div></a></li> -->
            </ul>
        </li>
        <li>
            <a href="https://www.bestattorney.com/referrals.html" data-event-category="Button" data-event-action="Referrals Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Attorney Referrals<div class="arrow-nav-right-big nopc"></div>
            </a>
        </li>
        <li>
            <a href="https://www.bestattorney.com/locations/" data-event-category="Button" data-event-action="Locations Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Locations<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
            <ul>
                <li class="title">Office Locations</li>
                <?php if ($options['nav'] != "orange-county") { ?>
                    <li><a href="https://www.bestattorney.com/orange-county/" data-event-category="Button" data-event-action="OC Nav Click" data-event-label="<?php echo $noparamuri; ?>">Orange County<div class="arrow-nav"></div></a></li>
                <?php

                }
                if ($options['nav'] != "los-angeles") { ?>
                    <li><a href="https://www.bestattorney.com/los-angeles/" data-event-category="Button" data-event-action="LA Nav Click" data-event-label="<?php echo $noparamuri; ?>">Los Angeles<div class="arrow-nav"></div></a></li>
                <?php

                }
                if ($options['nav'] != "riverside") { ?>
                    <li><a href="https://www.bestattorney.com/riverside/" data-event-category="Button" data-event-action="Riverside Nav Click" data-event-label="<?php echo $noparamuri; ?>">Riverside<div class="arrow-nav"></div></a></li>
                <?php

                }
                if ($options['nav'] != "san-bernardino") { ?>
                    <li><a href="https://www.bestattorney.com/san-bernardino/" data-event-category="Button" data-event-action="San Bernardino Nav Click" data-event-label="<?php echo $noparamuri; ?>">San Bernardino<div class="arrow-nav"></div></a></li>
                <?php

                } ?>
                <li><a href="https://www.bestattorney.com/locations/" data-event-category="Button" data-event-action="Other Service Areas Nav Click" data-event-label="<?php echo $noparamuri; ?>">Other Service Areas<div class="arrow-nav"></div></a></li>
            </ul>
        </li>
        <li>
            <a href="https://www.bestattorney.com/contact.html" data-event-category="Button" data-event-action="Contact Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Contact&nbsp;<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
            <ul>
                <li class="title">Contact Us</li>
                <li><a href="https://www.bestattorney.com/orange-county/contact-us.html" data-event-category="Button" data-event-action="Orange County Office Nav Click" data-event-label="<?php echo $noparamuri; ?>">Orange County Office<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/los-angeles/contact.html" data-event-category="Button" data-event-action="Los Angeles Office Nav Click" data-event-label="<?php echo $noparamuri; ?>">Los Angeles Office<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/riverside/contact.html" data-event-category="Button" data-event-action="Riverside Office Nav Click" data-event-label="<?php echo $noparamuri; ?>">Riverside Office<div class="arrow-nav"></div></a></li>
                <li><a href="https://www.bestattorney.com/san-bernardino/contact.html" data-event-category="Button" data-event-action="San Bernardino Office Nav Click" data-event-label="<?php echo $noparamuri; ?>">San Bernardino Office<div class="arrow-nav"></div></a></li>
            </ul>
        </li>
        <li>
            <a href="https://www.bestattorney.com/covid-19-business-interruption-claims/index.html" data-event-category="Button" data-event-action="Covid Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                Covid-19 CLaims<div class="arrow-nav-right-big nopc"></div>
            </a>
        </li>
    </ul>
    <div class="clear"></div>
</div>