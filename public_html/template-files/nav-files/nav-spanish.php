<div id="top-menu">
    <ul id="menu" style="list-style:none;">
        <li id="menu-home"><a href="https://www.bestattorney.com/abogados/">Inicio<div class="arrow-nav-right-big nopc"></div></a>
        <li><a href="https://www.bestattorney.com/abogados/sobre-nosotros/">Abogados&nbsp;<div class="arrow-nav notablet nomobile"></div>
                <div class="arrow-nav-right-big nopc"></div>
            </a>
            <ul class="full-panel atto">
                <li>
                    <p class="ptitle"><a href="https://www.bestattorney.com/abogados/sobre-nosotros/brian-chase.html">Abogados<div class="arrow-nav-right-big nopc"></div></a></p>
                    <div class="col1">
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/john-bisnar.html" class="atto-link" data-event-category="Button" data-event-action="John Bisnar Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="bisnar-atto-image"></div><span>John Bisnar</span><br /><strong>Socio Fundador</strong>
                            <div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/brian-chase.html" class="atto-link" data-event-category="Button" data-event-action="Brian Chase Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="chase-atto-image"></div><span>Brian Chase</span><br /><strong>Socio Mayoritario</strong>
                            <div class="clear"></div>
                        </a>
                        <div class="clear"></div></a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/scott-ritsema.html" class="atto-link" data-event-category="Button" data-event-action="Scott Ritsema Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="ritsema-atto-image"></div><span>Scott Ritsema</span><br /><strong>Abogado</strong>
                            <div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/shannon-barker.html" class="atto-link" data-event-category="Button" data-event-action="Shannon Barker Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="barker-atto-image"></div><span>Shannon Barker</span><br>
                            <strong>Administradora</strong>
                            <div class="clear"></div>
                        </a>
                    </div>
                    <div class="col2">

                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/jerusalem-beligan.html" class="atto-link" data-event-category="Button" data-event-action="Jerusalem Beligan Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="beligan-atto-image"></div><span>Jerusalem Beligan</span><br />Abogado<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/gavin-long.html" class="atto-link" data-event-category="Button" data-event-action="Gavin Long Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="long-atto-image"></div><span>H. Gavin Long</span><br />Abogado<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/steven-hilst.html" class="atto-link" data-event-category="Button" data-event-action="Steven Hilst Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="hilst-atto-image"></div><span>Steven Hilst</span><br />Abogado<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/tom-antunovich.html" class="atto-link" data-event-category="Button" data-event-action="Tom Antunovich Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="antunovich-atto-image"></div><span>Tom Antunovich</span><br>Abogado<div class="clear"></div>
                        </a>
                    </div>
                    <div class="col3">
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/ian-silvers.html" class="atto-link" data-event-category="Button" data-event-action="Ian Silvers Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="silvers-atto-image"></div>
                            <span>Ian Silvers</span><br>Abogado<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/krist-biakanja.html" class="atto-link" data-event-category="Button" data-event-action="Krist Biakanja Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="biankanja-atto-image"></div>
                            <span>Krist Biakanja</span><br>Abogado<div class="clear"></div>
                        </a>
                        <!-- <a href="https://www.bestattorney.com/abogados/sobre-nosotros/jordan-southwick.html" class="atto-link" data-event-category="Button" data-event-action="Jordan Southwick Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="atto-image" id="southwick-atto-image"></div>
                            <span>Jordan Southwick</span><br>Abogado<div class="clear"></div>
                        </a> -->
                    </div>
                    <div class="col4">
                        <a href="https://www.bestattorney.com/abogados/sobre-nosotros/" data-event-category="Button" data-event-action="Acerca de Bisnar Chase Nav Click" data-event-label="<?php echo $noparamuri; ?>">Sobre el bufete de Bisnar Chase
                            <div class="arrow-nav"></div></a>
                        <a href="https://www.bestattorney.com/about-us/lawyer-reviews-ratings.html" data-event-category="Button" data-event-action="Why Hire Us Nav Click" data-event-label="<?php echo $noparamuri; ?>">Por que&#180; Somos Los Mejores?<div class="arrow-nav"></div></a><a href="https://www.bestattorney.com/press-releases/" data-event-category="Button" data-event-action="Press Releases Nav Click" data-event-label="<?php echo $noparamuri; ?>">Comunicados de Prensa<div class="arrow-nav"></div></a>
                        <a href="https://www.bestattorney.com/giving-back/" data-event-category="Button" data-event-action="Giving Back Nav Click" data-event-label="<?php echo $noparamuri; ?>">Servicio Comunitario<div class="arrow-nav"></div></a><a href="https://www.bestattorney.com/blog/" data-event-category="Button" data-event-action="Read Our Blog Nav Click" data-event-label="<?php echo $noparamuri; ?>">Lee Nuestros Articulos<div class="arrow-nav"></div></a>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
        </li>
        <li><a href="https://www.bestattorney.com/abogados/lesiones-personales.html" data-event-category="Button" data-event-action="Practice Areas Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Areas de Práctica&nbsp;<div class="arrow-nav-right-big nopc"></div>
                <div class="arrow-nav notablet nomobile"></div>
            </a>
            <ul class="full-panel prac">
                <li>
                    <p class="ptitle"><a href="https://www.bestattorney.com/abogados/lesiones-personales.html" data-event-category="Button" data-event-action="ES-Practice Areas Nav Click (Header Text)" data-event-label="<?php echo $noparamuri; ?>">Areas de Práctica<div class="arrow-nav-right-big nopc"></div></a></p>
                    <div class="col1"><a href="https://www.bestattorney.com/abogados/lesiones-personales.html" class="prac-link" data-event-category="Button" data-event-action="ES-Personal Injury Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="personal-injury-image"></div>Lesiones <br/> Personales<div class="clear"></div>
                        </a><a href="https://www.bestattorney.com/abogados/accidentes-de-auto.html" class="prac-link" data-event-category="Button" data-event-action="ES-Car Accidents Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="car-accidents-image"></div>Accidentes Automovilísticos<div class="clear"></div>
                        </a><a href="https://www.bestattorney.com/abogados/defectos-automovilisticos.html" class="prac-link" data-event-category="Button" data-event-action="ES-Auto Defects Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="auto-defect-image"></div>Defectos Automovilísticos<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/accidentes-de-camiones.html" class="prac-link" data-event-category="Button" data-event-action="ES-Truck Accidents Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="truck-accident-image"></div> Accidentes de Camiones<div class="clear"></div>

                        </a>

                    </div>
                    <div class="col2">
                        <a href="https://www.bestattorney.com/abogados/productos-defectuosos.html" class="prac-link" data-event-category="Button" data-event-action="ES-Defective Products Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="defective-product-image"></div>Productos defectuosos<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/depositivos-medicos.html" class="prac-link" data-event-category="Button" data-event-action="ES-Medical Devices Nav Click" data-event-label="<?php
                                                                                                                                                                                                                    echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="defective-medical-product-image"></div>Dispositivos Médicos<div class="clear"></div>
                        </a>
                        <a href="https://www.bestattorney.com/abogados/drogas-peligrosas.html" class="prac-link" data-event-category="Button" data-event-action="ES-Pharmaceutical Litigation Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="pharmaceutical-litigation-image"></div>Farmacéutico<div class="clear"></div>
                        </a>

                        <a href="https://www.bestattorney.com/abogados/derecho-laboral.html" class="prac-link" data-event-category="Button" data-event-action="ES-Employment Law Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="employment-law-image"></div>Derecho Laboral<div class="clear"></div>
                        </a>
                                          <a href="https://www.bestattorney.com/abogados/muerte-injusta.html" class="prac-link" data-event-category="Button" data-event-action="ES-Employment Law Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="wrongful-death-image"></div>Muerte Injusta<div class="clear"></div>
                        </a>
                    </div>

                    <div class="col3">
                        <a href="https://www.bestattorney.com/abogados/responsibilidad-civil.html" class="prac-link" class="prac-link" data-event-category="Button" data-event-action="ES-Premises Liability Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="premises-liability-image"></div>
                            Responsabilidad del Local
                            <div class="clear"></div>
                        </a>

                        <a href="https://www.bestattorney.com/abogados/mordeduras-de-perro.html" class="prac-link" data-event-category="Button" data-event-action="ES-Dog Bites Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="dog-bite-image"></div>
                            Lesiones por Mordedura de Perro
                            <div class="clear"></div>
                        </a>

                        <a href="https://www.bestattorney.com/abogados/accidentes-de-bicicleta.html" class="prac-link" data-event-category="Button" data-event-action="ES-Bicycle Accident Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="bicycle-accident-image"></div>
                            Accidentes de Bicicleta
                            <div class="clear"></div>
                        </a>


                        <a href="https://www.bestattorney.com/abogados/accidentes-de-motocicleta.html" class="prac-link" data-event-category="Button" data-event-action="ES-Bicycle Accident Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="motorcycle-accident-image"></div>
                            Accidentes de Motocicleta
                            <div class="clear"></div>
                        </a>


                        <a href="https://www.bestattorney.com/abogados/accidentes-de-peatones.html" class="prac-link" data-event-category="Button" data-event-action="ES-Bicycle Accident Nav Click" data-event-label="<?php echo $noparamuri; ?>">
                            <div class="practice-areas-images" id="pedestrian-accident-image"></div>
                            Accidentes de Peatones
                            <div class="clear"></div>
                        </a>
                    </div>

                    <div class="col4">
                        <a href="https://www.bestattorney.com/abogados/recursos/que-hacer-despues-un-accidente.html" data-event-category="Button" data-event-action="ES-Accident Help Guide Nav Click" data-event-label="<?php echo $noparamuri; ?>">Qué hacer despues de un accidente<div class="arrow-nav"></div></a><a href="https://www.bestattorney.com/abogados/resultos-destacados.html" data-event-category="Button" data-event-action="Case Results Nav Click" data-event-label="<?php echo $noparamuri; ?>">Resultados<div class="arrow-nav"></div></a><a href="https://www.bestattorney.com/abogados/testimonios.html" data-event-category="Button" data-event-action="ES-Client Reviews Nav Click" data-event-label="<?php echo $noparamuri; ?>">Testimonios<div class="arrow-nav"></div></a><a href="https://www.bestattorney.com/abogados/recursos/elegir-un-abogado.html" data-event-category="Button" data-event-action="Cuota de base eventual Nav Click" data-event-label="<?php echo $noparamuri; ?>">Cuota de base eventual<div class="arrow-nav"></div></a></div>
                    <div class="clear"></div>
                </li>
            </ul>
        </li>
        <li><a href="https://www.bestattorney.com/abogados/resultos-destacados.html" data-event-category="Button" data-event-action="ES-Results Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Resultados<div class="arrow-nav-right-big nopc"></div></a></li>

        <li><a href="https://www.bestattorney.com/abogados/testimonios.html" data-event-category="Button" data-event-action="ES-Testimonials Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Testimonios<div class="arrow-nav-right-big nopc"></div></a></li>

        <li><a href="https://www.bestattorney.com/abogados/contactenos.html" data-event-category="Button" data-event-action="ES-Contactenos Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Contactenos<div class="arrow-nav-right-big nopc"></div></a></li>

        <li><a href="https://www.bestattorney.com/abogados/recursos/" data-event-category="Button" data-event-action="ES-Recursos Top Level Nav Click" data-event-label="<?php echo $noparamuri; ?>">Recursos<div class="arrow-nav-right-big nopc"></div></a></li>
        <div class="clear"></div>
    </ul>
    <div class="clear"></div>
</div> 