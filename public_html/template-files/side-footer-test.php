<?php if (!$options['isHome'] && !$options['isContact']) { ?>
	<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_was-this-page-helpful.php"); ?>
<?php

} ?>
</div> <!-- End #content div -->

<?php if (!$options["fullWidth"] && !$options['isBlog']) {
  include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/sidebar.php");
} else if ($options['isBlog']) {
  include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/sidebar-blog.php");
} else { ?>
	</div>
<?php

} ?>

<?php if (!$options['isHome']) {
  include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_awards.php");
  include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_have-a-question-english.php");
}

?>
<?php if ($options["showFooterVideos"]) { ?>

	<div id="bisnar-chase-difference" data-src="/images/bisnar-chase-difference.jpg"  class="">
		<div id="watch-our-videos">
		<p class="title">The <img class="" data-src="/images/logo-difference.png"   width="347" height="34" alt="BISNAR CHASE" style="vertical-align: -6px"> Difference</p>
			<a href="https://www.bestattorney.com/about-us/" class="more-about"></a>
			<div id="video-holder">
				<ul>
					<li>
						<div id="video-11" class="video-pane"><a href="http://www.youtube.com/embed/BhVPzkYsLuE?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button"></a></div>
						<p class="quote">&ldquo;I was in a serious auto accident when I was in law school. I had to hire a personal injury attorney and had a really bad experience.&rdquo;</p>
						<p class="speaker">John Bisnar</p>
						<p class="topic">on what made him want to become a personal injury attorney</p>
					</li>
					<li>
						<div id="video-bc1" class="video-pane"><a href="http://www.youtube.com/embed/D3c_pxDeswg?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button"></a></div>
						<p class="quote">&ldquo;If you hire Bisnar | Chase and we don't recover money for you in your case, you owe us absolutely nothing.&rdquo;</p>
						<p class="speaker">Brian Chase</p>
						<p class="topic">on whether or not you would owe money if your case was lost</p>
					</li>
					<li>
						<div id="video-12" class="video-pane"><a href="http://www.youtube.com/embed/PS0XXH4Gz9g?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button"></a></div>
						<p class="quote">&ldquo;Whatever the philosphy of the management is, is going to be carried through by the employees and it&rsquo;s going to reflect on the experience the clients have.&rdquo;</p>
						<p class="speaker">John Bisnar</p>
						<p class="topic">on his philosophy on running a law firm</p>
					</li>
					<li>
						<div id="video-bc2" class="video-pane"><a href="http://www.youtube.com/embed/jDlOenh-w-4?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button"></a></div>
						<p class="quote">&ldquo;The insurance companies are going to be investigating that accident the day it happens. You need to have a lawyer on your side the day it happens as well.&rdquo;</p>
						<p class="speaker">Brian Chase</p>
						<p class="topic">on when you should contact an attorney</p>
					</li>
					<li>
						<div id="video-13" class="video-pane"><a href="http://www.youtube.com/embed/nbnjoHTHLNQ?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button"></a></div>
						<p class="quote">&ldquo;The first thing we want to do with our clients is to relieve the stress. Make them feel comfortable. Treat them as an honored guest.&rdquo;</p>
						<p class="speaker">John Bisnar</p>
						<p class="topic">on how he would define superior client representation</p>
					</li>
					<li>
						<div id="video-bc3" class="video-pane"><a href="http://www.youtube.com/embed/IuD-S72PMxk?autoplay=1&wmode=transparent&enablejsapi=1&rel=0" class="play-now fancybox.iframe play-button" onClick="ga(['send', 'event', Videos', 'Play', 'Exposing Corporate Greed - Video']);"></a></div>
						<p class="quote">&ldquo;It's hard to answer that question right up front without a thorough analysis. What I can guarantee you is, with the resources of Bisnar | Chase we will maximize the value of your case.&rdquo;</p>
						<p class="speaker">Brian Chase</p>
						<p class="topic">on what your case is worth</p>
					</li>
				</ul>
			</div>
			<a href="https://www.bestattorney.com/" class="mycarousel-prev3"></a>
			<a href="https://www.bestattorney.com/" class="mycarousel-next3"></a>
		</div>
	</div>

<?php

} ?>
</div> <!-- end content-box -->

<footer id="footer">

	<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_footer-inner-english-test.php");
?>

	<span id="scripts">

		<?php if ($options['loadJquery']) { ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      			<script src="/js/vanillaSlideshow.js"></script>
<script>
	vanillaSlideshow.init({
		slideshow: true,
		delay: 15000,
		arrows: true,
		indicators: true,
		random: false,
		animationSpeed: '2s'
	});
</script>
		<?php

} ?>

		<?php if ($options['loadJqueryUi']) { ?>
	        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	    <?php

  } ?>

	    <!-- Load Common Styles and Scripts across all pages -->
		<link rel="stylesheet" href="<?php echo auto_version('/dist/css/common-styles.css'); ?>">
		<script src="<?php echo auto_version('/dist/js/common-scripts.js'); ?>"></script>

		<?php if ($options['isHome']) { ?>
			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/home-styles.css'); ?>">
			<script src="<?php echo auto_version('/dist/js/home-scripts.js'); ?>"></script>
		<?php

} else if ($options["isGeo"] || $options["isPa"]) { ?>
			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/pa-geo-styles.css'); ?>">
			<script src="<?php echo auto_version('/dist/js/pa-geo-scripts.js'); ?>"></script>
		<?php

} else if ($options["isBlog"]) { ?>
			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/blog-styles.css'); ?>">
			<script src="<?php echo auto_version('/dist/js/blog-scripts.js'); ?>"></script>
		<?php

} else if ($options["isContact"]) { ?>
			<link rel="stylesheet" href="<?php echo auto_version('/dist/css/contact-styles.css'); ?>">
			<script src="<?php echo auto_version('/dist/js/contact-scripts.js'); ?>"></script>
		<?php

} else { ?>
			<!-- <link rel="stylesheet" href="<?php echo auto_version('/dist/css/default-styles.css'); ?>"> -->
			<script src="<?php echo auto_version('/dist/js/default-scripts.js'); ?>"></script>
		<?php

} ?>

		<?php if ($options['loadLegacyBootstrap']) { ?>
			<script src="/js/bootstrap.min.js"></script>
			<link rel="stylesheet" href="/css/bootstrap.min.css">
		    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
		<?php

}


// THIS IS ADDING THE FEATURED IMAGE INTO JSON-LD TO TRY TO GET THAT STUPID THUMBNAIL INTO GOOGLE MOBILE SERPS
if (!isset($options['featuredImage'])) {
  $options['featuredImage'] = "https://www.bestattorney.com/images/featured-homepage-images/case-value.jpg";
}

echo $options['jsonld']; ?>

		<script>
			<?php if ($options['allowSidebarTruncate'] && !$options['fullWidth']) { ?>
				let truncate = true;
			<?php

} else { ?>
				let truncate = false;
			<?php

} ?>
		</script>

		<?php if ($environment == "development") {
    include_once($_SERVER['DOCUMENT_ROOT'] . "/../development-scripts.php");
  }

  if (!isset($_GET['test']) && !$options['noLiveChat']) { ?>
			<!-- Start Of NGage -->
			<div id="nGageLH" style="visibility:hidden; display: block; padding: 0; position: fixed; right: 0px; bottom: 0px; z-index: 5000;"></div>
			<script type="text/javascript">
				function loadLiveChat() {
					var element = document.createElement("script");
					element.src = "https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=154-2-128-85-73-160-224-153";
					// setTimeout(()=>document.body.appendChild(element),1000);
          // setTimeout(()=>document.body.appendChild(element));
          document.body.appendChild(element);
				}
				if (window.addEventListener) window.addEventListener("load", loadLiveChat, false);
				else if (window.attachEvent) window.attachEvent("onload", loadLiveChat);
				else window.onload = loadLiveChat;

      </script>

      <style>
        .livChatFloatingButton {
      display: none !important;
    }
      </style>
			<!-- End Of NGage -->
		<?php

} ?>
	</span>

</footer>
</body>
</html>
