<div id="sidebar">

    <?php





    if ($options["showSidebarContactForm"]) {
      $options['isSpanish'] ? include $_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/contact-forms/_spanish-sidebar-contact-form.php" : include $_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/contact-forms/_sidebar-contact-form.php";
    }




    if ($options["sidebarContentHtml"] != "") { ?>

    <div id="sb-custom-wrap" class="">
        <?php if ($options["sidebarContentTitle"]) {
          echo '<p class="title">' . $options["sidebarContentTitle"] . '</p>';
        } ?>
        <?php echo $options["sidebarContentHtml"]; ?>
    </div>
    <div class="clear"></div>

    <?php

  }

  if (isset($options['extraSidebar'])) {
    echo $options['extraSidebar'];
  }

  if (isset($options['sidebarLinks'])) { ?>

    <p class="rc-title">
        <?php echo $options['sidebarLinksTitle']; ?>
    </p>
    <div class="related-content">
        <ul>
            <?php subfooterLinks($options['sidebarLinks'], $noparamuri, true); ?>

            <div class="clear"></div>

            <?php

          }

          if (isset($options['caseResultsArray'])) {
            switch (count($options['caseResultsArray'])) {
              case 1:
                // echo "caseresults: ".var_dump($options['caseResultsArray']);
                printCaseResults($options['caseResultsArray'][0]);
                break;
              case 2:
                printCaseResults($options['caseResultsArray'][0], $options['caseResultsArray'][1]);
                break;
              case 3:
                printCaseResults($options['caseResultsArray'][0], $options['caseResultsArray'][1], $options['caseResultsArray'][2]);
                break;
            }

            echo '<div class="clear"></div>';
          }


          if (isset($options['videosArray']) && $options['videosArray'] != []) {
            echo '<div id="sb-videos">';
            echo '<p class="title">Videos</p>';

            $videoIndex = 1;

            foreach ($options['videosArray'] as $video) {
              if (strlen($video[0]) > 46) {
                $fontClass = "font12";
              } elseif (strlen($video[0]) > 40) {
                $fontClass = "font12";
              } else {
                $fontClass = "font15";
              }

              $videoLink = "https://www.youtube.com/embed/" . $video[2] . "?autoplay=1&wmode=transparent&enablejsapi=1&rel=0"; ?>

            <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
                <meta itemprop="embedURL" content="<?php echo $videoLink; ?>" />
                <meta itemprop="description" content="A sidebar video discussing the subject: '<?php echo $video[0]; ?>' on the <?php echo $noparamuri; ?> page." />
                <meta itemprop="uploadDate" content="Aug 14, 2017" />
                <a href="<?php echo $videoLink; ?>" class="video-<?php echo $videoIndex; ?> sb-video-link play-now fancybox.iframe" title="<?php echo $video[0]; ?>" data-jslghtbx data-jslghtbx-index="$videoIndex">
                    <?php
                    if (!$video[1]) {
                      $video[1] = '/images/video-images/video-image-' . rand(1, 8) . '.jpg';
                    }
                    ?>
                    <img src="<?php echo $video[1]; ?>" data-src="<?php echo $video[1]; ?>" class="" alt="<?php echo $video[0] . " Video Thumbnail"; ?>" itemprop="thumbnailUrl"/>
                </a>
                <meta itemprop="thumbnailUrl" content="<?php echo " https://www.bestattorney.com" . $video[1]; ?>" />
                <?php if ($video[0]) {
                  echo ("<div class='video$videoIndex videooverlay'><h4 itemprop='name' class='$fontClass'>");
                  echo $video[0];
                  echo ("</h4></div>");
                } ?>
            </div>

            <?php
            $videoIndex++;
          }

          echo '</div>';
        }

        if (isset($options['reviewsArray'])) {
          switch (count($options['reviewsArray'])) {
            case 1:
              getReviews($options['reviewsArray'][0]);
              break;
            case 2:
              getReviews($options['reviewsArray'][0], $options['reviewsArray'][1]);
              break;
            case 3:
              getReviews($options['reviewsArray'][0], $options['reviewsArray'][1], $options['reviewsArray'][2]);
              break;
          }

          echo '<div class="clear"></div>';
        }

        if ($options['loadBlog']) { ?>
            <div id="sb-blog-feed">
                <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_blog-feed-pa-geo.php"); ?>
            </div>
            <?php

          } ?>

    </div><!-- End Sidebar -->
</div><!-- End Centered-Content-Box -->
<div class="clear"></div> 