<?php
// Default Folder Options


$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false, 


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================
	// "sidebarLinksTitle" => "About Us",

	// "sidebarLinks" => array(),

	"showSidebarContactForm" => true,

	// "sidebarContentTitle" => "",

	// "sidebarContentHtml" => "",

    "extraSidebar" => "", 

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Bisnar Chase Brand Video', '/images/video-images/brian-brand-video.jpg', 'wmv1HKnhW7U'],
		['Brian Chase on American Law TV', '/images/featured-homepage-images/brian-american-law-television.jpg', 'BhVPzkYsLuE']
	], //array of video information that will show on the sidebar.

	"loadBlog" => false,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>