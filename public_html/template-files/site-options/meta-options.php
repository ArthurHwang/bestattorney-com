<?php


/*Page Detection - to determine different Nav bars, location customization, etc etc. */
$page = $_SERVER['REQUEST_URI'];
$path_parts = pathinfo($page);
$thispage = $path_parts['basename']; // extract name of page being diplayed
$thisdir = $path_parts['dirname']; // extracts folder name of page being displayed

// NOTE: when rendering the index of a subfolder the pages filename will actually be the folder name

// all links within includes MUST be absolute to prevent broken links/images
// this includes href,src and action declarations


// *******************************************************************************************
// PAGE DETECTION ROUTINES
// these are used for pages that show custom content instead of contact form in sidebar
// script sets up a slug used to show custom sidebar content instead of contact form on specified pages
// all editing of this custom content is done on this file, only upload to root of bestattorney.com
// custom content is defined in the first switch case routine => switch ($sidebarcontentshow)
// each case within the switch defines the custom sidebar content for these particular pages
// *******************************************************************************************


if (strpos($page, "?")) {
    $noparamuri = strstr($page, '?', true); 

    $thispage = $noparamuri;
    if (strpos($thispage, "/") !== false) {
        $location = strrpos($thispage, '/');
        $thispage = substr($thispage, $location+1);
        //I DON'T REMEMBER WHAT THIS DOES WHAT THE HELL.
    }
} else {
    $noparamuri = $page;
}

// *******************************************************************************************


// *******************************************************************************************
// META-OPTIONS SET (Phone Number, Address, Etc.)
// DON'T TOUCH THESE (unless we need to add new phone numbers/addresses, etc.)

switch ($options["location"]) {
    case "orange-county":
        if ( !isset($options["phone"]) ) {
            $options["phone"] = "(949) 203-3814";
            $options["callPhone"] = "9492033814";
        }
        if ( !isset($options["address"]) ) {
            $options["address"] = [
                "street" => "1301 Dove St, #120",
                "city" => "Newport Beach",
                "state" => "CA",
                "zip" => "92660",
                "gmap" => "https://www.google.com/maps/preview?ll=33.662156,-117.866355&z=16&t=m&hl=en-US&gl=US&mapclient=embed&iwloc=lyrftr:m,12551230997004165438"
            ]; 
        }
        if ( !isset($options["contactLink"]) ) {
        	$options["contactLink"] = "https://www.bestattorney.com/contact.html";
        }

        if (!isset($options['nav'])) {
            $options['nav'] = 'orange-county';
        }

        break;
    case "los-angeles":
        if ( !isset($options["phone"]) ) {
            $options["phone"] = "(323) 238-4683";
            $options["callPhone"] = "3232384683";
        }
        if ( !isset($options["address"]) ) {
            $options["address"] = [
                "street" => "6701 Center Drive West, 14th Fl.",
                "city" => "Los Angeles",
                "state" => "CA",
                "zip" => "90045",
            ]; 
        }

        if ( !isset($options["contactLink"]) ) {
        	$options["contactLink"] = "https://www.bestattorney.com/los-angeles/contact.html";
        }
        if (!isset($options['nav'])) {
            $options['nav'] = 'los-angeles';
        }

        break;
    case "riverside":
        if ( !isset($options["phone"]) ) {
            $options["phone"] = "(951) 530-3711"; 
            $options["callPhone"] = "9515303711"; 
        }
        if ( !isset($options["address"]) ) {
            $options["address"] = [
                "street" => "6809 Indiana Ave #148",
                "city" => "Riverside",
                "state" => "CA",
                "zip" => "92506"
            ]; 
        }
        if ( !isset($options["contactLink"]) ) {
        	$options["contactLink"] = "https://www.bestattorney.com/riverside/contact.html";
        }

        if (!isset($options['nav'])) {
            $options['nav'] = 'riverside';
        }
        break;
    case "san-bernardino":
        if ( !isset($options["phone"]) ) {
            $options["phone"] = "(909) 253-0750"; 
            $options["callPhone"] = "9092530750"; 
        }
        if ( !isset($options["address"]) ) {
            $options["address"] = [
                "street" => "473 E. Carnegie Drive, 2nd Floor, Room 221",
                "city" => "San Bernardino",
                "state" => "CA",
                "zip" => "92408"
            ]; 
        }
        if ( !isset($options["contactLink"]) ) {
            $options["contactLink"] = "https://www.bestattorney.com/san-bernardino/contact.html";
        }
        if (!isset($options['nav'])) {
            $options['nav'] = 'san-bernardino';
        }
        break;
    default:
        if ( !isset($options["phone"]) ) {
            $options["phone"] = "(800) 561-4887"; 
            $options["callPhone"] = "8005614887"; 
        }
        if ( !isset($options["address"]) ) {
            $options["address"] = [
                "street" => "1301 Dove St, #120",
                "city" => "Newport Beach",
                "state" => "CA",
                "zip" => "92660"
            ]; 
        }
        if ( !isset($options["contactLink"]) ) {
        	$options["contactLink"] = "https://www.bestattorney.com/contact.html";
        }
        
        if (!isset($options['nav'])) {
            $options['nav'] = 'default';
        }
        break;

}

// if (!isset($options['headerImageClass'])) {
//     $options['headerImageClass'] = 'content-box';
// }


/* ============ PAGE SUBJECT OPTIONS =============== */
// personalinjury
// dogbite
// accident
// autodefect
// productdefect
// medicaldefect
// drugdefect
// premisesliability
// truckaccident
// pedestrianaccident
// motorcycleaccident
// bicycleaccident
// employmentlaw
// wrongfuldeath
/* ================================================= */

if ($options['rewriteCaseResults']) {
    switch($options['pageSubject']) {
        case "dogbite":
            $options['caseResultsArray'] = ['search', 'bite bites dogs bitten', array('category','premises')];
            break;
        case "accident":
            $options['caseResultsArray'] = ['category','auto'];
            break;
        case "truckaccident":
            $options['caseResultsArray'] = ['search', 'truck tractor trailer 18-wheeler', array('category', 'auto')];
            break;
        case "motorcycleaccident":
            $options['caseResultsArray'] = ['search', 'motorcycle', array('category', 'auto')];
            break;
        case "bicycleaccident":
            $options['caseResultsArray'] = ['search', 'bicycle bike', array('category', 'auto')];
            break;
        case "pedestrianaccident": 
            $options['caseResultsArray'] = ['search', 'pedestrian crosswalk', array('category', 'auto')];
            break;
        case "autodefect":
            $options['caseResultsArray'] = ['category','auto-defect'];
            break;
        case "productdefect":
            $options['caseResultsArray'] = ['search', 'product defect defective', array('top')];
            break;
        case "drugdefect":
        case "medicaldefect":
            $options['caseResultsArray'] = ['search', 'medical defect defective', array('top')];
            break;
        case "premisesliability":
            $options['caseResultsArray'] = ['category','premises'];
            break; 
        case "employmentlaw":
            $options['caseResultsArray'] = ['category','other'];
            break;   
        default:
            break;  
    }
}


if ($options['spanishPageEquivalent'] == "" ) {
    switch ($options['pageSubject']) {
        case "dogbite":
            $options['spanishPageEquivalent'] = "mordeduras-de-perro.html";
            break;
        case "truckaccident":
        case "motorcycleaccident":
        case "pedestrianaccident":
        case "bicycleaccident":
        case "accident":
            $options['spanishPageEquivalent'] = "accidentes-de-auto.html";
            break;
        case "autodefect":
            $options['spanishPageEquivalent'] = "defectos-automovilisticos.html";
            break;
        case "productdefect":
            $options['spanishPageEquivalent'] = "productos-defectuosos.html";
            break;
        case "drugdefect":
        case "medicaldefect":
            $options['spanishPageEquivalent'] = "depositivos-medicos.html";
            break;
        case "premisesliability":
            $options['spanishPageEquivalent'] = "responsibilidad-civil.html";
            break;  
        default:
            $options['spanishPageEquivalent'] = "";
            break;  
    }
}

// ADD JSON
if($options['location'] == 'los-angeles') {
  include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_los-angeles-location.php";
} else if ($options['location'] == 'riverside') {
  include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_riverside-location.php";
} else if ($options['location'] == 'san-bernardino') {
  include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_san-bernardino-location.php";
} else {
  include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_orange-county-location.php";
}

switch ($options['pageSubject']) {
    case "dogbite":
        $options['formTitle'] = "Do I have a case for my dog bite injury?";
        $options['contactText'] = "Brief Description of Dog Bite Incident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_dog-bite-service.php";
        break;
    case "truckaccident":
        $options['formTitle'] = "Do I have a case for my truck accident injury?";
        $options['contactText'] = "Brief Description of your Truck Accident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_truck-accident-service.php";
        break;
    case "bicycleaccident": 
        $options['formTitle'] = "Do I have a case for my bicycle accident injury?";
        $options['contactText'] = "Brief Description of your Bicycle Accident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_bicycle-accident-service.php";
        break;
    case "pedestrianaccident": 
        $options['formTitle'] = "Do I have a case for my pedestrian accident injury?";
        $options['contactText'] = "Brief Description of your Pedestrian Accident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_pedestrian-accident-service.php";
        break;
    case "motorcycleaccident":
        $options['formTitle'] = "Do I have a case for my motorcycle accident injury?";
        $options['contactText'] = "Brief Description of your Motorcycle Accident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_motorcycle-accident-service.php";
        break;
    case "accident":
        $options['formTitle'] = "Do I have a case for my car accident injury?";
        $options['contactText'] = "Brief Description of your Car Accident Injury:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_car-accident-service.php";
        break;
    case "autodefect":
        $options['formTitle'] = "Do I have a case for my auto defect?";
        $options['contactText'] = "Brief Description of your Car Defect Injuries:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_auto-defect-service.php";
        break;
    case "productdefect":
        $options['formTitle'] = "Do I have a case for my product defect injury?";
        $options['contactText'] = "Brief Description of your Product Defect Injuries:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_product-defect-service.php";
        break;
    case "drugdefect":
        $options['formTitle'] = "Do I have a defective drug case?";
        $options['contactText'] = "Brief Description of your Employment Situation:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_drug-defect-service.php";
        break;
    case "medicaldefect":
    $options['formTitle'] = "Do I have a medical defect case?";
    $options['contactText'] = "Brief Description of your Medical Defect Injuries:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_medical-defect-service.php";
        break;
    case "premisesliability":
        $options['formTitle'] = "Do I have a premises liability case?";
        $options['contactText'] = "Brief Description of Premises Liability Incident:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_premises-liability-service.php";
        break;
    case "employmentlaw":
        $options['formTitle'] = "Do I have a employment law violation case?";
        $options['contactText'] = "Brief Description of your Employment Situation:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_employment-law-service.php";
        break;
        case "wrongfuldeath":
        $options['formTitle'] = "Do I have a wrongful death case?";
        $options['contactText'] = "Brief Description of the Wrongful Death Circumstances:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_wrongful-death-service.php";
        break;  
        default:
        $options['formTitle'] = "Do I have a case for my personal injury?";
        $options['contactText'] = "Brief Description of your Personal Injury:";
        include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/JSON-LD/_personal-injury-service.php";
        break;  
}

if (!isset($options['canonical']) || $options['canonical'] == "") {
    $options['canonical'] = ('https://www.bestattorney.com'.$noparamuri); /* Set Canonical Link and Remove URL Parameters, only if $canonicalurl is not already set before header loads. */
} 

if ($environment == "development") {
    $options['noLiveChat'] = true;
}


?>