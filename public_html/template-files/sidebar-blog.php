<div id="sidebar">

	<?php 
	if ($options["showSidebarContactForm"])   {
		include $_SERVER['DOCUMENT_ROOT']."/template-files/partials/contact-forms/_sidebar-contact-form.php";
	} 

	if ($options["sidebarContentHtml"] != "") { ?>

		<div id="sb-custom-wrap" class="">
			<?php if ($options["sidebarContentTitle"]) { echo '<p class="title">'.$options["sidebarContentTitle"].'</p>'; } ?>
			<?php echo $options["sidebarContentHtml"]; ?>
		</div>
		<div class="clear"></div>

	<?php }

	if ($options['isCategory'] || $options['isBlogHome']) { ?>

		<p class="rc-title"><?php bloginfo('name'); ?><a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?>" id="blog-home"></a></p>
		<div id="sb-custom-wrap">
			<p class="title">Recent Posts</p>
			<ul>
				<? $posts = get_posts('numberposts=6&order=DESC&orderby=post_date');
				foreach ($posts as $post) : start_wp(); ?>
					<li>
						<a href="<?php the_permalink() ?>">
							<?php 
							if (has_post_thumbnail()) {
								the_post_thumbnail(array(65), array( 'class' => 'alignleft recent-posts-image' ));
							} ?>
						</a>
						<a href="<?php the_permalink() ?>">
							<?php 
							the_title(); ?>
						</a>
						<br>
						<p class="recent-posts-category">Category: <?php the_category(", "); ?></p>
						<div class="clearfix"></div>
					</li>
					<?php endforeach;  ?>
			</ul>
			<ul>
				<li><a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?>" class="all-blog">&raquo; Personal Injury Blog Home</a></li>
				<!-- <li><a href="https://www.bestattorney.com/search.php" title="Blog Search" class="all-blog">&raquo; Search Our Blog</a></li> -->
			</ul>									
		</div>	

		<div id="categories-list" class="nomobile">
			<p class="title">Categories List</p>
			<ul>
			<?php 
			    $args = array(
				'show_option_all'    => '',
				'orderby'            => 'name',
				'order'              => 'ASC',
				'style'              => 'list',
				'show_count'         => 0,
				'hide_empty'         => 1,
				'use_desc_for_title' => 1,
				'child_of'           => 0,
				'feed'               => '',
				'feed_type'          => '',
				'feed_image'         => '',
				'exclude'            => '',
				'exclude_tree'       => '',
				'include'            => '',
				'hierarchical'       => 0,
				'title_li'           => __( '' ),
				'show_option_none'   => __( 'No categories' ),
				'number'             => null,
				'echo'               => 1,
				'depth'              => 1,
				'current_category'   => 0,
				'pad_counts'         => 0,
				'taxonomy'           => 'category',
				'walker'             => null
			    );
			    wp_list_categories( $args ); 
			?>
			</ul>
		</div>
		<br>

	<?php } ?>

	<?php if ($options['isBlogSingle'] == true) { ?>
		<p class="rc-title"><?php bloginfo('name'); ?><a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?>" id="blog-home"></a></p>
		<div id="sb-custom-wrap">
			<p class="title" style="padding:0 20px;">Recent Posts in <?php echo $category;?></p>
			<ul>
					<?php
					$args = array(
						'category_name'    => $category,
					);
					$posts = get_posts($args);
					foreach ($posts as $post) : start_wp(); ?>
						<li>
							<a href="<?php the_permalink() ?>">
								<?php if (has_post_thumbnail()) { ?>
									<img width="65" height="65" data-src="<?php echo get_the_post_thumbnail_url(); ?>" class="alignleft recent-posts-image wp-post-image" alt="<?php the_title(); ?>">
									<!-- the_post_thumbnail(array(65), array( 'class' => 'alignleft recent-posts-image' )); -->
								<?php } ?>
							</a>
							<a href="<?php the_permalink() ?>">
								<?php 
								the_title(); ?>
							</a>
							<br>
							<div class="clearfix"></div>
						</li>
					<?php endforeach; ?>
			</ul>
			<ul>
				<li><a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?>" class="all-blog">&raquo; Personal Injury Blog Home</a></li>
				<!-- <li><a href="https://www.bestattorney.com/search.php" title="Blog Search" class="all-blog">&raquo; Search Our Blog</a></li> -->
			</ul>
		</div>
	<?php } ?>

	
	<br>
	<div id="year-list" class="nomobile">
		<p class="title">Post Archives by Year</p>
		<ul id="blog-year-list">
		<?php $args = array(
			'type'            => 'yearly',
			'limit'           => '',
			'format'          => 'html', 
			'before'          => '<span class="archive-bullet"> &bull; </span>',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 1,
			'order'           => 'DESC'
		); ?>
			<?php wp_get_archives( $args ); ?>
		</ul>
	</div>

	<?php if (isset($options['extraSidebar'])) {
		echo $options['extraSidebar'];
	} 

	if (isset($options['sidebarLinks'])) { ?>

	    <p class="rc-title"><?php echo $options['sidebarLinksTitle']; ?></p>
	    <div class="related-content">
	        <ul>
	            <?php subfooterLinks($options['sidebarLinks'], $noparamuri, true); ?>

	    <div class="clear"></div>

	<?php }

	if (isset($options['caseResultsArray'])) {
		switch (count($options['caseResultsArray'])) {
			case 1:
				// echo "caseresults: ".var_dump($options['caseResultsArray']);
				printCaseResults($options['caseResultsArray'][0]);
				break;
			case 2:
				printCaseResults($options['caseResultsArray'][0], $options['caseResultsArray'][1]);
				break;
			case 3:			
				printCaseResults($options['caseResultsArray'][0], $options['caseResultsArray'][1], $options['caseResultsArray'][2]);
				break;
		}

		echo '<div class="clear"></div>';

	}
	

	if (isset($options['videosArray']) && $options['videosArray'] != []) {
		echo '<div id="sb-videos">';
	    echo '<p class="title">Videos</p>';

	    $videoIndex = 1;
	    
		foreach ($options['videosArray'] as $video) {
			if(strlen($video[0]) > 46) {
	    		$fontClass = "font12";
	    	} elseif (strlen($video[0]) > 40) {
	    		$fontClass = "font12";
	    	} else {
	    		$fontClass = "font15";
	    	}

	    	$videoLink = "https://www.youtube.com/embed/".$video[2]."?autoplay=1&wmode=transparent&enablejsapi=1&rel=0"; ?>

			<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
				<meta itemprop="embedURL" content="<?php echo $videoLink;?>" />
						 <meta itemprop="duration" content="T3M22S" />
					      <meta itemprop="uploadDate" content="2005-03-30"/>
				
                <a itemprop="thumbnailUrl" href="<?php echo $videoLink;?>" class="video-<?php echo $videoIndex; ?> sb-video-link" data-jslghtbx title="<?php echo $video[0];?>" data-jslghtbx data-jslghtbx-index="$videoIndex">
                	<img  data-src="<?php if($video[1]) { echo $video[1]; } else { echo('/images/video-images/video-image-'.rand(5,8).'.jpg');}?>" alt="<?php echo $video[0]." Video Thumbnail"; ?>" />
                </a>
                <?php if($video[0]) {
                	echo ("<div   itemprop='description' class='video$videoIndex videooverlay'><h4 itemprop='name' itemprop='description' class='$fontClass'>");
                		echo $video[0];
                	echo ("</h4></div>");
        		} ?>
            </div>

		<?php 
			$videoIndex++;
		}

		echo '</div>';
	} 

	if (isset($options['reviewsArray'])) {
		switch (count($options['reviewsArray'])) {
			case 1:
				getReviews($options['reviewsArray'][0]);
				break;
			case 2:
				getReviews($options['reviewsArray'][0], $options['reviewsArray'][1]);
				break;
			case 3:			
				getReviews($options['reviewsArray'][0], $options['reviewsArray'][1], $options['reviewsArray'][2]);
				break;
		}

		echo '<div class="clear"></div>';
	} ?>

</div><!-- End Sidebar -->
</div><!-- End Centered-Content-Box -->
<div class="clear"></div>