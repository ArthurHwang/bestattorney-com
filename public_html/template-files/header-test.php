<style>
	body {
		background-color:#e6dec3;
	}
	#title-head {
	    width: auto;
	    background: #ffffff;
	    min-height: 124px;
	}
	#title-head-inner {
	    /* width: 1100px; */
      /* margin: 0px auto; */
      margin-left: 114px;
	}
	#navbar {
	    width: 100%;
	    height: 53px;
	    position: absolute;
	    background: #222533;
	    z-index: 2000;
	}
	#navbar-inner {
	    width: 1100px;
	    height: 38px;
      /* margin: 0px auto 0px auto; */
      margin-left: 100px;
	    font-size: 12px;
	    box-sizing: border-box;
	}
	#icons-bar {
    position: relative;
    left: 500px;
	    display:flex;
	    justify-content: space-between;
	    align-content: center;
	    padding: 1px 10px;
	    z-index:500;
	    width:100%;
		box-sizing: border-box;
		line-height: 30px;
  }
  #icons-bar #search {
    margin: 0 10px;
  }
	ul#menu li {
	    float: left;
	    line-height: 54px;
	    text-transform: uppercase;
	}
	ul#menu li ul {
	    position: absolute;
	    display: block;
	    width: 215px;
	    left: -700em;
	    /* using left instead of display to hide menus because display: none isn't read by screen readers */
	    padding: 35px 40px 40px 40px;
	    margin: 0px 0px 0px 0px;
	    z-index: 1000;
	    background: #f5f3e9;
	    border-bottom: none;
	    font-size: 12px;
	    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",Helvetica, sans-serif;
	}
	<?php if (isset($options['headerImageClass'])) {
  echo ("
	        #centered-content-box, #content-box {
	            background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg.jpg') no-repeat center top !important;
	        }
	        @media only screen and (min-width:641px) and (max-width:1100px) {
	            #centered-content-box, #content-box {
	                background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg-tablet.jpg') no-repeat center top !important;
	            }
	        }
	        @media only screen and (min-width:1px) and (max-width:640px) {
	            #centered-content-box, #content-box {
	                background: url('/images/header-images/" . $options['headerImageClass'] . "-content-bg-mobile.jpg') no-repeat center top !important;
	            }
	        }");
} ?>
</style>

<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/site-options/meta-options.php");

/***********************************************
 Extra Page Options Set Here For The Whole Site.
 ***********************************************/

include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/site-options/sitewide-options.php");

/***********************************************
 All Header HTML & scripts
 ***********************************************/
?>

<!-- Base Element that is used by MaxCDN to reset all relative paths to the CDN -->
<?php if ($environment == "development") { ?>
	<base href=""/>
<?php

} else { ?>
	<base href="https://https-webfiles-bisnarchaseperso.netdna-ssl.com/"/>
<?php

} ?>

<!-- Normal Meta Info -->

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
<meta name="google-site-verification" content="AxlGywVV4zElv2U8uVF9nK59Gxg9Ug4_kNrmdQqQHyg" />

<?php
if (isset($options['noIndex']) && $options['noIndex'] == true) { ?>
	<meta name="robots" content="noindex" />
<?php

}

if (isset($options['searchWeight'])) { ?>
	<meta name="search-weight" content="<?php echo $options['searchWeight']; ?>" />
<?php

}

if (!$options['isBlog']) { ?>
	<meta property="og:url" content="<?php echo $options['canonical']; ?>" />

	<?php if (isset($options['ogTitle'])) { ?>
	<meta property="og:title" content="<?php echo $options['ogTitle']; ?>" />
	<?php

}

if (isset($options['ogDescription'])) { ?>
	<meta property="og:description"content="<?php echo $options['ogDescription']; ?>" />
	<?php

}

if (isset($options['ogImage'])) { ?>
	<meta property="og:image" content="<?php echo $options['ogImage']; ?>" />
	<?php

}
} ?>

<link rel="canonical" href="<?php echo $options['canonical']; ?>">

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WK3JJ3');</script>
<!-- End Google Tag Manager -->
<!-- Universal Analytics script included, cannot fire from GTM because GTM does not support plugins like autotrack-->
<!-- <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44427913-2', {'siteSpeedSampleRate': 100});
  ga('require', 'GTM-TQQVDKB');
  ga('require', 'autotrack');
  ga('send', 'pageview');

</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44427913-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-44427913-2', { 'optimize_id': 'GTM-TQQVDKB'});
</script>

<?php

/***********************************************
 Custom Header Pages.
 ***********************************************/
if ($options['isSpanish']) {
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-spanish.php");
} else if ($options['isHome']) {
  $noparamuri = "/";
  $options['canonical'] = 'https://www.bestattorney.com/';
  $options['fullWidth'] = true;
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-home-test.php");
} else if ($options['isBlog']) {
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-blog.php");
} else if ($options['isContact']) {
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-contact.php");
} else if ($options['isGeo'] || $options['isPa']) {
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-pa-geo.php");
} else {
  include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header-default.php");
}

?>

<!-- <script>
console.log('<?php echo ("page: " . $page . ", noparamuri: " . $noparamuri . ", thispage: " . $thispage . ", thisdir: " . $thisdir); ?>');
var noparamuri = "<?php echo $noparamuri; ?>";
</script> -->
