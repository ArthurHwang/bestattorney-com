<html lang="en">
<script>
  const pageSubject = "<?php echo $options['pageSubject']; ?>";
  const noparamuri = "<?php echo $noparamuri; ?>";
</script>

<style>
  @media only screen and (min-width: 1px) and (max-width: 640px) {

    #header-bar-right {
      display: none !important;
    }

    html body div#title-head-inner {
      height: initial !important;
    }

    html body div#title-head {
      height: initial !important;
    }

    #mobile-call-header p.button-title {
      color: #222533 !important;
      /* display: inline; */
    }

  }
</style>

</head>

<body id="template-pa-geo" lang="en-US">

  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>



  <!-- Main menu -->
  <div id="title-head">
    <div id="title-head-inner">
      <a href="https://www.bestattorney.com/" id="home-link" title="Bisnar|Chase - California Personal Injury Attorneys"></a>
      <div id="header-bar-right">
        <a href="tel:<?php echo $options['callPhone']; ?>" id="contact-link-text" class="" title="Call Today For Your Free Consultation!">
          <!-- <span id="call-text">Call now for Help!</span> -->
          <span class="phone-number fa-phone">
            <?php echo $options['phone']; ?>
          </span>
        </a>
        <a aria-label="Better Business Bureau"  id="topbbb" href="https://www.bbb.org/sdoc/business-reviews/lawyers/bisnar-chase-personal-injury-attorneys-in-newport-beach-ca-100046710/" target="_blank"></a><br>
        <a href="https://www.bestattorney.com/abogados/<?php echo $options['spanishPageEquivalent']; ?>" id="espanol">En Español</a>
        <!-- <a href="https://www.bestattorney.com/about-us/no-fee-guarantee-lawyer.html" id="no-win-no-fee" class="nomobile">No Win, No Fee - Guarantee</a> -->
      </div>
    </div>
  </div>

  <nav id="navbar">
    <div id="navbar-inner">
      <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_nav-icons-bar-general.php"); ?>
      <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/nav-files/nav-general.php"); ?>

      <div class="clear"></div>
      <div id="search-form">
        <form action="https://www.bestattorney.com/search.html" method="get">
          <p><input type="text" id="search-box" name="q" placeholder="type and hit enter" size="30" class="searchinput"></p>
        </form>
      </div>
    </div>
  </nav>

  <div class="clear"></div>

  <div id="content-pa" class="<?php echo $options[" headerImageClass"]; ?>">
    <div id="pa-background-container">
      <div id="pa-background-nested">
      </div>
    </div>
    <div id="centered-content-pa-box">
      <div id="pa-header">
        <div id="contact-form-panel">
          <p class="title">
            <?php echo $options['formTitle']; ?>
          </p>
          <p><span>Free Case Evaluation</span> - Our full time staff is ready to evaluate your case submission and will respond in a timely manner.</p>
          <?php include $_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/contact-forms/_practice-area-contact-form.php"; ?>
        </div>
      </div>
      <div id="free-consultations">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/contact-forms/_tab-dropdown-form.php"; ?>
        <a id="free-case-review-button-tablet" class="nomobile nopc free-case-review-button">Click for Your FREE Case Review</a>
        <a href="https://www.bestattorney.com/contact.html#contactform" class="nopc notablet free-case-review-button">Click for Your FREE Case Review</a>
      </div>
      <div id="mobile-call-header" class="pa-call">
        <a id="mobile-call-header-button" href="tel:<?php echo $options['callPhone']; ?>" class="contact-button nopc notablet">
          <p class="button-title fa-phone">
            CALL US TODAY
          </p>
          <p class="button-sub-title">
            And Ask For Your Free Consultation
          </p>
        </a>
      </div>
      <div id="<?php if ($options['fullWidth']) {
                  echo 'full-width';
                } else {
                  echo 'content';
                } ?>">