<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/preload.php");

include($_SERVER['DOCUMENT_ROOT'] . "/template-files/site-options/default-folder-options.php");

// ADD ALL OTHER PAGE OPTIONS HERE!
$options['searchWeight'] = 6;
$options['noIndex'] = true;
$options['fullWidth'] = true;
$options['noLiveChat'] = true;


// if ($_SERVER['REMOTE_ADDR'] != "209.234.159.178") {
//     header('HTTP/1.0 403 Forbidden');
//     exit("Your IP ".$_SERVER['REMOTE_ADDR']." does not match the correct IP. Please access this from the office IP address, or double check that we have not changed our IP address lately.");
// } 
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    require $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php'; // composer autoload
    $client = new \AlgoliaSearch\Client('VX0VUN8JFN', '4e30ddf002c2d2a2e42459523e61251e');
    $index = $client->initIndex('site_search');

    $url = $_POST['url'];
    $title = $_POST['title'];
    $imageUrl = $_POST['image'];
    $description = $_POST['description'];
    $weight = $_POST['weight'];
    $h1 = $_POST['h1'];
    $h2 = $_POST['h2'];
    $text = $_POST['text'];

    $allVars = [$url, $title, $description, $weight, $h1, $h2, $text];

    foreach ($allVars as $postVar) {
        if (!isset($postVar)) exit("You did not provide all the information needed. Please fill out the forms correctly.");
    }
    try {

        $index->addObject(
            [
                'url' => $url,
                'title' => $title,
                'image' => $imageUrl,
                'description' => $description,
                'weight' => $weight,
                'h1' => $h1,
                'h2' => $h2,
                'text' => $text
            ]
        );
    } catch (Exception $error) {
        error_log("Error from the Algolia search thing: ");
        error_log($error);
        header("HTTP/1.0 500 Internal Server Error");

        exit("There was a problem adding your record to the search index. Please check the error log.");
    }

    header("HTTP/1.0 200 OK");
    exit("Your record was added to the search index.");
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Add a page to the search index</title>
    <meta name="description" content="Our mission is to provide superior client representation in a compassionate and professional manner. Bisnar Chase has been winning cases for California plaintiffs for 40 years. No win, no fee guarantee and a 99% success rate for minor to serious injury cases.">

    <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header.php"); ?>


    <h1 id="h1title">Add a page to the search index:</h1>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/partials/_breadcrumbs.php"); ?>

    <p>If you've written a new page and it hasn't been entered into the index (ie. if you hit that little magnifying glass and type the title of your page and you can't find it in the results), then use this page to add it to the index!</p>
    <p>Enter all the information below. If you enter information incorrectly, it will be hard to correct it in the future, so please double check your work. </p>

    <p>This page is only accessible from the office IP address. If you try to submit something from home, it won't work. </p>

    <form id="submit-form">
        <label for="url">Page URL</label>
        <input type="text" name="url" placeholder="Please provide the full url starting with https://www.bestattorney.com/" required>

        <label for="title">Page Title</label>
        <input type="text" name="title" placeholder="This should be the same as your new page's title." required>

        <label for="imageUrl">Image URL (Optional) </label>
        <input type="text" name="image" placeholder="If this page has a primary image on it, enter the URL here.">

        <label for="description">Page Meta Description</label>
        <textarea name="description" placeholder="Enter your page's meta description here." required></textarea>
        <p></p>

        <label for="weight">Page Weight (Importance when Searching)</label>

        <input id="weight1" type="radio" name="weight" value="1" required><label class="radio-label" for="weight1">1</label>
        <input id="weight2" type="radio" name="weight" value="2" required><label class="radio-label" for="weight2">2</label>
        <input id="weight3" type="radio" name="weight" value="3" required><label class="radio-label" for="weight3">3</label>
        <input id="weight4" type="radio" name="weight" value="4" required><label class="radio-label" for="weight4">4</label>
        <input id="weight5" type="radio" name="weight" value="5" required><label class="radio-label" for="weight5">5</label>
        <p></p>
        <p>Information regarding search weight - Use this guide to determine what search weight value to add to this page.</p>
        <p>If this page is a....</p>
        <ul>
            <li>Main Practice Area Index Pages: <strong>1</strong></li>
            <li>Main Locations (eg. /mission-viejo/index.html): <strong>1</strong> </li>
            <li>Main Practice Area Other Pages: <strong>2</strong></li>
            <li>Resources: <strong>2</strong></li>
            <li>Location Other Folder Pages (eg. /mission-viejo/car-accidents.html): <strong>3</strong></li>
            <li>One-off Locations (locations/santa-monica-personal-injury.html: <strong>4</strong></li>
            <li>Others (highway locations, press releases, CMVC Pages): <strong>4</strong></li>
            <li>Blogs: <strong>5</strong> </li>
        </ul>
        <p></p>
        <label for="h1">Page H1</label>
        <input type="text" name="h1" placeholder="Enter the H1 Here"><br>

        <label for="h2">Page H2s</label>
        <input type="text" name="h2" placeholder="Enter multiple H2s here, separated by commas."><br>

        <label for="text">Page Text Snippet</label>
        <textarea type="text" name="text" placeholder="Enter a little bit of the page content (Max 2500 chars)" maxlength="2500"></textarea><br>

        <button type="submit">Submit Page to Index</button><br>
        <p id="submit-form-messages"></p>
    </form>

    <?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/side-footer.php"); ?>
    <style>
        label {
            display: block;
        }

        label.radio-label {
            display: inline-block;
            padding: 6px 30px 6px 5px;
            position: relative;
            bottom: 1px;
            cursor: pointer;
            left: 4px;
            font-size: 18px;
        }

        input[type=radio] {
            cursor: pointer;
        }

        input[type=text] {
            border: 1px solid #555;
            width: 500px;
            background-color: #fbf8f3;
            display: block;
        }

        textarea {
            width: 500px;
            min-height: 125px;
            background-color: #fbf8f3;
        }

        .ajax-success {
            color: green;
        }

        .ajax-failure {
            color: red;
        }
    </style>

    <script>
        $(function() {
            $("#submit-form").submit(function(e) {
                e.preventDefault();
                let postData = $("#submit-form").serialize();

                $.ajax({
                    method: "POST",
                    data: postData,
                    url: "https://www.bestattorney.com/template-files/scripts/add-page-to-index.php"
                }).done(function(data) {
                    console.log("Success!");
                    $("#submit-form-messages").addClass("ajax-success").removeClass("ajax-failure").text(data);
                }).fail(function(data) {
                    console.log("Failure");
                    $("#submit-form-messages").addClass("ajax-failure").removeClass("ajax-success").text(data);
                });
            });
        });
    </script>