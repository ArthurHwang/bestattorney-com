<?php

ini_set("error_log", "../log/text-message.log");
date_default_timezone_set('America/Los_Angeles');
// This page will take an incoming text and store it in a database.
// Then it will check if texting number has sent a text in the past week, and if not, it will make a request to IFTTT to send out an autoresponder email.
include $_SERVER['DOCUMENT_ROOT'] . "/../misc/text-database-creds.php";

if ($_SERVER['REQUEST_METHOD'] == "POST") {

  error_log("");
  error_log("======================================================");
  error_log("received POST to save-intake-text.php");
  $fromAddress = $_POST['from'];
  $subject = $_POST['subject'];
  $message = $_POST['body'];
  error_log("fromAddress: $fromAddress");
  error_log("phone number: $subject");
  error_log("message: $message");

  $phoneNumber = str_replace(str_split("-() "), "", $subject);
  $phoneNumber = substr($phoneNumber, -10);
  error_log("str replaced phone number, now: $phoneNumber");

  $time = time();
  $weekAgo = $time - 604800;
  $hour = intval(date("G"));
  $day  = date("w");

  if (!$phoneNumber || !$message || !$fromAddress) {
    error_log("Error: no phone number or message or fromAddress");
    error_log("======================================================");
    error_log("");
    exit;
  }

  //Connect to the database
  try {
    $link = new PDO('mysql:dbname=bestatto_intake_texts;host=localhost;', 'bestatto_txt_msg', $textDatabasePass);
    $link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    error_log("Error Connecting to the database: " . $e);
  }

  error_log("hour is : $hour"); //
  error_log("day is : $day"); // 1 is monday, 7 is sunday. or 0 is sunday?
  if (($hour < 8 || $hour > 16) || $day < 1 || $day > 5) {
    $sendAutoResponder = true;

    // Query to see if phone number had texted before.
    // TECHNICALLY THERE IS A BUG HERE. IT DOESN'T ACTUALLY CHECK IF WE HAVE SENT OUT AN AUTORESPONDER, IT JUST CHECKS IF THE CLIENT HAS SENT A TEXT IN THE LAST WEEK.
    // This means that a client could send a text every 6 days outside of business hours and would only ever get the autoresponder the first time.
    // We should fix this somehow. Add a "autoresponder sent?" field to the database? maybe.
    try {
      $selectQuery = "SELECT `timestamp` FROM `messages` WHERE `phone_number` = ? ORDER BY `timestamp` DESC LIMIT 1";
      $preparedSelectQuery = $link->prepare($selectQuery);
      if ($preparedSelectQuery->execute([$phoneNumber])) {
        while ($row = $preparedSelectQuery->fetch()) {
          if (!$row) {
            error_log("No records found");
            break;
          }

          error_log("retrieved record from DB: " . $row);
          $recordTime = strtotime($row['timestamp']);
          error_log("recordTime: $recordTime<br>");
          error_log("weekago: $weekAgo<br>");

          if ($recordTime > $weekAgo) {
            $sendAutoResponder = false;
            error_log("It's been less than a week, don't send the autoresponder</br>");
          }
        }
      } else {
        throw new Exception('Error selecting text messaging records from the database');
      }
    } catch (PDOException $e) {
      error_log($e);
    }
  } else {
    error_log("it is during work hours -  no autoresponder");
  }

  // Query to store text message in the database

  try {
    $insertQuery = "INSERT INTO `messages` (`phone_number`, `message`) VALUES (?, ?)";
    $preparedInsertQuery = $link->prepare($insertQuery);

    if ($preparedInsertQuery->execute([$phoneNumber, $message]) == false) {
      throw new Exception('Error Inserting Text Message into the Database');
    }
  } catch (PDOException $e) {
    error_log($e);
  }


  // if phone number has not texted, send out auto-responder
  if ($sendAutoResponder) {
    error_log("reached this point to send out the autoresponder.");
    //  exec(`/usr/bin/curl -X POST -H "Content-Type: application/json" -d '{"value1":"hellloooo","value2":"subject"}' https://maker.ifttt.com/trigger/send_autoresponse/with/key/po8NdGrZqb5Zw_O6GTrySFBVQxRGKeruQqnj5VWkorB`, $output, $return_var);
    // $output = `/usr/bin/curl -X POST -H "Content-Type: application/json" -d '{"value1":$fromAddress,"value2":$subject, "value3":$message}' https://maker.ifttt.com/trigger/send_autoresponse/with/key/po8NdGrZqb5Zw_O6GTrySFBVQxRGKeruQqnj5VWkorB`;
    exec('/usr/bin/curl -X POST -H "Content-Type: application/json" -d \'{"value1":$fromAddress,"value2":$subject,"value3":$message}\' https://maker.ifttt.com/trigger/send_autoresponse/with/key/po8NdGrZqb5Zw_O6GTrySFBVQxRGKeruQqnj5VWkorB', $output, $return_var);
    // exec('/usr/bin/curl -X POST -H "Content-Type: application/json" -d \'{"value1":$fromAddress,"value2":$subject,"value3":$message}\' https://wh.automate.io/webhook/5c93f9893b1f3067f7aea4c8', $output, $return_var);
    error_log("fromAddress: $fromAddress");
    error_log("subject: $subject");
    error_log("message: $message");
    error_log("output: $output");
    error_log(print_r($output));
    error_log(var_dump($output));
    error_log("returnvar: $return_var");
  }
} else {
  error_log("");
  error_log("======================================================");
  error_log("SOMEONE HAS TRIED ACCESSING SAVE-INTAKE-TEXT.PHP WITHOUT USING A POST REQUEST");
  error_log("======================================================");
  error_log("");
  header("HTTP/1.0 403 Forbidden");
  exit();
}
error_log("======================================================");
error_log("");
header("HTTP/2.0 200 OK");
