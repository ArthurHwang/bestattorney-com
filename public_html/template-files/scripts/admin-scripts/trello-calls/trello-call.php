<?php 


class TrelloCall {
    
    public $lastResponseHeader;
    
    public function __construct() {
        
    }

    public function makeCurlRequest($url, $queryParameters = false) {
        $lastHeader = "";
        $handleHeader = function($curl, $header_line) use ($lastHeader) {
            $lastHeader .= $header_line."<br>";
            return strlen($header_line);
        };

        include $_SERVER["DOCUMENT_ROOT"]."/../misc/trello-api-creds.php";
        $url .= $queryParameters ? "&" : "?";
        $url .= "key=$trelloApiKey&token=$trelloToken";

        $chObj = curl_init();
        curl_setopt($chObj, CURLOPT_URL, $url);
        curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
        curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chObj, CURLOPT_HEADERFUNCTION, $handleHeader);
        curl_setopt($chObj, CURLOPT_VERBOSE, true);
        curl_setopt($chObj, CURLOPT_HTTPHEADER,
            array(
                    'User-Agent: PHP Script',
                    'Content-Type: application/json;charset=utf-8'
                )
            ); 

        $response = curl_exec($chObj);
        $this->lastResponseHeader = $lastHeader;
        curl_close($chObj);
        return json_decode($response);
    }
}