<?php
// Gets the current sprint information from trello. 
// 1.Get all members. 
// 2. Get member/id/list/id/cards
// 
error_reporting(E_ALL);

include ("./trello-call.php");

$trelloCall = new TrelloCall();

$getUsersUrl = "https://api.trello.com/1/organizations/bisnarchasemarketing/members?fields=id,fullName,username,avatarUrl,url";
$trelloUsers = $trelloCall->makeCurlRequest($getUsersUrl, true);

$getCardsUrl = "https://api.trello.com/1/boards/Jl3BFF96/?cards=open&card_fields=name,shortUrl,idMembers,idList,dateLastActivity,desc";
$cards = $trelloCall->makeCurlRequest($getCardsUrl, true);

$getListsUrl = "https://api.trello.com/1/boards/Jl3BFF96/lists";
$lists = $trelloCall->makeCurlRequest($getListsUrl, false);

foreach($lists as $list) {
    $list->cards = [];
}
unset($list);


// This is definitely best practices. 
for($i = 0; $i < count($trelloUsers); $i++) {
    // echo("<br>============= START NEW LOOP OF USERS ================<br>");

    $user = $trelloUsers[$i];
    // echo("User: ".$user->fullName."</br>");

    $user->lists = [];
    foreach($lists as $list) {
        array_push($user->lists, clone($list) );
    }
    unset($list);

    // echo("finished adding lists. User->lists: <br>");
    // echo("===================== Lists ============== <br>");
    // echo("<pre>");
    // print_r($user->lists);
    // echo("</pre>");
    // echo("===================== /Lists ============== <br>");

    $user->cards = [];
    foreach($cards->cards as $card) {
        foreach($card->idMembers as $member) {
            if ($member == $user->id) {
                array_push($user->cards, $card);
                break;
            } 
        }
        unset($member);
    }
    unset($card);

    // echo ("<br>finished adding cards to user->cards:<br>");
    // echo("===================== User->Cards ============== <br>");
    // echo("<pre>");
    // print_r($user->cards);
    // echo("</pre>");
    // echo("===================== /User-> ============== <br>");

    // echo ("starting adding user->cards to user->lists>list:<br>");
    // echo("&nbsp;&nbsp;===================== User->lists->list ============== <br>");
    // echo("&nbsp;&nbsp;User: ".$user->fullName."</br>");
    
    foreach($user->cards as $card) {
        // echo("&nbsp;&nbsp;&nbsp;&nbsp;card to be sorted: <strong>".substr($card->name,0,30)."...</strong></br>");
        foreach($user->lists as $list) {
            // echo("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;list to check: ".$list->name."</br>");
            if ($card->idList === $list->id) {
                // echo("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Card is in list! Adding card to user->list->cards.<br><br>");
                array_push($list->cards, $card);
                break;
            } else {
                // echo("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Card is not in list: ".$list->name.". Next List. </br>");
            }
        }
        unset($list);
    }
    unset($card);

    // echo("&nbsp;&nbsp;===================== /User->lists->list ============== <br>");

    // echo("<br>===================== END USER =======================<br><br><br><br>");
}

$returnObject = [
    "users" => $trelloUsers,
    "cards" => $cards
    // "lists" => $lists
];

// echo("<pre>");
// print_r($returnObject);
// echo("</pre>");

header("HTTP/1.0 200 OK");
exit(json_encode($returnObject));