<?php 

$postData = json_decode(file_get_contents("php://input"));

$startDate = new DateTime($postData->startDate);
$endDate = new DateTime($postData->endDate);

if (!$startDate || !$endDate) {
    header("HTTP/1.0 400 Error");
    exit("Please set a start and end date!");
}

$startDateUnix = $startDate->getTimestamp();
$endDateUnix = $endDate->getTimestamp();

$signups = [];
// Read newsletter-log.txt line by line
$handle = fopen($_SERVER['DOCUMENT_ROOT']."/../misc/newsletter_log.txt", "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        // process the line read.
        
        $lineArray = explode(" ", $line);
        if (count($lineArray) > 1) {
            $date = new DateTime($lineArray[0]);
            $dateUnix = $date->getTimestamp();

            if ($dateUnix >= $startDateUnix && $dateUnix <= $endDateUnix) {
                array_push($signups, $lineArray);
            }

        }

    }

    fclose($handle);
} else {
    // error opening the file.
    error_log("Error opening Newsletter_log from get-newsletter-signups.php");
} 

header("HTTP/1.0 200 OK");
exit(json_encode($signups));