<?php
include ('./github-graphql.php');

class GithubCommits extends GithubGraphQL {
    protected $getCommitsQuery;
    protected $getCommitsVariables;
    protected $commitsEndCursor = null;

    protected $getRecentCommitsQuery;

    protected $startDate;
    protected $endDate;

    protected $gitCommits = [];

    public function __construct() {
        // This Class automatically sets the date range to last month. 
        // To set the date range, call setDateRange();
        $this->startDate = $this->getLastMonthStart();
        $this->endDate = $this->getLastMonthEnd();
    }

    public function setDateRange($startDate, $endDate) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getCommits() {
        $this->createGetCommitsQuery();

        $morePages = true;
        while($morePages) {
            $i = 0;
            $this->setVariables();
            $response = $this->makeCurlRequest($this->getCommitsQuery, $this->getCommitsVariables);
            $this->commitsEndCursor = $response->data->repository->ref->target->history->pageInfo->endCursor;
            $commits = $response->data->repository->ref->target->history->edges;
            $this->gitCommits = array_merge($this->gitCommits, $commits);

            if ($response->data->repository->ref->target->history->pageInfo->hasNextPage != 1) {
                $morePages = false;
            }

            if ($i > 10) {
                error_log("GitHubCommits->getCommits(): Breaking while loop this has gone far enough.");
                break;
            }
            $i++;
        }
    
        return $this->gitCommits;

    }

    public function getRecentCommits($numberOfCommits) {
        $this->createGetRecentCommitsQuery();
        $response = $this->makeCurlRequest($this->getRecentCommitsQuery, ["firstCommits" => $numberOfCommits]);
        $commits = $response->data->repository->ref->target->history->edges;
        return $commits;
    }

    // Sets up a GraphQL query to get commits. DON'T INDENT THIS
    protected function createGetCommitsQuery() {
        
        $this->getCommitsQuery = <<<'JSON'
query($cursor: String, $startDate: GitTimestamp, $endDate: GitTimestamp) {
    repository(owner: "bestattorney", name: "bestattorney.com") {
        ref(qualifiedName: "master") {
            target {
                ... on Commit {
                    id
                    history(first: 100, since: $startDate, until: $endDate, after: $cursor) {
                        totalCount
                        pageInfo {
                            hasNextPage
                            endCursor
                        }
                        edges {
                            node {
                                messageHeadline
                                author {
                                    name
                                    date
                                    avatarUrl
                                }
                                changedFiles
                                additions
                                deletions
                            }
                            cursor
                        }
                    }
                }
            }
        }
    }
}
JSON;
    } /*End createGetCommmitsQuery */

    protected function createGetRecentCommitsQuery() {
        
        $this->getRecentCommitsQuery = <<<'JSON'
query($firstCommits: Int!) {
    repository(owner: "bestattorney", name: "bestattorney.com") {
        ref(qualifiedName: "master") {
            target {
                ... on Commit {
                    id
                    history(first: $firstCommits) {
                        edges {
                            node {
                                messageHeadline
                                author {
                                  name
                                  date
                                  avatarUrl
                                }
                              	changedFiles
                              	additions
                              	deletions
                            }
                        }
                    }
                }
            }
        }
    }
}
JSON;
    } /*End createGetCommmitsQuery */

    protected function getLastMonthStart() {
        $datetime = new DateTime();
        $startOfMonth = strtotime("first day of last month");
        $datetime->setTimestamp($startOfMonth);
        return $datetime->format(DateTime::ATOM);
    }

    protected function getLastMonthEnd() {
        $datetime = new DateTime();
        $endOfMonth = strtotime("last day of last month");
        $datetime->setTimestamp($endOfMonth);
        return $datetime->format(DateTime::ATOM);
    }

    private function setVariables() {
        $this->getCommitsVariables = [
            'cursor' => $this->commitsEndCursor,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate
        ];
    }
}