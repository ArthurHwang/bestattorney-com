<?php

Class GithubGraphQL {
    
    protected $lastResponseHeader = ""; 

    public function __construct() {

    }

    protected function makeCurlRequest($query, $variables = "") {
        $lastHeader = "";
        $handleHeader = function($curl, $header_line) use ($lastHeader) {
            $lastHeader .= $header_line."<br>";
            return strlen($header_line);
        };

        $json = json_encode(['query' => $query, 'variables' => $variables]);

        $chObj = curl_init();
        curl_setopt($chObj, CURLOPT_URL, 'https://api.github.com/graphql');
        curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
        curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
        // curl_setopt($chObj, CURLOPT_HEADER, true);
        curl_setopt($chObj, CURLOPT_HEADERFUNCTION, $handleHeader);
        curl_setopt($chObj, CURLOPT_VERBOSE, true);
        curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
        curl_setopt($chObj, CURLOPT_HTTPHEADER,
            array(
                    'User-Agent: PHP Script',
                    'Content-Type: application/json;charset=utf-8',
                    'Authorization: bearer 051371695949579c9d2602d97dca5a5e713db61b'
                )
            ); 

        $response = curl_exec($chObj);
        $this->lastResponseHeader = $lastHeader;
        curl_close($chObj);
        return json_decode($response);
    }


}