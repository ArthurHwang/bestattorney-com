<?php

class GitUser {
    public $name;

    public $totalCommits;
    public $totalFileChanges;
    public $totalAdditions;
    public $totalDeletions;
    public $averageFileChanges;
    public $averageAdditions;
    public $averageDeletions;
    public $averageLineChangeSum;
    
    public function __construct($name) {
        $this->name = $name;
    }

    public function summateData($changedFiles, $additions, $deletions) {
        $this->totalCommits++;
        $this->totalFileChanges += $changedFiles;
        $this->totalAdditions += $additions;
        $this->totalDeletions += $deletions;
    }

    public function calculateMetaData() {
        if ($this->totalCommits > 0) {
            $this->averageFileChanges = floor($this->totalFileChanges/$this->totalCommits);
            $this->averageAdditions = floor($this->totalAdditions/$this->totalCommits);
            $this->averageDeletions = floor($this->totalDeletions/$this->totalCommits);
            $this->averageLineChangeSum = floor(($this->totalAdditions - $this->totalDeletions) / $this->totalCommits);
        }
    }

    public function getTotalCommits() {
        return $this->totalCommits;
    }

    public function getTotalFileChanges() {
        return $this->totalFileChanges;
    }

    public function getTotalAdditions() {
        return $this->totalAdditions;
    }

    public function getTotalDeletions() {
        return $this->totalDeletions;
    }

    public function getAverageFileChanges() {
        return $this->averageFileChanges;
    }

    public function getAverageAdditions() {
        return $this->averageAdditions;
    }

    public function getAverageDeletions() {
        return $this->averageDeletions;
    }

    public function getAverageLineChangeSum() {
        return $this->averageLineChangeSum;
    }

    
}