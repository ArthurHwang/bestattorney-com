<?php

class GithubStats {
    private $usersArray;

    public $mostCommitsUser;
    public $mostCommits;

    public $mostFileChangesUser;
    public $mostFileChanges;

    public $mostAdditionsUser;
    public $mostAdditions;

    public $mostDeletionsUser;
    public $mostDeletions;

    // Pass in an array of GithubUsers
    public function __construct($usersArray) {
        $this->usersArray = $usersArray;
        $this->calculateStats();
    }

    public function getUser($identifier) {
        if (gettype($identifier) == "String") {
            foreach($usersArray as $user) {
                if ($user->name == $identifier) return $user;
            }
            return false;
        } else if (gettype($identifier) == "Number") {
            return $usersArray[$identifier];
        } else {
            echo("Type of identifier is: ".gettype($identifier));
            return false;
        }
    }

    private function calculateStats() {
        $this->getMostCommits();
        $this->getMostFileChanges();
        $this->getMostAdditions();
        $this->getMostDeletions();
    }

    // Returns the GitUser with the most commits
    private function getMostCommits() {
        $maxCommits = 0;
        $maxUser = null;
        foreach($this->usersArray as $user) {
            $userCommits = $user->getTotalCommits();
            if ($userCommits >= $maxCommits) {
                $maxUser = $user;
                $maxCommits = $userCommits;
            }
        }
        $this->mostCommitsUser = $maxUser;
        $this->mostCommits = $maxCommits;
    }

    // Returns the GitUser with the most file changes
    private function getMostFileChanges() {
        $maxFileChanges = 0;
        $maxUser = null;
        foreach($this->usersArray as $user) {
            $userFileChanges = $user->getTotalFileChanges();
            if ($userFileChanges >= $maxFileChanges) {
                $maxUser = $user;
                $maxFileChanges = $userFileChanges;
            }
        }

        $this->mostFileChangesUser = $maxUser;
        $this->mostFileChanges = $maxFileChanges;

    }

    // Returns the GitUser with the most line changes
    private function getMostAdditions() {
        $maxAdditions = 0;
        $maxUser = null;
        foreach($this->usersArray as $user) {
            $userAdditions = $user->getTotalAdditions();
            if ($userAdditions >= $maxAdditions) {
                $maxUser = $user;
                $maxAdditions = $userAdditions;
            }
        }
        
        $this->mostAdditionsUser = $maxUser;
        $this->mostAdditions = $maxAdditions;
    }

    // Returns the GitUser with the most line deletions
    private function getMostDeletions() {
        $maxDeletions = 0;
        $maxUser = null;
        foreach($this->usersArray as $user) {
            $userDeletions = $user->getTotalDeletions();
            if ($userDeletions >= $maxDeletions) {
                $maxUser = $user;
                $maxDeletions = $userDeletions;
            }
        }
        $this->mostDeletionsUser = $maxUser;
        $this->mostDeletions = $maxDeletions;
    }

}