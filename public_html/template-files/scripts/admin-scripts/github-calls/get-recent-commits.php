<?php 

include ('./github-commits.php');
include ('./git-user.php');
include ('./github-stats.php');

$github = new GithubCommits();
$postData = json_decode(file_get_contents("php://input"));

// error_log(print_r($postData,true));

$startDate = $postData->startDate;
$endDate = $postData->endDate;
// error_log("got dates:");
// error_log($startDate);
// error_log($endDate);

$github->setDateRange($startDate, $endDate);

$allCommits = $github->getCommits();
$recentCommits = $github->getRecentCommits(5);

$Matt = new GitUser("Matt");
$Kristi = new GitUser("Kristi");
$Ryan = new GitUser("Ryan");
$Bobby = new GitUser("Bobby");
$Monica = new GitUser("Monica");
$Marketing = new GitUser("Marketing");

$usersArray = [$Matt, $Kristi, $Ryan, $Bobby, $Monica, $Marketing];

foreach($allCommits as $commit) {
    if(substr($commit->node->messageHeadline,0,5) == "Merge") {
        error_log(substr($commit->node->messageHeadline,0,5));
        break;
    }
    $changedFiles = $commit->node->changedFiles; 
    $additions = $commit->node->additions;
    $deletions = $commit->node->deletions;

    switch($commit->node->author->login) {
        case "mattcheah":
            $userIndex = 0;
            break;
        case "4kristi":
            $userIndex = 1;
            break;
        case "insarov":
            $userIndex = 2;
            break;
        case "bobbyhasbrook":
            $userIndex = 3;
            break;
        case "MReynoso2018":
            $userIndex = 4;
            break;
        default:
            $userIndex = 5;
            break;
        
    }

    $usersArray[$userIndex]->summateData($changedFiles, $additions, $deletions);
}

foreach($usersArray as $user) {
    $user->calculateMetaData();
}
$stats = new GithubStats($usersArray);

$returnObject = [
    "stats" => $stats,
    "userArray" => $usersArray,
    "recentCommits" => $recentCommits
];

error_log("Called get-recent-commits, printing out info:");
// error_log(print_r($returnObject, true));

header("HTTP/1.0 200 OK");
exit(json_encode($returnObject));
