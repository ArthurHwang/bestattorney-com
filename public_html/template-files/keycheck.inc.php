<?php 
// session_start();

function redirect($filename) {
	if (!headers_sent())
		header('Location: '.$filename);
	else {
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$filename.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
		echo '</noscript>';
	}
}

function parse_query_string($query_string) {
	$name_value_pairs = explode("&", $query_string);
	$query_string_array = array();
	foreach ($name_value_pairs as $name_value_pair) {
		$name_value_array = explode("=", $name_value_pair);
		$query_string_array[$name_value_array[0]] = $name_value_array[1];
	}
	return $query_string_array;
}

function secheck($checkvar) {
	$page_parse = parse_url($checkvar);
	return $page_parse['query'];
	}
	
function seextract($checkvar) {
	$page_parse = parse_url($checkvar);
	$search_engines = array("search.yahoo" => "p","images.google" => "prev","www.google" => "as_q","www.netster" => "Keywords","aol.com" => "query");			
	$referrer_hostname     = $page_parse['host'];
	$referrer_query_string = parse_query_string($page_parse['query']);
	if (count($referrer_query_string) != 0) {
		if (isset($referrer_query_string['q'])) { 
			$query_variable = "q";
		} else { 
			foreach($search_engines as $search_engine => $query_variable) { 
				if(strpos($referrer_hostname, $search_engine) !== false && isset($referrer_query_string[$query_variable])) { break; }
				$search_engine = "";
				$query_variable = "";
	}	}	}
	if ($query_variable != "") {
		$search_engine = $referrer_hostname;
		$referrer_display = urldecode($referrer_query_string[$query_variable]); 
	}
	if (strpos($referrer_hostname, "images.google") !== false) {
		$previous_query_string = urldecode($referrer_query_string['prev']);
		$referrer_display = parse_query_string($previous_query_string);
		$referrer_display = urldecode($referrer_display['/images?q']);
		$referrer_href  = "http://" . $referrer_hostname . $previous_query_string;
		$referrer_title = $referrer_href;
	}
	if ($page_parse['path'] != "" && $page_parse['path'] != "/"){
		$request_display = $page_parse['path'];
	}
	$referrer_display = substr($referrer_display, 0, 60);
	$hotlink = str_replace(" ","+",$checkvar);
	global $keys, $sengine, $referpage;
	$keys = htmlentities($referrer_display, ENT_QUOTES);
	$sengine = $referrer_hostname;
	$referpage = $hotlink;
}
$historys = "Activity Report:\n";
$historyArray = [];
$arr = explode(",", $_SESSION['route']);
foreach ($arr as $value) {
	if ((secheck($value)) != '' ) { 
		seextract($value);
		if ($sengine != $_SERVER['HTTP_HOST']) {
			$array_push($historyArray, "\nSearch Engine: ".$sengine."\nKeywords Used: ".$keys."\nReferring URL: ".$referpage."\n\n"); 
		} else { 
			$array_push($historyArray, $value."\n"); 
		}
	} elseif ($value != '') { 
		$array_push($historyArray, $value."\n");
	}
}
?>