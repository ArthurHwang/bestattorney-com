<?php

// This file is set to run every week? with a cronjob, accessible within the bestattorney.com cpanel.

//The file will get the contents of the medical devices recalls page on the FDA website, and parse the latest 5? 10? medical devices recalls. These devices are added to a file that will contain all of the medical devices. Then the file will be called to print out a widget wherever necessary.



//If something has gone wrong and we need to re-populate the list, set $rewriteNewList to true.
$rewriteNewList = true;

//This variable to be changed every new year, otherwise this will not work.
$url = "https://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm629347.htm";


//Stores the page HTML
$recallPageContent = file_get_contents($url);


if ($rewriteNewList) {

	if( preg_match_all('/(\/MedicalDevices\/Safety.*htm)(.*)(<linktitle>)(.*)(<\/linktitle>)(.|\n)*data-value="(\d*)">/iU', $recallPageContent, $recallMatches, PREG_SET_ORDER)) {

		$unserializedRecalls = array();

		foreach($recallMatches as $recall) {

			array_push($unserializedRecalls, array($recall[4], $recall[7], $recall[1]));

		}
	}

} else {

	// Finds all matches of recalls on this page.
	if( preg_match_all('/(\/MedicalDevices\/Safety.*htm)(.*)(<linktitle>)(.*)(<\/linktitle>)(.|\n)*data-value="(\d*)">/iU', $recallPageContent, $recallMatches, PREG_SET_ORDER)) {

			//Imports the current $recalls array, and unserializes it so it can be used.
			include($_SERVER['DOCUMENT_ROOT']."/medical-devices/recalls.php");
		$unserializedRecalls = unserialize($recalls);


		// For each of the recalls on the page, check if it exists in the $recalls file. If not, add it.
		foreach($recallMatches as $recall) {

			$recallExists = false;
			foreach ($unserializedRecalls as $storedRecall) {

				if( in_array($recall[4], $storedRecall)) {
					$recallExists = true;
					break;
				}

			}

			if (!$recallExists) {
				array_push($unserializedRecalls, array($recall[4], $recall[7], $recall[1]));
			}
		}

	} else {
		echo("match not found");
	}

}

//This just shows what the array now looks like.
print "<pre>";
print_r($unserializedRecalls);
print "</pre>";

//Now serialize that arry
$serialized = base64_encode(serialize($unserializedRecalls));

// And rewrite the old file,
file_put_contents("recalls.php", "<?php \$recalls = '$serialized'; ?> \n<!--Date updated: ".date("m/d/Y ")."-->");




?>
