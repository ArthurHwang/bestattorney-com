<?php
// Folder Options for /medical-devices

$pageSubject = $options['pageSubject'];

$recallHtml = "";
include($_SERVER['DOCUMENT_ROOT']."/medical-devices/recalls.php");
$unserializedRecalls = unserialize(base64_decode($recalls)); 


$recallsindex=1;

foreach($unserializedRecalls as $recall) {
	

	if($recallsindex < 6) {

		$recall[1] = substr_replace($recall[1], "-", 4, 0); 
		$recall[1] = substr_replace($recall[1], "-", 7, 0); 
		$recallDate = strtotime($recall[1]);

		$recallHtml .= "<li><strong>".date("M d, Y", $recallDate)."</strong>";
		$recallHtml .= "<br>";
		$recallHtml .= "<a class='medical-recall-link' href='http://www.fda.gov".$recall[2]."'>".$recall[0]."</a></li>";

	}
	$recallsindex++;
}


$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "medicaldefect", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Medical Defect Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
	    "Pharmaceutical Litigation" => "/pharmaceutical-litigation/",
	    "Bard Mesh Recalls" => "/medical-devices/bard-mesh-recalls.html",
	    "Birth Control Recalls" => "/medical-devices/birth-control-recalls.html",
	    "Boston Scientific Mesh Recalls" => "/medical-devices/boston-scientific-mesh-recalls.html",
	    "Depuy Hip Replacements" => "/medical-devices/depuy-hip-replacements.html",
	    "Hip Implants" => "/medical-devices/hip-implants.html",
	    "Stryker Recalls" => "/medical-devices/stryker-recalls.html",
	    "Transvaginal Mesh Victims" => "/medical-devices/transvaginal-mesh-victims.html",
	    "Zimmer Nexgen Knee Replacements" => "/medical-devices/zimmer-nexgen-knee-replacements.html",
	),

	"sidebarContentTitle" => "Defective Medical Product Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
                            <!--<li>BLUE BOX CONTENT</li>-->
                            <li>If you or a loved one have suffered as a result of a medical device defect, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                        </ul> 
                    </div>
                    <div id='sb-custom-wrap' class='pa-box-content'>

                        <p class='title-alt'><strong>The Most Recent Recalls From The FDA Are Listed Here:</strong></p>
                        <ul class='bpoint'>".$recallHtml."
                        </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["Transvaginal Mesh Implant Dangers & Complications", "/images/sb-video-X3Ui6vzRgRU.jpg", "81fmiEyrsRg"],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>