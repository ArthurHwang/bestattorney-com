<?php 


include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php"); 


//Sets up the database link. 
try {
	$link = new PDO('mysql:dbname=bestatto_tuffcookie;host=localhost;charset=utf8', 'bestatto_tufcook', 'BC1301dove');

	$link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	$errormessage .= "Database Connection Failed!<br>";
	$databaseError = true;
}

//Calls the database to get the number of votes. If there is no info for this story, create it with 0 votes.
$votesQuery = $link->prepare('SELECT `votes` FROM `bestatto_tuffcookie`.`votes` WHERE `id` = ?');

if($votesQuery->execute(array($id))) {
	$result = $votesQuery->fetch(PDO::FETCH_ASSOC);
	if($result['votes'] > -1) {
		$votes = $result['votes'];
	} else {

		$addStoryInfoQuery = $link->prepare('INSERT INTO `bestatto_tuffcookie`.`votes` (`id`, `votes`, `title`, `author`) VALUES (:id, "0", :title, :author)');

		try {
			if( $addStoryInfoQuery->execute(array('id' => $id, 'title' => $title, 'author' => $author))) {
				$votes = 0;
			} else {
				$errormessage .= "Cannot create new database record for this story!<br>";
				$databaseError = true;
			}
		} catch(PDOException $e) {
			$errormessage .= "Error in trying to create a database record for this story.<br>";
			$databaseError = true;
		}
	}
} else {
	$errormessage .= "Database Call to Voting Information Failed!<br>";
	$databaseError = true;
}

?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo($author."'s Story: One Tough Cookie Scholarship");?></title>
<meta name="description" content="<?php echo($title.' By '.$author);?>">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- Jquery -->
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
<!-- Jquery UI -->
<script   src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"   integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="   crossorigin="anonymous"></script>
<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="stylesheet" href="/one-tough-cookie/cookie-css.css?2">


<style>
	h1 {
		 padding-top: 100px;
	}
	   

	#vote-count {
		text-align: center;
		margin-bottom:30px;
	}

	#vote-count .integer{
		font-size: 40px;
	    background-color: #353535;
	    color: white;
	    font-family: "courier", serif;
	    width: 35px;
	    margin-right: 2px;
	    border-radius: 3px;
	    padding: 5px 10px;
	    box-shadow: 0px 0px 13px black inset;
	}

	.verified-true {
		font-size:50px;
		color:green;
		display:block;
		text-align:center;
		margin-top:20px;
	}

	.verified-false {
		font-size:50px;
		color:#BBB;
		display:block;
		text-align:center;
		margin-top:20px;
	}

	.verified-text {
		display:block;
		text-align:center;
		margin-top:-10px;
	}

	.vote {
		margin:20px auto 0;
		display:block;
		width:70%;
	}

	#voted, #error, #loading-icon {
		display:none;
	}

	#loading-icon {
		font-size:40px;
	}

	#headshot {
		float:left;
		width: 20%;
		padding: 0 20px 20px 0;
	}

	#story-image {
		float:left;
	}

</style>

</head>
<body>
	<nav class="navbar navbar-default">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </button>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      	<ul class="nav navbar-nav">
		      		<li >
		            	<a href="https://www.bestattorney.com/one-tough-cookie/">Scholarship Home</a>
		            </li>
					<li class="active">
						<a href="https://www.bestattorney.com/one-tough-cookie/submissions.html">Vote for Submissions</a>
					</li>
					<li >
						<a href="https://www.bestattorney.com/one-tough-cookie/rules.html">Rules</a>
					</li>
					<li>
						<a href="https://www.bestattorney.com/one-tough-cookie/tips.html">Tips</a>
					</li>
					<li >
						<a href="https://www.bestattorney.com/one-tough-cookie/submit.html"><strong>Submit Your Story</strong></a>
					</li>
		      	</ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li>
		            	<a id="root-link" class="no-tablet no-mobile" href="https://www.bestattorney.com">Scholarship Provided by Bisnar Chase</a> 
		            </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div id="header-image">

		<h1 id="h1" class="centered"><?php echo $title; ?></h1>
		<h2 class="centered">By: <?php echo $author; ?></h2>
		<div class="spacer"></div>

	</div>

	<div id="content">
		<div id="breadcrumbs">
			<div id="home-crumb"></div><a href="https://www.bestattorney.com/one-tough-cookie/submissions.html">Back To Submissions</a><div itemtype="http://data-vocabulary.org/Breadcrumb" class="breadcrumb"><span itemprop="title"><?php echo($title);?></span></div>
		</div>

		<?php if($databaseError) { ?>

			<div id="database-error" class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<p><strong>Database Error!</strong></p>
				<p><?php echo($errormessage);?></p>
				<p>We are sorry, you will be unable to vote at this time.</p>
			</div>

		<?php } ?>

		<div id="voted" class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><strong>Thanks for voting!</strong></p>
			<p>Keep voting for your favorite story every minute to increase your chances of winning!</p>
		</div>

		<div id="error" class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><strong>Error!</strong></p>
			<p id="error-p">Your vote was unable to be counted due to an error. Please contact us to fix this issue. Thanks!</p>
		</div>


		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" id="vote-count">
				
				<?php 
					
					foreach(str_split((string)$votes) as $integer) {
						echo("<span class='integer'>$integer</span>");
					}
				?>
				<span class="integer">Votes</span> <i id="loading-icon" class="fa fa-spinner fa-spin" aria-hidden="true"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h3><strong><?php echo($title."</strong> - ".$author); ?> </h3>
				<p><?php echo $school; ?></p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-8">
				<?php if($databaseError) { ?>
					<a id="vote-disabled" class="btn btn-primary btn-lg vote disabled" role="button">Voting Disabled!</a>
				<?php } else { ?>
					<a id="vote-<?php echo($id);?>" class="btn btn-primary btn-lg vote " role="button">Vote For This Story!</a>
				<?php } ?>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-4">
				<?php if ($verified) {
					echo('<i class="fa fa-check-circle-o verified-true" aria-hidden="true"></i><br><span class="verified-text">Verified</span>');
				} else {
					echo("<i class='fa fa-times-circle-o verified-false' aria-hidden='true'></i><br><span class='verified-text'>Non-Verified</span>");
				}?>
			</div>
		</div>

		<div class="clearfix"></div>
		<div style="border-bottom:2px solid #333;margin:20px auto;width:80%;"></div>

		<div id="story">
			<?php
				if($image != "") {
					echo ("<img src='".$image."' alt='".$author."' id='headshot' ");
					echo("<p id='image-story'>".$story."</p>");
				} else {
					echo("<p id='image-story'>".$story."</p>");
				}
			 ?>
		</div>

	</div>

</body>
<?php include($_SERVER['DOCUMENT_ROOT']."/one-tough-cookie/footer.php");?>

<script>

var nonvb = "vote";
	var id = 12345678
	var dimensions = (screen.width + "x" + screen.height);
	var platform = navigator.platform;
	var identifier = (dimensions + " " + platform);
	$("." + nonvb).click(function() {
	    $.ajax({
	        type: 'POST',
	        url: "https://www.bestattorney.com/one-tough-cookie/rpt.php",
	        data: {
	            identifier: identifier,
	            storyId: id
	        }
	    }).done(function(response) {
	        window.location = "https://www.bestattorney.com/giving-back/"
	    }).fail(function(response) {})
	});


	$(function() {
	    var voted = false;
	    $(".close").click(function() {
	        $(this).parent("div").hide()
	    });
	    $(".vote").click(function() {
	        if (voted) {
	            $("#error #error-p").text("You have already voted this session! Please wait a minute and refresh to vote again!");
	            $("#error").show()
	        } else {
	            $("#loading-icon").css({
	                "display": "inline-block"
	            });
	            var dimensions = (screen.width + "x" + screen.height);
	            var platform = navigator.platform;
	            var identifier = (dimensions + " " + platform);
	            $.ajax({
	                type: 'POST',
	                url: "https://www.bestattorney.com/one-tough-cookie/submit-vote.php",
	                data: {
	                    identifier: identifier,
	                    storyId: 12345678
	                }
	            }).done(function(response) {
	                $("#loading-icon").hide();
	                $("#voted").show();
	                voted = true;
	                console.log(response)
	            }).fail(function(response) {
	                $("#loading-icon").hide();
	                $("#error #error-p").text("We're sorry, your vote was unable to be counted because of an error. Please check back in an hour or so!");
	                $("#error").show()
	            })
	        }
	    })
	});

</script>
