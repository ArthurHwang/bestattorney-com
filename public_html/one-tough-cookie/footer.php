<footer>

	<!-- <div id="footer-content"> -->
		<!-- <div class="row"> -->
			<!-- <div class="col-md-4 col-sm-12 col-xs-12">
				<ul>
					<li><strong>Scholarship Navigation</strong></li>
					<li><a href="https://www.bestattorney.com/one-tough-cookie/">One Tough Cookie Home</a></li>
					<li><a href="https://www.bestattorney.com/one-tough-cookie/submit.html">Submit Your Story</a></li>
					<li><a href="https://www.bestattorney.com/one-tough-cookie/submissions.html">Vote for Submissions</a></li>
					<li><a href="https://www.bestattorney.com/one-tough-cookie/rules.html">Full Rules</a></li>
					<li><a href="https://www.bestattorney.com/one-tough-cookie/tips.html">Tips</a></li>
				</ul>
			</div> -->

			<!-- <div class="col-md-4 col-sm-12 col-xs-12">
				<ul>
					<li><strong>Bisnar Chase Navigation</strong></li>
					<li><a href="https://www.bestattorney.com">Home</a></li>
					<li><a href="https://www.bestattorney.com/practice-areas/">Practice Areas</a></li>
					<li><a href="https://www.bestattorney.com/about-us/testimonials.html">Testimonials</a></li>
					<li><a href="https://www.bestattorney.com/contact.html">Contact Us</a></li>
				</ul>
			</div> -->

			<!-- <div class="col-md-4 col-sm-12 col-xs-12">
				<p style="position:relative;left:25px;">Scholarship Presented By:</p>
				<div id="footer-logo"><a href="https://www.bestattorney.com/"><img data-src="/images/bc-white-logo-small.png" alt="Bisnar Chase"></a></div>
			</div> -->
	<!-- </div> -->
</footer>
<script>



$(function() {

	function changeTag() {
		$(".fadeToggle").delay(7000).fadeToggle(400, function() {
			changeTag();
		});
	}

	function parallax() {
		var scrolled = $(window).scrollTop();
		if($(window).width() > 1300) {
			$("#header-image").css({
				backgroundPositionY: -273+(scrolled*.4)+"px",
			});
		} else if($(window).width() > 1000) {
			$("#header-image").css({
				backgroundPositionY: -273+(scrolled*.2)+"px",
			});
		} 
	}

	changeTag();

	$(window).scroll(function(e){
	    parallax();
	});

});

</script>