<?php 

if (!isset($_SESSION['beginSession'])) {
	$_SESSION['beginSession'] = time();
}

function vote($storyId, $idstring) {
	/*try {
		global $link;
		$addVoteQuery = $link->prepare("UPDATE `votes` SET  `votes` = votes+1 WHERE id = ?");
		if(!$addVoteQuery->execute(array($storyId))) {
			header("HTTP/1.0 400 Error");
			exit("Error updating votes");
		}

		$updateTimestampQuery = $link->prepare("UPDATE `voters` SET `timestamp` = ?, `count` = count+1, `date` = ?, `lastVotedFor` = ? WHERE id_string = ?");
		if(!$updateTimestampQuery->execute(array(time(), date('F jS Y h:i:s A'), $storyId,$idstring))) {
			header("HTTP/1.0 400 Error");
			exit("Error updating Timestamp");
		}

	} catch(PDOException $e) {
		header("HTTP/1.0 400 Error");
		echo "We're sorry, there was an error with voting for this story.";
		echo($e);
		exit;
	}*/

	header("HTTP/1.0 400 Error");
	echo "All voting has ended!";
	exit;
}

//Sets up the database link.
try {
	$link = new PDO('mysql:dbname=bestatto_tuffcookie;host=localhost;charset=utf8', 'bestatto_tufcook', 'BC1301dove');
	$link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	header("HTTP/1.0 400 Error");
	echo "Error Connecting to Database!";
	exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {
	    $_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
	}

	$ip = $_SERVER['REMOTE_ADDR'];

	

	$voterId = $_POST['identifier'];
	$idstring = $voterId." ".$ip;

	$storyId = $_POST['storyId'];

	$votesQuery = $link->prepare('SELECT * FROM `bestatto_tuffcookie`.`voters` WHERE `id_string` = ?');
	if($votesQuery->execute(array($idstring))) {
		$result = $votesQuery->fetch(PDO::FETCH_ASSOC);

		if ($result) {
			
			if(time() - $result['timestamp'] >= 60){
				vote($storyId, $idstring);
				header("HTTP/1.0 200 OK");
				exit("Voting Successful!");
			} else {
				header("HTTP/1.0 403 Forbidden");
				echo "You have voted already in the past minute. Please wait until a minute has passed and try voting again!";
				exit;
			}


		} else {
			//This user hasn't voted before, increment vote count and add user. 
			
			try {

				$addVoterInfoQuery = $link->prepare('INSERT INTO `bestatto_tuffcookie`.`voters` (`id_string`, `timestamp`, `count`, `date`, `lastVotedFor`) VALUES (?, ?, ?, ?, ?)');
				$addVoterInfoQuery->execute(array($idstring, time(), 0, date('F jS Y h:i:s A'), 0));

			} catch(PDOException $e) {
				header("HTTP/1.0 400 Error");
				echo($e->getMessage ());
				exit;
			} 


			//Votes after creating a voter record.
			vote($storyId, $idstring);
			header("HTTP/1.0 200 OK");
			exit("New Vote Successful!");
				

		}
	}

} else {
	//Log Errors here to a new table?
	header("HTTP/1.0 403 Forbidden");
	echo "There was an error with the way you attempted to vote. This error has been logged and any vote tampering will result in submission disqualification.";
	exit;
}


?>