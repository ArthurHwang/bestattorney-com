<?php 

if (!isset($_SESSION['beginSession'])) {
	$_SESSION['beginSession'] = time();
}

echo("<script>");
echo("console.log('".(time()-$_SESSION['beginSession'])."');");
echo("</script>");

if(time() - $_SESSION['beginSession'] > 1800) {
	$longSession = true;
	$rand = rand(1,3);
	if ($rand == 1) {
		//do nothing
	} else if ($rand == 2) {
		$rand2 = true;
	} else {
		$rand3 = true;
	}
}

//This is set so that voting stops immediately when it hits 12am on sept 2.
if(time() > 1472799600) {
	$finished = true;
}



include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php"); 


//Sets up the database link. 
try {
	$link = new PDO('mysql:dbname=bestatto_tuffcookie;host=localhost;charset=utf8', 'bestatto_tufcook', 'BC1301dove');

	$link->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	$errormessage .= "Database Connection Failed!<br>";
	$databaseError = true;
}

//Calls the database to get the number of votes. If there is no info for this story, create it with 0 votes.
$votesQuery = $link->prepare('SELECT `votes` FROM `bestatto_tuffcookie`.`votes` WHERE `id` = ?');

if($votesQuery->execute(array($id))) {
	$result = $votesQuery->fetch(PDO::FETCH_ASSOC);
	if($result['votes'] > -1) {
		$votes = $result['votes'];
	} else {

		$addStoryInfoQuery = $link->prepare('INSERT INTO `bestatto_tuffcookie`.`votes` (`id`, `votes`, `title`, `author`) VALUES (:id, "0", :title, :author)');

		try {
			if( $addStoryInfoQuery->execute(array('id' => $id, 'title' => $title, 'author' => $author))) {
				$votes = 0;
			} else {
				$errormessage .= "Cannot create new database record for this story!<br>";
				$databaseError = true;
			}
		} catch(PDOException $e) {
			$errormessage .= "Error in trying to create a database record for this story.<br>";
			$databaseError = true;
		}
	}
} else {
	$errormessage .= "Database Call to Voting Information Failed!<br>";
	$databaseError = true;
}


$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo($author."'s Story: One Tough Cookie Scholarship");?></title>
<meta name="description" content="<?php echo($title.', By '.$author);?>">
<meta property="og:title" content="<?php echo($author."'s Story: One Tough Cookie Scholarship");?>" />
<meta property="og:description" content="<?php echo($title.', By '.$author);?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.bestattorney.com/one-tough-cookie/stories/<?php echo $id;?>.html" />
<?php 

if ($image == "") { ?>
<meta property="og:image" content="https://www.bestattorney.com/images/scholarshipbackground.jpg"/>
<?php } else { ?>
<meta property="og:image" content="https://www.bestattorney.com<?php echo $image;?>"/>
<?php } ?>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- Jquery -->
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
<!-- Jquery UI -->
<script   src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"   integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="   crossorigin="anonymous"></script>
<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="stylesheet" href="/one-tough-cookie/cookie-css.css?2">


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WK3JJ3');</script>
<!-- End Google Tag Manager -->
<!-- Universal Analytics script included, cannot fire from GTM because GTM does not support plugins like autotrack-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44427913-2', {'siteSpeedSampleRate': 100});
  ga('require', 'autotrack');
  ga('send', 'pageview');

</script>
<script async src='/js/autotrack.js'></script>

<style>
	h1 {
		 padding-top: 200px;
	}
	   

	#vote-count {
		text-align: center;
		margin-bottom:30px;
	}

	#vote-count .integer{
		font-size: 40px;
	    background-color: #353535;
	    color: white;
	    font-family: "courier", serif;
	    width: 35px;
	    margin-right: 2px;
	    border-radius: 3px;
	    padding: 5px 10px;
	    box-shadow: 0px 0px 13px black inset;
	}

	.verified-true {
		font-size:50px;
		color:green;
		display:block;
		text-align:center;
		margin-top:20px;
	}

	.verified-false {
		font-size:50px;
		color:#BBB;
		display:block;
		text-align:center;
		margin-top:20px;
	}

	.verified-text {
		display:block;
		text-align:center;
		margin-top:-10px;
	}

	.vote {
		margin:20px auto 0;
		display:block;
		width:70%;
	}

	#voted, #error, #loading-icon {
		display:none;
	}

	#loading-icon {
		font-size:40px;
	}

	#headshot {
		float:left;
		width: 20%;
		padding: 0 20px 20px 0;
	}

	#story-image {
		float:left;
	}

	#rand3button button {
		display: block;
	    margin: 0 auto;
	    width: 20%;
	    position:relative;
	    left:<?php echo(rand(-40,40));?>%;
	}

	<?php if($rand3) { ?>
		.voteforme:hover {
			cursor:default;
		}
	<?php } ?>

	<?php 
		/*if($longSession) {
			echo(".spacer {");
			echo("height:".rand(1,350)."px;");
			echo("}");
		}*/
	?>

</style>

</head>
<body>




	<nav class="navbar navbar-default">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </button>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      	<ul class="nav navbar-nav">
		      		<li >
		            	<a href="https://www.bestattorney.com/one-tough-cookie/">Scholarship Home</a>
		            </li>
					<li class="active">
						<a href="https://www.bestattorney.com/one-tough-cookie/submissions.html">Vote for Submissions</a>
					</li>
					<li >
						<a href="https://www.bestattorney.com/one-tough-cookie/rules.html">Rules</a>
					</li>
					<li>
						<a href="https://www.bestattorney.com/one-tough-cookie/tips.html">Tips</a>
					</li>
					<li >
						<a href="https://www.bestattorney.com/one-tough-cookie/submit.html"><strong>Submit Your Story</strong></a>
					</li>
		      	</ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li>
		            	<a id="root-link" class="no-tablet no-mobile" href="https://www.bestattorney.com">Scholarship Provided by Bisnar Chase</a> 
		            </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div id="header-image">

		<h1 id="h1" class="centered"><?php echo $title; ?></h1>
		<h2 class="centered">By: <?php echo $author; ?></h2>
		

	</div>
	<div class="spacer"></div>
	<div id="content">
		<div id="breadcrumbs">
			<div id="home-crumb"></div><a href="https://www.bestattorney.com/one-tough-cookie/submissions.html">Back To Submissions</a><div itemtype="http://data-vocabulary.org/Breadcrumb" class="breadcrumb"><span itemprop="title"><?php echo($title);?></span></div>
		</div>

		<?php if($databaseError) { ?>

			<div id="database-error" class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<p><strong>Database Error!</strong></p>
				<p><?php echo($errormessage);?></p>
				<p>We are sorry, you will be unable to vote at this time. Please contact marketing@bestattorney.com to resolve this issue.</p>
			</div>

		<?php } else if ($finished) {?>

			<div id="database-error" class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<p><strong>The One Tough Cookie Competition Has Concluded!</strong></p>
				<p>Thank you for participating in Bisnar Chase's #OneToughCookie Scholarship Competition. Voting is now closed.</p>
				<p>Our scholarship team will review the vote counts and will award $1,500 to the applicant with the highest amount of votes who has not been determined to be cheating. </p>
			</div>

		<?php 

			$databaseError = true;
			$rand = 1;

		} ?>

		<div id="voted" class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><strong>Thanks for voting!</strong></p>
			<p>Keep voting for your favorite story every minute to increase your chances of winning!</p>
		</div>

		<div id="error" class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><strong>Error!</strong></p>
			<p id="error-p">Your vote was unable to be counted due to an error. Please contact us to fix this issue. Thanks!</p>
		</div>


		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" id="vote-count">
				
				<?php 
					
					foreach(str_split((string)$votes) as $integer) {
						echo("<span class='integer'>$integer</span>");
					}
				?>
				<span class="integer">Votes</span> <i id="loading-icon" class="fa fa-spinner fa-spin" aria-hidden="true"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h3><strong><?php echo($title."</strong> - ".$author); ?> </h3>
				<p><?php echo $school; ?></p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-8">
				<?php if($databaseError) { ?>
					<a id="vote-disabled" class="btn btn-primary btn-lg vote disabled" role="button">Voting Disabled!</a>
				<?php } else if ($rand2) { ?>
					<a class="btn btn-primary btn-lg voteforme " role="button">Vote For This Story!</a>
					<a class="btn btn-primary btn-lg vote " role="button" style="display:none;">Vote For This Story!</a>
				<?php } else if ($rand3) { ?>
					<a class="btn btn-primary btn-lg voteforme" role="button" style="opacity:0;">Vote For This Story!</a>
				<?php } else { ?>
					<a class="btn btn-primary btn-lg vote " role="button">Vote For This Story!</a>
				<?php } ?>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-4">
				<?php if ($verified) {
					echo('<i class="fa fa-check-circle-o verified-true" aria-hidden="true"></i><br><span class="verified-text">Verified</span>');
				} else {
					echo("<i class='fa fa-times-circle-o verified-false' aria-hidden='true'></i><br><span class='verified-text'>Non-Verified</span>");
				}?>
			</div>
		</div>

		<div class="clearfix"></div>
		<div style="border-bottom:2px solid #333;margin:20px auto;width:80%;"></div>

		<div id="story">
			<?php
				if($image != "") {
					echo ("<img src='".$image."' alt='".$author."' id='headshot'> ");
					echo("<p id='image-story'>".$story."</p>");
				} else {
					echo("<p id='image-story'>".$story."</p>");
				}
			 ?>
		</div>
		<?php if ($rand3) { ?>
			<div id="rand3button">
				<button id="rand3btn" class="btn btn-lg btn-success centered rand3btn" type="submit">Submit your Vote!</button>
			</div>
		<?php } ?>

	</div>

</body>
<?php include($_SERVER['DOCUMENT_ROOT']."/one-tough-cookie/footer.php");?>

<?php if(!$finished) { ?>

	<script>
		<?php if ($rand2 || $rand3) { ?>
			var nonvb = "<?php if($rand2) { echo('vote'); } else if ($rand3) {echo('voteforme');} ?>";
			var id = <?php echo($id); ?>;
			eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('0 5=(8.g+"x"+8.h);0 2=f.2;0 1=(5+" "+2);$("."+j).e(3(){$.c({d:\'b\',i:"6://7.a.9/u-t-s/k.v",w:{1:1,y:r}}).q(3(4){m.l="6://7.a.9/n-o/"}).p(3(4){})});',35,35,'var|identifier|platform|function|response|dimensions|http|www|screen|com|bestattorney|POST|ajax|type|click|navigator|width|height|url|nonvb|rpt|location|window|giving|back|fail|done|id|cookie|tough|one|php|data||storyId'.split('|'),0,{}))
		<?php }?>

		eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$(2(){3 1=y;$(".w").g(2(){$(d).v("t").8()});$(".u").g(2(){z(1){$("#0 #0-p").h("A F s 1 d D! B C a G n k l c j!");$("#0").7()}i{$("#6-9").m({"q":"o-r"});3 e=(f.E+"x"+f.Y);3 5=X.5;3 b=(e+" "+5);$.H({W:\'V\',Z:"10://14.12.13/11-U-T/M-c.L",K:{b:b,I:J}}).N(2(4){$("#6-9").8();$("#1").7();1=O;S.R(4)}).Q(2(4){$("#6-9").8();$("#0 #0-p").h(4.P);$("#0").7()})}})});',62,67,'error|voted|function|var|response|platform|loading|show|hide|icon||identifier|vote|this|dimensions|screen|click|text|else|again|refresh|to|css|and|inline||display|block|already|div|<?php if ($rand2) {echo("voteforme");} else if ($rand3) { echo("rand3btn"); } else {echo("vote");}?>|parent|close||false|if|You|Please|wait|session|width|have|minute|ajax|storyId|<?php echo($id);?>|data|php|submit|done|true|responseText|fail|log|console|cookie|tough|POST|type|navigator|height|url|http|one|bestattorney|com|www'.split('|'),0,{}))
	</script>

<?php } ?>





		

<?php
//code that used to be here that worked fine:
/*

<?php if ($rand2 || $rand3) { ?>
	var nonvb = "<?php if($rand2) { echo('vote'); } else if ($rand3) {echo('voteforme');} ?>";
	var id = <?php echo($id); ?>;
	eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('0 5=(8.g+"x"+8.h);0 2=f.2;0 1=(5+" "+2);$("."+j).e(3(){$.c({d:\'b\',i:"6://7.a.9/u-t-s/k.v",w:{1:1,y:r}}).q(3(4){m.l="6://7.a.9/n-o/"}).p(3(4){})});',35,35,'var|identifier|platform|function|response|dimensions|http|www|screen|com|bestattorney|POST|ajax|type|click|navigator|width|height|url|nonvb|rpt|location|window|giving|back|fail|done|id|cookie|tough|one|php|data||storyId'.split('|'),0,{}))
<?php }?>

eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$(1(){4 2=D;$(".E").k(1(){$(e).C("B").7()});$(".y").k(1(){z(2){$("#0 #0-p").d("A F G 2 e L! g M a K J w f b I!");$("#0").6()}N{$("#c-5").s({"m":"v-t"});4 i=(j.u+"x"+j.n);4 8=r.8;4 9=(i+" "+8);$.o({q:\'H\',13:"19://1a.18.17/15-16-O/1b-b.1c",1i:{9:9,1g:1f}}).1d(1(3){$("#c-5").7();$("#2").6();2=1e;h.l(3)}).14(1(3){$("#c-5").7();$("#0 #0-p").d("T\'U S, P b Q V f W 11 12 10 Z 0. g X Y 1j!");h.l("R: "+3.1h);$("#0").6()})}})});',62,82,'error|function|voted|response|var|icon|show|hide|platform|identifier||vote|loading|text|this|to|Please|console|dimensions|screen|click|log|display|height|ajax||type|navigator|css|block|width|inline|refresh||<?php if ($rand2) {echo("voteforme");} else if ($rand3) { echo("rand3btn"); } else {echo("vote");}?>|if|You|div|parent|false|close|have|already|POST|again|and|minute|session|wait|else|cookie|your|was|Error|sorry|We|re|unable|be|check|back|an|of|counted|because|url|fail|one|tough|com|bestattorney|http|www|submit|php|done|true|<?php echo $id;?>|storyId|responseText|data|later'.split('|'),0,{}))

*/

?>