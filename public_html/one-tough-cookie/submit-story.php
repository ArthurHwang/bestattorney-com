<?php 


// The file that sends the scholarship story to marketing@bestattorney.com

require $_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';

/* CREATE THE SENDGRID MAIL OBJECT
====================================================*/
$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();

/*
if ($_SERVER["REQUEST_METHOD"] == "POST") {

	//sucuri workaround for getting client IP
	if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {
	    $_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
	}

	$name = $_POST['name'];
	$phone = trim($_POST['phone']);
	$validphone = true;
	$phonevalidate = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
	
	if (!preg_match( $phonevalidate, $homephone )) {
		$validphone = false;
	}

	$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL) && (!$validphone)) {
		header("HTTP/1.0 400 Bad Request");
        echo "Please provide a valid email or phone number!";
		exit;
	} 

	$school = trim($_POST['school']);

	$storyTitle = trim($_POST['title']);
	$story = trim($_POST['story']);


	$subject = "New Scholarship Submission";
	$emailBody = "
Someone has submitted a new scholarship submission. \n

Name: $name\n
Phone: $phone\n
Email: $email\n
School: $school\n\n
Story Title: $storyTitle\n
Story: $story\n
IP Address: ".$_SERVER['REMOTE_ADDR'];

	try {
	    $mail->
	    setFrom( $email )->
	    addTo("marketing@bestattorney.com")->
	    setSubject($subject)->
	    setText($emailBody);
	    
	    $response = $sendgrid->send($mail);

	    if (!$response) {
	        throw new Exception("Did not receive response.");
	    } else if ($response->message && $response->message == "error") {
	        throw new Exception("Received error: ".join(", ", $response->errors));
	    } else {
	    	header("HTTP/1.0 200 OK");
	    	exit("Message Sent Successfully");
	    }

	} catch ( Exception $e ) {
		header("HTTP/1.0 400 Bad Request");
	    var_export($e);
	    exit;
	}

}
else {
	header("HTTP/1.0 403 Forbidden");
	echo "There was a problem with your submission, please try again.";
	exit;
}
*/




?>