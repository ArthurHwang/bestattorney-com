<?php
// Folder Options for /orange-county
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "orange-county", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Orange County Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Auto Defects" => "/orange-county/auto-defects.html",
        "Bicycle Accidents" => "/orange-county/bicycle-accidents.html",
        "Brain Injury" => "/orange-county/brain-injury.html",
        "Bus Accidents" => "/orange-county/bus-accidents.html",
        "Car Accidents" => "/orange-county/car-accidents.html",
        "Dog Bites" => "/orange-county/dog-bites.html",
        "Employment Law" => "/orange-county/employment-lawyers.html",
        "Hit and Run Accidents" => "/orange-county/hit-and-run-accidents.html",
		"Hoverboard Injuries" => "/orange-county/hoverboard-injury-lawyers.html",                           
		"Motorcycle Accidents" => "/orange-county/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/orange-county/nursing-home-abuse.html",
		"Parking Lot Accidents" => "/orange-county/parking-lot-accidents.html",
        "Pedestrian Accidents" => "/orange-county/pedestrian-accidents.html",
        "Product Liability" => "/orange-county/product-liability-lawyers.html",
		"Slip and Fall Accidents" => "/orange-county/slip-and-fall-accidents.html",
        "Swimming Pool Accidents" => "/orange-county/swimming-pool-accidents.html",
        "Truck Accidents" => "/orange-county/truck-accidents.html",
        "Amusement Park Accidents" => "/orange-county/amusement-park-accident-lawyer.html",
        "Wage and Hour Disputes" => "/orange-county/wage-and-hour-disputes.html",
        "Wrongful Death" => "/orange-county/wrongful-death.html",
		"Contact OC Location" => "/orange-county/contact-us.html",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
