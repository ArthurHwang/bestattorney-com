<?php /*

Template Name: Index

*/

include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/preload.php");

include("folder-options.php");



// ADD ALL OTHER PAGE OPTIONS HERE!

?>



<!DOCTYPE html>

<html>

<head>

	<title>California Personal Injury Lawyers - Award Winning CA Accident Attorneys</title>

	<?php if ((!is_page()) && (!is_single()) && (!is_home())) { ?>

		<meta name="description" content="Contact our California personal injury lawyers for a free consultation of a car accident, dog bite, auto defect, or serious injury. Our CA accident attorneys are award winning & dedicated. Call 949-203-3814">

	<?php } else if (is_paged()) {

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

		<meta name="description" content="Blog Archives - Page: <?php echo $paged . ' of ' . $wp_query->max_num_pages; ?>">

	<?php } ?>



	<?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/header.php"); ?>



	<?php

	if (is_home()) { ?>

		<?php $options['isBlogHome'] = true; ?>

		<h1 id="h1title">
			<!--<a href="<?php echo get_option('home'); ?>/">--><?php bloginfo('name'); ?>
			<!--</a>-->
		</h1>

	<?php } else if (is_archive()) { ?>

		<h1 id="h1title"><?php wp_title('Archive: '); ?></h1>

	<?php } elseif (is_date()) { ?>

		<h1 id="h1title"><a href="<?php echo $thepagelink; ?>" title="<?php wp_title(' '); ?>"><?php wp_title(' '); ?> Archive</a></h1>

	<?php } ?>

	<!-- Blog Body -->

	<?php $thepagelink = $_SERVER['REQUEST_URI']; //Why do I need this?



	if (is_archive()) { ?>

		<div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <?php wp_title(' '); ?></div>

	<?php  } elseif (is_date()) { ?>

		<div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <a href="<?php echo get_option('home'); ?>/archives/">archives</a> <?php wp_title(' '); ?></div>

	<?php  } ?>



	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



			<?php if ((is_single()) || (is_page())) { // If this is a normal page and not a home/category/archive page. 

				$options['isBlogSingle'] = true;

				/***********************************************************************************************************/

				/* The following PHP and styles are to set the post's header image to the main image of the category that is provided in the "edit category options" in the wordpress dashboard. */

				/***********************************************************************************************************/

				$category_id = get_cat_ID($category[0]->cat_name);

				$categories = get_the_category();

				if (!empty($categories)) {

					$category = esc_html($categories[0]->name);
				}





				if (function_exists('get_terms_meta')) {

					$backgroundimageurl = get_terms_meta($category_id, 'background', true);
				}



				if ($backgroundimageurl != "") {

					$tabletbackground = (substr($backgroundimageurl, 0, (strlen($backgroundimageurl) - 4)) . '-tablet.jpg');

					$mobilebackground = (substr($backgroundimageurl, 0, (strlen($backgroundimageurl) - 4)) . '-mobile.jpg');



					echo "

		  		<style>

					#bisnar-and-chase {

					  background: url('https://www.bestattorney.com/blog/wp-content/themes/attorney/images/content-bg-template.jpg') no-repeat center top;

					}



					#bisnar-chase, #personal-injury-lawyers {

					  background: url('" . $backgroundimageurl . "') no-repeat center top !important;

					}



					@media only screen and (max-width:1100px) and (min-width: 751px) {

						#bisnar-chase, #personal-injury-lawyers {

					  		background: url('" . $tabletbackground . "') no-repeat center top !important;

						}

					}

					@media only screen and (max-width:750px) {

						#bisnar-chase, #personal-injury-lawyers {

					  		background: url('" . $mobilebackground . "') no-repeat center top !important;

						}

					}

				</style>

		  	";
				}



				/********************************************************************************************************/

				/* ==============================  End code to change the header image. =============================== */

				/********************************************************************************************************/



				/********************************************************************************************************/

				/* ===============  Check if The Page is meant to be full width (without the sidebar). ================ */

				/********************************************************************************************************/

				if (get_post_meta(get_the_ID(), 'fullwidth', true) == "true") {

					$fullwidth = true;

					echo '<style> #content { width:100%;} </style> ';
				}

				/********************************************************************************************************/

				/* =======================================  End fullwidth code. ======================================= */

				/********************************************************************************************************/



				$blogTitle = get_the_title(); ?>



				<h1 id="h1title" class="<?php if (strlen($blogTitle) > 68) {

																	echo "smaller-title";
																} ?>">
					<!--<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">--><?php echo $blogTitle; ?>
					<!--</a>-->
				</h1>



				<div class="wp_post" id="post-<?php the_ID(); ?>">

					<div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <?php the_category(' ') ?> <?php the_title(); ?></div>

				<?php } else { ?>

					<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

				<?php }  ?>

				<?php if (!is_page()) { ?>

					<div class="author-text">
						<p>By <?php the_author_link() ?> on <?php the_time('F j, Y') ?> - <span class="comment-text"><?php comments_popup_link(__('No comments'), __('1 comment'), __('% comments'), '', __('Comments off')); ?></span></p>
					</div>

				<?php } ?>

				<div class="blog-content-wrap">



					<?php if (has_post_thumbnail()) {

						echo ("<div id='post-image-thumbnail' style='max-width:300px;float:right;padding: 0 0 0 15px;margin-bottom:15px;'>");
						the_post_thumbnail('medium', array("class" => "alignright"));
						echo ("<div class='clearfix'></div>");

						if (get_post(get_post_thumbnail_id())->post_excerpt != "") {
							//If there's a caption, put it here. 
							echo ("<p style='font-size:10px;float:right;padding: 10px 5px 0px 10px;line-height: 12px;'>");
							echo get_post(get_post_thumbnail_id())->post_excerpt;
							echo ("</p>");
						}

						echo ("</div>");
					}

					// If this is the homepage, truncate the results. Else, leave it as normal cause it WILL eff up the blog pages. 
					if (is_home()) {
						the_content('...Read the rest &raquo;', false, true);
					} else {
						the_content('...Read the rest &raquo;');
					} ?>







				</div>



				<?php



				if ((is_single()) || (is_page())) { ?>

				</div>

			<?php } ?>



			<div class="clear"></div>



			<?php if (!is_page()) { ?>



				<div align="center">

					<div class="meta-box-wrap">

						<div class="meta-box">

							<div class="meta-inner-box">

								<p>Posted in: <?php the_category(', ') ?></p>

								<?php

								/* Adds the Author Name and Bio at the end of a post only if the post is not an archive or home page of the blog, and if a bio exists. */

								$author = get_the_author_meta('display_name');

								if (get_the_author_meta('description') && (is_single() || is_page())) : ?>

									<div>

										<h3>About the Author: <?php echo $author; ?></h3>

										<!-- If The author is Matt Cheah, add his profile picture there. -->

										<!--<?php if ($author == "Matt Cheah") : ?><img src="https://www.bestattorney.com/blog/wp-content/uploads/2015/04/mcheah-e1428941912461.jpg" style="float:left;margin-right:20px;padding-top:14px;" /><?php endif; ?>

									<div style="float:left;width:75%;">

										<p><?php the_author_meta('description'); ?></p>

									</div> -->

									</div>

									<div class="clear"></div>

								<?php endif; ?>

								<p><?php if (function_exists('the_tags')) : ?><?php the_tags(' Tags:  ', ', ', ' '); ?><?php endif; ?> <?php if (function_exists('the_bunny_tags')) : ?><?php the_bunny_tags('Tags:  ', ' ', ', '); ?><?php endif; ?></p>

							</div>

						</div>

					</div>

				</div>

				<?php comments_template(); ?>

			<?php } ?>

		<?php endwhile; ?>

		<div id="navi">
			<div id="navi-previous">
				<?php next_posts_link('&laquo; Previous Entries') ?>
			</div>
			<!-- <div id="rss-feed">
						<a href="https://www.bestattorney.com/blog/feed">RSS Feed</a>
					</div> -->
			<div id="navi-next">
				<?php previous_posts_link('Next Entries &raquo;') ?>
			</div>
		</div>
		<div class="clear"></div>

	<?php else : ?>
		<h1 id="h1title">Not Found</h1>

		<p>Sorry, but you are looking for something that isn't here.</p>
		<p><a href="https://www.bestattorney.com/blog/">Return to Blog Home</a></p>

		<?php require($_SERVER['DOCUMENT_ROOT'] . '/blog/wp-blog-header.php'); ?>
		<div id="sb-custom-wrap">
			<p class="title">Recent Posts</p>
			<ul>
				<?php
				query_posts('numberposts=10&order=DESC&orderby=post_date');
				if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li>
							<a href="<?php the_permalink() ?>">
								<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail(array(65), array('class' => 'alignleft recent-posts-image'));
								} ?>
							</a>
							<a href="<?php the_permalink() ?>">
								<?php
								the_title(); ?>
							</a>
							<br>
							<p class="recent-posts-category">Category: <?php the_category(", "); ?></p>
							<div class="clearfix"></div>
						</li>
					<?php endwhile;
				else : ?>
					<p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
			</ul>
		</div>
		<style>
			#sb-custom-wrap {
				width: 100% !important;
			}
		</style>

	<?php endif; ?>

	<!-- End Blog Body -->

	<?php include($_SERVER['DOCUMENT_ROOT'] . "/template-files/side-footer.php"); ?>





	<?php wp_footer(); ?>

	</body>

</html>