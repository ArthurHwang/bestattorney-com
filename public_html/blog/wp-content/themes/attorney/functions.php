<?php 
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'previous_post_rel_link', 10, 0);
remove_action( 'wp_head', 'next_post_rel_link', 10, 0);

function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function noindex_search_page() {

	if ( !is_search() )
		return;

	echo '<meta name="robots" content="noindex, nofollow">';

}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}

add_action( 'widgets_init', 'arphabet_widgets_init' );

add_action('wp_head', 'noindex_search_page', 10);
add_theme_support( 'post-thumbnails' );
add_filter( 'get_pagenum_link', 'user_trailingslashit' ); // Used to get rid of the trailing slash on the end of the Previous Page and Next Page links to paginated category + archive pages.

?>