<?php /*
Template Name: Categories
*/
include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php");
include("folder-options.php");

// ADD ALL OTHER PAGE OPTIONS HERE!
$options['isCategory'] = true;

?>

<!DOCTYPE html>
<html>
<head>
<title>Category: <?php single_cat_title( '', true ); ?> - Bisnar Chase Blog</title>
<meta name="description" content="<?php single_cat_title( '', true ); ?> - The Bisnar Chase Personal Injury Attorneys Blog | List of posts under the category '<?php single_cat_title( '', true ); ?>'">



<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/header.php"); ?> 

<?php /*CSS BACKGROUND HEADER generated based on the category */
	$cat = $wp_query->get_queried_object();
	$category_id = $cat->term_id;

	if (function_exists('get_terms_meta')) {
		$backgroundimageurl = get_terms_meta($category_id, 'background', true);
	}
	if ($backgroundimageurl != "") {
	  	$tabletbackground = (substr($backgroundimageurl, 0, (strlen($backgroundimageurl)-4)) . '-tablet.jpg');
	  	$mobilebackground = (substr($backgroundimageurl, 0, (strlen($backgroundimageurl)-4)) . '-mobile.jpg');
	  }
	 else {
	 	$backgroundimageurl = "/images/content-bg.jpg";
	 	$tabletbackground = "/images/content-bg-tablet.jpg";
	 	$mobilebackground = "/images/content-bg-mobile.jpg";
	 }
?>
<style>
#bisnar-and-chase {
  background: url("https://www.bestattorney.com/blog/wp-content/themes/attorney/images/content-bg-template.jpg") no-repeat center top;
}

#bisnar-chase, #personal-injury-lawyers {
  background: url("<?php echo $backgroundimageurl; ?>") no-repeat center top !important;
}

@media only screen and (max-width:1100px) and (min-width: 751px) {
	#bisnar-chase, #personal-injury-lawyers {
  		background: url("<?php echo $tabletbackground; ?>") no-repeat center top !important;
	}
}
@media only screen and (max-width:750px) {
	#bisnar-chase, #personal-injury-lawyers {
  		background: url("<?php echo $mobilebackground; ?>") no-repeat center top !important;
	}
}

</style>

<div class="clear"></div>

			<h1 id="h1title">Category: <?php single_cat_title( '', true ); ?></h1>

<!-- Blog Body -->
<?php 				$thepagelink = $_SERVER['REQUEST_URI']; ?>
				
				<div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <?php wp_title(' '); ?></div>
				<?php if (category_description( $category_id ) != '') { ?>
				<div class="category-description well well-lg">
					<h2><?php single_cat_title( '', true ); ?> - Bisnar Chase Blog</h2>
					<?php echo category_description( $category_id ); ?> 	
				</div>
				<?php } ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="wp_post" id="post-<?php the_ID(); ?>">
				<?php	if ((is_single()) || (is_page())) { ?><h1 id="h1title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1><div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <?php the_category(' ') ?> <?php the_title(); ?></div><?php } else{ ?><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2> <?php }  ?>
				<?php if (!is_page())  {?><div class="author-text"><p>By <?php the_author_link() ?> on <?php the_time('F j, Y') ?> - <span class="comment-text"><?php comments_popup_link(__('No comments'), __('1 comment'), __('% comments'), '', __('Comments off')); ?></span></p></div> <?php } ?>
					<div class="blog-content-wrap">
						<?php if (has_post_thumbnail()) {
							the_post_thumbnail('medium', array( 'class' => 'alignright' ));
						}
						the_content('...Read the rest &raquo;'); ?></div>
				</div>
				<div class="clear"></div>
			 <?php if (!is_page()) { ?>
				<div align="center">
					<div class="meta-box-wrap">
						<div class="meta-box">
							<div class="meta-inner-box">
								<p>Posted in: <?php the_category(', ') ?></p>
								<p><?php if(function_exists('the_tags')) : ?><?php the_tags(' Tags:  ', ', ', ' '); ?><?php endif; ?> <?php if(function_exists('the_bunny_tags')) : ?><?php the_bunny_tags('Tags:  ', ' ', ', '); ?><?php endif; ?></p>
								<p>
								<?php if ($user_ID) : ?><img alt="<?php _e('Edit',TEMPLATE_DOMAIN); ?>" src="<?php bloginfo('template_directory'); ?>/images/edit-icon-16x16.gif" class="blog-rss" /> <?php edit_post_link(__('Edit',TEMPLATE_DOMAIN),'',''); ?><?php endif; ?></p>
							</div>
						</div>
					</div>
				</div>
				<p>&nbsp;</p>
				<?php comments_template(); ?>
			<?php } ?>
<?php endwhile; ?>
				<div align="center"><div id="navi"><div id="navi-previous"><?php next_posts_link('&laquo; Previous Entries') ?></div><div id="navi-next"><?php previous_posts_link('Next Entries &raquo;') ?></div><div class="clear"></div></div></div>
<?php else : ?>
				<h1>Not Found</h1>
				<p>Sorry, but you are looking for something that isn't here.</p>
<?php endif; ?>
<!-- End Blog Body -->

				<hr />
				<div class="clear"></div>

<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/side-footer.php"); ?> 


<?php wp_footer(); ?>
</body>
</html>