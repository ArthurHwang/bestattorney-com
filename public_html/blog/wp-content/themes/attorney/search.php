<?php /*

Template Name: Search

*/

include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php");

include("folder-options.php");


// ADD ALL OTHER PAGE OPTIONS HERE!
$options['isBlogSingle'] = true;

// Get Escaped Search Queries. 
$query = htmlspecialchars($_GET['q']);
?>

<!DOCTYPE html>

<html>

<head>

<title>Bisnar Chase Blog: Search our Archives</title>

<meta name="description" content="Search our personal injury blog for news and information about product defects and dangerous situations that may affect you!">

<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/header.php"); ?> 

<?php 

the_post();
$blogTitle = get_the_title(); ?>

<h1 id="h1title" class="<?php if (strlen($blogTitle) > 68) {
	echo "smaller-title";
}?>"><?php echo $blogTitle; ?></h1>
	
<div class="wp_post" id="post-<?php the_ID(); ?>">
	<div id="breadcrumbs"><a href="<?php echo get_option('home'); ?>/">blog home</a> <?php the_category(' ') ?> <?php the_title(); ?></div>
	<div class="blog-content-wrap">


        <form role="search" method="get" id="searchform" class="searchform">
            <label class="screen-reader-text" for="q">Search for:</label>
            <input type="text" value="" name="q" id="q" placeholder="Enter your search term here:">
            <input type="submit" id="searchsubmit" value="Search">
        </form>
        <style>
	        #searchform {
	        	text-align:left;
	        }
    		#searchform label {
    			text-align:left;
    			width: 15%;
			    display: inline-block;
			    max-width: 130px;
			    min-width: 78px;
	        }		
        	#content #searchform #q {
    		    height: 35px;
    		    display:inline-block;
    		    width:55%;
    		    min-width:200px;
        	}
        	#searchform #searchsubmit {
        		height: 35px;
			    margin: 20px 0;
			    width: 20%;
			    min-width:80px;
			    background-color: #21323f;
			    color: white;
			    cursor: pointer;
			    padding:0;
        	}

        	.search-result {
        		padding-bottom:10px;
        		border-bottom:1px solid #21323f;
        		margin-bottom:10px;

        	}
        	.result-excerpt {
        		font-size:11px;
        		line-height: 16px;
        	}
        	.search-read-more {
        		float:right;
        		font-size:14px;
        	}
        </style>

        <p id="showing-results"></p>
        <div id="search-results">

        </div>
	</div>
</div>
<div class="clear"></div>


<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/side-footer.php"); ?> 
<?php wp_footer(); ?>

</body>
<script>
<?php if ($query) { ?>
	$(function() {
		let query = "<?php echo $query; ?>";
		getSearchResults(query);
		$("#q").val(query);
	});
<?php } ?>

    $("#searchform").submit(function(e) {
        e.preventDefault();
        var query = $("#q").val();
        getSearchResults(query);
    });

    function getSearchResults(query) {
        $.ajax({
            method: 'get',
            url: "https://www.bestattorney.com/blog/wp-json/wp/v2/posts?per+page=10&search="+query, 
            success: function(responseData) {
	            console.log("success");
	            displayResults(responseData, query);
	        },
	        fail: function(responseData) {
	            console.log("failure");
	            console.log(responseData);
	            $("#showing-results").html("Error: Could not find results for '"+query+"'");
	        }
        });
    }
    function displayResults(results, query) {
    	let searchHtml = ""; 
    	console.log(results);
    	for(var i=0; i < results.length; i++) {
    		searchHtml += "<div class='search-result'>";
    		searchHtml += "<h3 class='result-title'><a href='"+results[i].link+"'>"+results[i].title.rendered+"</a></h3>";
    		searchHtml += "<div class='result-excerpt'>"+results[i].excerpt.rendered.substring(0, 300)+"... <a class='search-read-more' href='"+results[i].link+"'><em>...Read Full Post</em></a></div>";
    		searchHtml += "</div>";
    	}
    	$("#search-results").html(searchHtml);
    	$("#showing-results").html("Showing "+results.length+ " result(s) for "+query);
    }

</script>

</html>