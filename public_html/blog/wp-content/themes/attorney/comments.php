<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie ?>
		<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
<?php	return; }
	}
	/* This variable is for alternating comment background */
	$oddcomment = 'alt';
?>
    <!-- You can start editing here. -->
<?php if ($comments) : ?>
	<h2 id="comments"><?php comments_number('No Responses', 'One Response', '% Responses' );?> to &#8220;<?php the_title(); ?>&#8221;</h2>
	<ol class="wp_commentlist">
		<?php foreach ($comments as $comment) : ?>
		<li class="<?php echo $oddcomment; ?>" id="comment-<?php comment_ID() ?>"> On <small class="commentmetadata"><?php comment_date('F jS, Y') ?> at <?php comment_time() ?> <?php edit_comment_link(); ?></small><br />
			<cite><?php comment_author_link() ?></cite> said: <?php if ($comment->comment_approved == '0') : ?><em>Your comment is awaiting moderation.</em><?php endif; ?><br />
			<?php comment_text() ?></li>
		<?php /* Changes every other comment to a different class */
			if ('alt' == $oddcomment) $oddcomment = ''; else $oddcomment = 'alt'; ?>
		<?php endforeach; /* end for each comment */ ?>
	</ol>
<?php else : // this is displayed if there are no comments so far ?>
	<?php if ('open' == $post->comment_status) : ?>
	<!-- If comments are open, but there are no comments. -->
	<?php else : // comments are closed ?>
	<!-- If comments are closed, display nothing. -->
	<?php endif; ?>
<?php endif; ?>
<?php if ('open' == $post->comment_status) : ?>
	<h2 id="respond">Leave a Reply:</h2>
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>
<?php else : ?>
	<?php if ( $user_ID ) : ?>
	<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout &raquo;</a></p>
	<?php endif; ?>
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post"  id="commentform">
		<?php if ( !$user_ID ) : ?>
		  <label for="author" class="cformlt">Name<?php if ($req) echo " (required)"; ?>:</label>
		  <p class="cformrt"><input type="text" name="author" id="author" class="ciform" value="<?php echo $comment_author; ?>" size="40" tabindex="1" /></p>
		  <div class="clear"></div>
		  <label for="email2" class="cformlt">E-Mail<?php if ($req) echo " (required)"; ?>:</label>
		  <p class="cformrt"><input type="text" name="email" id="email2" class="ciform" value="<?php echo $comment_author_email; ?>" size="40" tabindex="2" /></p>
		  <div class="clear"></div>
		  <label for="url" class="cformlt">Website:</label>
		  <p class="cformrt"><input type="text" name="url" id="url" class="ciform" value="<?php echo $comment_author_url; ?>" size="40" tabindex="3" /></p>
		  <div class="clear"></div>
		<?php endif; ?>
		  <label for="comment" class="cformlt">Comment:</label>
		  <p class="cformrt"><textarea name="comment" id="comment" cols="76" rows="6" tabindex="4" class="ctform"></textarea></p>
		  <div class="clear"></div>
		  <p class="alcenter clear"><input name="submit" type="submit" tabindex="5" value="Submit Comment" class="button" /></p>
		<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
		<?php do_action('comment_form', $post->ID); ?>
	</form>

<?php endif; // If registration required and not logged in ?>
<?php endif; // if you delete this the sky will fall on your head ?>