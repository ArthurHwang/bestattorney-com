<?php 
require 'vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';

$sendgrid = new SendGrid( $sg_username, $sg_password );
$inquirydropmail = new SendGrid\Email();
$outbounddropmail = new SendGrid\Email();

$events = json_decode(file_get_contents("php://input"));

foreach ($events as $event) {
	$text = "";
	$text .= ("Date: ".date("m.d.y g:i a")."\n");
	$text .= ("ID: ".$event->timestamp."\n");
	$text .= ("Email: ".$event->email."\n");
	$text .= ("Event: ".$event->event."\n");
	$text .= ("Reason: ".$event->reason."\n\n");
	$text .= ("______________________________\n\n");

	
	$allrecords .= $text;


	if ($event->event == "dropped" || $event->event == "deferred" || $event->event == "bounce") {
		
		$notdelivered .= $text;

		if ($event->email == "inquiry@bestattorney.com" || $event->email == "inquiry@bisnarchase.com") {
			$emailmessage = ("This is an automated notification of an email that was sent through the contact form on Bestattorney.com, but was unable to be delivered to inquiry@bestattorney.com.
Usually this is just spam, but occasionally a client will attempt to contact us and will not receive a response because of an error like this. \n
Because this notice is sent when it determines that an email to OUR servers has dropped, the email address of the person who tried to contact us is not available to be included in this email \n\n
To determine the email of the intake:\n
 - Open the page on our website: https://www.bestattorney.com/sendgrid-php/all-records.txt \n
 - Find the following email record: (will usally be at the bottom of the document unless you waited a while since receiving this email.) \n"
 . $text .
" - an email record adjacent (usually below) this one should have the email address of client who attempted to contact us. Check the email address of this record against the emails that we have received in the inquiry inbox, and if there are no matches, check if a copy of the email was sent to bisnarchaseattorneys@gmail.com. If not, contact this email to see if they need a lawyer (as long as it is not a spammy looking email address.)\n
 - Sometimes there will be no matching record of email, which indicates that the client filling out the form did not add an email address. Unfortunately this means we're SOL on this client unless they contact again. \n\n
 If you have any questions, please contact Matt Cheah. (Mcheah@bisnarchase.com or matt.cheah@gmail.com if I'm no longer working at B|C (sorry, suckers) )");
		
			$inquirydropmail->
			setFrom("inquiry@bisnarchase.com")->
			addTo("inquiry@bisnarchase.com")->
			setSubject("ATTENTION: Dropped Email Send From Contact Form")->
			setText($emailmessage);
			    
			$response1 = $sendgrid->send($inquirydropmail);

			if (!$response1) {
				mail("marketing@bestattorney.com","Event-Webhook Email Not Going Through", "For some reason the \$inquirydropmail in bestattorney.com/sendgrid-php/event-webhook.php is not going through. Check on this, dawg. (From Matt Cheah - check the /sendgrid-php/event-webhook.php file");
			}
		}
	}
	else {
		$delivered .= $text;
	}

	
}

file_put_contents("all-records.txt", $allrecords, FILE_APPEND | LOCK_EX);
file_put_contents("dropped-records.txt", $notdelivered, FILE_APPEND | LOCK_EX);
file_put_contents("delivered-records.txt", $delivered, FILE_APPEND | LOCK_EX);

?>