<?php
// Folder Options for /costa-mesa
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 3,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "orange-county", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"nav" => "default",

	"sidebarLinksTitle" => "Costa Mesa Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
        "Bicycle Accidents" => "/costa-mesa/bicycle-accidents.html",
        "Car Accidents" => "/costa-mesa/car-accidents.html",
        "Dog Bites" => "/costa-mesa/dog-bites.html",
        "Employment Law" => "/costa-mesa/employment-lawyers.html",
        "Motorcycle Accidents" => "/costa-mesa/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/costa-mesa/nursing-home-abuse.html",
        "Truck Accidents" => "/costa-mesa/truck-accidents.html",
        "Wrongful Death" => "/costa-mesa/wrongful-death.html",
        "Costa Mesa Home" => "/costa-mesa/",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>