<?php
// Folder Options for /anaheim
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 3,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "orange-county", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"cityString" => "Anaheim",

	"nav" => "default",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
        "Bicycle Accidents" => "/anaheim/bicycle-accidents.html",
        "Brain Injury" => "/anaheim/brain-injury.html",
        "Bus Accidents" => "/anaheim/bus-accidents.html",
        "Car Accidents" => "/anaheim/car-accidents.html",
        "Dog Bites" => "/anaheim/dog-bites.html",
        "Employment Law" => "/anaheim/employment-lawyers.html",
        "Motorcycle Accidents" => "/anaheim/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/anaheim/nursing-home-abuse.html",
        "Pedestrian Accidents" => "/anaheim/pedestrian-accidents.html",
        "Slip and Fall Accidents" => "/anaheim/slip-and-fall-accidents.html",
        "Truck Accidents" => "/anaheim/truck-accidents.html",
        "Wrongful Death" => "/anaheim/wrongful-death.html",
        "Anaheim Home" => "/anaheim/",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>