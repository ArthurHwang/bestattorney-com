$(function() {
  // ===============================

  // Start loading the results.

  // ===============================

  // $("#case-filter-area input").change(function() {

  // 	maxResults = 10;

  // 	loadResults(10, filterResults());

  // });

  window.addEventListener('popstate', function(event) {
    // Re-displays older results when the back button is clicked.
    $('#case-list').html(event.state)
  })

  $('.read-more').on('click', '.collapsed', function() {
    height = $('#philosophy-container').outerHeight()

    $('#philosophy-well').animate(
      {
        height: height + 100
      },
      800
    )

    $('.read-more .btn')
      .toggleClass('expanded collapsed')
      .text('Collapse')
  })

  $('.read-more').on('click', '.expanded', function() {
    $('#philosophy-well').animate(
      {
        height: '150px'
      },
      800
    )

    $('.read-more .btn')
      .toggleClass('expanded collapsed')
      .text('Read More')
  })
})

const mix = {
  methods: {
    filterResults: function filterResults() {
      if (this.urlOptions.caseId != '') {
        var catastrophic = 'all'

        var locations = []

        var types = []

        var value = 'all'

        return [locations, types, value, catastrophic, this.urlOptions.caseId]
      } else {
        var catastrophic = true

        if (vm.urlOptions.catastrophicFilter == 'false') {
          // $(".cat-sel").toggleClass("catastrophic-unselected active");

          catastrophic = false
        } else if ($('.catastrophic-unselected').attr('id') == 'catastrophic-injuries') {
          catastrophic = false
        }

        var locations = []

        var types = []

        var value = $('#case-filter-area #award input[type="radio"]:checked').val()

        $('#case-filter-area #locations input:checked').each(function() {
          locations.push($(this).val())
        })

        $('#case-filter-area #type input:checked').each(function() {
          types.push($(this).val())
        })

        return [locations, types, value, catastrophic]
      }
    },
    loadResults: function loadResults(max, arrays) {
      var catastrophic = arrays[3]

      var locations = arrays[0]

      var types = arrays[1]

      var value = arrays[2]

      arrays.unshift(max)

      arrays = JSON.stringify(arrays)

      $.ajax({
        type: 'POST',

        url: 'https://www.bestattorney.com/case-results/case-results-data.php',

        data: 'array=' + arrays
      })
        .done(function(data) {
          //console.log(data);
          vm.caseList = $.parseJSON(data)

          // history.pushState(contents[0], "", window.location.href); // This just saves the content that is showing right now so it can be re-displayed when the user hits the back button or something. This is a little broken because it doesn't update the checkboxes and stuff when the back button is clicked but i'm too lazy to fix that.

          if (vm.caseList[1] == 0) {
            $('#number-of-results').html(vm.caseList[1])
            var filterSentence = 'for '
            var typesArray = []
            var locationsArray = []
            var valueText
            var index

            //Catastrophic
            if (catastrophic == true) {
              filterSentence += 'catastrophic'
            } else if (catastrophic == false) {
              filterSentence += 'non-catastrophic'
            }

            //Category
            if (types.length == 0) {
              typesArray.push(' incidents')
            } else {
              for (index = 0, len = types.length; index < len; ++index) {
                switch (types[index]) {
                  case 'auto':
                    typesArray.push(' car accidents')
                    break
                  case 'motorcycle':
                    typesArray.push(' motorcycle accidents')
                    break
                  case 'pedestrian':
                    typesArray.push(' pedestrian accidents')
                    break
                  case 'autodefect':
                    typesArray.push(' auto defects')
                    break
                  case 'premises':
                    typesArray.push(' premises liability incidents')
                    break
                  case 'other':
                    typesArray.push(' uncategorized incidents')
                    break
                }
              }

              if (types.length > 1) {
                typesArray.splice(types.length - 1, 0, ' or')
              }
            }

            filterSentence += typesArray
            filterSentence = filterSentence.replace('or,', 'or')

            //Location
            if (locations[0] != null) {
              filterSentence += ' in'
              for (index = 0, len = locations.length; index < len; ++index) {
                switch (locations[index]) {
                  case 'la':
                    locationsArray.push(' Los Angeles')

                    break

                  case 'oc':
                    locationsArray.push(' Orange County')

                    break

                  case 'norcal':
                    locationsArray.push(' Northern California')

                    break

                  case 'other':
                    locationsArray.push(' other locations')

                    break
                }
              }

              if (locations.length > 1) {
                locationsArray.splice(locations.length - 1, 0, ' or')
              }

              filterSentence += locationsArray

              filterSentence = filterSentence.replace('or,', 'or')
            }

            //value

            if (value != 'all') {
              switch (value) {
                case '1+':
                  valueText = ' where the case is worth more than $1,000,000'
                  break
                case '5+':
                  valueText = ' where the case is worth more than $5,000,000'
                  break
                case 'conf':
                  valueText = ' where the case result is confidential'
                  break
              }

              //console.log(value,valueText);

              filterSentence += valueText
            }

            $('#filter-text').html(filterSentence)
          } else {
            $('#number-of-results').html(vm.caseList[1])

            if (vm.caseList[2] > max) {
              $('#total-results').html(' out of ' + vm.caseList[2])

              $('#more-button').slideDown()
            } else {
              $('#total-results').html('')

              $('#more-button').slideUp()
            }

            $('#filter-text').html('')
          }
        })
        .fail(function() {
          console.log('failed to get case results from AJAX request !')
        })
    }
  }
}

var vm = new Vue({
  el: '#case-results-app',
  data: {
    caseList: [],
    urlOptions: {
      caseId: null,
      categoryFilter: null,
      locationFilter: null,
      awardFilter: null,
      catastrophicFilter: true
    },
    maxResults: 10
  },
  computed: {
    // caseList: function() {
    // 	$.ajax({
    // 		type: 'POST',
    // 		url: "https://www.bestattorney.com/case-results/case-results-data.php",
    // 		data:"caseIds"
    // 	}).done(function(data) {
    // }
    catastrophicButton: function() {
      var active = this.urlOptions.catastrophicFilter
      var catastrophicUnselected = !this.urlOptions.catastrophicFilter
      console.log('active: ' + active)
      return { active: active, 'catastrophic-unselected': catastrophicUnselected }
    },
    nonCatastrophicButton: function() {
      var active = !this.urlOptions.catastrophicFilter
      var catastrophicUnselected = this.urlOptions.catastrophicFilter
      console.log('active: ' + active)
      return { active: active, 'catastrophic-unselected': catastrophicUnselected }
    }
  },
  methods: {
    addMore: function() {
      console.log('clicked')
      this.maxResults += 10
      this.loadResults(this.maxResults, this.filterResults())
    },
    selectCatastrophic: function() {
      console.log('selectCat')
      this.maxResults = 10
      this.catastrophicFilter = true
      this.loadResults(10, this.filterResults())
    },
    selectNonCatastrophic: function() {
      console.log('selectNonCat')
      this.maxResults = 10
      this.catastrophicFilter = false
      this.loadResults(10, this.filterResults())
    }
  },
  mixins: [mix],
  created: function() {
    this.loadResults(10, this.filterResults())
    this.urlOptions.caseId = ''
  }
})
