<div id="results-wrap" class="jcarousel-wrapper">

	<ul id="results-wrap-ul" data-jcarousel="true">

  <li id="result-1"><div class="result-index">1</div><a href="https://www.bestattorney.com/case-results/?id=1&expand=true"><span>$38,650,000</span><br />Motorcycle Accident</a></li>

          <li id="result-2"><div class="result-index">2</div><a href="https://www.bestattorney.com/case-results/?id=2&expand=true"><span>$30,000,000</span><br />Motorcycle Accident</a></li>

          <li id="result-3"><div class="result-index">3</div><a href="https://www.bestattorney.com/case-results/?id=3&expand=true"><span>$24,744,764</span><br />Auto Defect</a></li>

          <li id="result-4"><div class="result-index">4</div><a class="result-confidential" ><span>$23,091,098</span><br />Product Liability</a></li>

          <li id="result-5"><div class="result-index">5</div><a href="https://www.bestattorney.com/case-results/?id=4&expand=true"><span>$16,444,904</span><br />Driver Negligence</a></li>

          <li id="result-6"><div class="result-index">6</div><a class="result-confidential" ><span>$16,000,000</span><br />Auto Accident</a></li>

          <li id="result-7"><div class="result-index">7</div><a href="https://www.bestattorney.com/case-results/?id=5&expand=true"><span>$10,030,000</span><br />Premises Negligence</a></li>

          <li id="result-8"><div class="result-index">8</div><a class="result-confidential" ><span>$9,800,000</span><br />Motor Vehicle Accident</a></li>

          <li id="result-9">
						<div class="result-index">
							9
						</div>
						<a class="result-confidential">
							<span>$8,500,000</span>
							<br/>
							Motor Vehicle Accident
						</a>
					</li>

          <li id="result-10">
						<div class="result-index">
							10
						</div>
						<a href="https://www.bestattorney.com/case-results/?id=5&expand=true">
							<span>$8,250,000</span>
							<br/>
							Premises Liability
						</a>
					</li>

          <li id="result-11">
			<div class="result-index">
				11
			</div>
			<a class="result-confidential" >
				<span>$7,998,073</span>
				<br/>
				Product Liability
			</a>
		</li>

		<li id="result-12">
			<div class="result-index">
				12
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=6&expand=true">
				<span>$5,500,000</span>
				<br/>
				Defective Door Latch
			</a>
		</li>

		<li id="result-13">
			<div class="result-index">
				13
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=7&expand=true">
				<span>$3,000,000</span>
				<br/>
				Pedestrian Accident
			</a>
		</li>

		<li id="result-14">
			<div class="result-index">
				14
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=8&expand=true">
				<span>$3,000,000</span>
				<br/>
				Motorcycle Accident
			</a>
		</li>



		<li id="result-15">
			<div class="result-index">
				15
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=10&expand=true">
				<span>$3,000,000</span>
				<br/>
				Trolley vs Bicycle Accident
			</a>
		</li>

		<li id="result-16">
			<div class="result-index">
				16
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=17&expand=true">
				<span>$2,815,958</span>
				<br/>
				Premises Liability
			</a>
		</li>

		<li id="result-17">
			<div class="result-index">
				17
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=19&expand=true">
				<span>$2,800,000</span>
				<br/>
				Premises Liability
			</a>
		</li>

		<li id="result-18">
			<div class="result-index">
				18
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=30&expand=true">
				<span>$2,500,000</span>
				<br/>
				Auto Defect
			</a>
		</li>

		<li id="result-19">
			<div class="result-index">
				19
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=31&expand=true">
				<span>$2,500,000</span>
				<br/>
				Auto Defect
			</a>
		</li>

		<li id="result-20">
			<div class="result-index">
				20
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=50&expand=true">
				<span>$2,000,000</span>
				<br/>
				Wage & Hour
			</a>
		</li>

		<li id="result-21">
			<div class="result-index">
				21
			</div>
			<a href="https://www.bestattorney.com/case-results/?id=32&expand=true">
				<span>$1,925,000</span>
				<br/>
				Auto
			</a>
		</li>

	</ul>

</div>
