<?php
// Folder Options for /case-results

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.


	"spanishPageEquivalent" => "resultos-destacados.html",


	// =====================================
	// Defining Sidebar Content
	// =====================================
	"sidebarLinksTitle" => "Client Stories",

	"sidebarLinks" => array(
       "Jaklin Romine: Auto Defect" => "/case-results/jaklin-romine.html",
       "Lucas Vogt: Motorcycle" => "/case-results/lucas-vogt.html",
       "Olivier Maciel: Premises" => "/case-results/olivier-maciel.html"
    ),

	"showSidebarContactForm" => true,

	"sidebarContentTitle" => "Our Case Results",

	"sidebarContentHtml" => "
		<p style='padding-bottom:18px;'>Thanks for reading through our case results.</p>
	    <p><a href='https://www.bestattorney.com/case-results/' style='color:#70ABDC;'>Click here to go to the full case result stories page</a></p>
	    <p><a href='https://www.bestattorney.com/case-results/top-results.html' style='color:#70ABDC;'>Click here to see a list of our top case results!</a></p>
	    <ul class='bpoint' style='border-top: 1px solid #2e4c66; padding: 19px 0 13px;margin:0 34px;'>
	    </ul>
	",

    "extraSidebar" => "",

	"caseResultsArray" => [], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	// "loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>