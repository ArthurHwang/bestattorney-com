<?php



$caseresults = array(

  array(

    "id" => 1,

    "imageurl" => "motorcycle-vogt.jpg",

    "caption" => "Vogt v. Saldate",

    "header" => "Temporarily Confidential",

    "type" => "Motorcycle Accident",

    "summary" => "Temporarily Confidential",

    "doi" => "July 25, 2011",

    "plaintiff" => "Lucas Vogt",

    "defendant" => "Andrew Saldate",

    "injuries" => array("Traumatic Brain Injury", "Multiple Fractures", "Road Rash"),

    "s1" => "The Accident: ",

    "p1" => "Lucas Vogt was travelling on the 91 freeway through Anaheim when a car changed lanes and collided with his motorcycle. Vogt was knocked off his motorcycle into traffic where he was hit 2 times by oncoming cars and suffered multiple traumatic injuries.",

    "s2" => "The total cost for Lucas Vogt's medical expenses ",

    "p2" => "2 years after the accident was about 2 million dollars, but Vogt was still not able to walk or speak due to the injuries he sustained in the accdident, and his life would be forever changed due to the physical and emotional challenges he'd encounter on the road to recovery. Bisnar Chase was able to settle 2 cases against the drivers that hit Vogt subsequently after he had been knocked off his motorcycle, and took the primary case to trial. Bisnar Chase ended up winning a judgment of $30 million for Lucas Vogt and his family, to help him as he recovers from his injuries.",

    "award" => "$38,650,000",

    "awardint" => 38650000,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/attorneys/brian-chase.html",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Anaheim", "Plaintiff's Residence" => "Menifee", "Plaintiff's Location" => "Riverside"),

    "category" => array("motorcycle", "auto"),

    "catastrophic" => true,
  ),

  array(

    "id" => 2,

    "imageurl" => "motorcycle-vogt.jpg",

    "caption" => "Vogt v. Saldate",

    "header" => "Lucas Vogt v. Andrew Saldate",

    "type" => "Motorcycle Accident",

    "summary" => "Motorcyclist struck by 2 cars on the freeway",

    "doi" => "July 25, 2011",

    "plaintiff" => "Lucas Vogt",

    "defendant" => "Andrew Saldate",

    "injuries" => array("Traumatic Brain Injury", "Multiple Fractures", "Road Rash"),

    "s1" => "The Accident: ",

    "p1" => "Lucas Vogt was travelling on the 91 freeway through Anaheim when a car changed lanes and collided with his motorcycle. Vogt was knocked off his motorcycle into traffic where he was hit 2 times by oncoming cars and suffered multiple traumatic injuries.",

    "s2" => "The total cost for Lucas Vogt's medical expenses ",

    "p2" => "2 years after the accident was about 2 million dollars, but Vogt was still not able to walk or speak due to the injuries he sustained in the accdident, and his life would be forever changed due to the physical and emotional challenges he'd encounter on the road to recovery. Bisnar Chase was able to settle 2 cases against the drivers that hit Vogt subsequently after he had been knocked off his motorcycle, and took the primary case to trial. Bisnar Chase ended up winning a judgment of $30 million for Lucas Vogt and his family, to help him as he recovers from his injuries.",

    "award" => "$30,000,000",

    "awardint" => 30000000,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/case-results/lucas-vogt.html",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Anaheim", "Plaintiff's Residence" => "Menifee", "Plaintiff's Location" => "Riverside"),

    "category" => array("motorcycle", "auto"),

    "catastrophic" => true,
  ),


  array(

    "id" => 3,

    "imageurl" => "seatback-dummy-romine.jpg",

    "caption" => "A dummy in a seatback failure test for Romine's case",

    "header" => "Romine v. Johnson Controls",

    "type" => "Seatback Failure Auto Defect",

    "summary" => "Rear-end accident, seatback broke resulting in quadriplegia",

    "doi" => "October 21, 2006",

    "plaintiff" => "Jaklin Mikhal Romine",

    "defendant" => "Johnson Controls",

    "injuries" => array("Severe Brain Injury", "Quadriplegia"),

    "s1" => "The Accident: ",

    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",

    "s2" => "Johnson Controls ",

    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",

    "award" => "$24,744,764+",

    "awardint" => 24744764,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/case-results/jaklin-romine.html",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),

    "category" => array("auto-defect", "auto"),

    "catastrophic" => true,
  ),


  array(

    "id" => 4,

    "imageurl" => "christopher-chan-interview.jpg",

    "caption" => "Christopher Chan suffered a brain injury after being hit by a bike.",

    "header" => "Christopher Chan vs City of Hanford",

    "type" => "Bicycle Accident",

    "summary" => "Bicyclist struck by van - city-created dangerous intersection",

    "doi" => "May 31, 2005",

    "plaintiff" => "Christopher Chan",

    "defendant" => "City of Hanford",

    "injuries" => array("Severe Brain Damage"),

    "s1" => "The Accident: ",

    "p1" => "Christopher Chan was biking home from school a week before his middle school graduation, and was hit by a van driving 35-45mph as he rode through an intersection. He was thrown up to 50 feet and had to be airlifted to a medical center in Fresno. He suffered extensive brain injury that permanently left him with cognitive deficiencies.",

    "s2" => "Bisnar Chase claimed ",

    "p2" => "that the City of Hanford was negligent and at fault for allowing the neighborhood around the intersection to develop, but to not include crosswalks or set up traffic signals to prevent a dangerous intersection. At the point of Christopher's accident, drivers were unable to see pedestrians and bicyclists who entered the intersection, thereby making it a pedestrian 'trap'. Bisnar Chase secured a total of $16 million for Christopher Chan and his family due to medical bills and future loss of earnings and enjoyment of life.",

    "award" => "$16,444,904",

    "awardint" => 16444904,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/case-results/christopher-chan.html",

    "location" => array("norcal"),

    "relatedLocations" => array("Accident Location" => "Hanford", "Plaintiff's Residence" => "Hanford", "Plaintiff's Location" => "Fresno"),

    "category" => array("auto, other"),

    "catastrophic" => true,
  ),


  array(

    "id" => 5,

    "imageurl" => "blue-cleaning-maciel.jpg",

    "caption" => "Olivier Maciel was 2 years old when he drank a cleaning agent that was left out on a patio. ",

    "header" => "Maciel v. Defendant Cleaning Corporation",

    "type" => "Premises Liability",

    "summary" => "Child drank liquid detergent that was left  out on a patio.",

    "doi" => "November 10, 2008",

    "plaintiff" => "Olivier Maciel",

    "defendant" => "[Confidential Defendent]",

    "injuries" => array("Chemical Burns to the Esophagus"),

    "s1" => "The Accident: ",

    "p1" => "2-year-old Olivier Maciel and his mother were visiting his great grandmother in a nursing home, when Oilver drank some liquid laundry detergent that had been left in a cup on an interior patio. Olivier suffered chemical burns which required him to have surgery to install a feeding tube into his stomach so that he could get nutrition.",

    "s2" => "Bisnar Chase won total final award totaled $10,030,000.",

    "p2" => "",

    "award" => "$10,030,000",

    "awardint" => 10030000,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/case-results/olivier-maciel.html",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Tustin", "Plaintiff's Residence" => "Corona"),

    "category" => array("premises"),

    "catastrophic" => true,
  ),


  array(

    "id" => 6,
    "imageurl" => "eucalyptus-tree-chairez.jpg",

    "caption" => "Veronica Chairez was rendered a paraplegic when she was struck by a falling branch.",

    "header" => "Veronica Chairez v. The State of California",

    "type" => "Premises Negligence",

    "summary" => "Victim struck by a falling branch that the state failed to inspect.",

    "doi" => "July 26, 1998",

    "plaintiff" => "Veronica Chairez",

    "defendant" => "State of California",

    "injuries" => array("Paraplegia"),

    "s1" => "The Accident: ",

    "p1" => "Veronica and her friend Ziola were picnicing underneath a large Eucalyptus tree at the Lake Perris State Recreation Area. Suddenly a large branch weighing 600-800 pounds broke off from the tree and hit Veronica on the head, rendering her a paraplegic. An expert forrester from the State of California revealed that it was normal for Eucalyptus trees to drop large limbs in the summertime.",

    "s2" => "Because this was a well known phenomena, ",

    "p2" => "The State of California’s Department of Parks and Recreation mandated that these trees be inspected and pruned every 2 years. At the time that Veronica was injured, the inspection was more than a year overdue, and that specific tree had already lost another large branch, increasing the likelyhood that it would happen again. Bisnar Chase was able to recover $8,250,000 for Veronica as a result of the Department of Parks and Recreation's negligence.",

    "award" => "$8,250,000",

    "awardint" => 8250000,

    "awardcat" => "5+",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Moreno Valley"),

    "category" => array("premises"),

    "catastrophic" => true,
  ),


  array( // This one was in an annuity - make sure that number goes up

    "id" => 7,

    "imageurl" => "car-door-griselda.jpg",

    "caption" => "Jane Doe was thrown from her car due to a defective door latch",

    "header" => "Jane Doe vs. [Defendant Corporation]",

    "type" => "Defective Door Latch",

    "summary" => "Victim thrown from the vehicle when a defective car door opened during a crash",

    "doi" => "October 6, 2013",

    "plaintiff" => "Jane Doe",

    "defendant" => "[Confidential Defendent]",

    "injuries" => array("Brain Injury", "Broken Ribs", "Loss of Mobility"),

    "s1" => "Jane Doe was on her way to the hospital after midnight ",

    "p1" => "with her 2 year old goddaughter, Delilah, who was running an extremely high fever. They were both in the back seat of a 2010 sedan driven by Delilah’s mother and Jane’s cousin, Maria, with Delilah’s father in the front seat. The four of them were going through an intersection in Los Angeles when they were hit by a drunk driver who was running a red light. The car’s door latch on Jane's side failed, flinging open the door. As the car was spinning out, Jane and Delilah were thrown from the car. Jane wrapped herself around Delilah to protect her as they were in motion, and the child fortunately emerged from the accident with only minor injuries.",

    "s2" => "Jane, on the other hand, ",

    "p2" => "suffered devastating injuries, including traumatic brain injury and broken bones. She was admitted to a rehab hospital, but now requires full-time care and is 100% dependent on her parents. She is severely limited in mobility and requires multiple therapies to improve her condition. Our team at Bisnar Chase was able to recover 5.5 million dollars from a confidential defendant, to help take care of Jane in the long term.",

    "award" => "$5,500,000",

    "awardint" => 5500000,

    "awardcat" => "5+",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Los Angeles", "Plaintiff's State of Residence" => "Texas"),

    "category" => array("auto-defect", "auto"),

    "catastrophic" => true,
  ),




  array(

    "id" => 8,

    "imageurl" => "dog-in-road-butenhoff.jpg",

    "caption" => "Joseph Butenhoff was trying to protect a husky who had been hit by a car, when he was struck by a speeding driver",

    "header" => "Joseph Butenhoff v. Bautista",

    "type" => "Pedestrian Accident",

    "summary" => "Victim struck by a speeding vehicle while trying to help a dog in the road",

    "doi" => "December 3, 2012",

    "plaintiff" => "Joseph Butenhoff",

    "defendant" => "Rae Anne Bautista",

    "injuries" => array("Severe Brain Injury", "Fractures", "Lung Contusion"),

    "s1" => "The Accident: ",

    "p1" => "Joseph Butenhoff was standing in the middle of a Long Beach road directing traffic around a friends husky, who had been hit by a car. Butenhoff was hit by defendent Rae Anne Bautista, who was not paying attention to the road and drove straight into him, resulting in a severe brain injury amongst other injuries.",

    "s2" => "Allstate made Butenhoff an offer of $100,000 ",

    "p2" => "to counter the $875,000 claim that he filed, but Bisnar Chase wouldn't accept it. After taking Bautista to trial, the jury awarded Butenhoff half a million dollars for past pain and suffering, and $2.5 million to take care of his future.",

    "award" => "$3,000,000",

    "awardint" => 3000000,

    "awardcat" => "1+",

    "storylink" => "http://www.prlog.org/12484570-bisnar-chase-secures-3-million-jury-verdict-for-client-with-traumatic-brain-injury.html",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Long Beach", "Plaintiff's Residence" => "Long Beach"),

    "category" => array("pedestrian", "auto"),

    "catastrophic" => true,
  ),




  array(

    "id" => 9,

    "imageurl" => "motorcycle-prock.jpg",

    "caption" => "Rickey Prock was hit by a delivery driver who was on the job",

    "header" => "Rickey Prock v. A. Levy and J. Zentner Corporation",

    "type" => "Motorcycle Accident",

    "summary" => "Motorcyclist hit by a delivery driver on the job",

    "doi" => "July 7, 1993",

    "plaintiff" => "Ricky Prock",

    "defendant" => "A. Levy and J. Zentner Corporation",

    "injuries" => array("Severed Foot", "Fingers Fused Together"),

    "s1" => "The Accident: ",

    "p1" => "Rickey Prock was newly married when a car came across 3 lanes and hit him while he was on his motorcycle, sending him 30 feet down the street. His foot was nearly severed off his leg and he had other extensive injuries. His new wife Debbie was undergoing chemotherapy and neither of them were able to work to pay off their bills.",

    "s2" => "When Bisnar Chase stepped in, ",

    "p2" => "Rickey and Debbie had been told by their first lawyer that they wouldn't get more than $15,000 due to insurance limits. Bisnar Chase reviewed the case details and found that the driver that hit Rickey was on the job, and A. Levy and J. Zentner Corporation, the company that the driver worked for, was responsible for all damages. Bisnar Chase was able to recover 3 million dollars for the Procks, and they were able to afford the 6 surgeries and years of physical therapy to repair Rickey's foot.",

    "award" => "$3,000,000",

    "awardint" => 3000000,

    "awardcat" => "1+",

    "storylink" => "https://www.bestattorney.com/case-results/rickey-prock.html",

    "location" => array("norcal"),

    "relatedLocations" => array("Accident Location" => "Sacramento"),

    "category" => array("motorcycle", "auto"),

    "catastrophic" => true,
  ),




  array(

    "id" => 10,

    "imageurl" => "crossing-street-villamor.jpg",

    "caption" => "Jasmine Villamor was struck walking through an intersection",

    "header" => "Jasmine Villamor vs. John Bragonnier",

    "type" => "Pedestrian Accident",

    "summary" => "Pedestrian struck by a car in an intersection",

    "doi" => "August 14, 2007",

    "plaintiff" => "Jasmine Villamor",

    "defendant" => "John Bragonnier",

    "injuries" => array("Hip and Lower Back Pain", "Disc Injuries Requiring Lower Back Surgery"),

    "s1" => "The Accident: ",

    "p1" => "Jasmine Villamor was jogging to work from her home in Redondo Beach, CA, when she entered a pedestrian crossing and was hit on her left side by a man driving a 2005 Prius. She was knocked to the ground and was taken to the hospital by one of her co-workers.",

    "s2" => "Jasmine continued to feel pain",

    "p2" => " in her lower back and legs, forcing her to go through rounds of physical therapy, and eventually surgery, almost 2 years later. She worked as a dance teacher at a studio, but was only able to teach in a limited capacity, and missed 6 months of work due to her surgery. Bisnar Chase was able to secure a settlement of 1 million dollars to pay for Jasmine's medical bills, lost earnings, future lost earnings, and general damage.",

    "award" => "$1,000,000",

    "awardint" => 1000000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Hermosa Beach", "Plaintiff's Residence" => "Redondo Beach"),

    "category" => array("pedestrian"),

    "catastrophic" => false,
  ),


  array(

    "id" => 11,

    "imageurl" => "azusa-trolley-farris.jpg",

    "caption" => "Julie Farris was hit by a trolley at APU",

    "header" => "Julie Farris vs. City of Azusa",

    "type" => "Trolley vs Bicycle Accident",

    "summary" => "Victim struck by a trolley while riding a bike",

    "doi" => "February 19, 2008",

    "plaintiff" => "Julie Farris",

    "defendant" => "City of Azusa",

    "injuries" => array("4-Month Coma", "Brain Damage", "Multiple Fractures"),

    "s1" => "The Accident: ",

    "p1" => "Julie Farris was riding her bike through the City of Azusa when she became trapped between parked cars on her right side and an incoming trolley on the left side. The trolley struck her handlebars on the left side, turning her bike violently to the left and throwing her over her handlebars head first into the trolley, giving her catastrophic brain injuries and putting her in a 4 month coma.",

    "s2" => "Julie had 1.7 million dollars in medical bills",

    "p2" => " and will require lifetime care for her injuries due to the neurological trauma she experienced. Experts determined that despite continual occupational, physical, and speech therapy, Julie would not be able to have net earnings in a competetive workplace. Bisnar Chase was able to secure multiple settlements for Julie and her family totaling 3 million dollars, for future medical care, loss of future earnings, and general damages.",

    "award" => "$3,000,000",

    "awardint" => 3000000,

    "awardcat" => "$1+",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Azusa", "Plaintiff's Residence" => "Costa Mesa"),

    "category" => array("auto, other"),

    "catastrophic" => true,
  ),


  array(

    "id" => 12,

    "imageurl" => "pedicure-griffeth.jpg",

    "caption" => "Kristina Griffeth's foot was infected at a nail salon due to unsanitary conditions.",

    "header" => "Kristina Griffeth vs Paradise Nails",

    "type" => "Premises Liability",

    "summary" => "Victim given an infection at a nail salon",

    "doi" => "December 21, 2013",

    "plaintiff" => "Kristina Griffeth",

    "defendant" => "CNA Insurance & Paradise Nail Salon",

    "injuries" => array("Diagnosed with MRSA and Cellulitis", "Nerve Damage in Leg"),

    "s1" => "The Accident: ",

    "p1" => "Kristina Griffeth was on vacation in Monterey, CA, getting a pedicure at Paradise Nails. The nail technician accidentally nicked her foot, and Kristina became ill with flu-like symptoms by the end of the day, due to unsanitary conditions in the nail salon. After going to the emergency room, she was diagnosed with MRSA and Cellulitis, and told that she could possibly lose her leg.",

    "s2" => "The doctors were able to control the infection",

    "p2" => " and Kristina was able to be released from the hospital after 2 weeks. Her total medical expenses were $53,000 and she had to go through 6 months of physical therapy due to nerve damage caused by her infection. Bisnar Chase was able to secure a settlement of $200,000 for Kristina to cover her medical expenses, lost vacation time, and general damages.",

    "award" => "$200,000",

    "awardint" => 200000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("norcal"),

    "relatedLocations" => array("Accident Location" => "Monterey"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),




  array(

    "id" => 13,

    "imageurl" => "red-light-nehrlich.jpg",

    "caption" => "Mary Nehrlich was unable to receive disability benefits from her own insurance company who she worked for",

    "header" => "Mary Nehrlich v. [Defendant Insurance Company]",

    "type" => "Insurance Bad Faith",

    "summary" => "Insurance refused to pay for victim's disability claim",

    "doi" => "October 18, 1995",

    "plaintiff" => "Mary Nehrlich",

    "defendant" => "[Defendant Insurance Company]",

    "injuries" => array("Traumatic Head Injury", "Knee and Shoulder Surgeries"),

    "s1" => "The Accident: ",

    "p1" => "Mary Nehrlich was out buying her son a birthday gift in Huntington Beach, when she was hit by a speeding driver who ran a red light. Mary's car was totaled and she suffered traumatic injury to her head, resulting in reduced mental abilities in areas of her brain that had suffered severe trauma.",

    "s2" => "Mary was a top salesman for her insurance company, ",

    "p2" => "so she assumed that her company would take care of her in such an awful accident. Unfortunately, the company she worked for refused to believe that she was disabled, and decided not to pay her disability insurance. Bisnar Chase stepped in for Mary and fought a hard five-year battle against the insurance company, in which we were able to recover $500,000 for recovery from the car accident, and a substantial amount in an insurance bad faith settlement that remains confidential.",

    "award" => "$500,000+",

    "awardint" => 500000,

    "awardcat" => "",

    "storylink" => "https://www.bestattorney.com/verdicts-and-settlements/mary-nehrlich.html",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Huntington Beach"),

    "category" => array("other", "auto"),

    "catastrophic" => true,
  ),




  array(

    "id" => 14,

    "imageurl" => "seatbelts-rojas.jpg",

    "caption" => "Felipe Rojas lost his wife and sustained extensive injuries in a DUI accident",

    "header" => "Felipe Rojas v. General Motors",

    "type" => "Defective Seatbelt",

    "summary" => "Victim's seatbelt broke and ejected him from the car",

    "doi" => "October 24, 2003",

    "plaintiff" => "Felipe Rojas",

    "defendant" => "General Motors",

    "injuries" => array("2 Month Coma", "Kidney Failure", "Loss of Right Leg Function"),

    "s1" => "The Accident: ",

    "p1" => "Felipe Rojas was travelling to a bowling alley in Irvine with his family when a drunk driver sideswiped them. Felipe's seatbelt had broken during the accident and he had been ejected from the car into a tree on the side of the road. Felipe's wife and one of their daughter's friends had died in the collision. Felipe was in a coma for 2 months, and had to go through 15 reconstructive surgeries, as well as consistent dialysis and physical therapy. He was in the hospital for 4 months and in a wheelchair for six months after that.",

    "s2" => "Felipe was an inventory manager at Home Depot, ",

    "p2" => "but was unable to keep his job after his accident. When Bisnar Chase met with him, we examined his car and determined that he had a seatbelt failure case against General Motors. After many months of depositions and court visits we were able to obtain a sizeable, though confidential, settlement for Felipe to take care of his disabilities. Felipe was also able to set aside money for his children's college funds. Though his wife is gone, Felipe notes, he is still filled with joy when his children are around.",

    "award" => "[Confidential]",

    "awardint" => 0,

    "awardcat" => "conf",

    "storylink" => "https://www.bestattorney.com/verdicts-and-settlements/felipe-rojas.html",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Irvine", "Plaintiff's Residence" => "Dana Point"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 15,

    "imageurl" => "dog-bite-gates.jpg",

    "caption" => "Jean Gates was attacked by a dog and sustained multiple injuries to her face.",

    "header" => "Jean Gates vs Stephen Moore",

    "type" => "Dog Bite/Premises",

    "summary" => "Victim was bitten in the face by a dog",

    "doi" => "January 8, 2015",

    "plaintiff" => "Jean Gates",

    "defendant" => "Stephen Moore",

    "injuries" => array("Lip Torn from the Jaw"),

    "s1" => "The Incident: ",

    "p1" => "Jean Gates was talking a walk with her granddaughter in her apartment complex in San Clemente and saw Stephen Moore walking his dog coming from the opposite direction. She bent down to pet the dog and it jumped up and bit her in the face, knocking her to the ground onto her back.",

    "s2" => "a complex surgical operation was required ",

    "p2" => "to repair the tissue in the lower third of Jean's face. Her total medical bills exceeded $65,000 and she still had cosmetic imperfections in her left lip. Jean Gates was 86 at the time of the accident, and was suffering from mild dementia, so when Bisnar Chase was able to recover the insurance policy limit of $100,000 for her, it was a big win for her family. ",

    "award" => "$100,000",

    "awardint" => 100000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "San Clemente", "Plaintiff's Residence" => "San Clemente"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),


  array(

    "id" => 16,

    "imageurl" => "flowers-gonzales.jpg",

    "caption" => "Lucia Doe was fatally injured when her car rolled over after being struck from behind.",

    "header" => "Simon Doe et al. vs. [Defendant Corporation]",

    "type" => "Auto Defect",

    "summary" => "Car rolled over after being struck from behind and seatbelts failed.",

    "doi" => "July 16, 2006",

    "plaintiff" => "Simon Doe",

    "defendant" => "[Defendant Corporation]",

    "injuries" => array("Wrongful Death: Rollover/Seatbelt defect"),

    "s1" => "The Accident: ",

    "p1" => "Simon Doe was traveling with his family on the I-15 in Hesperia, when he was struck from behind by Terence Chavez. Simon lost control of the vehicle, spun out, and rolled over on the road. Simon's wife, Lucia, was partially ejected from the vehicle during the accident and died at the scene. The rest of the family also suffered less serious injuries. ",

    "s2" => "Bisnar Chase found several issues with the Doe car ",

    "p2" => "which contributed to the severity of the accident. First off, the car manufacturer could have included an electronic stability control which would have prevented a roll over, but negligently decided not to. The seat belts were also not created to protect the passenger in the event of a rollover - a fact that was already acknowledged but ignored by the defendant corporation. Ultimately, Bisnar Chase was able to recover a total of $8,500,000 for the Doe family.",

    "award" => "$8,500,000",

    "awardint" => 8500000,

    "awardcat" => "5+",

    "storylink" => "",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Hesperia", "Plaintiff's Residence" => "La Mirada"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 17,

    "imageurl" => "broken-wall-valentine.jpg",

    "caption" => "Greg Valentine was injured when an improperly built wall fell on him",

    "header" => "Greg Valentine vs. Jeffrey Stoddard",

    "type" => "Premises Liability",

    "summary" => "Improperly constructed wall fell onto victim",

    "doi" => "December 30, 1998",

    "plaintiff" => "Greg Valentine & Stephen Smiddy",

    "defendant" => "Jeffrey Stoddard",

    "injuries" => array("Blunt Chest Trauma", "Crushed Pelvis", "Multiple Fractures, Burns, and Aterial Bleeding"),

    "s1" => "The Accident: ",

    "p1" => "Gregory Valentine and Stephen Smiddy were contractors who were called out to demolish a wall that separated 2 properties owned by Jeffrey Stoddard. They were using a concrete saw to take out small chunks of the wall when the wall fell on them, crushing Valentine and hitting Smiddy's left leg. Valentine had to be rushed to the hospital ICU and was not expected to live. After 2 weeks of life support, 10 surgeries, and transfers to transitional care and rehabilitation units, Greg was finally discharged after 2 months. ",

    "s2" => "Greg still had to undergo chiropractic care and physical therapy ",

    "p2" => "and was unable to work in the same capacity that he had worked prior to the accident. The construction crew had determined that the wall was built improperly - having been supported with rebar in the front section of the wall but unsupported in the back section where the wall fell. This was later confirmed by an investigator. Greg Valentine was awarded $2,815,958 by a judge for his loss of earnings and general damages.",

    "award" => "$2,815,958",

    "awardint" => 2815958,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Laguna Niguel", "Plaintiff's Residence" => "Cerritos"),

    "category" => array("premises"),

    "catastrophic" => true,
  ),


  array(

    "id" => 18,

    "imageurl" => "car-dent-hemli-munoz.jpg",

    "caption" => "Isabelle was pinned between her car door and the car of a driver who was not paying attention.",

    "header" => "Isabelle Hemli-Munoz Vs. Hailey Hamilton",

    "type" => "Auto Accident",

    "summary" => "Pedestrian on the side of the road struck by a car",

    "doi" => "January 18, 2014",

    "plaintiff" => "Isabelle Hemli-Munoz",

    "defendant" => "Hailey Hamilton",

    "injuries" => array("8 Fractured Ribs", "Punctured Lung", "Concussion"),

    "s1" => "The Accident: ",

    "p1" => "Isabelle Hemli-Munoz was headed to a dance practice and was in the back seat of her sister's car which was parked on the side of the road. She started to exit the car, stepping out into the road, when a car driven by a minor struck her and pinned her to her car door. She suffered broken bones, a punctured lung, and a concussion, and had to spend the following year recovering.",

    "s2" => "The insurance companies determined that Isabelle was 100% at fault ",

    "p2" => "for her injuries, due to her not yielding to oncoming drivers before she got out of the car. Bisnar Chase, however, determined that because there were marks on the car showing that the defendant sideswiped the parked car before hitting Isabelle, meaning that the defendant was driving too close to the side of the road and completely at fault for the accident. Bisnar Chase was able to recover a total of $155,000 for the Isabelle's injuries and recovery bills.  ",

    "award" => "$155,000",

    "awardint" => 155000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Brea", "Plaintiff's Residence" => "Brea"),

    "category" => array("auto", "pedestrian"),

    "catastrophic" => false,
  ),


  array(

    "id" => 19,

    "imageurl" => "guard-rail-hernandez.jpg",

    "caption" => "Kathleen Hernandez was rendered paraplegic in a car accident",

    "header" => "Hernandez v. Department of Transportation",

    "type" => "Poor Road Design/Premises",

    "summary" => "Lack of guardrail on a road led to extensive injuries during crash",

    "doi" => "September 10, 1999",

    "plaintiff" => "Kathleen Hernandez",

    "defendant" => "Caltrans",

    "injuries" => array("Paraplegia"),

    "s1" => "The Accident: ",

    "p1" => "Kathleen Hernandez and her sister Ashlee were travelling through Brea in a car driven by Rochelle Ramos. They were pursued by Ramos' former boyfriend, Raul Contreras. Contreras intentionally rammed their car on the highway and forced them to take an off-ramp exit. Ramos lost control of the car and skidded off the roadway and down an embankment. Ashlee Hernandez was killed and Kathleen was rendered paraplegic due to the accident.",

    "s2" => "Raul Contreras was convicted of murder",

    "p2" => " but Kathleen alleged that the lack of a guardrail at the site of the accident constituted a dangerous condition of public property, which created a reasonably foreseeable risk of the injury that was incurred. Bisnar Chase found that there were 3 previous similar accidents that happened in the same area within the prior 18 months, and that Caltrans was negligent by not providing a safe roadway to prevent injuries from accidents like this. Bisnar Chase was able to recover 2.8 million dollars for the Hernandez family.",

    "award" => "$2,800,000",

    "awardint" => 2800000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Brea", "Plaintiff's Residence" => "West Covina"),

    "category" => array("auto", "premises"),

    "catastrophic" => true,
  ),


  /*array( // 15 passenger van, rollover, 9.8 million period.

			"id" => 20,

			"imageurl" => "15-passenger-van-rabinovich.jpg",

			"caption" => "A 15-passenger van rolled over, killing 7 of its' occupants.",

			"header" => "Michael Doe Vs. [Defendant Corporation]",

			"type" => "15 Passenger Van Rollover",

			"summary" => "",

			"doi" => "March 8, 2000",

			"plaintiff" => "Michael Doe",

			"defendant" => "[Defendant Corporation]",

			"injuries" => array("Wrongful Death"),

			"s1" => "The Accident: ",

			"p1" => "Yury Doe was driving with family and friends in a rented 15 passenger van from Los Angeles to Vegas. 15 miles north of the Nevada border, he lost control of the van and it slipped into the median divider. The van rolled over several times before coming to rest in a ravine. 7 of the 13 passengers were fatally injured.",

			"s2" => "Out of several lawsuits that took place",

			"p2" => " as a result of the tragic accident, Bisnar Chase represented John Doe against the car manufacturer for making an unsafe van that did not protect its' occupants in the event of a rollover. All of the passengers that were killed during the crash were ejected from the vehicle while it was rolling over, which showed that the seatbelts were also defective and did not do their job of keeping their occupants in the car seats. The total award from all lawsuits filed by the victims and family members was $9,800,000.",

			"award" => "$9,800,000",

			"awardint" => 9800000,

			"awardcat" => "5+",

			"storylink" => "",

			"location" => array("la", "other"),

			"relatedLocations" => array("Accident Location" => "Nevada", "Car Rental Location" => "Los Angeles"),

			"category" => array("auto-defect"),

			"catastrophic" => true,
		),
   */

  array(

    "id" => 21,

    "imageurl" => "crossing-street-villamor.jpg",

    "caption" => "Raul Silva was killed in a DUI",

    "header" => "Maria Silva vs Mark Levander",

    "type" => "DUI Pedestrian Hit",

    "summary" => "Pedestrian struck by driver under the influence. ",

    "doi" => "January 17, 2015",

    "plaintiff" => "Maria Silva",

    "defendant" => "Mark Levander",

    "injuries" => array("Wrongful Death"),

    "s1" => "The Accident: ",

    "p1" => "Raul Silva was crossing Calhoun Street in Indio when Mark Levander struck him with his car and killed him. The police report stated that Raul was at fault due his running into the path of a vehicle which constituted a hazard.",

    "s2" => "Raul's case was brought to us by his mother, Maria Silver, ",

    "p2" => "who was 63 at the time of the accident. Raul had contributed substantially to his mothers support before the accident. It was determied that Mark Levander had been under the influence of drugs and alcohol at the time of the accident, and he was driving recklessly and negligently. Due to this evidence, Bisnar Chase demanded that Levander's insurance company meet the policy limits and shortly after the limits of $100,000 were tendered.",

    "award" => "$100,000",

    "awardint" => 100000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Indio", "Plaintiff's Residence" => "Indio"),

    "category" => array("pedestrian", "auto"),

    "catastrophic" => true,
  ),


  array(  //Mai Talamo

    "id" => 22,

    "imageurl" => "stop-sign-mai.jpg",

    "caption" => "Mai was ejected from her car during a rollover",

    "header" => "Mai Doe vs. [Defendant Corporation]",

    "type" => "Defective Seatbelt",

    "summary" => "Seatbelt came undone during a crash",

    "doi" => "May 31, 2014",

    "plaintiff" => "Mai Doe",

    "defendant" => "[Defendant Corporation]",

    "injuries" => array("Paraplegia", "Traumatic Brain Injury"),

    "s1" => "The Accident: ",

    "p1" => "Mai Doe was driving near her home town of Morgan Hill near midnight when she approached a stop sign that she hadn't immediately seen. Mai had to brake suddenly, which resulted in her SUV overcorrecting and rolling over. ",

    "s2" => "Mai's seatbelt came undone during the rollover",

    "p2" => " and she was ejected from the SUV, suffering brain injuries and multiple fractures that rendered her paraplegic in a wheelchair for 9 months. Bisnar Chase was able to retrieve the vehicle and conduct tests showing that the manufacturer had created a defective seatbelt that did not protect Mai in the event of her accident. We were able to recover $300,000 for Mai and her family to pay for her medical bills, and pain and suffering.",

    "award" => "$300,000",

    "awardint" => 300000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("norcal"),

    "relatedLocations" => array("Accident Location" => "Gilroy", "Plaintiff's Residence" => "Morgan Hill"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 23,

    "imageurl" => "rashell-wage-and-hour.jpg",

    "caption" => "[Defendant Company] committed wage violations",

    "header" => "Rashell Doe vs. [Defendant Corporation]",

    "type" => "Wage & Hour",

    "summary" => "Company did not compensate employees for overtime or meal periods",

    "doi" => "Case Opened 7/2014",

    "plaintiff" => "Rashell Doe",

    "defendant" => "[Defendant Company]",

    "injuries" => array("Wage Violation"),

    "s1" => "Rashell Doe was an employee at a mortgage company ",

    "p1" => "in Orange County. The company did not take bonuses into account when calculating the regular rate of pay, and therefore did not pay employees adequately for overtime or for premiums mandated by law when the employee misses a meal period or rest break. Bisnar Chase was represented Rashell and other class members against the defendant company and obtained $500,000 in compensation for members of the class.",

    "s2" => "",

    "p2" => "",

    "award" => "$500,000",

    "awardint" => 500000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Plaintiff's Residence" => "San Juan Capistrano"),

    "category" => array("other", "employment-law"),

    "catastrophic" => false,
  ),


  array(

    "id" => 24,

    "imageurl" => "motorcycle-prock.jpg",

    "caption" => "David Iorillo was struck by a drunk driver",

    "header" => "David Iorillo vs. Drunk Driver",

    "type" => "DUI",

    "summary" => "Drunk driver struck an off-duty police officer",

    "doi" => "December 8, 2002",

    "plaintiff" => "David Iorillo",

    "defendant" => "Drunk Driver",

    "injuries" => array("Knee, Groin, and Back Injries"),

    "s1" => "David Iorillo was an officer in the San Diego Police Department. ",

    "p1" => "He was riding his motorcycle off-duty when he was rear-ended by a drunk driver and found himself on the ground in an intersection. Because he was catapulted off of his bike, David had to undergo knee surgeries and physical therapy for both his back and knee.",

    "s2" => "Being a police officer meant that ",

    "p2" => "David had to be in peak physical condition, and he was concerned that this accident would end his career. However, David was able to get back to regular duty after 6 months, and in the meantime he hired Bisnar Chase to represent him. Bisnar Chase mediated the payment of the medical bills and insurance reimbursement and was able to get the insurance company to accept the $100,000 demand.  ",

    "award" => "$100,000",

    "awardint" => 100000,

    "awardcat" => "",

    "storylink" => "https://www.bestattorney.com/case-results/david-iorillo.html",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "San Diego"),

    "category" => array("auto", "motorcycle"),

    "catastrophic" => false,
  ),


  // array(

  //   "id" => 25,

  //   "imageurl" => "dog-bite-lang.jpg",

  //   "caption" => "Nicholas was bitten by his neighbor's pitbull",

  //   "header" => "Nicholas Lang vs Liberty Insurance",

  //   "type" => "Dog Bite/Premises",

  //   "summary" => "Hand bitten by dog",

  //   "doi" => "July 17th, 2015",

  //   "plaintiff" => "Nicholas Lang",

  //   "defendant" => "Liberty Insurance",

  //   "injuries" => array("Bites to Arm, Elbow, and Hand", "2cm superficial laceration on fingers"),

  //   "s1" => "Nicholas Lang was a handyman ",

  //   "p1" => "who was bitten by his neighbors pitbull when he was entering the yard. He was treated for puncture wounds at the hospital and was unable to work without experiencing pain and reopening his wounds.",

  //   "s2" => "Nicholas had a $2,500 work contract ",

  //   "p2" => "that he lost due to his injuries, and could not work while his hand was healing. Bisnar Chase was able to secure a $5,600 settlement from the insurance company due to their insured housing a dangerous dog.",

  //   "award" => "$5,600",

  //   "awardint" => 5600,

  //   "awardcat" => "",

  //   "storylink" => "",

  //   "location" => array("norcal"),

  //   "relatedLocations" => array("Accident Location" => "Tuolumne"),

  //   "category" => array("premises"),

  //   "catastrophic" => false,
  // ),


  array(

    "id" => 26,

    "imageurl" => "rear-end-ferris.jpg",

    "caption" => "Jennifer Ferris suffered a concussion in an accident",

    "header" => "Jennifer Ferris vs Blaze Blackburn",

    "type" => "Rear-End Crash",

    "summary" => "Victim Rear-ended in a car accident",

    "doi" => "February 12th, 2015",

    "plaintiff" => "Jennifer Ferris",

    "defendant" => "Blaze Blackburn",

    "injuries" => array("Concussion", "Cervicogenic Headaches"),

    "s1" => "Jennifer Ferris was driving through Aliso Viejo ",

    "p1" => "on Aliso Creek Road when she was rear-ended in a 5 car pile-up. The force of the accident totaled Jennifer's 2013 Lexus and gave her a concussion, cervicogenic headaches, and left thumb pain.",

    "s2" => "Jennifer had a pre-existing condition of nerve entrapment ",

    "p2" => "and carpal tunnel in her left wrist, which was exacerbated by her car accident. Bisnar Chase pursued a settlement for Jennifer by making a claim for the pre-existing medical conditions in addition to her other injuries, from which we were able to obtain $40,000 for Jennifer's injuries and property damage to her car.",

    "award" => "$40,000",

    "awardint" => 40000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Aliso Viejo", "Plaintiff's Residence" => "Laguna Niguel"),

    "category" => array("auto"),

    "catastrophic" => false,
  ),


  array(

    "id" => 27,

    "imageurl" => "red-lights-coffey.jpg",

    "caption" => "Jayne Coffey was killed when her car struck an illegally parked truck",

    "header" => "Steve Coffey vs. Dale Howell",

    "type" => "Rear-End Crash",

    "summary" => "Woman killed when her car struck an illegally parked truck",

    "doi" => "August 19th, 2006",

    "plaintiff" => "Steve Coffey",

    "defendant" => "Dale Howell",

    "injuries" => array("Fatal Injuries to Head and Neck"),

    "s1" => "Jayne Coffey was a passenger in a car driven by Velvia Ledbetter. ",

    "p1" => "They were driving northbound on the 15 freeway through Fontana at 4am, when Ledbetter suddenly lost control of her car for unknown reasons. They hit a big rig truck that was sitting on the side of the road and Jayne was killed on impact.",

    "s2" => "The driver of the big rig was Dale Howell, ",

    "p2" => "who had pulled over to the side of the road to sleep. However, in doing so he had violated a California Vehicle Code (21718) for stopping on the highway when his vehicle was not disabled and he was able to operate it responsibly. Bisnar Chase represented Steve and Erin Coffey, Jayne's respective husband and daughter, and was able to settle for $1,000,000 in a commercial claim with Howell's insurance company.",

    "award" => "$1,000,000",

    "awardint" => 1000000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Fontana", "Plaintiff's Residence" => "Rancho Santa Margarita"),

    "category" => array("auto"),

    "catastrophic" => true,
  ),


  array(

    "id" => 28,

    "imageurl" => "flowers-herod.jpg",

    "caption" => "Marilyn Herod was killed when a car struck her flower booth",

    "header" => "Michele Herod vs. [Multiple Defendants]",

    "type" => "Auto Vs. Pedestrian",

    "summary" => "Pedestrian killed when a car struck her flower booth",

    "doi" => "May 13th, 2007",

    "plaintiff" => "Michele Herod",

    "defendant" => "[Multiple Defendants]",

    "injuries" => array("Fatally Struck"),

    "s1" => "Harley Daniels worked at a Pepsi bottling plant ",

    "p1" => "and left his 12am shift early to go home. While driving westbound on Century blvd, witnesses saw him speeding and possibly racing another car. The cars sideswiped each other and Daniels was forced across the street, driving through an iron fence and into a flower stand killing 2 pedestrians and injuring 3 others.",

    "s2" => "Marilyn Herod was running the flower stand ",

    "p2" => "and had been paying the auto repair shop where the stand was located. She was killed immediately when Daniels' car hit her. Bisnar Chase represented her daughter, Michele Herod, in the wrongful death lawsuit that was filed. We obtained a settlement of $50,000 from the auto shop for not providing a safe environment for Herod, a settlement of $1,490,000 from the insurance company, and a confidential amount from Pepsi.",

    "award" => "$1,540,000+",

    "awardint" => 1540000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Los Angeles", "Plaintiff's Residence" => "Corona", "Victim's Residence" => "Riverside"),

    "category" => array("pedestrian"),

    "catastrophic" => true,
  ),


  array(

    "id" => 29,

    "imageurl" => "trailer-cavano.jpg",

    "caption" => "Ronald Cavano was killed on impact with a trailer",

    "header" => "Susan Cavano et al. vs. Lorenzo Lozano",

    "type" => "Auto Accident",

    "summary" => "Man killed on impact with a trailer",

    "doi" => "May 8th, 2006",

    "plaintiff" => "Susan Cavano",

    "defendant" => "Lorenzo Lozano",

    "injuries" => array("Fatal Crash"),

    "s1" => "Ronald Cavano was driving west on Simpson Rd ",

    "p1" => "in Riverside county, returning home after attending a bowling tournament in Las Vegas. Lorenzo Lozano was at a stop sign perpendicular to Simpson Rd, in a tractor unit carrying 2 bottom dump trailers. Lozano started to cross the street as Ronald Cavano was approaching. Ronald braked to avoid the trailers, but skidded into the draw bar between the two trailers, and was killed upon impact.",

    "s2" => "The police determined ",

    "p2" => "that Lozano did not account for the extra length of the truck due to his two trailers, and needed more time and distance so he could cross the road safely and not impede cross traffic. Bisnar Chase represented Ronald's wife Susan, and his children, in settling a $497,000 claim with Lozano's commercial insurance policy. ",

    "award" => "$497,000",

    "awardint" => 497000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Riverside", "Plaintiff's Residence" => "San Jacinto"), //Technically Winchester

    "category" => array("auto"),

    "catastrophic" => true,
  ),


  array(

    "id" => 30,

    "imageurl" => "black-suv-richard.jpg",

    "caption" => "Richard Doe's truck rolled over and crushed him",

    "header" => "Richard Doe vs. [Defendant Auto Manufacturer]",

    "type" => "Auto Defect",

    "summary" => "Man crushed when his car rolled over on him",

    "doi" => "November 12th, 2007",

    "plaintiff" => "Richard Doe",

    "defendant" => "[Defendant Auto Manufacturer]",

    "injuries" => array("Incomplete Quadriplegia"),

    "s1" => "In the afternoon of November 12, 2007,",

    "p1" => " witnesses reported seeing a black SUV driving down highway 17 in Arizona, weaving in and out of traffic and occasionally going off the road. The driver was in diabetic shock and struck Richard Doe's truck. ",

    "s2" => "Richard Doe's truck rolled over",

    "p2" => " and the roof crushed in on itself, rendering him a quadriplegic but with some remaining use of his arms. Bisnar Chase represented Richard in filing a lawsuit against the manufacturer of his truck. The truck was supposed to keep the driver save in the event of a rollover and the roof crushing inwards was considered a defect. Bisnar Chase secured a $2,500,000 settlement for Richard Doe for his permanent injuries.",

    "award" => "$2,500,000",

    "awardint" => 2500000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Arizona", "Plaintiff's State of Residence" => "Minnesota",),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 31,

    "imageurl" => "speeding-tiffany.jpg",

    "caption" => "Tiffany Doe was paralyzed due to a lack of safety features",

    "header" => "Tiffany Doe vs. [Defendant Auto Manufacturer]",

    "type" => "Auto Defect",

    "summary" => "Woman paralyzed due to a lack of safety features on her vehicle",

    "doi" => "June 10th, 2010",

    "plaintiff" => "Tiffany Doe",

    "defendant" => "[Defendant Auto Manufacturer]",

    "injuries" => array("Quadriplegia"),

    "s1" => "Tiffany Doe was driving down the 405 through Carson, ",

    "p1" => "when a speeding car driven by an unknown party started to move dangerously towards another driver, Amy Smith. Smith had to turn her car sharply to avoid a collision, but lost control of her car and spun around into the 405 central barrier. Tiffany, who was driving in the carpool lane at the time, was unable to slow down in time and struck Smith's car head on, suffering cervical fractures with subluxation from striking her head on the roof of the car.",

    "s2" => "Tiffany's injuries required ",

    "p2" => "that she be confined to a wheelchair and dependent on a ventilator, likely for the rest of her life. Bisnar Chase represented her against the manufacturer who built her car, claiming that as the manufacturers, they had a duty to provide a safe environment in their cars. This could have been accomplished by providing side curtain airbags in every car they manufactured, but the manufacturer only offered this as a much more expensive option. Bisnar Chase was able to obtain a settlement of $2,500,000 for Tiffany Doe so that her family would be able to take care of her for her future.",

    "award" => "$2,500,000",

    "awardint" => 2500000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Carson", "Plaintiff's Residence" => "Lake Elizabeth"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 32,

    "imageurl" => "highway-5-rojas.jpg",

    "caption" => "Rufo and Lourdes Rojas were killed in a car accident",

    "header" => "Rojas Family vs. Kim Wu",

    "type" => "Auto, Insurance Bad Faith",

    "summary" => "A family's van rolled into a median and ejected 2 members",

    "doi" => "June 17th, 2009",

    "plaintiff" => "The Rojas Family",

    "defendant" => "Kim Wu",

    "injuries" => array("2 Deaths", "Severe Head Trauma", "Multiple Fractures"),

    "s1" => "In 2009, Ivn Rojas was driving ",

    "p1" => "5 other family members from Renton, WA to Long Beach, CA to attend a convention. As they were passing Los Banos on Highway 5, driver Kim Wu cut into their lane, forcing Ivn to pull hard to the left to avoid being struck by Wu's car. Their car rolled into the median and 2 passengers, Lourdes and Rufo, were ejected from the vehicle and died.",

    "s2" => "Wu's vehicle was covered by a $1 million policy",

    "p2" => " issued by an unnamed insurance company. Bisnar Chase provided a demand for the policy limits to this insurance company, due to the many injuries and medical bills sustained by the family in addition to the pain and suffering of losing their loved ones. However, their demand went unanswered and was allowed to expire by the insurance adjuster. After attending a mediation, Bisnar Chase secured the policy limits plus an extra $875,000 due to the bad faith on the part of the insurance company. The Rojas family was also able to obtain $50,000 from their own insurance company.",

    "award" => "$1,925,000",

    "awardint" => 1925000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("norcal"),

    "relatedLocations" => array("Accident Location" => "Los Banos", "Plaintiff's State of Residence" => "Washington"),

    "category" => array("auto", "other"),

    "catastrophic" => true,
  ),


  array(

    "id" => 33,

    "imageurl" => "motorcycle-begin.jpg",

    "caption" => "Thomas Begin's motorcycle after being rear-ended",

    "header" => "Thomas Begin vs Michael Whitley",

    "type" => "Auto v. Motorcycle",

    "summary" => "Man was rear-ended on his motorcycle",

    "doi" => "May 30th, 2015",

    "plaintiff" => "Thomas Begin",

    "defendant" => "Michael Whitley",

    "injuries" => array("Mild Concussion", "Shoulder Contusion"),

    "s1" => "Thomas Begin was riding his motorcycle",

    "p1" => " northbound on Pacific Coast Highway through Seal Beach. At the intersection of 12th street, the light turned yellow as he was approaching it, so he slowed to a stop. Driver Michael Whitley was behind him and attempted to slow down, but experienced his brakes \"bouncing\" back against his foot and was unable to stop in time.",

    "s2" => "Whitley struck Thomas Begin's motorcycle from behind ",

    "p2" => "and sent Begin sprawling into the intersection. Witnesses say that they did not hear any tire screeching before the crash. Begin suffered a mild concussion and other minor injuries but was discharged from the hospital the same day. The defendant had a insurance policy limit of $50,000 and Bisnar Chase was able to secure that amount for our client to account for his medical bills and property damage.",

    "award" => "$50,000",

    "awardint" => 50000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Seal Beach", "Plaintiff's Residence" => "Huntington Beach"),

    "category" => array("motorcycle"),

    "catastrophic" => false,
  ),


  array(

    "id" => 34,

    "imageurl" => "green-turn-king.jpg",

    "caption" => "Michelle King was struck by a driver running a red light",

    "header" => "Michelle King vs. Louvela Guillory",

    "type" => "Auto Accident",

    "summary" => "Woman struck by a driver running a red light",

    "doi" => "April 19th, 2007",

    "plaintiff" => "Michelle King",

    "defendant" => "Louvela Guillory",

    "injuries" => array("Loss of Hearing in Left Ear", "Disc Protrusions"),

    "s1" => "Michelle King was in her Jeep ",

    "p1" => "making a left turn onto Azusa Ave from the 60 freeway offramp, when a car driven by Louvela Guillory ran a red light and t-boned her car. Both drivers gave statements that they were driving through the intersection at a green light, but witness statements confirmed that Michelle King had the right of way.",

    "s2" => "Michelle's injuries included ",

    "p2" => "the loss of hearing in her left ear, 7 disc protrusions, and constant back, chest, and neck pain. Her losses of property damage and lost wages in addition to her injuries, were estimated to be around $30,000. Bisnar Chase was able to settle for the policy limits of $50,000 for Michelle.",

    "award" => "$50,000",

    "awardint" => 50000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Industry", "Plaintiff's Residence" => "Covina"), //West Covina?

    "category" => array("auto"),

    "catastrophic" => false,
  ),


  array(

    "id" => 35,

    "imageurl" => "pirates-chappell.jpg",

    "caption" => "Carole was injured at a dinner/show event",

    "header" => "Carole Chappell vs. CA Dinner Entertainment",

    "type" => "Premises Liability",

    "summary" => "Woman injured at a dinner/show event",

    "doi" => "October 16th, 2011",

    "plaintiff" => "Carol Chappell",

    "defendant" => "CA Dinner Entertainment",

    "injuries" => array("Rotator Cuff Tears", "Exacerbation of Cervical Condition"),

    "s1" => "To celebrate her 76th birthday, ",

    "p1" => "Carole Chappell's family took her to the Pirate's Dinner Adventure in Buena Park - a dinner show with stunt performers and pyrotechnics. As a part of the show, the performers called Carole to climb up to the pirate ship to acknowledge her birthday, by stepping on a plastic stool and grabbing a steering wheel to pull herself up. ",

    "s2" => "When Carole attempted to pull herself up ",

    "p2" => "with the wheel, the wheel spun, making her lose her balance and fall 8 feet to the floor, knocking her unconscious. She was admitted to the hospital for two nights and diagnosed with a subcutaneous hematoma, a rotator cuff tear and a severe exacerbation of an existing cervical spine condition, which required steroid injections along her spine. Bisnar Chase argued that the Pirate's venue had a duty to ensure a safe environment for their customers and that their method of getting the Carole to the pirate boat was clearly unsafe and negligent. CA Dinner Entertainment settled for $110,000 which provided for Carole's medical bills and continued care.",

    "award" => "$110,000",

    "awardint" => 110000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Buena Park", "Plaintiff's Residence" => "West Covina"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),


  array(

    "id" => 36,

    "imageurl" => "bicycle-accident-carreon.jpg",

    "caption" => "Steve Carreon was struck by a car on this corner",

    "header" => "Steve Carreon vs. Grandvil Carter",

    "type" => "Auto vs. Bicycle",

    "summary" => "Man struck by a car while on his bicycle",

    "doi" => "September 23rd, 2014",

    "plaintiff" => "Steve Carreon",

    "defendant" => "Grandvil Carter",

    "injuries" => array("Tibial Right Knee Fracture"),

    "s1" => "Steve Carreon was riding his bike through Seal Beach",

    "p1" => " at the intersection of St. Cloud and Seal Beach Blvd. He waited until the light turned green and started biking through the intersection, when his Grandvil Carter made a right turn through the intersection after checking his left side for oncoming cars. Carter did not see Steve or his bike, and struck him with his car, knocking him over. ",

    "s2" => "Steve was knocked onto his right knee ",

    "p2" => "and sustained a tibial right knee fracture, for which he required surgery and several months of physical therapy. The police officer who filled out the traffic collision report noted that Carter was at fault for failing to yield to a pedestrian in a crosswalk (Vehicle Code 21650). Bisnar Chase represented Steve and demanded the policy limits of $50,000 from defendant's insurance company, Geico.",

    "award" => "$50,000",

    "awardint" => 50000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Seal Beach", "Plaintiff's Residence" => "Buena Park"),

    "category" => array("auto"),

    "catastrophic" => false,
  ),


  array(

    "id" => 37,

    "imageurl" => "coffee-burn-corigliano.jpg",

    "caption" => "Mia was burned by a spilled cup of coffee",

    "header" => "Mia Corigliano vs. 85C Cafe",

    "type" => "Premises Liability",

    "summary" => "Child given second degree burns from a cup of coffee",

    "doi" => "March 13th, 2010",

    "plaintiff" => "Mia Corigliano",

    "defendant" => "85C Cafe",

    "injuries" => array("Second Degree Burns over %14 of the body"),

    "s1" => "Four-year-old Mia Corigliano was at the Irvine Diamond Jamboree plaza ",

    "p1" => "with her cousin Kayla and her aunt Margie Pierce. Mia's father had gone to use the bathroom and left Mia under Margie's care. Margie had ordered a coffee from 85 Degrees Cafe, and had left it on the table as it was too hot to drink. Kayla reached over to Mia and in doing so, knocked the cup of coffee over and it spilled into Mia's lap, giving her immediate second degree burns on her abdomen, thighs, arms, and legs.",

    "s2" => "Mia's Father took her to an urgent care",

    "p2" => " down the street, where she stayed for 11 days after the accident. Mia was required to visit the burn clinic several times for the next 9 months after the accident. Bisnar Chase procured video footage of the accident from a security camera, and using this evidence was able to obtain two settlements from both Pierce's home insurance and from 85C Cafe, totalling $174,000. ",

    "award" => "$274,000",

    "awardint" => 274000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Irvine", "Plaintiff's Residence" => "Newport Beach"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),


  array(

    "id" => 38,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "Germain Clark was involved in a crash with an aggressive driver",

    "header" => "Germain Clark vs. Nawaid Rana",

    "type" => "Auto vs. Auto",

    "summary" => "Aggressive Driver struck made an unsafe lane changed and caused a crash",

    "doi" => "August 1st, 2012",

    "plaintiff" => "Germain Clark",

    "defendant" => "Nawaid Rana",

    "injuries" => array("Torn Rotator Cuff", "Exacerbated Spine Strain"),

    "s1" => "Germain Clark was driving southbound",

    "p1" => " on the 405, about to pass Bake pkwy, when he noticed traffic slowing down ahead and saw a silver BMW in the fast lane at 70-75 miles an hour. When this car saw the traffic ahead, he started to switch lanes aggressively, and swerved in front of Germain, braking quickly. Germain was unable to avoid the driver, Nawaid Rana, and struck the back right of his car. The officer investigating the scene noted that Rana was at fault for making an unsafe lane change.",

    "s2" => "Germain's immediate injuries came from the airbag",

    "p2" => " that had gone off and given him first degree burns on his arms. However, Germain had injured 3 disks and had clavicle surgery several years prior to the accident, and these injuries were severely exacerbated. Germain's doctors recommended physical therapy, surgery, and lumbar injections. After the accident, Rana's insurance provider Allstate was unwilling to pay out any money in claims despite the officer at the scene finding Rana at fault. Bisnar Chase began to litigate the case but eventually settled for $37,000 for Germain.",

    "award" => "$37,000",

    "awardint" => 37000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Irvine", "Plaintiff's Residence" => "Dana Point"),

    "category" => array("auto"),

    "catastrophic" => false,
  ),


  array(

    "id" => 39,

    "imageurl" => "dog-bite-jaramillo.jpg",

    "caption" => "Emma Jaramillo was bitten in the face by a food-aggressive dog",

    "header" => "Emma Jaramillo vs Sanchez Family",

    "type" => "Dog Bite/Premises",

    "summary" => "Girl bitten by food-aggressive dog",

    "doi" => "January 10th, 2009",

    "plaintiff" => "Emma Jaramillo",

    "defendant" => "Sanchez Family",

    "injuries" => array("Severe Bites to Face and Head"),

    "s1" => "Five-year-old Emma Jaramillo and her family ",

    "p1" => "were at the house of Juan and Julia Sanchez. They had a husky named Dandy, who was eating her food when Emma approached, wanting to say hi. In a display of food aggression, Dandy attacked Emma and bit her multiple times in the face, head, and shoulders. The wounds included long scars and deep puncture wounds in the cheek, scalp, and shoulder blade.",

    "s2" => "Emma was severely traumatized by the attack, ",

    "p2" => "and required 8 therapist sessions over the course of 2 months, in addition to hospital and medication costs. Emma developed scarring along her face and was recommended a laser treatment with the possibility of requiring surgical scar restoration in 10 years. After Emma had finished treating and Bisnar Chase sent a demand out, a $300,000 settlement was decided upon within 20 days.",

    "award" => "$300,000",

    "awardint" => 300000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Azusa", "Plaintiff's Residence" => "Whittier"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),


  array(

    "id" => 40,

    "imageurl" => "t-boned-car-onys.jpg",

    "caption" => "Veronica Onys lost her leg in a car accident",

    "header" => "Veronica Onys vs. The State of California",

    "type" => "Auto vs. Auto",

    "summary" => "Woman lost a leg in a car accident",

    "doi" => "February 22nd, 2004",

    "plaintiff" => "Veronica Onys",

    "defendant" => "State of California",

    "injuries" => array("Left Leg Amputation", "Collapsed Lung"),

    "s1" => "Veronica Onys was a 19-year-old student at UC Irvine, ",

    "p1" => "driving north on the 5 freeway through Commerce. It had been raining that day and a dip in the highway created a deep puddle in the middle of the road. Upon hiting this puddle, Veronica's car hydroplaned and skidded into the center divider next to the left lane. The driver behind her, Pedro Montesdeoca, was unable to slow down in time and struck her car on the driver side, crushing her left leg and requiring it to be amputated.",

    "s2" => "The California Department of Transportation (Caltrans) ",

    "p2" => "was determined to be negligent on 3 fronts by Bisnar Chase. Primarily, California roads are given a \"skid number\" that represents how resistant the roads are to skidding. An acceptable standard is a 30, and any section of road below 30 requires a formal report sent to the district. The skid number of the road in question was 16, and no report was filed to correct this dangerous rating. Caltrans was also negligent in allowing the road to hold so much water in the puddle instead of having the water drain properly, and the lack of a shoulder on the left side of the road also contributed to Veronica's accident. Caltrans settled for $485,000 to ensure that Veronica had an easier future after her accident.",

    "award" => "$485,000",

    "awardint" => 485000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Commerce", "Plaintiff's Residence" => "Irvine"),

    "category" => array("auto"),

    "catastrophic" => true,
  ),




  // array(

  // 		"id" => 41,

  // 		"imageurl" => "pit-bull-morales.jpg",

  // 		"caption" => "Paulette Morales' lower lip was ripped off by a pit bull",

  // 		"header" => "Paulette Morales vs. Fidel Medina",

  // 		"type" => "Dog Bite/Premises",

  // 		"summary" => "Dog bit a woman in the face",

  // 		"doi" => "July 19th, 2009",

  // 		"plaintiff" => "Paulette Morales",

  // 		"defendant" => "Fidel Medina, et al.",

  // 		"injuries" => array("Lower Lip Ripped Off"),

  // 		"s1" => "Paulette Morales was at a party in Oceanside",

  // 		"p1" => " at the residence of Fidel Medina. The guests were told to use the grandmother's room on the first floor if they wanted to smoke, but had not been warned that there was a dog being held in a garage next to the grandmother's room. When Paulette decided she wanted to smoke around 11pm, she accidentally opened the door to the garage, where the Medina's Pitbull, Patron was being held. Patron immediately pushed open the door, jumped up, and bit Paulette in the face, ripping her lower lip off.",

  // 		"s2" => "Paulette was kicked out of the Medina house immediately after the attack ",

  // 		"p2" => "by Fidel Medina's daughter, Lizette. She was forced to deal with her severe injuries and emotional trauma on her own. Paulette called an ambulance from a payphone and was taken to the hospital where her lip and chin were surgically repaired. Due to the attack, Paulette suffered severe emotional trauma, exacerbated by her removal from the house without aid or attention. Despite admission that Paulette did nothing wrong and that there was no warning given to guests that there would be a dog in the garage, the defendants refused to take full responsibility for the attack. Bisnar Chase filed a lawsuit and eventually settled for $125,000.",

  // 		"award" => "$125,000",

  // 		"awardint" => 125000,

  // 		"awardcat" => "",

  // 		"storylink" => "",

  // 		"location" => array("other"),

  // 		"relatedLocations" => array("Accident Location" => "Oceanside", "Plaintiff's Residence" => "Carlsbad"),

  // 		"category" => array("premises"),

  // 		"catastrophic" => false,
  // 	),


  array(

    "id" => 42,

    "imageurl" => "oncoming-traffic-mione.jpg",

    "caption" => "Carlo Mione was forced into oncoming traffic",

    "header" => "Carlo Mione vs Samkol Truong",

    "type" => "Auto vs. Auto",

    "summary" => "Reckless driver caused a crash and injuries",

    "doi" => "May 28th, 2011",

    "plaintiff" => "Carlo Mione",

    "defendant" => "Samkol Truong",

    "injuries" => array("Concussion", "Spine Damage", "Broken Ribs"),

    "s1" => "Carlo Mione was severely injured",

    "p1" => " as he was travelling down Alton road through Irvine. The defendant Samkol Truong was driving recklessly in excess of 70mph, tailgating multiple vehicles. After approaching another car from behind, Truong did not slow down, and instead, swerved to the left and collided with Carlo's Dodge Magnum. The impact of this crash forced Carlo into oncoming traffic and caused a collision with a car coming the opposite direction.",

    "s2" => "Carlo suffered a concussion, rib fractures, and multiple spine injuries ",

    "p2" => "from the crash. He continues to suffer from back and neck pain, as well as cognitive impairments and insomnia. In addition to the special damages of past and future medical expenses, Bisnar Chase also sought general damages for pain and suffering and loss of consortium, as well as punitive damages against Truong for his sheer recklessness in driving unsafely. Eventually a settlement was reached at mediation for $600,000.",

    "award" => "$600,000",

    "awardint" => 600000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Irvine", "Plaintiff's Residence" => "Lake Forest"),

    "category" => array("auto"),

    "catastrophic" => true,
  ),


  array(

    "id" => 43,

    "imageurl" => "bus-pedestrian-sweigart.jpg",

    "caption" => "Terry was hit by a bus that was accelerated into him from a stop.",

    "header" => "Terry Sweigart vs. LA Transportation Authority",

    "type" => "Bus vs. Pedestrian",

    "summary" => "Pedestrian struck by a bus in LA",

    "doi" => "July 22, 2002",

    "plaintiff" => "Terry Sweigart",

    "defendant" => "LA County Transportation Authority",

    "injuries" => array("Subdural Hematoma", "Concussion", "Hearing & Sight Loss"),

    "s1" => "Terry Sweigart was crossing the street in downtown LA",

    "p1" => " when he was struck by an MTA bus that he had thought was allowing him to cross. Terry testified that he had stopped at the edge of the curb and had made eye contact with the driver of the stopped bus, waiting to see if the driver was going to move. After waiting a reasonable amount of time, Terry decided to cross, and was halfway across the lane before he saw the bus moving toward him. He attempted to return to the curb, but the bus knocked him to the ground and he hit his head. ",

    "s2" => "The bus driver failed to yield to a pedestrian ",

    "p2" => "in a crosswalk, in a clear violation of California vehicle code 21950. Terry suffered a concussion and a hip injury, in addition to hearing and sight loss. The doctors recommended a hip replacement surgery and were unable to pinpoint an injury in his brain from which he continually suffers excruciating pain. As a result of the permanent loss in quality of life, the LA County Transportation Authority settled for $850,000.",

    "award" => "$825,000",

    "awardint" => 825000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Los Angeles", "Plaintiff's Residence" => "Covina"),

    "category" => array("pedestrian"),

    "catastrophic" => true,
  ),


  array(

    "id" => 44,

    "imageurl" => "spice-eckhardt.jpg",

    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",

    "header" => "Devin Eckhardt vs. [Defendant Smoke Shop]",

    "type" => "Product Liability",

    "summary" => "Teenager passed away from synthetic spice",

    "doi" => "July 12th, 2014",

    "plaintiff" => "Devin & Veronica Smith",

    "defendant" => "[Defendant Smoke Shop]",

    "injuries" => array("Wrongful Death"),

    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",

    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",

    "s2" => "Spice has been a legal and easily purchaseable substance ",

    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",

    "award" => "[Confidential]",

    "awardint" => 0,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),

    "category" => array("other"),

    "catastrophic" => true,
  ),


  array(

    "id" => 45,

    "imageurl" => "suburban-rollover-meza.jpg",

    "caption" => "4 people were killed when their van rolled over",

    "header" => "Emerita Meza et al. vs. General Motors",

    "type" => "Auto Defect",

    "summary" => "Many deaths resulting from a  ",

    "doi" => "June 4th, 2013",

    "plaintiff" => "Emerita Meza et al.",

    "defendant" => "General Motors",

    "injuries" => array("4 Deaths", "Multiple Injuries to 4 Survivors"),

    "s1" => "Hector Meza was driving his family through Juarez, Mexico, ",

    "p1" => "with a total of 8 passengers in his 2001 Chevy Suburban. The car drove slightly off the road and Hector was unable to control the vehicle as he attempted to drive it back onto the road. The SUV tipped over and rolled over 3, ejecting 5 of the 8 occupants. The 3 passengers in the second row were all ejected and were pronounced dead at the scene. Hector was taken to the hospital but died several hours later. ",

    "s2" => "Bisnar Chase represented Emerita Meza,",

    "p2" => " who survived the crash, but tragically lost her husband and her son. Bisnar Chase brought the action against General Motors for the suburban's known stability defects and propensity to rollover, in addition to the defective seatbelts which failed to hold and protect their occupants in the event of a crash, and finally for the unsafe condition of the roof supporting columns which were unable to withstant the roof-crushing forces of the rollover. General Motors ended up settling for a confidential amount to benefit the Meza family as compensation for bodily injuries, financial support, and loss of consortium.",

    "award" => "[Confidential]",

    "awardint" => 0,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Mexico", "Plaintiff's Residence" => "Lynwood", "Victim's Residence" => array("Bakersfield", "Compton")),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 46,

    "imageurl" => "car-fire-smith.jpg",

    "caption" => "Jocelyn Smith was killed by a car fire",

    "header" => "Antonio Smith vs. [Defendant Auto Manufacturer]",

    "type" => "Auto Defect",

    "summary" => "Auto defect caused a car fire",

    "doi" => "June 4th, 2013",

    "plaintiff" => "Antonio Flores",

    "defendant" => "[Defendant Auto Manufacturer]",

    "injuries" => array("Wrongful Death"),

    "s1" => "Jocelyn Smith was sitting in the front seat of a car ",

    "p1" => " travelling through Riverside. Driver Ashley Doe suddently passed out and lost control of the vehicle, causing it to collide with a raised center island where it struck 2 metal sign posts, caught fire, and became totally engulfed in flames. Jocelyn Smith sustained terrible burns over 64% of her body and passed away 7 weeks later due to complications with her injuries.",

    "s2" => "The car fire was started when the car ran over a post, ",

    "p2" => "which in turn ripped through the gas tank that that was struck by the car, and the gasoline was ignited by the friction of the car on the concrete. Bisnar Chase was hired by Jocelyn's parents, Antonio and Judith, to bring action against the Manufacturer who made the car. The fuel tank did not have an adequate liner that prevent it from being ruptured by objects on the road, and therefore was a direct cause of Jocelyn's death. The defendant auto manufacturer settled for $650,000 which was used to pay for medical bills, funeral services, and for non-economic damages suffered by Jocelyn's parents.",

    "award" => "$650,000",

    "awardint" => 650000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Riverside", "Plaintiff's Residence" => "Riverside"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),


  array(

    "id" => 47,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "Masseuses at this defendant company were not being paid for all of their time worked.",

    "header" => "Kristi Doe vs. [Defendant Massage Company]",

    "type" => "Wage & Hour",

    "summary" => "Masseuses were not being paid for all their time worked",

    "doi" => "Case Opened 7/2014",

    "plaintiff" => "[Defendant Massage Company]",

    "defendant" => "Kristi Doe",

    "injuries" => array("Wage Violation"),

    "s1" => "A Class Action Case: ",

    "p1" => "Kristi Smith and other massage therapists at a massage spa in Corona filed a suit against their employer for minimum wage, overtime, meal period, and rest period violations. These employees also did not receive documented statements about their hours worked and wages earned, which is a violation of the California labor code.",

    "s2" => "The spa at which they were working ",

    "p2" => "paid the members of this class under a piece-rate system - meaning they were paid for every massage they gave. However, California law requires that the employees be compensated for pre-and-post shift duties like cleaning equipment, emptying trash cans, and refilling lotions. These massage therapists were not paid for this time, and were also not able to take meal breaks or 10-minute breaks in some circumstances. These are also legally mandated by the State of California. The defendant settled for 105,000 in response to their wage and hour violations.",

    "award" => "$105,000",

    "awardint" => 105000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Corona", "Plaintiff's Residence" => "Riverside"),

    "category" => array("other", "employment-law", "class-action"),

    "catastrophic" => false,
  ),


  array(

    "id" => 48,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "This defendant sandwich shop did not properly provide meal and rest breaks.",

    "header" => "Josephine Doe vs. [Defendant Sandwich Shop]",

    "type" => "Wage & Hour",

    "summary" => "Sandwich shop did not provide meal and rest breaks",

    "doi" => "Case Opened 8/2013",

    "plaintiff" => "[Defendant Sandwich Shop]",

    "defendant" => "Josephine Doe",

    "injuries" => array("Wage Violation"),

    "s1" => "A Class Action Case: ",

    "p1" => " Josephine Doe was a general manager at a sandwich shop in Anaheim, and claimed that the company was not paying employees properly. More specifically, that they were failing to provide meal-and-rest breaks, failing to pay employees for all wages due at the time of termination of employment, and dispersing inaccurate wage statements.",

    "s2" => "The case was divided into 4 subclasses, ",

    "p2" => "with different members of the class falling under one or more of these subclasses. The primary subclass dealt with meal breaks. The sandwich shop's policy did not allow for meal breaks before completing 5 hours of work, and as a result, the employees regularly took late lunch breaks. The meal break policy also did not permit them to leave the company property - an illegal restriction in California. The defendant sandwich shop settled for $250,000 for the members of this class.",

    "award" => "$250,000",

    "awardint" => 250000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Employer Location" => "Anaheim", "Plaintiff's Residence" => "Torrance"),

    "category" => array("other",  "employment-law", "class-action"),

    "catastrophic" => false,
  ),


  array(

    "id" => 49,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "This rental car company misclassified employees as 'exempt' and did not pay overtime",

    "header" => "Joseph Doe vs [Defendant Rental Company]",

    "type" => "Wage & Hour",

    "summary" => "Rental car company misclassified employees as 'exempt'",

    "doi" => "Case Opened 1/2013",

    "plaintiff" => "Joseph Doe",

    "defendant" => "[Defendant Rental Company]",

    "injuries" => array("Wage Violation"),

    "s1" => "Joseph Doe was a branch manager",

    "p1" => " at a rental car company, and was classified as an exempt executive employee - meaning that he was salaried and not paid overtime for the extra hours he worked. The legal requirements mandate that an exempt employee regularly manage 2 other employees, has say in the hiring process, regularly exercises independent judgment, and whose primary duties meet the test of exemption.",

    "s2" => "Bisnar Chase argued that the executive exemption did not apply to Joseph. ",

    "p2" => "Joseph regularly only oversaw one other employee at his office, he spent much of his time doing duties that would not be classified as exempt employees, due to policies by the company that forced the branch managers to cut employees hours. Therefore, Joseph Doe should have been paid for the overtime that he worked. This company's lack of sufficient staffing also prevented Joseph from taking lunch breaks occasionally, and their written vacation policies violated California law. This rental company settled for an amount of $125,000 in lost wages.",

    "award" => "$125,000",

    "awardint" => 125000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Plaintiff's Residence" => "Anaheim"),

    "category" => array("other", "employment-law"),

    "catastrophic" => false,
  ),


  array(

    "id" => 50,

    "imageurl" => "working-overtime-chinatsu.jpg",

    "caption" => "",

    "header" => "Chinatsu Doe vs [Defendant Loan Company]",

    "type" => "Wage & Hour",

    "summary" => "Loan company did not pay overtime and allow for meal breaks",

    "doi" => "Case Opened 9/2013",

    "plaintiff" => "Chinatsu Doe Doe",

    "defendant" => "[Defendant Loan Company]",

    "injuries" => array("Wage Violation"),

    "s1" => "A Class Action Case: ",

    "p1" => "Chinatsu Doe and 1050 members of a punitive class action filed a lawsuit against a payday loan company which did not pay out overtime and allow for meal breaks after 5 hours, or secondary rest breaks when working an average of 7 hours. ",

    "s2" => "Bisnar Chase was provided with timeclock reports of the employees",

    "p2" => " by the defendant payday loan company, but these names of the employees in this report were redacted. Therefore, Bisnar Chase was required to take a sample of the data and use it to determine an average amount of wage violations for every employee. The employees were determined to have an average of 1 missed mealtime a week for an average of 30 minutes of unpaid overtime per employee, as well as 2.5 late meal times every week, and 1 missed rest period every day. Bisnar Chase also asserted that penalties needed to be assessed on the company due to inaccurate wage statements, interest, and waiting time as terminated employees did not receive their due wages at the time of termination. The payday loan company ended up settling for $2,000,000 for the members of this class.",

    "award" => "$2,000,000",

    "awardint" => 2000000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Employee Location" => "La Brea", "Plaintiff's Residence" => "Los Angeles"),

    "category" => array("other",  "employment-law", "class-action"),

    "catastrophic" => false,
  ),


  array(

    "id" => 51,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "",

    "header" => "Ronald Smith vs [Defendant Hospital]",

    "type" => "Wage & Hour",

    "summary" => "Hospital pressured employees to underreport their hours",

    "doi" => "Case Opened 6/2012",

    "plaintiff" => "Ronald Smith",

    "defendant" => "[Defendant Hostpital]",

    "injuries" => array("Wage Violation"),

    "s1" => "A Class Action Case: ",

    "p1" => "Ronald Smith, with the assistance of Bisnar Chase, brought a class action lawsuit against a cerritos hospital at which he had worked as a therapist. The wage violations were as follows: Unpaid overtime, premium wages for meal-and-rest break violations, and penalties for inaccurate wage statements. The class action suit consisted of 72 members who were employed or formerly employed at this hospital.",

    "s2" => "The hospital pressured their employees to underreport their hours",

    "p2" => " in order to avoid overtime compensation, going so far as specifically telling them that overtime was not allowed. The therapists however, were expected to work off-the-clock without pay. The hospital also did not allow uninterrupted meal breaks for their employees, and did not allow them to leave the premises when they took their meal breaks, which is a violation of wage law. Since the hospital did not record the correct hours that the therapists work, they were also guilty of not providing accurate wage statements. This defendant hospital settled for $600,000 as compensation for all of the class memebers.",

    "award" => "$600,000",

    "awardint" => 600000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Employer Location" => "Cerritos", "Plaintiff's Residence" => "Corona"),

    "category" => array("other", "employment-law", "class-action"),

    "catastrophic" => false,
  ),


  /*array(

			"id" => 52,

			"imageurl" => "gavel-with-money.jpg",

			"caption" => "",

			"header" => "William Brown vs [Defendant Hospital]",

			"type" => "Wage & Hour",

			"summary" => "",

			"doi" => "Case Opened 6/2012",

			"plaintiff" => "Ronald Smith",

			"defendant" => "[Defendant Hospital]",

			"injuries" => array("Wage Violation"),

			"s1" => "A Class Action Case: ",

			"p1" => "Ronald Smith, with the assistance of Bisnar Chase, brought a class action lawsuit against a cerritos hospital at which he had worked as a therapist. The wage violations were as follows: Unpaid overtime, premium wages for meal-and-rest break violations, and penalties for inaccurate wage statements. The class action suit consisted of 72 members who were employed or formerly employed at this hospital.",

			"s2" => "The hospital pressured their employees to underreport their hours",

			"p2" => " in order to avoid overtime compensation, going so far as specifically telling them that overtime was not allowed. The therapists however, were expected to work off-the-clock without pay. The hospital also did not allow uninterrupted meal breaks for their employees, and did not allow them to leave the premises when they took their meal breaks, which is a violation of wage law. Since the hospital did not record the correct hours that the therapists work, they were also guilty of not providing accurate wage statements. This defendant hospital settled for $600,000 as compensation for all of the class memebers.",

			"award" => "$600,000",

			"awardint" => 600000,

			"awardcat" => "conf",

			"storylink" => "",

			"location" => array("la"),

			"relatedLocations" => array("Employer Location" => "Cerritos", "Plaintiff's Residence" => "Corona" ),

			"category" => array("other"),

			"catastrophic" => false,
		),
   */

  array(

    "id" => 53,

    "imageurl" => "gavel-with-money.jpg",

    "caption" => "",

    "header" => "William Smith vs. [Defendant Hair Salon]",

    "type" => "Wage & Hour",

    "summary" => "Hair salon did not pay employees for all the time worked ",

    "doi" => "Case Opened 5/2013",

    "plaintiff" => "Ronald Smith",

    "defendant" => "[Defendant Hair Salon]",

    "injuries" => array("Wage Violation"),

    "s1" => "A Class Action Case: ",

    "p1" => "This class action was brought against a defendant hair salon by 758 current and former employees. The employees of said salon were paid on commision, and if they completed a shift without earning more than they would have being paid minimum wage, the defendant would make up the difference.",

    "s2" => "The primary issue with this hair salon's practices ",

    "p2" => "was that it required employees to arrive at work 15 minutes prior to opening, and also required post-closing work activities. The employees were not compensated for this time that they spent before and after the salon was open. The employees were also not reimbursed for tools and travel expenses that they were required to pay out of pocket for. The defendant settled at mediation for $575,000.",

    "award" => "$575,000",

    "awardint" => 575000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Employer Location" => "Oceanside", "Plaintiff's Residence" => array("Rancho Santa Margarita", "Hawthorne")),

    "category" => array("other", "employment-law", "class-action"),

    "catastrophic" => false,
  ),


  array(

    "id" => 54,

    "imageurl" => "pit-bull-morales.jpg",

    "caption" => "Paulette Morales' lower lip was ripped off by a pit bull",

    "header" => "Paulette Morales vs. Fidel Medina",

    "type" => "Dog Bite/Premises",

    "summary" => "Victim was bitten in the face by a dog",

    "doi" => "January 8, 2015",

    "plaintiff" => "Jean Gates",

    "defendant" => "Stephen Moore",

    "injuries" => array("Lip Torn from the Jaw"),

    "s1" => "The Incident: ",

    "p1" => "Jean Gates was talking a walk with her granddaughter in her apartment complex in San Clemente and saw Stephen Moore walking his dog coming from the opposite direction. She bent down to pet the dog and it jumped up and bit her in the face, knocking her to the ground onto her back.",

    "s2" => "a complex surgical operation was required ",

    "p2" => "to repair the tissue in the lower third of Jean's face. Her total medical bills exceeded $65,000 and she still had cosmetic imperfections in her left lip. Jean Gates was 86 at the time of the accident, and was suffering from mild dementia, so when Bisnar Chase was able to recover the insurance policy limit of $100,000 for her, it was a big win for her family. ",

    "award" => "$125,000",

    "awardint" => 125000,

    "awardcat" => "",

    "storylink" => "",

    "location" => array("oc"),

    "relatedLocations" => array("Accident Location" => "San Clemente", "Plaintiff's Residence" => "San Clemente"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),

  array(

    "id" => 55,

    "imageurl" => "flowers-gonzales.jpg",

    "caption" => "Lucia Doe was fatally injured when her car rolled over after being struck from behind.",

    "header" => "Confidential",

    "type" => "Auto Defect",

    "summary" => "Confidential",

    "doi" => "July 16, 2006",

    "plaintiff" => "Simon Doe",

    "defendant" => "[Defendant Corporation]",

    "injuries" => array("Wrongful Death: Rollover/Seatbelt defect"),

    "s1" => "The Accident: ",

    "p1" => "Simon Doe was traveling with his family on the I-15 in Hesperia, when he was struck from behind by Terence Chavez. Simon lost control of the vehicle, spun out, and rolled over on the road. Simon's wife, Lucia, was partially ejected from the vehicle during the accident and died at the scene. The rest of the family also suffered less serious injuries. ",

    "s2" => "Bisnar Chase found several issues with the Doe car ",

    "p2" => "which contributed to the severity of the accident. First off, the car manufacturer could have included an electronic stability control which would have prevented a roll over, but negligently decided not to. The seat belts were also not created to protect the passenger in the event of a rollover - a fact that was already acknowledged but ignored by the defendant corporation. Ultimately, Bisnar Chase was able to recover a total of $8,500,000 for the Doe family.",

    "award" => "$14,443,205",

    "awardint" => 14443205,

    "awardcat" => "5+",

    "storylink" => "https://www.bestattorney.com/attorneys/brian-chase.html",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Hesperia", "Plaintiff's Residence" => "La Mirada"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),

  array(

    "id" => 56,

    "imageurl" => "h.gavin.long-reduced.webp",

    "caption" => "Lucia Doe was fatally injured when her car rolled over after being struck from behind.",

    "header" => "Cunningham vs. Narconon et al.",

    "type" => "Facility Negligence",

    "summary" => "Wife and Children of Man Who Died by Suicide While in Rehab",

    "doi" => "July 16, 2006",

    "plaintiff" => "Simon Doe",

    "defendant" => "[Defendant Corporation]",

    "injuries" => array("Wrongful Death: Rollover/Seatbelt defect"),

    "s1" => "The Accident: ",

    "p1" => "Simon Doe was traveling with his family on the I-15 in Hesperia, when he was struck from behind by Terence Chavez. Simon lost control of the vehicle, spun out, and rolled over on the road. Simon's wife, Lucia, was partially ejected from the vehicle during the accident and died at the scene. The rest of the family also suffered less serious injuries. ",

    "s2" => "Bisnar Chase found several issues with the Doe car ",

    "p2" => "which contributed to the severity of the accident. First off, the car manufacturer could have included an electronic stability control which would have prevented a roll over, but negligently decided not to. The seat belts were also not created to protect the passenger in the event of a rollover - a fact that was already acknowledged but ignored by the defendant corporation. Ultimately, Bisnar Chase was able to recover a total of $8,500,000 for the Doe family.",

    "award" => "$11,000,000",

    "awardint" => 11000000,

    "awardcat" => "5+",

    "storylink" => "https://www.prlog.org/12768353-bisnar-chase-secures-11-million-jury-verdict-for-wife-and-children-of-man-who-died-by-suicide-while-in-rehab.html",

    "location" => array("la", "other"),

    "relatedLocations" => array("Accident Location" => "Hesperia", "Plaintiff's Residence" => "La Mirada"),

    "category" => array("premises"),

    "catastrophic" => true,
  ),

  array(

    "id" => 57,

    "imageurl" => "oncoming-traffic-mione.jpg",

    "caption" => "Jocelyn Smith was killed by a car fire",

    "header" => "Confidential Motor Vehicle Accident",

    "type" => "Motor Vehicle Accident",

    "summary" => "Temporarily Confidential",

    "doi" => "June 4th, 2013",

    "plaintiff" => "Antonio Flores",

    "defendant" => "[Defendant Auto Manufacturer]",

    "injuries" => array("Wrongful Death"),

    "s1" => "Jocelyn Smith was sitting in the front seat of a car ",

    "p1" => " travelling through Riverside. Driver Ashley Doe suddently passed out and lost control of the vehicle, causing it to collide with a raised center island where it struck 2 metal sign posts, caught fire, and became totally engulfed in flames. Jocelyn Smith sustained terrible burns over 64% of her body and passed away 7 weeks later due to complications with her injuries.",

    "s2" => "The car fire was started when the car ran over a post, ",

    "p2" => "which in turn ripped through the gas tank that that was struck by the car, and the gasoline was ignited by the friction of the car on the concrete. Bisnar Chase was hired by Jocelyn's parents, Antonio and Judith, to bring action against the Manufacturer who made the car. The fuel tank did not have an adequate liner that prevent it from being ruptured by objects on the road, and therefore was a direct cause of Jocelyn's death. The defendant auto manufacturer settled for $650,000 which was used to pay for medical bills, funeral services, and for non-economic damages suffered by Jocelyn's parents.",

    "award" => "$11,000,000",

    "awardint" => 11250000,

    "awardcat" => "conf",

    "storylink" => "",

    "location" => array("other"),

    "relatedLocations" => array("Accident Location" => "Riverside", "Plaintiff's Residence" => "Riverside"),

    "category" => array("auto", "auto-defect"),

    "catastrophic" => true,
  ),

  array(

    "id" => 58,

    "imageurl" => "dog-bite-jaramillo.jpg",

    "caption" => "Emma Jaramillo was bitten in the face by a food-aggressive dog",

    "header" => "Emma Jaramillo vs Sanchez Family",

    "type" => "Dog Bite/Premises",

    "summary" => "Girl bitten by food-aggressive dog",

    "doi" => "January 10th, 2009",

    "plaintiff" => "Emma Jaramillo",

    "defendant" => "Sanchez Family",

    "injuries" => array("Severe Bites to Face and Head"),

    "s1" => "Five-year-old Emma Jaramillo and her family ",

    "p1" => "were at the house of Juan and Julia Sanchez. They had a husky named Dandy, who was eating her food when Emma approached, wanting to say hi. In a display of food aggression, Dandy attacked Emma and bit her multiple times in the face, head, and shoulders. The wounds included long scars and deep puncture wounds in the cheek, scalp, and shoulder blade.",

    "s2" => "Emma was severely traumatized by the attack, ",

    "p2" => "and required 8 therapist sessions over the course of 2 months, in addition to hospital and medication costs. Emma developed scarring along her face and was recommended a laser treatment with the possibility of requiring surgical scar restoration in 10 years. After Emma had finished treating and Bisnar Chase sent a demand out, a $300,000 settlement was decided upon within 20 days.",

    "award" => "$3,000,000",

    "awardint" => 3000000,

    "awardcat" => "",

    "storylink" => "/case-results",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Azusa", "Plaintiff's Residence" => "Whittier"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),

  array(

    "id" => 59,

    "imageurl" => "dog-bite-jaramillo.jpg",

    "caption" => "Emma Jaramillo was bitten in the face by a food-aggressive dog",

    "header" => "Emma Jaramillo vs Sanchez Family",

    "type" => "Dog Bite/Premises",

    "summary" => "Girl bitten by food-aggressive dog",

    "doi" => "January 10th, 2009",

    "plaintiff" => "Emma Jaramillo",

    "defendant" => "Sanchez Family",

    "injuries" => array("Severe Bites to Face and Head"),

    "s1" => "Five-year-old Emma Jaramillo and her family ",

    "p1" => "were at the house of Juan and Julia Sanchez. They had a husky named Dandy, who was eating her food when Emma approached, wanting to say hi. In a display of food aggression, Dandy attacked Emma and bit her multiple times in the face, head, and shoulders. The wounds included long scars and deep puncture wounds in the cheek, scalp, and shoulder blade.",

    "s2" => "Emma was severely traumatized by the attack, ",

    "p2" => "and required 8 therapist sessions over the course of 2 months, in addition to hospital and medication costs. Emma developed scarring along her face and was recommended a laser treatment with the possibility of requiring surgical scar restoration in 10 years. After Emma had finished treating and Bisnar Chase sent a demand out, a $300,000 settlement was decided upon within 20 days.",

    "award" => "$500,000",

    "awardint" => 500000,

    "awardcat" => "",

    "storylink" => "/case-results",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Azusa", "Plaintiff's Residence" => "Whittier"),

    "category" => array("premises"),

    "catastrophic" => false,
  ),

  array(
    "id" => 60,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Adam Vasquez",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 61,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Christopher Chavez",
    "type" => "Motor Vehicle Accident",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto"),
    "catastrophic" => true,
  ),


  array(
    "id" => 63,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Rogelio Santibanez",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,800,000",
    "awardint" => 1800000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),


  array(
    "id" => 67,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Gabriel Maciel",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,500,000",
    "awardint" => 1500000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 68,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Nicole Swapp",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,300,000",
    "awardint" => 1300000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 69,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Minh Lee",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 70,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "John Rich",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,000,000",
    "awardint" => 1000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 71,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential", // Maria Moreno
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Moreno, Maria",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$10,000,000",
    "awardint" => 10000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 72,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential", //George Sharp
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$5,000,000",
    "awardint" => 5000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 73,
    "imageurl" => "spice-eckhardt.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Sonny James Morgan",
    "type" => "Product Liability",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,500,000",
    "awardint" => 3500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other"),
    "catastrophic" => true,
  ),

  array(
    "id" => 74,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Eleanor Domalaog",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$3,500,000",
    "awardint" => 3500000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 75,
    "imageurl" => "spice-eckhardt.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Marylou Gilsleider",
    "type" => "Class Action",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$1,850,000",
    "awardint" => 1850000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action"),
    "catastrophic" => true,
  ),

  array(
    "id" => 76,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Justin Vaughan",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,515,000",
    "awardint" => 1515000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 77,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Orland Sylve",
    "type" => "Motor Vehicle Accident",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,500,000",
    "awardint" => 1500000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 77,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Sahar Farahani",
    "type" => "Motor Vehicle Accident",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,000,000",
    "awardint" => 1000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 78,
    "imageurl" => "rashell-wage-and-hour.jpg",
    "caption" => "Wage & Hour",
    "header" => "Tamika Doe vs. Defendant Employer that provides services in the Inland Empire",
    "type" => " Wage & Hour Class Action and Private Attorneys General Act (“PAGA”) Case.",
    "summary" => "Defendant did not provide compliant meal periods and rest breaks and employees were not paid for all hours worked",
    "doi" => "October 21,2019",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,750,000",
    "awardint" => 1750000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("other", "class-action", "employment-law"),
    "catastrophic" => false,
  ),

  array(
    "id" => 79,
    "imageurl" => "rashell-wage-and-hour.jpg",
    "caption" => "Wage & Hour",
    "header" => "Sam Doe v. Defendant Employer (San Diego County Superior Court)",
    "type" => "Private Attorneys General Act (“PAGA”) Representative Action for civil penalties.",
    "summary" => "Defendant failed to provide compliant meal periods and rest breaks and failed to pay wages for all hours worked, which also resulted in derivative claims.",
    "doi" => "October 21,2019",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$950,000",
    "awardint" => 950000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("other", "employment-law"),
    "catastrophic" => false,
  ),

  array(

    "id" => 80,

    "imageurl" => "flowers-gonzales.jpg",

    "caption" => "Marilyn Herod was killed when a car struck her flower booth",

    "header" => "Pedestrian accident vs. ATM Collision",

    "type" => "Auto Vs. Pedestrian",

    "summary" => "Pedestrian truck by Vehicle at ATM",

    "doi" => "May 13th, 2007",

    "plaintiff" => "Michele Herod",

    "defendant" => "[Multiple Defendants]",

    "injuries" => array("Fatally Struck"),

    "s1" => "Harley Daniels worked at a Pepsi bottling plant ",

    "p1" => "and left his 12am shift early to go home. While driving westbound on Century blvd, witnesses saw him speeding and possibly racing another car. The cars sideswiped each other and Daniels was forced across the street, driving through an iron fence and into a flower stand killing 2 pedestrians and injuring 3 others.",

    "s2" => "Marilyn Herod was running the flower stand ",

    "p2" => "and had been paying the auto repair shop where the stand was located. She was killed immediately when Daniels' car hit her. Bisnar Chase represented her daughter, Michele Herod, in the wrongful death lawsuit that was filed. We obtained a settlement of $50,000 from the auto shop for not providing a safe environment for Herod, a settlement of $1,490,000 from the insurance company, and a confidential amount from Pepsi.",

    "award" => "$5,250,000",

    "awardint" => 5250000,

    "awardcat" => "1+",

    "storylink" => "",

    "location" => array("la"),

    "relatedLocations" => array("Accident Location" => "Los Angeles", "Plaintiff's Residence" => "Corona", "Victim's Residence" => "Riverside"),

    "category" => array("pedestrian"),

    "catastrophic" => true,
  ),

  // STEVES CASES
  array(
    "id" => 81,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,000,000",
    "awardint" => 1000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 82,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,225,000",
    "awardint" => 1225000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 83,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,235,000",
    "awardint" => 1235000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 84,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,300,000",
    "awardint" => 1300000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 85,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,500,000",
    "awardint" => 1500000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 86,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$1,980,000",
    "awardint" => 1980000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 87,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 88,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 89,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 90,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,250,000",
    "awardint" => 2250000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 91,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,250,000",
    "awardint" => 2250000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),
  array(
    "id" => 92,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$5,250,000",
    "awardint" => 5250000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 93,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$5,830,000",
    "awardint" => 5830000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),



  array(
    "id" => 94,
    "imageurl" => "seatback-dummy-romine.jpg",
    "caption" => "A dummy in a seatback failure test for Romine's case",
    "header" => "Acona",
    "type" => "Seatback Failure Auto Defect",
    "summary" => "A seatback failure auto defect resulting in serious injury",
    "doi" => "October 21,2006",
    "plaintiff" => "Jaklin Mikhal Romine",
    "defendant" => "Johnson Controls",
    "injuries" => array("Severe Brain Injury", "Quadriplegia"),
    "s1" => "The Accident: ",
    "p1" => "Jaklin Romine was stopped at a light in Pasadena when the car behind her was hit at a high speed, instantly killing Christopher Clark, the driver inside. Clark's Nissan Altima was pushed into Jalkin's car at a moderate speed, which resulted in Jaklin's seat breaking, propelling her back into the back passenger seat and hitting her head on the headrest, resulting in incomplete quadriplegia and severe head injury.",
    "s2" => "Johnson Controls ",
    "p2" => "designed and manufactured the seat that collapsed, and had also developed a different seat that was more structurally sound than the one installed in Jaklin's car. Unfortunately, Johnson Controls elected to produce the cheaper seats to increase their profits, without being concerned for their consumers' safety. Jaklin was awarded $24.7 million in a jury verdict.",
    "award" => "$2,100,000",
    "awardint" => 2100000,
    "awardcat" => "5+",
    "storylink" => "",
    "location" => array("la"),
    "relatedLocations" => array("Accident Location" => "Pasadena", "Plaintiff's Residence" => "Los Angeles"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),


  array(
    "id" => 95,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Consumer Class Action",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$117,500,000",
    "awardint" => 117500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action"),
    "catastrophic" => true,
  ),

  array(
    "id" => 96,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Wage / Class",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$1,700,000",
    "awardint" => 1700000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action", "employment-law"),
    "catastrophic" => false,
  ),


  array(
    "id" => 97,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Employment",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$1,750,000",
    "awardint" => 1750000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action", "employment-law"),
    "catastrophic" => false,
  ),


  array(
    "id" => 98,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Employment",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$1,850,000",
    "awardint" => 1850000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action"),
    "catastrophic" => false,
  ),


  array(
    "id" => 99,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Wage & Hour",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$2,000,000",
    "awardint" => 2000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action"),
    "catastrophic" => false,
  ),

  array(
    "id" => 100,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Consumer Class Action",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$2,250,000",
    "awardint" => 2250000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action"),
    "catastrophic" => false,
  ),

  array(
    "id" => 101,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Employment",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$2,500,000",
    "awardint" => 2500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action", "employment-law"),
    "catastrophic" => false,
  ),

  array(
    "id" => 97,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Wage & Hour",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Devin & Veronica Smith",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$1,750,000",
    "awardint" => 1750000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("other", "class-action", "employment-law"),
    "catastrophic" => false,
  ),



  // METICULOUS ENTRY STARTS HERE

  array(
    "id" => 98,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Dennis, Trevor",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$10,500,000",
    "awardint" => 10500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 99,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect / Seatback",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Rabinovich, Michael",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$10,050,000",
    "awardint" => 10050000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 100,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Santos, Ricardo",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$6,800,000",
    "awardint" => 6800000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 101,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Flores, Griselda",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,830,000",
    "awardint" => 5830000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 102,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Heather, Lehr",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,250,000",
    "awardint" => 5250000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 103,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Heather, Lehr",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,250,000",
    "awardint" => 5250000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 104,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Gonzales, et al.",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,000,000",
    "awardint" => 5000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 105,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Garcia, Jose",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,000,000",
    "awardint" => 5000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 106,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Garcia, Jose",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$5,000,000",
    "awardint" => 5000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),


  array(
    "id" => 107,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Dias, Manuel",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$4,500,000",
    "awardint" => 4500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 108,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Newman, Joshua",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$4,250,000",
    "awardint" => 4250000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 109,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Confidential",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Segoviano, Sandro",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$4,250,000",
    "awardint" => 4250000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 110,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Confidential",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Gluck, Randy",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$4,000,000",
    "awardint" => 4000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 111,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Motorcycle vs. Auto",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Diaz, Carlos",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,732,551",
    "awardint" => 3732551,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 112,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Aguirre-Nunez, Sandra",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,500,000",
    "awardint" => 3500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto-defect", "auto"),
    "catastrophic" => true,
  ),

  array(
    "id" => 113,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Premise Liability",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Morgan, Sonny James",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,500,000",
    "awardint" => 3500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("premises"),
    "catastrophic" => true,
  ),

  array(
    "id" => 114,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Blas, Maria",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,299,000",
    "awardint" => 3299000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto", "auto-defect"),
    "catastrophic" => true,
  ),

  array(
    "id" => 115,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Premise Liability",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Schrock, Stacey",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,000,000",
    "awardint" => 3000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("premises"),
    "catastrophic" => true,
  ),


  array(
    "id" => 116,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "MVA",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Araniva, Oscar",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,000,000",
    "awardint" => 3000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto", "auto-defect"),
    "catastrophic" => true,
  ),

  array(
    "id" => 117,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "De Rosas, Anthony",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,000,000",
    "awardint" => 3000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto", "auto-defect"),
    "catastrophic" => true,
  ),

  array(
    "id" => 118,
    "imageurl" => "gavel-with-money.jpg",
    "caption" => "Connor Eckhardt passed away after smoking a legally purchased spice",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Confidential",
    "doi" => "July 12th,2014",
    "plaintiff" => "Hernandez, Balmore",
    "defendant" => "[Defendant Smoke Shop]",
    "injuries" => array("Wrongful Death"),
    "s1" => "19-year-old Connor Eckhardt visited a smoke shop ",
    "p1" => "in Santa Ana and purchased some spice. He went to a friend's house where they started to smoke the spice. After taking one or two hits from the spice, Connor lay down and did not get back up. He had experienced multiple organ failure and had to be transported to Hoag Hospital. Within 2 days doctors declared him brain dead as a result of the inhalation of the spice.",
    "s2" => "Spice has been a legal and easily purchaseable substance ",
    "p2" => "and used to be labeled as \"incense\" or \"potpourri\", leading to the general impression that it is a safe drug. This is far from the truth, however, as this form of synthetic pot can lead to psychosis, seizures, heart attack, and in this case, death. The smoke shop that sold this spice knowingly allowed it to be distributed despite the danger it posed to anyone who used it. This makes the defendant liable for damages suffered by Devin and Veronica Smith, Connor's parents. The defendant smoke shop settled for a confidential amount.",
    "award" => "$3,000,000",
    "awardint" => 3000000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Santa Ana", "Plaintiff's Residence" => "Sacramento", "Victim's Residence" => "San Clemente"),
    "category" => array("auto", "auto-defect"),
    "catastrophic" => true,
  ),

  array(
    // Roof crush and lack of rollover side curtain airbag case.  1 tbi (driver) and arm off right front passenger.  FYI .  This is a confidential settlement.

    "id" => 119,
    "imageurl" => "black-suv-richard.jpg",
    "caption" => "Roof crush and lack of rollover side curtain airbag",
    "header" => "Confidential",
    "type" => "Auto Defect",
    "summary" => "Roof crush and lack of rollover side curtain airbag",
    "doi" => "November 12th,2007",
    "plaintiff" => "Richard Doe",
    "defendant" => "[Defendant Auto Manufacturer]",
    "injuries" => array("Incomplete Quadriplegia"),
    "s1" => "In the afternoon of November 12, 2007,",
    "p1" => " witnesses reported seeing a black SUV driving down highway 17 in Arizona, weaving in and out of traffic and occasionally going off the road. The driver was in diabetic shock and struck Richard Doe's truck. ",
    "s2" => "Richard Doe's truck rolled over",
    "p2" => " and the roof crushed in on itself, rendering him a quadriplegic but with some remaining use of his arms. Bisnar Chase represented Richard in filing a lawsuit against the manufacturer of his truck. The truck was supposed to keep the driver save in the event of a rollover and the roof crushing inwards was considered a defect. Bisnar Chase secured a $2,500,000 settlement for Richard Doe for his permanent injuries.",
    "award" => "$1,500,000",
    "awardint" => 1500000,
    "awardcat" => "conf",
    "storylink" => "",
    "location" => array("oc"),
    "relatedLocations" => array("Accident Location" => "Arizona", "Plaintiff's State of Residence" => "Minnesota",),
    "category" => array("auto", "auto-defect"),
    "catastrophic" => true,
  ),




);
