<?php
	// This file contains all of the case results data which is printed out on /case-results/ by way of an AJAX call.
	// When /case-results/ first loads, this file is called to print out the top 10 highest award cases.
	// When the filters are clicked on, that filter change is sent in a parameter to this file, which then prints out a filtered list via AJAX.
	// Array of case results
	// NOTE: Types of locations are: "la", "oc", "norcal", "other"
	// NOTE: Types of accidents are: "auto", "motorcycle", "auto-defect", "premises", "pedestrian", "other" [MORE HERE LATER]
	include($_SERVER['DOCUMENT_ROOT']."/case-results/case-database.php");

	$array = json_decode(stripslashes($_POST["array"])); //All location / type / value / max / catastrophic data is added into this array from AJAX.
	$max = $array[0];
	$locations = $array[1];
	$types = $array[2];
	$values = $array[3];
	$catastrophic = $array[4];
	$caseids = $array[5];
	$i=1; //Counter for case results
	$catastrophicmatch = array();
	$locmatch = array();
	$loctypematch = array();
	$finalmatch = array();
	//Check here if filter matches the criteria sent over in $array. If not, break. Do not pass go, do not increment $i.
	if ($caseids) {
		//echo("<script> console.log('output of caseids: ".$caseids."');</script>");
		foreach ($caseresults as $result) {
				if (in_array((string)$result['id'], $caseids)) {
					array_push($finalmatch, $result);
				}
		}
	}
	else {
		foreach ($caseresults as $result) { //filter by catastrophic or not.
			if ($catastrophic == $result['catastrophic']) {
				array_push($catastrophicmatch, $result);
			}
		}
		foreach ($catastrophicmatch as $number => $result) { //filter by case location
			if (!empty($locations)) { //If only certain locations are selected, do this.
				foreach ($result['location'] as $listedlocation) { // Do the following for every location result in this case result.
					if (in_array($listedlocation, $locations)) {
						array_push($locmatch, $result); //Puts the matched item into the location match array.
						break;
					}
					/*else {
						continue 2; //Check back on validity of this code - will it add the correct locations?
					} */
				}
			} else { //If no locations are selected, all the location match array should equal the case results array. (no location filter)
				array_push($locmatch, $result);
			}
		}
		foreach ($locmatch as $result) {// Filter by case Type
				if (!empty($types)) { //If only certain types are selected, do this.
					foreach ($result['category'] as $listedtype) { // Do the following for every type result in this case result.
						if (in_array($listedtype, $types)) {
							array_push($loctypematch, $result); //Puts the matched item into the loc type match array.
							break;
						}
					}
				} else { //If no types are selected, all the type match array should equal the locmatch array. (no type filter)
					array_push($loctypematch, $result);
				}
		}
		foreach ($loctypematch as $result) {//Filter by Case value
				if ($values == "all" || ($result['awardcat'] == "5+" && $values != "conf")) {
					array_push($finalmatch, $result);
				} else if ($result['awardcat'] == "1+" && $values == 1) {
					array_push($finalmatch, $result);
				} else if ($result['awardcat'] == "conf" && $values == "conf") {
					array_push($finalmatch, $result);
				}
		}
	}
	//Sort these by the integer value of the award - more award is higher.
	function compareByName($a, $b) {
	  return $b["awardint"] - $a["awardint"];
	}
	usort($finalmatch, 'compareByName');

	//NOW DO for each case result in finalmatch, check $i and print caseresultsinfo.
	/*
	$caseresultsinfo;
	foreach ($finalmatch as $result) {
		if ($i < $max+1) {
			$caseresultsinfo .= printResult($result, $i);
			$i++;
		}
	}
	*/

	//echo json_encode(array($caseresultsinfo,$i-1, count($finalmatch)));
	echo json_encode($finalmatch);

	// Prints out the results
	/*

	function printResult($result, $i) {
		foreach($result['injuries'] as $injury) {
			$injuries .= "<li>".$injury."</li>\n";
		}
		if ($result['storylink'] != "") {
			$fullstory = '<p class="case-full-story"><a href="'.$result['storylink'].'" target="_blank"><span class="nomobile">Read The </span>Full Story  <i class="fa fa-chevron-circle-right"></i></a></p>';
		}
		$resultcontent .= '
			<div id="case'.$i.'" class="case-container">
				<div class="case-top border-box row">
				<div class="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
					<div class="case-picture">
						<img src="/images/case-results/'.$result['imageurl'].'">
					</div>
					<div class="case-caption gutter">
						'.$result['caption'].'
					</div>
				</div>
				<div class="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box">
					<div class="case-header border-box">
						<h2 ';
		if (strlen($result['header']) > 42 ){
			$resultcontent .= 'style="font-size:18px;"';
		} else if (strlen($result['header']) > 34 ){
			$resultcontent .= 'style="font-size:20px;"';
		}
		$resultcontent .= '>'.$result['header'].'</h2>
					</div>
					<div class="case-type border-box gutter">
							<strong>Type of Accident / Injury: </strong> '.$result['type'].'<br>
							<p class="result-date"><strong>Date of Incident: </strong> '.$result['doi'].'</p>';
		if ($result['summary'] != "") {
			$resultcontent .= '<p class="result-summary"><strong>Summary: </strong>'.$result['summary'].'</p>';
		}
		$resultcontent .= '</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="case-bottom border-box">
				<div class="case-award">
						<h2 ><strong>Award:</strong> '.$result['award'].'</h2> '.$fullstory.'
				</div>
			</div>
		</div>
		<div class="case-divider"></div>
		';
		return $resultcontent;
	}

	*/

?>
