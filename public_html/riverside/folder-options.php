<?php
// Folder Options for /riverside
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "riverside", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Riverside Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Amusement Park Accidents" => "/riverside/amusement-park-accidents.html",
        "Auto Accidents" => "/riverside/auto-accidents.html",
		"Auto Defects" => "/riverside/auto-defect-lawyers.html",	
        "Bus Accidents" => "/riverside/bus-accidents.html",
        "Dog Bites" => "/riverside/dog-bites.html",
        "FAQ's" => "/riverside/faqs.html",
        "Hiring a Lawyer" => "/riverside/hiring-a-lawyer.html",
        "Motorcycle Accidents" => "/riverside/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/riverside/nursing-home-abuse.html",
		"Parking Lot Accidents" => "/riverside/parking-lot-accidents.html",
        "Pedestrian Accidents" => "/riverside/pedestrian-accidents.html",
        "Premises Liability" => "/riverside/premises-liability.html",
        "Product Liability" => "/riverside/product-liability.html",
        "Rollover Accidents" => "/riverside/rollover-accidents.html",
        "Roof Crush Injuries" => "/riverside/roof-crush-injuries.html",
        "Seat Back Failures" => "/riverside/seat-back-failure.html",
        "Slip and Fall Accidents" => "/riverside/slip-and-fall-accidents.html",
        "Swimming Pool Accidents" => "/riverside/swimming-pool-accidents.html",
        "Train Accidents" => "/riverside/train-accidents.html",
        "Truck Accidents" => "/riverside/truck-accidents.html",			
        "Watercraft Accidents" => "/riverside/watercraft-accidents.html",
        "Wrongful Death" => "/riverside/wrongful-death.html",
        "Riverside Contact Page" => "/riverside/contact.html",
        "Riverside Home" => "/riverside/",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>