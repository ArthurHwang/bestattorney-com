<?php
// Folder Options for /bus-accidents/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "accident";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Bus Accident Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	// "sidebarLinks" => array(
 //    ),

	"sidebarContentTitle" => "Bus Accident Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'><li>Serving California since 1978.</li>
                            <li>Recovered over $650 Million in verdicts and settlements</li>
                            <li>Rated top attorneys year after year.</li>
                            <li>No recovery, no fee promise.</li>
                            <li>If you or a loved one have suffered in a bus accident as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                            </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category','auto'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Are the Insurance Companies on My Side?', '', 'DkKmXYasbr4'],
		['Can My Lawyer Help Me Get My Car Repaired?', '', 'WSuOEz40ZPA']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
