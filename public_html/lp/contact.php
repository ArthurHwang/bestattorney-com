<?php



// Workaround Sucuri to get the real IP Address

if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {

	$_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];

}



//

// SendGrid PHP Library Example

//

// This example shows how to send email through SendGrid

// using the SendGrid PHP Library.  For more information

// on the SendGrid PHP Library, visit:

//

//     https://github.com/sendgrid/sendgrid-php

//



require $_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php';

require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';





/* CREATE THE SENDGRID MAIL OBJECT

====================================================*/

$sendgrid = new SendGrid( $sg_username, $sg_password );

$mail = new SendGrid\Email();

$mail2 = new SendGrid\Email();



/* SEND MAIL

/  Replace the the address(es) in the setTo/setTos

/  function with the address(es) you're sending to.

====================================================*/



if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$realname = $_POST['realname'];

	$homephone = trim($_POST['homephone']);

	$validphone = true;

	$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);

	$comment = trim($_POST['comment']);

	$noparamuri = $_POST['noparamuri'];

	$type = $_POST['type'];

	$checked = $_POST['checkedArray'];

	if(empty($checked)) {

		array_push($checked, "n/a");

	}





	if ( $realname == 'Name:') {$realname = '';}



	$phonevalidate = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";

	if (!preg_match( $phonevalidate, $homephone )) {

		$validphone = false;

	}



	if (!filter_var($email, FILTER_VALIDATE_EMAIL) && (!$validphone)) {

			header("HTTP/1.0 400 Bad Request");    

            if ((($realname != "") && ($realname != "Name:") && ($realname != "Nombre:")) || (($comment != "") && (strlen($comment) > 50))) {

	            mail("inquiry@bestattorney.com","Invalid Contact Form Submission", "This is an automatic notification that someone has tried to submit a contact form without filling in a correct phone number or email address. \n

Below is a summary of the information that they have entered so far. If we do not receive a complete intake email from them in 5 minutes, please try to use the existing information below to contact them. \n

Name: ".$_POST['realname']."

Phone Number: ".$_POST['homephone']."

Email: ".$_POST['email']."

Message: ".$_POST['comment']."\n

Type of Injury: ".$_POST['type']."\n

IP Address: ".$_SERVER['REMOTE_ADDR']);

        	}

        	exit("Please provide a valid email or phone number!");

		} 



	include($_SERVER['DOCUMENT_ROOT']."/template-files/keycheck.inc.php");

	$subject = "BestAttorney.com Short Case Evaluation Request - From:".$realname;

	$EmailBody = "Name: $realname\nHomephone: $homephone\nEmail: $email\nType of Injury: $type\nDamages: ";



	foreach($_POST['checkedArray'] as $check) {

		$EmailBody .= $check."\n";

	}



	$EmailBody .= "\n\n\nBrief description of accident/injury:\n $comment\n";

	$EmailFooter="\nSenders IP address: ".$_SERVER['REMOTE_ADDR']."\n\n".$historys."\n\n Contact Form was submitted from the following page: ".$noparamuri;

	$Message = $EmailBody.$EmailFooter;

	$EmailBanner="______________________________________________________________________\n                         BISNAR CHASE\n  Personal Injury Attorneys        -- Making a difference for every client, one at a time. --\n\nhttps://www.bestattorney.com                         Call: 949-203-3814\n======================================================================";



	$EmailResponse = "Dear $realname,\n\nYour inquiry has been received as copied below.\n\nFOR IMMEDIATE ASSISTANCE, call us at 949-203-3814.\n\nYou will receive a telephone call between 8:30 a.m. and 4:30 p.m. on business days. We are available from 8:30 a.m. to 8:30 p.m., seven days a week (excluding major holidays) for a new client inquiry. In emergencies, we are always available.\n\nIf you have been injured in a car accident, please go to https://www.bestattorney.com/resources/injured-in-car-accident.html.\n\nIf you need information regarding a product defect, please visit https://www.bestattorney.com/defective-products/\n\nThank you for your inquiry. We are here to assist you.\nBISNAR CHASE\nwww.bestattorney.com\n949-203-3814\n\n\n____________________  Information you provided: ______________________\n\n".$EmailBody."_______________________________________________________________";



	if($realname != '') {

		try {

		    $mail->

		    setFrom( "inquiry@bestattorney.com" )->

		    setReplyTo($email)->

		    setFromName($realname)->

		    addTo( "inquiry@bestattorney.com" )->

		    setSubject($subject)->

		    setText($Message);

		    

		    $response = $sendgrid->send($mail);



	    if (!$response) {

	        throw new Exception("Did not receive response.");

	    } else if ($response->message && $response->message == "error") {

	        throw new Exception("Received error: ".join(", ", $response->errors));

	    } 

		} catch ( Exception $e ) {

		    var_export($e);

		    exit;

		}

		try {

		    $mail2->

		    setFrom( "inquiry@bestattorney.com" )->

		    addTo( $email )->

		    setSubject("(Bisnar Chase) Thanks for Contacting Us!")->

		    setText($EmailResponse);

		    

		    $response2 = $sendgrid->send($mail2);



		    if (!$response2) {

		        throw new Exception("Did not receive response.");

		    } else if ($response2->message && $response2->message == "error") {

		        throw new Exception("Received error: ".join(", ", $response2->errors));

		    } else {

		    	header("HTTP/1.0 200 OK");

		    	//redirect('https://www.bestattorney.com/thank-you.html');

		        exit("Thank You, Your Contact Form Has Been Sent!");



		    }

		} catch ( Exception $e ) {

		    var_export($e);

		    exit;

		}

		redirect('https://www.bestattorney.com/thank-you.html');

	}

	else {

		header("HTTP/1.0 400 Bad Request");

		mail("inquiry@bestattorney.com","Invalid Contact Form Submission", "This is a automatic notification that someone has tried to submit a contact form without filling in their name. \n

Below is a summary of the information that they have entered so far. If we do not receive a complete intake email from them in 5 minutes, please use the existing information below to contact them. \n



Phone Number: ".$_POST['homephone']."\n

Email: ".$_POST['email']."\n

Type of Injury: ".$_POST['type']."\n

Message: ".$_POST['comment']."\n

Submitted from this page: ".$_POST['noparamuri']."\n

IP Address: ".$_SERVER['REMOTE_ADDR']);



		exit("Please provide your name!");

	}

} else {

	header("HTTP/1.0 403 Forbidden");

	exit("There was a problem with your submission, please try again.");

}



?>