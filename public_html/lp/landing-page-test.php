<?php

$page = $_SERVER['REQUEST_URI'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/preload.php");
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Base Element that is used by MaxCDN to reset all relative paths to the CDN -->
    <base href="http://webfiles.bisnarchaseperso.netdna-cdn.com/" />

    <title>
        <?php echo $title; ?>
    </title>
    <meta name="description" content="<?php echo $meta; ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex,nofollow">

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WK3JJ3');</script>
    <!-- End Google Tag Manager -->
    <!-- Universal Analytics script included, cannot fire from GTM because GTM does not support plugins like autotrack-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44427913-2', {'siteSpeedSampleRate': 100});
  ga('require', 'autotrack');
  ga('send', 'pageview');

</script>
    <script async src='/js/autotrack.js'></script>

    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif:400,700|Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $csslocation; ?>" type="text/css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/personal-injury-attorneys-pc.css" type="text/css" media="all"/><![endif]-->

    <script>
        console.log("page: <?php echo $page; ?>");
        var pageSubject = "<?php echo $pagesubject; ?>";
    </script>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="/js/jquery-combined.js"></script>
    <script type="text/javascript" src="/js/breakpoints.js"></script>
    <script type="text/javascript" src="/js/scripts.js"></script>

    <meta name="viewport" content="width=device-width, user-scalable=yes" />




    <style>

        #bisnar-chase {
	background: url('<?php echo $backgroundurl; ?>') no-repeat center top !important;
	background-size:1903px;
}

a#contact-link {
	background: url("<?php echo $trackingimageurl; ?>") !important;
}

p {
	line-height: 16px;
}

#second-step {
	/*display:none !important;*/
}

#first-step {
	color:#CCC;
	display:absolute;
}

#first-step ul{
  list-style: none;
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;
}

#first-step ul li {
	display: block;
	position: relative;
	float: left;
	width: 100%;
	height: 48px;
}

#first-step #check-label li {
	height:44px;
}

#first-step ul li input[type=radio] {
  position: absolute;
  visibility: hidden;
}

.lp-content #check-label input {
    height: 30px;
    width: 66px;
    padding: 0 auto;
    display:inline-block;
}

#first-step #radio-label li label {
  display: block;
  position: relative;
  font-weight: 300;
  font-size: 20px;
  padding: 15px 15px 15px 50px;
  margin: 5px auto;
  z-index: 9;
  cursor: pointer;
  -webkit-transition: all 0.25s linear;
}

#first-step #check-label li label {
    position: relative;
    font-weight: 300;
    font-size: 18px;
    padding: 15px 15px 15px 15px;
    margin: 0;
    cursor: pointer;
    -webkit-transition: all 0.25s linear;
    bottom: 10px;
}

#first-step ul li:hover label {
	color: #FFFFFF;
}

#first-step ul li .check{
  display: block;
  position: absolute;
  border: 5px solid #AAAAAA;
  border-radius: 100%;
  height: 20px;
    width: 20px;
    top: 20px;
  left: 10px;
	z-index: 5;
	transition: border .25s linear;
	-webkit-transition: border .25s linear;
}

#first-step ul li:hover .check {
  border: 5px solid #FFFFFF;
}

#first-step ul li .check::before {
  display: block;
  position: absolute;
	content: '';
  border-radius: 100%;
  height: 10px;
   width: 10px;
  top: 5px;
  left: 5px;
  margin: auto;
	transition: background 0.25s linear;
	-webkit-transition: background 0.25s linear;
}

#first-step input[type=radio]:checked ~ .check {
  border: 5px solid #e8bd40;
}

#first-step input[type=radio]:checked ~ .check::before{
  background: #ad7e11
}

#first-step input[type=radio]:checked ~ label{
  color: #e8bd40;
}

#first-step-button {
	top: 8px;
	position:relative;
    width: 276px;
    margin: auto;
    display: block;
    color: #583807;
    font-size: 18px;
    box-sizing: border-box;
}

#submit-button {
	color: #583807;
    font-size: 18px;
}

#lp-form-wrap {
	height:439px;
    box-shadow: inset 0px 0px 250px 100px rgb(33,50,63);
    position:relative;
    z-index: 1;
    border-radius: 9px;
}

#lp-form-wrap::after {
    content: "";
    background: url("/images/results-bg.jpg") repeat-x center bottom;
    position: absolute;
    z-index: -1;
    opacity: .7;
    top: 0;
	  left: 0;
	  bottom: 0;
	  right: 0;
	 border-radius: 9px;
}

.lp-free-case {
	font-weight: 600;
    font-family: 'franklin gothic', sans-serif;
    font-size:32px;
}

.carousel-inner {
	height:377px;
}

.carousel-indicators {
    bottom: -20px;
    display:none;
}

.carousel-indicators li, .carousel-indicators .active {
	height:7px;
	width:7px;
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #222;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #222;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #222;
}
:-moz-placeholder { /* Firefox 18- */
  color: #222;
}


@media only screen and (max-width:1100px) {
	a#contact-link {
		background: url("<?php echo $trackingimageurltablet; ?>") !important;
	}
	#bisnar-chase {
		background: url('<?php echo $backgroundurltablet; ?>') no-repeat center top !important;
		background-size:auto;
		background-position: center top;
	}

	.lp-content input {
    	width: 100%;
	}
}
@media only screen and (max-width:750px) {
	#bisnar-chase {
		background: url('<?php echo $backgroundurlmobile; ?>') no-repeat center top !important ;
		background-size:auto;
		background-position: center top;
	}
}

@media only screen and (max-width:645px) {
	#contact-link {
		display:none;
	}
}

@media only screen and (max-width:450px) {
	.lp-free-case {
		font-size:26px;
	}

	#first-step #check-label li label {
		font-size:14px;
		padding:0;
	}

	#first-step-button {
		width:100%;
	}


}

</style>
</head>

<body>



    <div id="california-personal-injury">
        <div id="california-personal-injury-attorney"><a href="https://www.bestattorney.com/" id="home-link" title="Bisnar|Chase - California Personal Injury Attorneys"></a><a href="https://www.bestattorney.com/contact.html" id="contact-link" title="Call Today For Your Free Consultation!"></a>
            <div class="clear"></div>
        </div>
    </div>
    <div id="bisnar-and-chase">
        <div id="bisnar-chase">
            <div id="lp-top">
                <div class="lp-content">

                    <h1 class="lp-header">
                        <?php echo $pageh1; ?>
                    </h1>
                    <div class="clear"></div>
                    <h2 class="lp-sub-header">
                        <?php echo $pageh2; ?>
                    </h2>
                    <div id="cta-box">
                        <h2 id="cta" class="lp-h2">Get a Free Case Evaluation!<i id="lp-arrow-right" class="fa fa-long-arrow-right"></i><i id="lp-arrow-down" class="fa fa-long-arrow-down"></i></h2>
                    </div>
                    <div id="mobile-call">
                        <a href="tel:<?php echo $trackingnumber; ?>" id="call-today-button"></a>
                    </div>
                    <div id="lp-form-wrap">
                        <h4 class="lp-free-case" style="color:white;text-align:center;">Do You Have A Case?</h4>
                        <form id="lp-dual-step-form" action="https://www.bestattorney.com/lp/contact.php" method="POST">
                            <p><input type="hidden" name="noparamuri" value="<?php echo $page; ?>"></p>
                            <div id="dual-step-box" class="carousel slide">

                                <ol class="carousel-indicators">
                                    <li data-target="#dual-step-box" data-slide-to="0" class="active"></li>
                                    <li data-target="#dual-step-box" data-slide-to="1"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div id="first-step" class="item active">
                                        <?php if ($twoStepType == "injury") { ?>
                                        <p>How Did the Injury Occur?</p>
                                        <ul id="radio-label">
                                            <li>
                                                <input id="car-radio" type="radio" name="type" value="Car Accident">
                                                <label for="car-radio">Car Accident</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <input id="product-radio" type="radio" name="type" value="Defective Product">
                                                <label for="product-radio">Defective Product</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <input id="premises-radio" type="radio" name="type" value="Premises Negligence">
                                                <label for="premises-radio">Premises Negligence</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <input id="drug-radio" type="radio" name="type" value="Drug/Medical">
                                                <label for="drug-radio">Drug or Medical Device</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <input id="dog-radio" type="radio" name="type" value="Dog Bite">
                                                <label for="dog-radio">Dog Bite</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <input id="other-radio" type="radio" name="type" value="Personal Injury">
                                                <label for="other-radio">Other</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                        </ul>

                                        <?php

                                      } else { ?>
                                        <p>Do You Require Compensation?<br>
                                            <small>Select All That Apply:</small></p>

                                        <ul id="check-label">
                                            <li>
                                                <input id="injuries" type="checkbox" name="checked" value="Injuries">
                                                <label for="injuries">Injuries</label>

                                            </li>
                                            <li>
                                                <input id="hospitalization" type="checkbox" name="checked" value="Hospitalization">
                                                <label for="hospitalization">Hospitalization</label>

                                            </li>
                                            <li>
                                                <input id="property" type="checkbox" name="checked" value="Property Damage">
                                                <label for="property">Property Damage</label>

                                            </li>
                                            <li>
                                                <input id="work" type="checkbox" name="checked" value="Time off work">
                                                <label for="work">Time Off Work</label>

                                            </li>
                                            <li>
                                                <input id="pain" type="checkbox" name="checked" value="Pain & Suffering">
                                                <label for="pain">Pain & Suffering</label>
                                                <!--<div class="check"><div class="inside"></div></div>-->
                                            </li>
                                            <li>
                                                <input id="other" type="checkbox" name="checked" value="Other">
                                                <label for="other">Other</label>
                                                <!--<div class="check"><div class="inside"></div></div>-->
                                            </li>
                                        </ul>
                                        <input id="first-step-button" class="btn btn-warning" value="Do I Have A Case?">
                                        <?php

                                      } ?>
                                    </div>

                                    <div id="second-step" class="item">
                                        <p><input class="iform2" name="realname" autocomplete="name" placeholder="Name:"></p>
                                        <p><input class="iform2" name="email" autocomplete="email" placeholder="Email:"></p>
                                        <p><input class="iform2" name="homephone" autocomplete="tel" placeholder="Phone Number:"></p>
                                        <p><textarea class="tform2" name="comment" rows="3" cols="5" placeholder='Brief Description of Personal Injury Incident:'></textarea></p>
                                        <p><input id="submit-button" type="submit" name="submit" value="Send My Request" class="btn btn-warning"></p>
                                    </div>
                                </div>
                            </div>
                            <p id="contact-form-messages"></p>
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="landing-page-box-1" class="landing-page-box">
                <div class="lp-content">
                    <?php echo $lpbox1; ?>
                </div>
            </div>
            <div id="landing-page-box-2" class="landing-page-box">
                <div class="lp-content">
                    <img id="lp-staff" src="/lp/images/bisnar-chase-staff.jpg">
                    <h2 id="lp2-title" class="lp-h2">
                        <?php echo $lpbox2title; ?>
                    </h2>
                    <div class="clear"></div>
                    <div id="lp-firm-image">
                    </div>
                    <div class="vertical-divider">
                    </div>
                    <div id="lp2-page-content">
                        <?php echo $lpbox2content; ?>
                        <h2 class="lp-h1 centered">Call
                            <?php echo $trackingnumber; ?> Now!</h2>

                    </div>
                    <div class="clear"></div>
                    <div class="horizontal-divider"></div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="landing-page-box-3" class="landing-page-box">
                <div class="lp-content">
                    <table id="testimonials">
                        <tbody>
                            <tr id="testimonial-text">
                                <td>"I knew I made the right choice with Bisnar Chase because they went above and beyond to help."</td>
                                <td>"They really worked hard for a higher payout in my case and kept me informed along the way."</td>
                                <td>"I couldn't have wished or wanted for a better experience here at Bisnar Chase."</td>
                            </tr>
                            <tr id="testimonial-author">
                                <td>- M. Cheng</td>
                                <td>- E. Shaffer</td>
                                <td>- A. Bennet</td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="lp-testimonial-mobile">
                        <div id="lp-testimonial-1" class="lp-testimonial">
                            <p>"I knew I made the right choice with Bisnar Chase because they went above and beyond to help."</p>
                            <p align="right">- M. Cheng</p>
                        </div>
                        <div id="lp-testimonial-2" class="lp-testimonial">
                            <p>"They really worked hard for a higher payout in my case and kept me informed along the way."</p>
                            <p align="right">- E. Shaffer</p>
                        </div>
                        <div id="lp-testimonial-3" class="lp-testimonial">
                            <p>"I couldn't have wished or wanted for a better experience here at Bisnar Chase."<br><br></p>
                            <p align="right">- A. Bennet</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="horizontal-divider"></div>
                    <div id="awards-ratings-reviews" class="scroll-fade">
                        <a href="https://www.bestattorney.com/blog/chase-ritsema-attorney-award/" id="consumer-attorneys" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="super-lawyers" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="newsweek" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="time" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="http://www.bbb.org/orange-county/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-llp-in-newport-beach-ca-100046710" id="bbb" onclick="window.open(this.href,'new','');return false"></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        $ocaddress = "<p id='office-details' itemscope='' itemtype='http://schema.org/Attorney'><span itemprop='name'>Bisnar Chase Personal Injury Attorneys</span><span class='nopc notablet'><br></span> <span class='nomobile'>�</span> 1301 Dove St #120<span class='nopc notablet'><br></span> Newport Beach, CA 92660</span> <span class='nopc'><br></span><span class='notablet nomobile'>�</span> local: (949) 203-3814<span class='nopc notablet'><br></span> <span class='nomobile'>�</span> <a href='https://www.google.com/maps/preview?ll=33.662156,-117.866355&amp;z=16&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=embed&amp;iwloc=lyrftr:m,12551230997004165438' onclick='window.open(this.href,'new','');return false'>Get Directions</a></p>";
        $laaddress = "<p id='office-details' itemscope='' itemtype='http://schema.org/Attorney'><span itemprop='name'>Bisnar Chase Personal Injury Attorneys</span><span class='nopc notablet'><br></span> <span class='nomobile'>�</span> 6701 Center Drive West 14th Fl.<span class='nopc notablet'><br></span> Los Angeles, CA 90045</span> <span class='nopc'><br></span><span class='notablet nomobile'>�</span> local: (323) 238-4683<span class='nopc notablet'><br></span> <span class='nomobile'>�</span> <a href='https://goo.gl/maps/v95mt' onclick='window.open(this.href,'new','');return false'>Get Directions</a></p>";
        $labottomaddress = '6701 Center Drive West, 14th Fl., Los Angeles, CA 90045 - Tel:(323) 238-4683';
        $ocbottomaddress = '1301 Dove St. #120, Newport Beach, CA 92660 - Tel: (949) 203-3814';
        $lacontact = "los-angeles/contact_us.html";
        $occontact = "contact.html";
        if ($addressIsLA) {
          $address = $laaddress;
          $bottomaddress = $labottomaddress;
          $contacturl = $lacontact;
        } else {
          $address = $ocaddress;
          $bottomaddress = $ocbottomaddress;
          $contacturl = $occontact;
        } ?>
        <div id="california-personal-injury-attorneys">
            <div id="california-personal-injury-lawyers">
                <?php echo $address; ?>
                <div id="social-media">
                    <p class="title">Connect</p><a href="https://plus.google.com/+Bestattorney/about" class="plus" title="Find us on Google Plus" onclick="window.open(this.href,'new','');return false"></a><a href="http://www.facebook.com/california.attorney" title="Follow us on Facebook" class="facebook" onclick="window.open(this.href,'new','');return false"></a><a href="https://twitter.com/#!/bisnarchase" class="twitter" title="Follow us on Twitter" onclick="window.open(this.href,'new','');return false"></a><a href="https://www.youtube.com/user/BisnarChaseAttorneys" class="youtube" title="View our YouTube Videos" onclick="window.open(this.href,'new','');return false"></a><a href="http://www.linkedin.com/company/bisnar-chase" class="linkedin" title="Follow us on LinkedIn" onclick="window.open(this.href,'new','');return false"></a>
                </div>
                <div id="sitemap">
                    <p class="title">Sitemap</p>
                    <ul class="ul1">
                        <li><a href="https://www.bestattorney.com/">Home</a></li>
                        <li><a href="https://www.bestattorney.com/about-us/">About Us</a></li>
                        <li><a href="https://www.bestattorney.com/attorneys/bisnar.html">John Bisnar</a></li>
                        <li><a href="https://www.bestattorney.com/attorneys/chase.html">Brian Chase</a></li>
                    </ul>
                    <ul class="ul2">
                        <li><a href="https://www.bestattorney.com/giving-spotlight.html">Giving Back</a></li>
                        <li><a href="https://www.bestattorney.com/lawyer-reviews-ratings.html">Why Hire Us?</a></li>
                        <li><a href="https://www.bestattorney.com/practice_areas.html">Practice Areas</a></li>
                        <li><a href="https://www.bestattorney.com/testimonials.html">Reviews</a></li>
                    </ul>
                    <ul class="ul3">
                        <li><a href="https://www.bestattorney.com/press-releases.html">Press</a></li>
                        <li><a href="https://www.bestattorney.com/testimonials.html">Testimonials</a></li>
                        <li><a href="https://www.bestattorney.com/case_results.html">Case Results</a></li>
                        <li><a href="https://www.bestattorney.com/<?php echo $contacturl; ?>">Contact Us</a></li>
                    </ul>
                    <ul class="ul4">
                        <li><a href="https://www.bestattorney.com/blog/">Read Our Blog</a></li>
                        <li><a href="https://www.bestattorney.com/resources/">Resources</a></li>
                        <li><a href="https://www.bestattorney.com/abogados/">En Espa�ol</a></li>
                        <li><a href="https://www.bestattorney.com/sitemap.html">Sitemap</a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="disclaimer">
                    <p class="title">Copyright &amp; Disclaimer</p>
                    <p><strong>Disclaimer:</strong> The legal information presented at this site should not be construed to be formal legal advice, nor the formation of an attorney-client relationship. Any results set forth here were dependent on the facts of that case and the results will differ from case to case.</p>
                    <p><strong>Bisnar Chase</strong> serves all of California. In addition, we represent clients in other states through our associations with local law firms. Through the local firm, we will be admitted to practice law in their state, pro hac vice, meaning "for this particular occasion." When in our client's interest, we employ the local law firm (at no additional cost to our client) to assist us with routine court appearances and discovery proceedings to more efficiently pursue our client's cause.</p>
                    <p><strong>Copyright � 1999-<script>
                                document.write(new Date().getFullYear())
                            </script> Bisnar Chase Personal Injury Attorneys</strong> - All rights reserved. Location:
                        <?php echo $bottomaddress; ?>
                    </p>
                </div>
            </div>
        </div>
        <div id="top-menu" style="visibility:hidden;">
            <!-- This is just a bandaid so there are no javascript errors -->
            <div id="top-menu-fb"></div>
        </div>
</body>
<footer>


</footer>

</html>

<script>
    var checkedArray = [];
    var type = "n/a";

    $('#dual-step-box').carousel({
        interval: false,
        wrap: false
    });

    $("#first-step input[type='radio']").click(function() {
        setTimeout(function() {
            $("#dual-step-box").carousel('next');
        }, 300);
        $(".carousel-indicators").fadeIn();
        var radioValue = $(this).val();
        $("#second-step textarea").attr("placeholder", "Brief Description of your " + radioValue + " Incident:");
        ga('send', 'event', 'Interaction', 'Two-step form popup click (radio)', '<?php echo ($_SERVER["REQUEST_URI"]); ?>');
    });

    $("#first-step-button").click(function() {

        $("input:checked").each(function() {
            checkedArray.push($(this).val());
        });
        $("#dual-step-box").carousel('next');
        $(".carousel-indicators").fadeIn();
        $("#second-step textarea").attr("placeholder", "Brief Description of your Case:");
        ga('send', 'event', 'Interaction', 'Two-step form popup click (checkbox)', '<?php echo ($_SERVER["REQUEST_URI"]); ?>');
    });

    var form = $('#lp-dual-step-form');
    // Set up an event listener for the contact form.

    form.submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        var realname = $("input[name='realname']").val();
        var homephone = $("input[name='homephone']").val();
        var email = $("input[name='email']").val();
        var comment = $("input[name='comment']").val();
        var noparamuri = $("input[name='noparamuri']").val();

        //Submitting the form via ajax.
        $(".carousel-indicators").hide();
        $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: {
                    realname: realname,
                    homephone: homephone,
                    email: email,
                    comment: comment,
                    type: type,
                    checkedArray: checkedArray,
                    noparamuri: noparamuri
                }
            })

            .done(function(response) {
                $("#contact-form-messages").text(response);
                location.href = "https://www.bestattorney.com/thank-you.html";
            })

            .fail(function(data) {
                // Set the message text.
                if (data.responseText != '') {
                    $('#contact-form-messages').text(data.responseText);
                } else {
                    $('#contact-form-messages').text('We\'re sorry, An error occured and your message could not be sent.');
                }
            });
    });
</script> 