<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/template-files/preload.php"); ?>

<!DOCTYPE html>
<html>

<head>
    <!-- Base Element that is used by MaxCDN to reset all relative paths to the CDN -->
    <!--<base href="http://webfiles.bisnarchaseperso.netdna-cdn.com/"/>-->

    <title>
        <?php echo $title; ?>
    </title>
    <meta name="description" content="<?php echo $meta; ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex">
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif:400,700|Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/style.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $csslocation; ?>" type="text/css" media="all">
    <!--[if lt IE 9]><link rel="stylesheet" href="/personal-injury-attorneys-pc.css" type="text/css" media="all"/><![endif]-->
    <meta name="viewport" content="width=device-width, user-scalable=yes" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script>
        var pageSubject = "<?php echo $pagesubject; ?>";
</script>

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WK3JJ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WK3JJ3');</script>
    <!-- End Google Tag Manager -->
    <!-- Universal Analytics script included, cannot fire from GTM because GTM does not support plugins like autotrack-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44427913-2', {'siteSpeedSampleRate': 100});
  ga('require', 'autotrack');
  ga('send', 'pageview');

</script>
    <script async src='/js/autotrack.js'></script>

    <style>

        #bisnar-chase {
	background: url('<?php echo $backgroundurl; ?>') no-repeat center top !important;
	background-size:1903px;
}

a#contact-link {
	background: url("<?php echo $trackingimageurl; ?>") !important;
}

@media only screen and (max-width:1100px) {
	a#contact-link {
		background: url("<?php echo $trackingimageurltablet; ?>") !important;
	}
	#bisnar-chase {
		background: url('<?php echo $backgroundurltablet; ?>') no-repeat center top !important;
		background-size:auto;
		background-position: center top;
	}
}
@media only screen and (max-width:750px) {
	#bisnar-chase {
		background: url('<?php echo $backgroundurlmobile; ?>') no-repeat center top !important ;
		background-size:auto;
		background-position: center top;
	}
}


</style>
</head>

<body>
    <div id="california-personal-injury">
        <div id="california-personal-injury-attorney"><a href="https://www.bestattorney.com/" id="home-link" title="Bisnar|Chase - California Personal Injury Attorneys"></a><a href="https://www.bestattorney.com/contact.html" id="contact-link" title="Call Today For Your Free Consultation!"></a><a href="/abogados" id="espanol">En Español</a>
            <div class="clear"></div>
        </div>
    </div>
    <div id="bisnar-and-chase">
        <div id="bisnar-chase">
            <div id="lp-top">
                <div class="lp-content">

                    <h1 class="lp-header">
                        <?php echo $pageh1; ?>
                    </h1>
                    <div class="clear"></div>
                    <h2 class="lp-sub-header">
                        <?php echo $pageh2; ?>
                    </h2>
                    <div id="cta-box">
                        <h2 id="cta" class="lp-h2">Evaluation Caso Gratuita!<i id="lp-arrow-right" class="fa fa-long-arrow-right"></i><i id="lp-arrow-down" class="fa fa-long-arrow-down"></i></h2>
                    </div>
                    <div id="mobile-call">
                        <a href="tel:<?php echo $trackingnumber; ?>" id="call-today-button"></a>
                    </div>
                    <div id="lp-form-wrap">
                        <h4 class="lp-free-case" style="color:white;text-align:center;">Evaluation Caso Gratuita!</h4>
                        <form id="case-evaluation-form" action="https://www.bestattorney.com/template-files/contact-files/contact.php" method="POST">
                            <p><input type="hidden" name="formtest" value="test"></p>
                            <div id="form-flex">
                                <p><input class="iform2" name="realname" value="Nombre:" autocomplete="name" onfocus="if(this.value == 'Nombre:') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Nombre:';}"></p>
                                <p><input class="iform2" name="email" value="Correo Electronico:" autocomplete="email" onfocus="if(this.value == 'Correo Electronico:') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Correo Electronico:';}"></p>
                                <p><input class="iform2" name="homephone" value="Telefono:" autocomplete="tel" onfocus="if(this.value == 'Telefono:') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Telefono:';}"></p>
                            </div>
                            <p><textarea class="tform2" name="comment" rows="3" cols="5" onfocus="if(this.value == '<?php echo $caseevaluationtext; ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php echo $caseevaluationtext; ?>';}"><?php echo $caseevaluationtext; ?></textarea></p>
                            <p><input type="submit" name="submit" value="" class="subform2" id="form-submit-button"></p>
                            <p id="contact-form-messages"></p>
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="landing-page-box-1" class="landing-page-box">
                <div class="lp-content">
                    <?php echo $lpbox1; ?>
                </div>
            </div>
            <div id="landing-page-box-2" class="landing-page-box">
                <div class="lp-content">
                    <img id="lp-staff" src="/lp/images/bisnar-chase-staff.jpg">
                    <h2 id="lp2-title" class="lp-h2">
                        <?php echo $lpbox2title; ?>
                    </h2>
                    <div class="clear"></div>
                    <div style="float:left;
							width:350px;
							height:465px;
							background-image: url('/lp/images/bisnar-chase-espanol.jpg');">
                    </div>
                    <div class="vertical-divider">
                    </div>
                    <div id="lp2-page-content">
                        <?php echo $lpbox2content; ?>
                        <h2 class="lp-h1 centered">Llame
                            <?php echo $trackingnumber; ?> Ahora!</h2>

                    </div>
                    <div class="clear"></div>
                    <div class="horizontal-divider"></div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="landing-page-box-3" class="landing-page-box">
                <div class="lp-content">
                    <table id="testimonials">
                        <tbody>
                            <tr id="testimonial-text">
                                <td>"Yo sabía que tomé la decisión correcta con Bisnar Chase porque fueron por encima de todo para ayudar."</td>
                                <td>"Ellos realmente trabajaron duro por un pago más alto en mi caso y me mantuvieron informado a lo largo del camino."</td>
                                <td>"Yo no podría haber deseado o querido una mejor experiencia aquí en Bisnar Chase."</td>
                            </tr>
                            <tr id="testimonial-author">
                                <td>- M. Cheng</td>
                                <td>- E. Shaffer</td>
                                <td>- A. Bennet</td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="lp-testimonial-mobile">
                        <div id="lp-testimonial-1" class="lp-testimonial">
                            <p>"Yo sabía que tomé la decisión correcta con Bisnar Chase porque fueron por encima de todo para ayudar."</p>
                            <p align="right">- M. Cheng</p>
                        </div>
                        <div id="lp-testimonial-2" class="lp-testimonial">
                            <p>"Ellos realmente trabajaron duro por un pago más alto en mi caso y me mantuvieron informado a lo largo del camino."</p>
                            <p align="right">- E. Shaffer</p>
                        </div>
                        <div id="lp-testimonial-3" class="lp-testimonial">
                            <p>"Yo no podría haber deseado o querido una mejor experiencia aquí en Bisnar Chase."<br><br></p>
                            <p align="right">- A. Bennet</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="horizontal-divider"></div>
                    <div id="awards-ratings-reviews" class="scroll-fade">
                        <a href="https://www.bestattorney.com/blog/chase-ritsema-attorney-award/" id="consumer-attorneys" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="super-lawyers" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="newsweek" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="https://www.bestattorney.com/lawyer-reviews-ratings.html" id="time" onclick="window.open(this.href,'new','');return false"></a>
                        <a href="http://www.bbb.org/orange-county/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-llp-in-newport-beach-ca-100046710" id="bbb" onclick="window.open(this.href,'new','');return false"></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        $ocaddress = "<p id='office-details' itemscope='' itemtype='http://schema.org/Attorney'><span itemprop='name'>Bisnar Chase Personal Injury Attorneys</span><span class='nopc notablet'><br></span> <span class='nomobile'>•</span> 1301 Dove St #120<span class='nopc notablet'><br></span> Newport Beach, CA 92660</span> <span class='nopc'><br></span><span class='notablet nomobile'>•</span> local: (949) 203-3814<span class='nopc notablet'><br></span> <span class='nomobile'>•</span> <a href='https://www.google.com/maps/preview?ll=33.662156,-117.866355&amp;z=16&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=embed&amp;iwloc=lyrftr:m,12551230997004165438' onclick='window.open(this.href,'new','');return false'>Direciones</a></p>";
        $laaddress = "<p id='office-details' itemscope='' itemtype='http://schema.org/Attorney'><span itemprop='name'>Bisnar Chase Personal Injury Attorneys</span><span class='nopc notablet'><br></span> <span class='nomobile'>•</span> 6701 Center Drive West 14th Fl.<span class='nopc notablet'><br></span> Los Angeles, CA 90045</span> <span class='nopc'><br></span><span class='notablet nomobile'>•</span> local: (323) 238-4683<span class='nopc notablet'><br></span> <span class='nomobile'>•</span> <a href='https://goo.gl/maps/v95mt' onclick='window.open(this.href,'new','');return false'>Direciones</a></p>";
        $labottomaddress = '6701 Center Drive West, 14th Fl., Los Angeles, CA 90045 - Tel:(323) 238-4683';
        $ocbottomaddress = '1301 Dove St. #120, Newport Beach, CA 92660 - Tel: (949) 203-3814';
        $lacontact = "los-angeles/contact_us.html";
        $occontact = "contact.html";
        if ($addressIsLA) {
          $address = $laaddress;
          $bottomaddress = $labottomaddress;
          $contacturl = $lacontact;
        } else {
          $address = $ocaddress;
          $bottomaddress = $ocbottomaddress;
          $contacturl = $occontact;
        } ?>
        <footer id="footer">
            <div id="footer-inner">
                <?php echo $address; ?>
                <div id="social-media">
                    <p class="title">Connect</p><a href="https://plus.google.com/+Bestattorney/about" class="plus" title="Find us on Google Plus" onclick="window.open(this.href,'new','');return false"></a><a href="http://www.facebook.com/california.attorney" title="Follow us on Facebook" class="facebook" onclick="window.open(this.href,'new','');return false"></a><a href="https://twitter.com/#!/bisnarchase" class="twitter" title="Follow us on Twitter" onclick="window.open(this.href,'new','');return false"></a><a href="https://www.youtube.com/user/BisnarChaseAttorneys" class="youtube" title="View our YouTube Videos" onclick="window.open(this.href,'new','');return false"></a><a href="http://www.linkedin.com/company/bisnar-chase" class="linkedin" title="Follow us on LinkedIn" onclick="window.open(this.href,'new','');return false"></a>
                </div>
                <div id="sitemap">
                    <p class="title">Sitemap</p>
                    <ul class="ul1">
                        <li><a href="https://www.bestattorney.com/">Home</a></li>
                        <li><a href="https://www.bestattorney.com/about-us/">About Us</a></li>
                        <li><a href="https://www.bestattorney.com/attorneys/bisnar.html">John Bisnar</a></li>
                        <li><a href="https://www.bestattorney.com/attorneys/chase.html">Brian Chase</a></li>
                    </ul>
                    <ul class="ul2">
                        <li><a href="https://www.bestattorney.com/giving-spotlight.html">Giving Back</a></li>
                        <li><a href="https://www.bestattorney.com/lawyer-reviews-ratings.html">Why Hire Us?</a></li>
                        <li><a href="https://www.bestattorney.com/practice_areas.html">Practice Areas</a></li>
                        <li><a href="https://www.bestattorney.com/testimonials.html">Reviews</a></li>
                    </ul>
                    <ul class="ul3">
                        <li><a href="https://www.bestattorney.com/press-releases.html">Press</a></li>
                        <li><a href="https://www.bestattorney.com/testimonials.html">Testimonials</a></li>
                        <li><a href="https://www.bestattorney.com/case_results.html">Case Results</a></li>
                        <li><a href="https://www.bestattorney.com/<?php echo $contacturl; ?>">Contact Us</a></li>
                    </ul>
                    <ul class="ul4">
                        <li><strong><a href="https://www.bestattorney.com/abogados/">En Español</a></strong></li>

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="disclaimer">
                    <p class="title">Copyright &amp; Disclaimer</p>
                    <p><strong>Advertencia:</strong> La información legal presentada en este sitio no debe interpretarse como asesoramiento legal formal, ni la creación de una relación abogado-cliente. Cualquier resultado mostrado aqui depende de los hechos de ese caso y los resultados serán diferentes de un caso a otro.</p>
                    <p><strong>Bisnar Chase</strong> sirve a toda California. Además, representamos a clientes en otros estados a través de nuestras asociaciones con firmas de abogados locales. A través de la firma local, seremos admitidos a ejercer leyes en su estado, pro hac vice, que significa "para esta ocasión especial." Cuando está en el interés de nuestros clientes, contamos con la firma de abogados local (sin costo adicional para nuestros clientes) para ayudarnos a comparecer ante la corte en asuntos de rutina y los procedimientos de investigación para perseguir más eficazmente la causa de nuestro cliente.</p>
                    <p><strong>Copyright © 1999-<script>
                                document.write(new Date().getFullYear())
                            </script> Bisnar Chase Personal Injury Attorneys</strong> - Reservados todos los derechos.
                        <?php echo $bottomaddress; ?>
                    </p>
                </div>
            </div>
        </footer>
        <div id="top-menu" style="visibility:hidden;">
            <div id="top-menu-fb"></div>
        </div>
</body>
<span id="scripts">
    <script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="/js/breakpoints.js"></script>
    <script type="text/javascript" src="/js/scripts.js"></script>
    <!-- Google Code for Ad Leads (Call or Form) Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
chosen link or button. -->
    <script type="text/javascript">
        /* <![CDATA[ */
        goog_snippet_vars = function() {
            var w = window;
            w.google_conversion_id = 1024246018;
            w.google_conversion_label = "VyoOCP3yglsQgoKz6AM";
            w.google_remarketing_only = false;
        }
        // DO NOT CHANGE THE CODE BELOW.
        goog_report_conversion = function(url) {
            goog_snippet_vars();
            window.google_conversion_format = "3";
            window.google_is_call = true;
            var opt = new Object();
            opt.onload_callback = function() {
                if (typeof(url) != 'undefined') {
                    window.location = url;
                }
            }
            var conv_handler = window['google_trackConversion'];
            if (typeof(conv_handler) == 'function') {
                conv_handler(opt);
            }
        }
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js">
    </script>
</span>

</html>