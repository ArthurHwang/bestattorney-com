<?php
// Folder Options for /defective-products

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "productdefect", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Defective Product Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Failure to Warn" => "/defective-products/failure-to-warn.html",
        "Foreign Manufacturers" => "/defective-products/foreign-manufacturers.html",
		"Mass Torts" => "/defective-products/mass-tort-lawyer.html",
        "Household Appliances" => "/defective-products/household-appliances.html",
		"E-Cig Injuries" => "/defective-products/e-cig-injury-lawyer.html",
				"Talcum Powder / Ovarian Cancer" => "/pharmaceutical-litigation/talcum-powder.html",
				"Allergan Biocell Breast Implants" => "/defective-products/allergan-biocell-litigation.html",
				"Hoverboard Explosions" => "/orange-county/hoverboard-injury-lawyers.html",
				"Drone Accidents" => "/defective-products/drone-crash-liability-lawyer.html",
        "Li-Ion Battery Explosions" => "/defective-products/lithium-ion-battery-explosion-lawyer.html",
		"Roundup Weed Killer" => "/misc/roundup-weed-killer-cancer-lawyers.html",
		"Defective Products Home" => "/defective-products/",
	),

	"sidebarContentTitle" => "Product Liability Attorneys",

	"sidebarContentHtml" => "
	<ul class='bpoint'>
		<li>Dangerous and defective products cause thousands of injuries to people every year. When dealing with product liability, the rules are different from basic injury law. It's often times easier to collect.</li>
    	<li>Types of defects: Marketing, Manufacturing and Design defects.</li>
    	<li>If you or a loved one have suffered a product liability injury, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['top'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Brian Chase on Biocell Breast Implants', '/images/2019-assets/mqdefault.jpg', '28k5yi7GtBM'],
		['Brian Chase on Ecigs', '/images/2019-assets/mqdefault.jpg', 'NFQxF6zslhI']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>