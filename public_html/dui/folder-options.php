<?php
// Folder Options for /dui

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "accident", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "DUI Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Personal Rights for Victims of Drunk Driving" => "/dui/drunk-driving-handbook.html",
        "Drunk Driving - Is It Murder?" => "/dui/drunk-driving-murder.html",
        "Drunk Driving Technology" => "/dui/drunk-driving-technology.html",
        "End of Drunk Driving" => "/dui/end-of-drunk-driving.html",
        "Recovering Personal Damages" => "/dui/recovering-personal-damages.html",
        "Victim Rights" => "/dui/victims-rights.html",
        "DUI Home" => "/dui/",
	),

	"sidebarContentTitle" => "DUI Accident Victim Lawyers",

	"sidebarContentHtml" => "
	<ul class='bpoint'>
        <li>If you or a loved one have suffered as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. <li>Our Skilled DUI attorneys have over four decades handling DUI injury cases in California and have a 99% success rate. Get the compensation you deserve from experienced trial lawyers.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['top'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [0, array(21)], // see getReviews() in functions.php

	"videosArray" => [
		['Bisnar Chase Takes on Drunk Driving Cases', '', 'vpM-_8G4GzE'],
		['Can I Obtain Punitive Damages In My DUI Case?', '', 'YX9TpQxVGKo']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
