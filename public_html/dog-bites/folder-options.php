<?php
// Folder Options for /dog-bites

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "dogbite", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Dog Bite Location Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Newport Beach" => "/newport-beach/dog-bites.html",
        "Irvine" => "/irvine/dog-bites.html",
        "Fullerton" => "/fullerton/dog-bites.html",
        "Los Angeles" => "/los-angeles/dog-bites.html",
        "Costa Mesa" => "/costa-mesa/dog-bites.html",
        "Santa Ana" => "/santa-ana/dog-bites.html",
        "Fountain Valley" => "/fountain-valley/dog-bites.html",
        "San Bernardino" => "/san-bernardino/dog-bites.html",
        "Riverside" => "/riverside/dog-bites.html",
        "Anaheim" => "/anaheim/dog-bites.html",
        "Garden Grove" => "/garden-grove/dog-bites.html"
	),

	"sidebarContentTitle" => "Dog Bite Resources",

	"sidebarContentHtml" => "
	<div id='dogbites-sidebar'>
	<ul class='sidebar-buttons'>	
        <li><a href='https://www.bestattorney.com/dog-bites/dangerous-dog-breeds.html'>Dangerous Breeds</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/factors-causing-canine-aggression.html'>Factors Causing Canine Aggression</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/first-aid-dog-bites.html'>First Aid for Dog Bites</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/injury-infections-dog-bites.html'>Injury and Infections from Dog Bites</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/prevention-tips.html'>Dog Bite Prevention</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/statistics.html'>Dog Bite Statistics</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/victim-rights.html'>Victim Rights</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/why-dogs-bite.html'>Why Dogs Bite</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/california-dog-bite-laws.html'>California Dog Bite Laws</a></li>
        <li><a href='https://www.bestattorney.com/dog-bites/criminal-penalties.html'>Criminal Penalties</a></li>
				<li><a href='https://www.bestattorney.com/dog-bites/insurance-ban-breeds.html'>Insurance Breed Bans</a></li>
		</ul>
		</div>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['top'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['John Bisnar Discusses Dog Attack Lawsuit Claims', '/images/sb-video-aGlklcSADWA.jpg', 'aGlklcSADWA'],
		['Dog Bite Liability - Who is at Fault if Your Dog Bites Someone?', '/images/sb-video-_LQ8M7WPjik.jpg', '_LQ8M7WPjik']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.

	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>