
<?php
// Folder Options for /bicycle-accidents/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "bicycleaccident";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Bicycle Accident Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Bicycle Accident Lawsuits" => "/bicycle-accidents/lawsuits.html",
        "Orange County Bicycle Accidents" => "/orange-county/bicycle-accidents.html",
        "Los Angeles Bicycle Accidents" => "/los-angeles/bicycle-accidents.html",
        "Newport Beach Bicycle Accidents" => "/newport-beach/bicycle-accidents.html",
        "Bicycle FAQs" => "/bicycle-accidents/bicycle-faqs.html",
        "Bicycle Accident Brain Injuries" => "/bicycle-accidents/bicycle-accident-brain-injury.html",
        "Roadway Construction Accidents" => "/bicycle-accidents/roadway-construction.html",
        "Electric Bicycle Accidents" => 
		"/bicycle-accidents/electric-bicycle-accident-lawyer.html",
        "Bicycle Accident Wrongful Death" => 
		"/bicycle-accidents/wrongful-death.html",
        "Bicycle Accidents Home" => "/bicycle-accidents/",
    ),

	"sidebarContentTitle" => "Bicycle Accident Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
                            	<li>If you or a loved one have suffered in a bicycle accident as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                            </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['search','bike biking bicycle bike,'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['What is my Case Worth?', '', 'IuD-S72PMxk'],
		['Do I Owe Money If My Case Is Lost?', '', 'D3c_pxDeswg']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>