<?php
// Folder Options for /motorcycle-accidents

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "motorcycleaccident", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Motorcycle Accidents Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
        "Dangerous Highways" => "/motorcycle-accidents/dangerous-highways.html",
        "Dangerous Roadways" => "/motorcycle-accidents/dangerous-roadways.html",
        "Obstructed Roadways" => "/motorcycle-accidents/obstructed-roadways.html",
        "Safety Tips" => "/motorcycle-accidents/safety-tips.html",
        "Van Collisions" => "/motorcycle-accidents/van-collisions.html",
        "Motorcycle Accidents Home" => "/motorcycle-accidents/",
    ),

	"sidebarContentTitle" => "Motorcycle Accident Attorneys:",

	"sidebarContentHtml" => "<ul class='bpoint'>
                            <!--<li>BLUE BOX CONTENT</li>-->
                            <li>If you or a loved one have been injured in a motorcycle accident, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today!</li>
                            <li><strong><a href='https://www.bestattorney.com/car-accidents/rear-end-accident-lawyer.html'>Rear-End Accidents:</a></strong> Many of our motorcycle accident victims have come to us because they were rear-ended, most of the time at a stop light. Even rear-end crash at low speeds can result in massive injuries for a rider. <a href='https://www.bestattorney.com/case-results/?id=24+33'>See some of these case results.</a></li>
                            <li><strong><a href='https://www.bestattorney.com/car-accidents/t-bone-car-accident-lawyer.html'>T-bone Accidents:</a></strong> A lot of motorcyclists are T-boned because drivers do not pay attention and don't see a motorcyclist until it is too late. These accidents have a high potential to result in catastrophic injury.</li>
                        </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["Compensation for Motorcycle Accident Injuries", "/images/sb-video-wcANLLjVkVY.jpg", "wcANLLjVkVY"],
		["Causes of Motorcycle Accidents", "/images/sb-video-rRijyD6aP-E.jpg", "rRijyD6aP-E"],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>