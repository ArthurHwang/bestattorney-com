import { submitContactForm } from '../js/form-submissions';

// Main Contact.html Page form
if (document.getElementById('contact-form-insert')) {
    submitContactForm("cn-form-wrap", "contact-form-messages", false);
}

// Location Contact Page Form (LA/SB/Riverside)
if (document.getElementById('contact-form-wrap')) {
    submitContactForm("case-evaluation-form", "contact-form-messages", false);
}
