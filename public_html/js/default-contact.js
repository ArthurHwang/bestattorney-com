import { submitContactForm } from '../js/form-submissions';

// Default Sidebar Form
if (document.getElementById('free-case-evaluation')) {
    submitContactForm("case-evaluation-form", "contact-form-messages", false);
}

// Tab Dropdown Form
if (document.getElementById('consultation-form-wrap')) {
    submitContactForm("drop-evaluation-form", "drop-form-messages", false);
}

// Form for Motor Vehicle Code Pages
if (document.getElementById('contact-form-insert-mvc')) {
    submitContactForm("case-evaluation-form", "contact-form-messages", false);
}

// Form for Spanish Contact Page
if (document.getElementById('contact-form-insert-espanol')) {
    submitContactForm("cn-form-wrap", "contact-form-messages", true);
}

// Default Spanish Sidebar Form
if (document.getElementById('contactenos')) {
    submitContactForm("case-evaluation-form", "contact-form-messages", true);
}



