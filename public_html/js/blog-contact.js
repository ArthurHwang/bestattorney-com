import { submitContactForm } from '../js/form-submissions';

// Default Sidebar Form
if (document.getElementById('free-case-evaluation')) {
    submitContactForm("case-evaluation-form", "contact-form-messages", false);
}

// Tab Dropdown Form
if (document.getElementById('consultation-form-wrap')) {
    submitContactForm("drop-evaluation-form", "drop-form-messages", false);
}



