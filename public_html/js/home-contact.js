import { submitContactForm } from '../js/form-submissions';

let isSpanish = false;
if (document.body.id == 'template-espanol') {
    isSpanish = true;
}

submitContactForm("case-evaluation-form", "contact-form-messages", isSpanish);