$(function() {
  if (document.getElementById('contact-us')) {
    var e = document.getElementById('contact-us')
    e.innerHTML =
      '<p class="title">Free Case Evaluation</p><p class="subtext">Our full time staff is ready to evaluate your case submission and will respond in a timely manner.</p><form id="case-evaluation-form" action="https://www.bestattorney.com/template-files/contact-files/contact.php" method="POST" ><p><input type="hidden" name="noparamuri" value="/"></p><div id="contact-lt"><p><input class="form-hp" name="form-hp" value="" autocomplete="off" placeholder="Have you received medical attention?"><input class="iform" name="realname" placeholder="Name:"></p><p><input class="iform" name="homephone" placeholder="Phone Number:"></p><p><input class="iform" name="email" placeholder="Email Address:"></p>				</div><div id="contact-rt"><p><textarea class="tform" name="comment" rows="3" cols="5" placeholder="' +
      contactText +
      '"></textarea></p></div><div class="clear"></div><p><input type="submit" name="submit" value="" class="subform" ></p></form><p id="contact-form-messages"></p>'
  }

  $('#featured-container')
    .on('jcarousel:create jcarousel:reload', function() {
      var element = $(this),
        width = element.innerWidth()

      element.jcarousel('items').css('width', width + 'px')
    })
    .jcarousel({
      wrap: 'circular',
      list: '#featured-list'
    })
    .jcarouselAutoscroll({
      interval: 6000,
      target: '+=1',
      autostart: true
    })

  $('.featured-prev').on('click', function() {
    $('#featured-container')
      .jcarousel('scroll', '-=1')
      .jcarouselAutoscroll('stop')
  })

  $('.featured-next').on('click', function() {
    $('#featured-container')
      .jcarousel('scroll', '+=1')
      .jcarouselAutoscroll('stop')
  })

  $('#pagination')
    .on('jcarouselpagination:active', 'li', function() {
      $(this).addClass('active')
    })
    .on('jcarouselpagination:inactive', 'li', function() {
      $(this).removeClass('active')
    })
    .jcarouselPagination({
      item: function(page) {
        return (
          '<li id="nav-fragment-' +
          parseInt(page) +
          '" class="nav-fragment"><a  id="frag-' +
          page +
          '" onClick="return false;" data-event-category="Button" data-event-action="Carousel Pane ' +
          page +
          ' Navigation Click" data-event-label="/"></a></li>'
        )
      }
    })

  $('.nav-fragment').on('click', function() {
    $('#featured-container').jcarouselAutoscroll('stop')
  })

  $('#homepage-video-overlay-play').on('click', function() {
    document.getElementById('home-brand-video').pause()
    $('#featured-container').jcarouselAutoscroll('stop')
  })

  /* ========================================================
	jQuery for featured video slider
	======================================================== */

  $('#featured-videos')
    .on('jcarousel:create jcarousel:reload', function() {
      var element = $(this),
        width = element.innerWidth()

      element.jcarousel('items').css('width', width + 'px')
    })
    .on('jcarousel:scroll', function(event, carousel, target, animate) {
      var currentIndex = $('#featured-videos')
        .jcarousel('fullyvisible')
        .data('index')
      if (target == '+=1') {
        if (currentIndex === 4) {
          currentIndex = 1
        } else {
          currentIndex++
        }
      } else if (target == '-=1') {
        if (currentIndex === 1) {
          currentIndex = 4
        } else {
          currentIndex--
        }
      }
      showTitles(currentIndex)
    })
    .jcarousel({
      wrap: 'circular',
      list: '#featured-videos-list'
    })
    .jcarouselAutoscroll({
      interval: 7000,
      target: '+=1',
      autostart: true
    })

  $('#featured-videos-prev').on('click', function() {
    $('#featured-videos')
      .jcarousel('scroll', '-=1')
      .jcarouselAutoscroll('stop')
  })

  $('#featured-videos-next').on('click', function() {
    $('#featured-videos')
      .jcarousel('scroll', '+=1')
      .jcarouselAutoscroll('stop')
  })

  function fadeTitles() {
    $('#what-our-clients-say p').fadeOut(200)
  }

  function showTitles(currentIndex) {
    const slide = $('#featured-videos-' + currentIndex)
    const title = slide.data('title')
    const subtitle = slide.data('subtitle')
    const link = slide.data('link')

    $('#what-our-clients-say .title').fadeOut(200, function() {
      $(this)
        .html("<a href='" + link + "'>" + title + '</a>')
        .fadeIn(200)
    })
    $('#what-our-clients-say .featured-subtitle').fadeOut(200, function() {
      $(this)
        .html("<a href='" + link + "'>" + subtitle + '</a>')
        .fadeIn(200)
    })
  }

  const go = document.getElementById('go')
  if (go) {
    go.addEventListener('click', function(e) {
      e.preventDefault()
      const paselectJump = document.getElementById('paselect')
      const url = paselectJump.value
      window.open(url, '_self')
    })
  }
})

window.onload = loadResources

// loadResources();

window.addEventListener('resize', checkResize)

function elem(id) {
  return document.getElementById(id)
}

var trueResize

function checkResize() {
  if (trueResize) {
    clearTimeout(trueResize)
  }

  trueResize = setTimeout(function() {
    loadResources()
    $('#featured-container').jcarousel('reload')
  }, 300)
}

function loadResources() {
  const isSpanish = document.getElementById('template-espanol')
  // console.log("resources Loaded");
  if (window.outerWidth > 640) {
    // Do this for Tablet and Desktop
    const video = elem('home-brand-video')
    if (video && !video.children[0]) {
      const source = document.createElement('source')
      source.src = 'images/video/BC-Brand-Video.webm'
      source.type = 'video/webm'
      const source2 = document.createElement('source')
      source2.src = 'images/video/BC-Brand-Video.mp4'
      source2.type = 'video/mp4'
      video.appendChild(source)
      video.appendChild(source2)
    }

    const paMedicalDevices = elem('pa-medical-devices-img')

    if (isSpanish) {
      // Is spanish homepage
      paMedicalDevices.src = '/images/pa-ind-nursing-abuse.jpg'
    } else {
      // Is Homepage
    }
  }

  if (window.outerWidth > 1100) {
    // Only load these resources on desktop
    if (!isSpanish) {
      // difference.style.background = "#1a0d0b url(/images/bisnar-chase-difference.jpg) no-repeat center top";
    }
  } else if (window.outerWidth > 640) {
    // only tablet

    if (!isSpanish) {
      // difference.style.background = "#1a0d0b url(/images/bisnar-chase-difference-tablet.jpg) no-repeat center top";
    }
  } else {
    //only mobile
    const featuredMobile = elem('featured-mobile-wrap')
    featuredMobile.style.backgroundImage = 'url(/images/experience.jpg)'
  }
}
