if (window.addEventListener)
  window.addEventListener('load', lazyLoadAll, false);
else if (window.attachEvent) window.attachEvent('onload', lazyLoadAll);
else window.onload = lazyLoadAll;

function lazyLoadAll() {
  lazyLoadImgs();
  lazyLoadMaps();
  lazyLoadYoutube();
}

/**
 * Finds all elements with a data-src attribute
 * If it's an <img>, load as src. Else load in background-image.
 * If browser supports webp, load the webp file, else load default.
 */

function lazyLoadImgs() {
  const images = document.querySelectorAll('[data-src]');
  images.forEach.call(images, (image) => {
    image.addEventListener(
      'error',
      function () {
        image.src = image.dataset.src;
        return;
      },
      {
        once: true,
      }
    );

    const imageUrl = image.dataset.src;
    if (image.nodeName === 'IMG') {
      if (Modernizr.webp && Modernizr.webplossless) {
        image.src = imageUrl.slice(0, -3) + 'webp';
      } else {
        image.src = imageUrl;
      }
    } else {
      if (Modernizr.webp && Modernizr.webplossless) {
        checkExists(imageUrl.slice(0, -3) + 'webp')
          .then(() => {
            image.style.backgroundImage =
              "url('" + imageUrl.slice(0, -3) + 'webp' + "')";
          })
          .catch(() => {
            image.style.backgroundImage = "url('" + imageUrl + "')";
          });
      } else {
        image.style.backgroundImage = "url('" + imageUrl + "')";
      }
    }
  });
}

/**
 * Finds all elements with a 'data-map' attribute
 * Creates a iframe as a child with the src set as the data-map attr.
 */
function lazyLoadMaps() {
  const maps = document.querySelectorAll('[data-map]');

  maps.forEach.call(maps, (map) => {
    map.innerHTML = `    
      <iframe allowfullscreen src=${map.dataset.map}></iframe> 
    `;
    Object.entries(map.dataset).forEach((attr) => {
      map.children[0].setAttribute(attr[0], attr[1]);
      map.children[0].setAttribute('border', '0');
      map.children[0].setAttribute('allowfullscreen', true);
      map.children[0].setAttribute('frameborder', '0');
    });
    map.setAttribute('class', 'google-maps');
  });
}

/**
 * Finds all elements with a 'data-youtube' attribute
 * Creates a iframe as a child with the src set as the data-youtube attr.
 */
function lazyLoadYoutube() {
  const videos = document.querySelectorAll('[data-youtube]');

  videos.forEach.call(videos, (video) => {
    video.innerHTML = `    
      <iframe allowfullscreen src=${video.dataset.youtube}></iframe> 
    `;
    Object.entries(video.dataset).forEach((attr) => {
      video.children[0].setAttribute(attr[0], attr[1]);
      video.children[0].setAttribute('border', '0');
      video.children[0].setAttribute('allowfullscreen', true);
      video.children[0].setAttribute('frameborder', '0');
    });
    video.setAttribute('class', 'youtube-video');
  });
}

function checkExists(url) {
  return new Promise(function (res, rej) {
    $.ajax({
      url: url,
      method: 'HEAD',
      error: () => {
        rej();
      },
      success: () => {
        res();
      },
    });
  });
}
