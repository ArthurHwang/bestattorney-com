// Basic Variables - Email Allowed, is spanish, etc.

export function submitContactForm(formId, formMessagesId, isSpanish) {
  let emailAllowed = true
  let form = document.getElementById(formId)
  let formMessages = document.getElementById(formMessagesId)
  $(form).submit(function(e) {
    e.preventDefault()
    if (emailAllowed == true) {
      emailAllowed = false
      var formData = $(form).serialize()
      console.log('Form Submitted. Serialized info is: ')
      console.log(formData)

      $.ajax({
        type: 'POST',
        url: isSpanish ? 'https://www.bestattorney.com/template-files/contact-files/contactenos.php' : 'https://www.bestattorney.com/template-files/contact-files/contact.php',
        data: formData
      })
        .done(function(response) {
          // console.log(response)
          $(formMessages).text('Form Submitted!')
          location.href = isSpanish ? 'https://www.bestattorney.com/abogados/thank-you-espanol.html' : 'https://www.bestattorney.com/thank-you.html'
        })
        .fail(function(data) {
          emailAllowed = true
          if (data.responseText && data.responseText !== '') {
            $(formMessages).text(data.responseText)
          } else {
            $(formMessages).text(isSpanish ? 'Lo siento, no podemos enviar su mensaje' : "We're sorry, An error occured and your message could not be sent.")
          }
        })
    }
  })
}
