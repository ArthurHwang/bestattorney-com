////////////////////////////////////////// Carousels //////////////////////////////////////////


// Vertical Results Carousel


$("#results-wrap").jcarousel({
    vertical: true,
    animation: {
        duration: 300,
    },
    transitions: {
        transforms: true,
        transforms3d: true,
        easing: 'ease'
    }
});

$("#results-prev").on('click', function () {
    $("#results-wrap").jcarousel('scroll', '-=1');
});
$("#results-next").on('click', function () {
    $("#results-wrap").jcarousel('scroll', '+=1');
});
