/*! jCarousel - v0.3.5 - 2017-03-15
 * http://sorgalla.com/jcarousel/
 * Copyright (c) 2006-2017 Jan Sorgalla; Licensed MIT */
!(function(a) {
  'use strict'
  a.jCarousel.plugin('jcarouselControl', {
    _options: { target: '+=1', event: 'click', method: 'scroll' },
    _active: null,
    _init: function() {
      ;(this.onDestroy = a.proxy(function() {
        this._destroy(), this.carousel().one('jcarousel:createend', a.proxy(this._create, this))
      }, this)),
        (this.onReload = a.proxy(this._reload, this)),
        (this.onEvent = a.proxy(function(b) {
          b.preventDefault()
          var c = this.options('method')
          a.isFunction(c) ? c.call(this) : this.carousel().jcarousel(this.options('method'), this.options('target'))
        }, this))
    },
    _create: function() {
      this.carousel()
        .one('jcarousel:destroy', this.onDestroy)
        .on('jcarousel:reloadend jcarousel:scrollend', this.onReload),
        this._element.on(this.options('event') + '.jcarouselcontrol', this.onEvent),
        this._reload()
    },
    _destroy: function() {
      this._element.off('.jcarouselcontrol', this.onEvent),
        this.carousel()
          .off('jcarousel:destroy', this.onDestroy)
          .off('jcarousel:reloadend jcarousel:scrollend', this.onReload)
    },
    _reload: function() {
      var b,
        c = a.jCarousel.parseTarget(this.options('target')),
        d = this.carousel()
      if (c.relative) b = d.jcarousel(c.target > 0 ? 'hasNext' : 'hasPrev')
      else {
        var e = 'object' != typeof c.target ? d.jcarousel('items').eq(c.target) : c.target
        b = d.jcarousel('target').index(e) >= 0
      }
      return this._active !== b && (this._trigger(b ? 'active' : 'inactive'), (this._active = b)), this
    }
  })
})(jQuery)
