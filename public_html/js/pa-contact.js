import { submitContactForm } from '../js/form-submissions';

// Primary pa-geo contact form
if (document.getElementById('contact-form-pa')) {
    submitContactForm("pa-case-evaluation-form", "pa-contact-form-messages", false);
}

// Tab Dropdown Form
if (document.getElementById('consultation-form-wrap')) {
    submitContactForm("drop-evaluation-form", "drop-form-messages", false);
}


