$(function() {

//////////////////////////////// sticky menu script ////////////////////////////////

////////// navbar position

	// our function that decides weather the navigation bar should have "fixed" css position or not.
	function sticky_navigation() {
		var scroll_top = $(window).scrollTop();
	    var title_height = $('#title-head').height();
		// our current vertical position from the top
		// if we've scrolled more than the navigation, change its position to fixed to stick to top,
		// otherwise change it back to relative
		if (scroll_top > title_height) {
			$('#navbar').addClass('frozen-nav');
		} else {
			$('#navbar').removeClass('frozen-nav');
		}
	};

	// run our function on load
	sticky_navigation();

	// and run it again every time you scroll
	$(window).scroll(function() {
		sticky_navigation();
	});


// toggle index extra content
	function opindexcontent() {
		$('#content-expand').slideToggle('slow', function() {
				// Animation complete.
		});
	}
	
////////////////////////////////////////// Forms //////////////////////////////////////////


	// Helpful Contact Forms at the bottom of the pages.

	// Variable to control people clicking on the send button multiple times.
	var emailAllowed = true;

    var form1 = $('#helpful-form');
    var form1Messages = $('#form-messages');

    // Set up an event listener for the contact form.
	$(form1).submit(function(event) {
	    event.preventDefault();
	    var formData = form1.serialize();
		$.ajax({
			type: 'POST',
			url: 'https://www.bestattorney.com/template-files/contact-files/helpful-form.php',
			data: formData
		}).done(function(response) {
		    // Make sure that the formMessages div has the 'success' class.
		    form1Messages.removeClass('error');
		    form1Messages.addClass('success');

		    // Set the message text.
		    form1Messages.text(response);

		    // Clear the form.
		    $('.helpful-radio').val('');
		    $('#helpful-textarea').val('');
		    $('#helpful-email').val('');
		    document.getElementById('not-helpful').style.display = 'none';
		}).fail(function(data) {

		    // Make sure that the formMessages div has the 'error' class.
		    form1Messages.removeClass('success');
		    form1Messages.addClass('error');

		    // Set the message text.
		    if (data.responseText !== '') {
		        form1Messages.text(data.responseText);
		    } else {
		        form1Messages.text('We\'re sorry, An error occured and your message could not be sent.');
		    }
		});
	});

// -------------------- Newsletter Stuff -------------------- //

	// display validation tooltip if no email is given and button is clicked
	$('#newsletter').submit(function(event) {
		// prevent default action (page refresh)
		event.preventDefault();
		// check if email input is empty
		var email_address = $('#nl-email').val();
		if (!email_address) {
			$('#nl-validation').html("Please enter your email address.");
			$('#nl-validation').fadeIn(200)
		} else {
			// check for "@" and "." in email input
			var at_position = email_address.indexOf("@");
			var dot_position = email_address.lastIndexOf(".");
			if (at_position < 1 || dot_position < at_position + 2) {
				$('#nl-validation').html("Please enter a valid email.");
				$('#nl-validation').fadeIn(200)
			// do ajax stuff if email looks ok
			} else {
				var post_url = "../template-files/contact-files/add-contact.php"
				$.ajax({
					url: post_url,
					type: 'POST',
					data: {"email": email_address},
				})
				.done(function(response) {
						// alert("success!", response);
						$('#nl-validation').html("Thank you! You have been added to our mailing list.");
						$('#nl-validation').fadeIn(250)
						console.log('email submitted');
					})
					.fail(function(response) {
						console.log('error:', response);
						$('#nl-validation').html('Error: '+response);
						$('#nl-validation').fadeIn(200)
					});
			}
		}
	});

	// fade out tooltip when user starts typing email
	$('#nl-email').keydown(function(event) {
		if (event.which != 13) {
			$('#nl-validation').fadeOut(3000);
		};
	});

// -------------------- Other Stuff -------------------- //

//For when we need to use popovers to display the full review on the sidebar reivews:
//$('[data-toggle="popover-review"]').popover();

	
	/* ========== Truncate Sidebar Section =========== */
	
	if (truncate) {
		truncateSidebar();
	} 

	/**
	* Checks if the sidebar is too long / extends past the content by 1000 pixels	
	*/
	function sidebarTooLong() {
		var contentHeight = document.getElementById("content").offsetHeight
		var sidebarHeight = document.getElementById("sidebar").offsetHeight
		return (sidebarHeight - contentHeight > 1000);
	}

	/**
	* Truncates sidebar until it's not longer than the content by 1000px
	*/
	function truncateSidebar() {
		if (sidebarTooLong()) {
			var blog = document.getElementById("sb-blog-feed");

			var removedArray = [];
			var removedString = "sidebar too long, removed: ";

			if (blog) { blog.remove(); removedArray.push("blog");}

			if (sidebarTooLong()) {
				var videos = document.getElementById("sb-videos")
				if (videos) { videos.remove(); removedArray.push("videos"); }

				if (sidebarTooLong()) {
					var reviews = document.getElementById("sb-reviews")
					if (reviews) { reviews.remove(); removedArray.push("reviews"); }
				}
			}

			removedArray.forEach(function(element) {
				removedString += element+", ";
			});
		}
	}
});

