// Toggles various template items - menu, search bar, forms, case results.

const search = document.getElementById("search");
const menu = document.getElementById("menu-link");
const dropdownForm = document.getElementById("free-case-review-button-tablet");
const helpful = document.getElementById("helpful-button");

/*==================== Navigation Bar ====================*/
$(document).ready(function() {

		$('.icon-link').on({
			mouseenter: function() {
				$(this).children('i').addClass('active-mobile-icon');
			},
			mouseleave: function () {
				/* this doesn't work unless display is none
				     !$(id).is(':visible')
				   but this does
					   !(el.offsetWidth > 0 && el.offsetHeight > 0)
				*/
				var menuId = $(this).data('dropdown');
				if (menuId) {
				  var el = document.getElementById(menuId.substring(1));
					//console.log(el.id, 'is', ((!(el.offsetWidth > 0 && el.offsetHeight > 0))?'not ':'') + 'visible');
					if (!(el.offsetWidth > 0 && el.offsetHeight > 0)) {
						$(this).children('i').removeClass('active-mobile-icon');
					};
				} else {
					$(this).children('i').removeClass('active-mobile-icon');
				};
			},
			click: function (event) {
				event.stopPropagation();
				$('.active-mobile-icon').removeClass('active-mobile-icon');
				var menuId = $(this).data('dropdown');
				if (menuId) {
				  var el = document.getElementById(menuId.substring(1));
					if (!(el.offsetWidth > 0 && el.offsetHeight > 0)) {
						$(this).children('i').addClass('active-mobile-icon');
					};
					if ( $(this).attr('id') === "menu-link" ) {
					  $('#search-form').hide();
						$('#top-menu').toggleClass('top-menu-toggled');
					} else if ($(this).attr('id') === "search") {
						$('#search-form').fadeToggle(100, function() {
							$('#top-menu').removeClass('top-menu-toggled');
						});
					};
				};
			}
		});

    // keep toggled menus open when user clicks directly on them
    $('#search-form').on('click', function(e) {
      e.stopPropagation();
    });
    $('#top-menu').on('click', function(e) {
      e.stopPropagation();
    });

    // hide toggled menus when user clicks anywhere else
    $(document).on('click scroll', function(event) {
			if (event.type == 'scroll' && $(document).scrollTop() < 200) {
				return;
			};
			// if ($(document).width() < 1100) {
			$('#top-menu').removeClass('top-menu-toggled');
			// };

      $('#search-form').fadeOut(50);
			//$('#search-form').removeClass('top-menu-toggled');

			$('#search i').removeClass('active-mobile-icon');
			$('#menu-link i').removeClass('active-mobile-icon');
      // console.log('hide top-menu, hide search-form');
    });
  });

/*==================== Consultation Form ====================*/

if (dropdownForm) {
	dropdownForm.addEventListener('click', function(e) {
		e.preventDefault();
		$("#consultation-form-wrap").slideToggle();

	});
}

/*==================== "Was this page helpful?" Buttons ====================*/

if (helpful) {
	const notHelpful = document.getElementById("unhelpful-button");
	helpful.addEventListener('click', function(e) {
		var formMessages = document.getElementById('form-messages');
	    formMessages.classList.add('success');
	    document.getElementById('not-helpful').classList.add('display-none');
	    document.getElementById('not-helpful').classList.remove('display-block');
	    formMessages.innerHTML = 'Thank You for Providing Your Input!';
	});
	notHelpful.addEventListener('click', function(e) {
	    var formMessages = document.getElementById('form-messages');
	    formMessages.innerHTML = '';
	    document.getElementById('not-helpful').classList.add('display-block');
	    document.getElementById('not-helpful').classList.remove('display-none');
	    document.getElementById('no-fill').value = "";
	});
	function helpfulSubmit() {
		document.getElementById("loader").classList.remove("display-none");
		//get all of the form information.
		var helpfulForm = document.getElementById('helpful-form'),
			helpfulFormMessages = document.getElementById('form-messages'),
			helpfulReason = document.querySelector('input[name="unhelpful"]:checked').value,
			helpfulText = document.getElementById("helpful-textarea").value,
			helpfulEmail = document.getElementById("helpful-email").value,
			submittedPage = document.getElementById('helpful-page').value;

		var xhrHelpful = new XMLHttpRequest();

		xhrHelpful.open('POST', 'https://www.bestattorney.com/template-files/contact-files/helpful-form.php');
		xhrHelpful.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		xhrHelpful.onload = function() {
			// console.log("onload happened.");
		    if (xhrHelpful.status === 200) {
		    	helpfulFormMessages.innerHTML = xhrHelpful.responseText;
		    	helpfulFormMessages.classList.remove('error');
		    	helpfulFormMessages.classList.add('success');
		    	document.getElementById('not-helpful').classList.add("display-none");
		    	document.getElementById('not-helpful').classList.remove("display-block");
		    } else if (xhrHelpful.status !== 200) {
		        helpfulFormMessages.classList.add('error');
		    	helpfulFormMessages.classList.remove('success');
		    	if (xhrHelpful.responseText !== '') {
			        helpfulFormMessages.innerHTML = xhrHelpful.responseText;
			    } else {
			        helpfulFormMessages.innerHTML = 'We\'re sorry, An error occured and your message could not be sent.';
			    }
		    }
		    document.getElementById("loader").classList.add("display-none");
		};
		xhrHelpful.send(encodeURI('unhelpful=' + helpfulReason + '&recommendations=' + helpfulText + "&email=" + helpfulEmail + "&page=" + submittedPage));
	}
}
