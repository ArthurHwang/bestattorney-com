
<?php
// Folder Options for /resources
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"spanishPageEquivalent" => "recursos/",

	// =====================================
	// Defining Sidebar Content
	// =====================================

	"showSidebarContactForm" => true,

	"extraSidebar" => "<p class='rc-title'>Resources Contents</p>
<div class='related-content'>
	<ul>
		<li><a href='https://www.bestattorney.com/resources/#resource-category-1'>Car Accident Resources</a></li>
		<li><a href='https://www.bestattorney.com/resources/#resource-category-2'>The Legal Process</a></li>
		<li><a href='https://www.bestattorney.com/car-accidents/rental-car-safety.html#resource-category-3'>
Rental Car Safety Resources</a></li>
		<li><a href='https://www.bestattorney.com/resources/#resource-category-4'>Car Accident Prevention</a></li>
		<li><a href='https://www.bestattorney.com/resources/#resource-category-5'>		 Misc Resources</a></li>
		<li class='separator'></li>
		<li><a href='https://www.bestattorney.com/resources/value-of-your-personal-injury-claim.html'>What's the Value of my Injury Claim?</a></li>
		<li><a href='https://www.bestattorney.com/resources/injured-in-car-accident.html'>What to do after a Car Accident</a></li>
		<li><a href='https://www.bestattorney.com/faqs/'>Personal Injury FAQs</a></li>
		<li><a href='https://www.bestattorney.com/resources/understanding-comparative-negligence.html'>Understanding Comparative Negligence</a></li>
		<li><a href='https://www.bestattorney.com/resources/what-is-gap-insurance.html'>What is Gap Insurance?</a></li>
		<li><a href='https://www.bestattorney.com/resources/legal-dictionary.html'>Dictionary of Legal Terms</a></li>
		<li><a href='https://www.bestattorney.com/blog/'>Bisnar Chase Blog</a></li>
		<li><a href='https://www.bestattorney.com/resources/bisnar-chase-videos.html'>Bisnar Chase Videos</a></li>
		<li class='separator'></li>
		<li><a href='https://www.bestattorney.com/practice-areas.html'>Areas of Practice</a></li>	
		<li><a href='https://www.bestattorney.com/about-us/testimonials.html'>Testimonials & Reviews</a></li>
		<li><a href='https://www.bestattorney.com/case-results/'>Case Results & Success Stories</a></li>
		<li><a href='https://www.bestattorney.com/about-us/'>The Bisnar Chase Difference</a></li>
	</ul>
	<div class='clear'></div>
</div>", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>
