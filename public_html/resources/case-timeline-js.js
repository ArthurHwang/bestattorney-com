var phase1 = true; // If this is true, we can start from the beginning. 
var phase2 = false;
var phase3 = false;
var phase4 = false;
var phase2LegalClicked = false; //If the choice has been made between a lawsuit and settlement, this will be changed to true, the choice that was not selected will be faded to .2, and the first bouncing arrow will appear to expand the flow chart.
var caseTypeSet = false; //If the first expand arrow has been clicked, the case type (settlement or lawsuit) is set and you can no longer change it. 

var typeOfCase = ""; //Variable used for setting the type of case. This will probably be removed eventually so the timeline can just be one subject.

var emergency; //This is to set whether yes or no was clicked in the right side healing section. 

var casePath; //casePath contains either the string "settlement" or "lawsuit" and is used to dynamically select the correct jquery object to animate on.

var stopbounce = false;

var damagesArray = ["<i class='fa fa-wrench'></i> Property Damage","<i class='fa fa-building-o'></i> Lost Wages", "<i class='fa fa-wheelchair'></i> Pain and Suffering"]; //damages array is the array in which we put the html that will go in the damages box near the demand section. It contains 3 items by default, and more is added to it when list items in the 'Treatment for injures' section are clicked. 

var injuriesObject = {};

var damagesExpanded = false;

var damagesDisplayed = false; //The timeline should expand past the damages section ONLY after both the expand-3 arrow has been clicked and a list item has been clicked from the treatment for injuries section. If the expand 3 arrow has been clicked, damagesExpanded will be changed to true, and if the list item has been clicked, damagesDisplayed will be true. If both of these are true, expand to the next section. 

var afterDemandFired = false; // This checks if afterDemand has already fired and if it is true, after demand doesn't fire again after a li click.





function listDamages() {

	var damageshtml;

	var settlement9Height;



	if (damagesArray.length > 3) {

		damageshtml = "";

		for (var i=damagesArray.length-1; i>=0; i--) {

			damageshtml += "<div class='damage-item'>"+damagesArray[i]+"</div>";

		}

		if (damagesArray.length < 7) {

			settlement9Height = "215px";

		} else if (damagesArray.length < 10) {

			settlement9Height = "250px";

		}

	} else {

		damageshtml = "<h3 class='centered'>Select your medical treatment under the 'Focus on Healing' section</h3>";

		settlement9Height = "180px";

	}



	damageshtml += "<div class='clearfix'></div>";

	$("#damages-list").html(damageshtml);	

	$("#settlement-9").animate({

			height:settlement9Height,

	},200);



}



function runAnimations(functionArray){ // A function to execute functions in series so animations are not run in parallel.

    var func = functionArray.splice(0,1);

      	func[0]().promise().done(function(){

        if(functionArray.length > 0 ) runAnimations(functionArray);

    });

}



function afterDemand() {

	if (afterDemandFired == false) {

		s9lAnimate4 = function() {

			$("#section-9 .line-2").css("border-left-width","2px").animate({

				height:"70px",

			},200);



			return $(" .timeline-element");



		}



		s9lAnimate5 = function() {

			

			$("#more-info-required").animate({

				height:"200px",

				width:"350px",

				borderWidth:"2px",

			},200);



			return $(".timeline-element");

		}



		s9lAnimate6 = function() {



			$("#section-9 .line-3").css("border-right-width","2px").animate({

				height:"300px",

			},500);



			$("#more-info-required .box-text-question i").animate({

				opacity:1,

			},200);



			$("#more-info-required").addClass("overflow");



			return $(".timeline-element");

		}



		s9lAnimate7 = function() {



			$("#offer-made").animate({

				height:"205px",

				width:"370px",

				borderWidth:"2px",

			},200);



			$("html, body").animate({scrollTop: "+=200"}, 600);



			return $(".timeline-element");

		}



		s9lAnimate8 = function() {

			$("#section-9 .line-4").css("border-left-width", "2px").animate({

				height:"93px",

			},200, "linear", function() {

				$(this).css("border-bottom-width", "2px").animate({

					width:"210px",

				},200,"linear");

			});



			$("#offer-made .box-text-question i").animate({

				opacity:1,

			},200);



			$("#offer-made").addClass("overflow");



			return $(".timeline-element");

		}



		s9lAnimate9 = function() {

			$("#section-10 .line-1").css("border-right-width", "2px").animate({

				height:"93px",

			},300, "linear", function() {

				$(this).css("border-bottom-width", "2px").animate({

					width:"220px",

					left: "412px"

				},300, "linear");

			});



			return $(".timeline-element");

		}



		s9lAnimate10 = function() {

			$("#offer-rejected").css("border-width", "2px").animate({

				width:"279px",

				height:"122px",

				left: "133px",

    			top: "26px",

			},200);



			return $(".timeline-element");

		}



		s9lAnimate11 = function() {

			$("#section-10 .line-4").css("border-right-width", "2px").animate({

				height:"50px",

			},300, "linear", function() {

				$(this).css("border-bottom-width", "2px").animate({

					width:"200px",

				},300, "linear");

			});



			return $(".timeline-element");

		}



		s9lAnimate12 = function() {

			$("#section-10 .line-3").css("border-left-width", "2px").animate({

				height:"277px",

				bottom: "204px"

			},300, "linear", function() {

				$(this).css("border-top-width", "2px").animate({

					width:"494px",

				},300, "linear");

			});



			return $(".timeline-element");

		}



		s9lAnimate13 = function() {



			return $(".timeline-element");

		}



		s9lAnimate14 = function() {



			return $(".timeline-element");

		}



		s9lAnimate15 = function() {

			$("#section-10 .line-5").css("border-left-width", "2px").animate({

				height:"200px",

			},200);



			return $(".timeline-element");

		}



		s9lAnimate16 = function() {

			$("#offer-accepted").css("border-width", "2px").animate({

				height:"120px",

				width:"500px",

			},200);



			$("html, body").animate({scrollTop: "+=200"}, 600);



			return $(".timeline-element");

		}



		s9lAnimate17 = function() {

			$("#section-10 .line-6").css("border-left-width", "2px").animate({

				height:"100px",

			},300);



			$("#offer-accepted .box-text-question i").animate({

				opacity:1,

			},200);



			$("#offer-accepted").addClass("overflow");



			return $(".timeline-element");

		}



		s9lAnimate18 = function() {

			$("#case-finished").css("border-width", "2px").animate({

				height:"120px",

				width:"500px",

			},200);



			$("html, body").animate({scrollTop: "+=300"}, 600);



			return $(".timeline-element");

		}



		s9lAnimate19 = function() {



			$("#part-4").delay(1500).slideDown(1000);



			return $(".timeline-element");

		}





		runAnimations([s9lAnimate4, s9lAnimate5, s9lAnimate6, s9lAnimate7, s9lAnimate8, s9lAnimate9, s9lAnimate10, s9lAnimate11, s9lAnimate12, s9lAnimate13, s9lAnimate14, s9lAnimate15, s9lAnimate16, s9lAnimate17, s9lAnimate18, s9lAnimate19]);

		afterDemandFired = true;

	}

}



function resetTimeline() {



	phase1 = true;

	phase2 = false;

	phase3 = false;

	phase4 = false;

	phase2LegalClicked = false;

	caseTypeSet = false;

	typeOfCase = "";

	afterDemandFired = false;

	damagesExpanded = false;

	damagesDisplayed = false;

	stopbounce = true;

	damagesArray = ["<i class='fa fa-wrench'></i> Property Damage","<i class='fa fa-building-o'></i> Lost Wages", "<i class='fa fa-wheelchair'></i> Pain and Suffering"];

	animateButtons($("rect"), {width:0, height:0}, 400);

	



	var timelineElements = [];

	$.fx.off = true;

	$(".timeline-element").stop(true,true).each(function() {

		timelineElements.unshift($(this));

	});

	$.fx.off = false;



	$("html, body").animate({scrollTop: "0"}, 2000);

	hideElements(timelineElements);



	$(".fade-element").animate({

		opacity:"0",

	},300);



	



	$(".timeline-checked").toggleClass("timeline-checkbox timeline-checked");

	$("#timeline rect").attr("class", "timeline-element timeline-button");

	$("#timeline .choice-box").css("opacity","1");

	$(".documentation-paragraph").fadeOut();

	$(".expand-arrow i").slideUp();

	$(".overflow").removeClass("overflow");





}



function hideElements(timelineElements) {

	var func = timelineElements.splice(0,4);

	func[0].animate({

		height: "0px"

	},60, function() {

		func[0].removeAttr('style');

	}).promise();



	func[1].animate({

		height: "0px"

	},60, function() {

		func[1].removeAttr('style');

	}).promise();



	func[2].animate({

		height: "0px"

	},60, function() {

		func[2].removeAttr('style');

	}).promise();



	func[3]

	.animate({

		height: "0px"

	},60, function() {

		func[3].removeAttr('style');

	}).promise().done(function(){

		if(timelineElements.length > 3 ) { 

			hideElements(timelineElements); 

		} else {

			func[0].animate({

				height: "0px"

			},60, function() {

				func[0].removeAttr('style');

			});

			func[1].animate({

				height: "0px"

			},60, function() {

				func[1].removeAttr('style');

			});

			func[2].animate({

				height: "0px"

			},60, function() {

				func[2].removeAttr('style');

			});

			func[3].animate({

				height: "0px"

			},60, function() {

				func[3].removeAttr('style');

			});



			var curHeight = $("#timeline").height();

			$('#timeline').css('height', 'initial');

			var autoHeight = $("#timeline").height();



			$("#timeline").height(curHeight).animate({

				height:autoHeight,

			},1000,function() {

				$(this).css("height","initial");

			});

		}

	});//A function that hides all elements sequentially, called in resetTimeline

}



function animateButtons($el, attrs, speed) {



    // duration in ms

    speed = speed || 400;



    var start = {}, // object to store initial state of attributes

        timeout = 20, // interval between rendering loop in ms

        steps = Math.floor(speed/timeout), // number of cycles required

        cycles = steps; // counter for cycles left



    // populate the object with the initial state

    $.each(attrs, function(k,v) {

        start[k] = $el.attr(k);

    });



    (function loop() {

        $.each(attrs, function(k,v) {  // cycle each attribute

            var pst = (v - start[k])/steps;  // how much to add at each step

            $el.attr(k, function(i, old) {

                return +old + pst;  // add value do the old one

            });

        });



        if (--cycles) // call the loop if counter is not exhausted

            setTimeout(loop, timeout);

        else // otherwise set final state to avoid floating point values

            $el.attr(attrs);



    })(); // start the loop

}



function bounce(number) {

	if (stopbounce) return;

	$("#expand-arrow-"+number+" i")

	.animate({

	top: "+=30px"

	}, 500, "easeInCubic")

	.animate({

	top: "-=30px"

	}, 500, "easeOutCubic", function() {

		bounce(number);

	});

	//console.log("bounced - "+number);

}



function parallax(){

  var scrolled = $(window).scrollTop();

  $('#background-image img').css('top',(scrolled*0.4)+'px');

  $("#parallax-viewfinder").css('top',(scrolled)-490+'px');

}



window.onbeforeunload = function(){

	window.scrollTo(0,0);

}



$(function() {

	

	$('[data-toggle="popover"]').popover();



	if ($(window).width() < 1100) {

		if ((screen.width) >= 1100) {

			$("#expand-browser").show();

		} else {

			$("#no-expand-browser").show();

		} 

		$('#width-warning').modal('show');

	}



	



	//START IN PHASE 1.



	//THIS IS PHASE 2. CLICKING ON WHAT KIND OF ACCIDENT YOU'VE BEEN IN. 

	$("#timeline #injuries-button-box #car-accident").click(function() {

		if (phase1 === true && typeOfCase != $(this).attr("id") ) {

			phase1 = false;

			phase2 = true;



			s1lAnimate1 = function() {



				$("html, body").animate({scrollTop: "+=700"}, 500);



				$("#section-1 .line-1").animate({

					height: "50px",

				},100);

				

				return $(" .timeline-element");

			}



			s1lAnimate2 = function() {

				$("#section-1-hire").animate({

					height:"50px",

					width:"570px",

					borderWidth:"2px",

				},100).delay(200);



				return $(" .timeline-element");

			}



			s1lAnimate3 = function() {



				return $(" .timeline-element");

			}



			s1lAnimate4 = function() {

				$("#section-1 .line-3").animate({

					height: "50px",

				},100);

				

				return $(" .timeline-element");

			}



			s1lAnimate5 = function() {



				$("#constantly-taking-place").animate({

					height:"70px",

					width:"520px",

					borderWidth:"2px",

					left:"-65px"

				},100);



				return $(" .timeline-element");

			}



			s1lAnimate6 = function() {



				$("#focus-on-healing .line-1").animate({

					height:"50px",

					borderLeftWidth:"2px",

				},100);



				return $(" .timeline-element");

			}



			s1lAnimate7 = function() {

				$("#phase-2-healing").animate({

					height:"100px",

					width:"350px",

					borderWidth:"2px",

					left:"93px"

				},100);



				return $(" .timeline-element");

			}



			s1lAnimate8 = function() {

				$("#phase-2-healing").addClass("overflow");

				$("#phase-2-healing .box-text-question i").animate({

					opacity:1

				},200);



				$("#focus-on-healing .line-2").animate({

					borderLeftWidth:"2px",

					height:"40px"

				},100, "linear");



				return $(" .timeline-element");

			}



			s1lAnimate9 = function() {



				$("#focus-on-healing .line-3").animate({

					borderTopWidth:"2px",

					width:"149px"

				},100, "linear", function() {

					$(this).animate({

						borderLeftWidth:"2px",

						borderRightWidth:"2px",

						height:"40px"

					},100);

				});



				return $(" .timeline-element");

			}



			s1lAnimate10 = function() {

				animateButtons($("#emergency-no rect"), {width:75, height:75}, 100);

				$("#emergency-no rect").animate({

			    	width: "50%",

			    	height: "50%"

			    },100, function() {

			    	$("#emergency-no .box-text").animate({

			    		opacity:"1"

			    	},100);



			    });



				animateButtons($("#emergency-yes rect"), {width:75, height:75}, 100);

				$("#emergency-yes rect").animate({

			    	width: "50%",

			    	height: "50%"

			    }, 200, function() {

			    	$("#emergency-yes .box-text").animate({

			    		opacity:"1"

			    	},200);



			    });

				



				return $(" .timeline-element");

			}



			runAnimations([s1lAnimate1,s1lAnimate2,s1lAnimate3,s1lAnimate4,s1lAnimate5,s1lAnimate6,s1lAnimate7, s1lAnimate8, s1lAnimate9, s1lAnimate10]);

		}

	});



	//THIS IS PHASE 3. CLICKING ON IF YOU'VE REQUIRED EMERGENCY SERVICES OR HOSPITALIZATION

	$("#timeline #focus-on-healing .choice-box rect").click(function() {

			

			//This assigns the button that was clicked a class of "timelinebuttonselected"

			$(this).attr("class", "timeline-element timeline-button timelinebuttonselected");

			$("html, body").animate({scrollTop: "+=350"}, 500);

			// If you clicked yes, do the following. 

			if ($(this).attr('id') == "rect-yes") {



				// This was a bad accident. You will not go to space today.

				emergency = true;



				// Animate the 'no' button to be less visible 

				$("#emergency-no").animate({

					opacity:".4"

				},200);



				// If we have already clicked on one of the buttons, remove the timelineselectedclass from the no button, animate yes button to full, and make sure the lines are correct. 

				if (phase3 === true || phase4 === true) {

					$("#rect-no").attr("class", "timeline-element timeline-button");

					$("#emergency-yes").animate({

						opacity:"1"

					},200);

					$("#phase-3-medical .line-1").animate({

						height:"0px",

						borderRightWidth:"0px"

					},200);

				}



				//Animate lines down and the box with the list items opening. 

				$("#phase-3-medical .line-2").animate({

					height:"70px",

					borderRightWidth:"2px"

				},200, function (){

					$("#phase-3-treatment").animate({

						height:"258px",

						width:"320px",

						right:"30px",

						borderWidth:"2px",

					},200);

					$("#phase-3-treatment .box-text .emergency-treatments").fadeIn(200);

				});



			// If you clicked no, do the following. 

			} else if ($(this).attr('id') == "rect-no") {



				//This was not that bad of an accident. You may still go to space today.

				emergency = false;



				//Animate the yes button to be less visible.

				$("#emergency-yes").animate({

					opacity:".4"

				},200);



				// If we have already clicked on one of the buttons, remove the timelineselectedclass from the yes button, animate no button to full, and make sure the lines are correct. 

				if (phase3 === true || phase4 === true) {

					$("#rect-yes").attr("class", "timeline-element timeline-button");

					$("#emergency-no").animate({

						opacity:"1"

					},200);



					$("#phase-3-medical .line-2").animate({

						height:"0px",

						borderRightWidth:"0px"

					},200);

				}



				//Animate lines down and the box with the list items opening.

				$("#phase-3-medical .line-1").animate({

					height:"70px",

					borderRightWidth:"2px"

				},200, function() {

					$("#phase-3-treatment").animate({

						height: "205px",

						width:"320px",

						right:"30px",

						borderWidth:"2px",

					},200);

					$("#phase-3-treatment .box-text .emergency-treatments").fadeOut(200);

				});

						

			}



			//This signifies that the focus on healing buttons have already been pressed.

			//If one of the list items has already been pressed, then don't set this because it is irrelevant.

			if(phase4 == false) {

				phase2 = false;

				phase3 = true;

			}

	});

	

	//THIS IS PHASE 4. CLICKING ON MEDICAL ATTENTION

	$("#timeline #phase-3-treatment li").click(function() {

		

		//Set damagesDisplayed to true so that if another list item is clicked, the animations are not run again. 	

		damagesDisplayed = true;



		// Switch classes to display the green checkmark on items that are selected

		$(this).children("i").toggleClass("timeline-checkbox timeline-checked");



		//For the value that is selected, set damages as the HTML we want in the later section.

		var damages;

		var injury;

		var key

		switch($(this).children("i").data("checkbox")) {

			case "icu":

				damages = "<i class='fa fa-hospital-o'></i> Hospital/ICU Bills";

				injury = "<div class='contact-injury'>Hospitalization<span class='x-mark'>X</span></div>";

				key = 1;

				break;

			case "surgery":

				damages = "<i class='fa fa-heart'></i> Surgery Payments";

				injury = "<div class='contact-injury'>Surgery<span class='x-mark'>X</span></div>";

				key = 2;

				break;

			case "physical-therapy":

				damages = "<i class='fa fa-male'></i> Physical Therapy Bills";

				injury = "<div class='contact-injury'>Physical Therapy<span class='x-mark'>X</span></div>";

				key = 3;

				break;

			case "medication":

				damages = "<i class='fa fa-medkit'></i> Cost of Medications";

				injury = "<div class='contact-injury'>Medication<span class='x-mark'>X</span></div>";

				key = 4;

				break;

			case "practitioner":

				damages = "<i class='fa fa-user-md'></i> Cost of Specialist Visits";

				injury = "<div class='contact-injury'>Specialist Visits<span class='x-mark'>X</span></div>";

				key = 5;

				break;

		}



		// If this is the first time clicking one of these list items, do this. 

		if (phase3 === true ) {

			

			$("html, body").animate({scrollTop: "+=400"}, 600);



			$("#documentation-"+$(this).children("i").data("checkbox")).fadeIn();



			$("#phase-4-medical .line-1").animate({

				borderRightWidth:"2px",

				height:"95px",

			},100,function() {

				$("#phase-4-medical #phase-4-instructions").animate({

					height:$("#phase-4-instructions .box-text").outerHeight()+20,

					width:"425px",

					borderWidth:"2px",

					left: "15px",

				},100);



			});

			

			damagesArray.push(damages);

			injuriesObject[key] = injury;



			stopbounce = false;

			bounce(1);

			

			$("#expand-arrow-1 i ").css("display","inline").animate({

				opacity:1,

			},1000);



			phase3 = false;

			phase4 = true;

			

		// Else if this is not the first time pressing one of these buttons, do this. 

		} else if (phase4 === true ) {

				var paragraphHeight = $("#documentation-"+$(this).children("i").data("checkbox")).data("height")+30;



				// If we are checking this button, show the documentation that the button represents, and update the height to be the correct height. 

				if ($(this).children("i").hasClass("timeline-checked")) {

					

					// If the documentation for this button is not showing, show it!

					if ($("#documentation-"+$(this).children("i").data("checkbox")).css("display") != "block") {



						$("#documentation-"+$(this).children("i").data("checkbox")).slideDown(200);

						$("#phase-4-instructions").animate({

							height: "+="+paragraphHeight

						},200);

						$("#constant-line-long").animate({

							height: "+="+paragraphHeight

						},200);



					}

					if (damagesArray.indexOf(damages) == -1 ) {

						damagesArray.push(damages);

					}

					if (!injuriesObject[key]) {

						injuriesObject[key] = injury;

					}

					

				// If we are unchecking the box, then do this. 

				} else if ($(this).children("i").hasClass("timeline-checkbox")) {



					//If the documentation for this box is showing....

					if ($("#documentation-"+$(this).children("i").data("checkbox")).css("display") == "block") {



						// If the documentation is showing AND it's a practitioner, check if there are other practitioners that are checked.

						if ($(this).children("i").data("checkbox") == "practitioner") {

							var practitionerChecked = false;	

							$("#phase-3-treatment li .timeline-checked").each(function() {

								if ($(this).data("checkbox") == "practitioner") {

									practitionerChecked = true;

								}

							});



							// If there are no other practitioners that are checked, remove this documentation. 

							if (practitionerChecked == false) {



								$("#documentation-"+$(this).children("i").data("checkbox")).slideUp(200);

								$("#phase-4-instructions").animate({

									height: "-="+paragraphHeight

								},200);

								$("#constant-line-long").animate({

									height: "-="+paragraphHeight

								},200);



								for (var i=damagesArray.length-1; i>=0; i--) {

								    if (damagesArray[i] === damages) {

								        damagesArray.splice(i, 1);

								    }

								}

								delete injuriesObject[key];

							}



						// If its not a practitioner, remove this documentation.

						} else {

							$("#documentation-"+$(this).children("i").data("checkbox")).slideUp(200);

							$("#phase-4-instructions").animate({

								height: "-="+paragraphHeight

							},200);



							$("#constant-line-long").animate({

								height: "-="+paragraphHeight

							},200);



							for (var i=damagesArray.length-1; i>=0; i--) {

							    if (damagesArray[i] === damages) {

							        damagesArray.splice(i, 1);

							    }

							}

							delete injuriesObject[key];

						}



					}



				}

		}

		

		var contactHtml = "";



		for (var key in injuriesObject){

			contactHtml += "<div id='injury-"+key+"' class='evaluation-injury'>";

			contactHtml += injuriesObject[key]

			contactHtml += "</div>";

		}

		$("#contact-injuries-box").html(contactHtml);



		// I technically don't need to run afterDemand() and the steps can be run in its' own click event. Change this eventually. 

		if (damagesExpanded == true && damagesDisplayed == true) {

			afterDemand();

		}});

	

	//Phase 4.5 since this was added later. Animates the left side of the tree 

	$("#expand-arrow-1").on("click", "i", function() {

		

		s3lAnimate1 = function() {



			stopbounce = true;

			$("#expand-arrow-1 i").slideUp(50);



			$("html, body").animate({scrollTop: 500}, 600);



			return $(" .timeline-element");

		}



		s3lAnimate2 = function() {



			$("#section-1 .line-2").animate({

				height: "50px",

			},200);



			return $(" .timeline-element");

		}



		s3lAnimate3 = function() {



			$("#phase-2-legal #phase-2-lit-or-no").delay(200).animate({

				height:"150px",

				width:"500px",

				borderWidth:"2px",

				right:"30px",

			},200);



			return $(" .timeline-element");

		}



		s3lAnimate4 = function() {



			$("#phase-2-legal .line-2").delay(200).animate({

				height: "50px",

				borderRightWidth: "2px",

			},100);



			return $(" .timeline-element");

		}



		s3lAnimate5 = function() {



			$("#phase-2-legal .line-3").css("border-top-width","2px").animate({

				width:"283px"

			},100, function() {

				$(this).css({

					borderLeftWidth:"2px",

					borderRightWidth:"2px"

				}).animate({

					height:"46px"

				},100);

			});



			return $(" .timeline-element");

		}



		s3lAnimate6 = function() {



			animateButtons($("#phase-2-legal-no rect"), {width:75, height:75}, 100);

			$("#phase-2-legal-no rect").animate({

		    	width: "50%",

		    	height: "50%"

		    },200, function() {

		    	$("#phase-2-legal-no .box-text").animate({

		    		opacity:"1"

		    	},200);

		    });

		    animateButtons($("#phase-2-legal-yes rect"), {width:75, height:75}, 100);

		    $("#phase-2-legal-yes rect").animate({

		    	width: "50%",

		    	height: "50%"

		    },200, function() {

		    	$("#phase-2-legal-yes .box-text").animate({

		    		opacity:"1"

		    	},200);

		    });



			return $(" .timeline-element");

		}



		runAnimations([s3lAnimate1,s3lAnimate2,s3lAnimate3,s3lAnimate4,s3lAnimate5, s3lAnimate6]);

	});



	//PHASE 5. I'M KIND OF OVER THE PHASE THING NOW BUT THIS IS PHASE-2-LEGAL-YES OR PHASE-2-LEGAL-NO CLICKING. 

	$("#phase-2-legal rect").click(function() {

		//The following can only be executed if caseTypeSet has not been set. If it has, you have to reset in order to change.

		if (caseTypeSet == false) {



			// If you've clicked Yes, do this ish. 

			if ($(this).attr('id') == "rect-legal-yes") {



				// We will file a lawsuit. 

				casePath = "lawsuit";



				// Add classes, animate lines down, opacity animated. 

				$(this).attr("class", "timeline-element timeline-button timelinebuttonselected rect-legal-yes-element");



				$("#phase-2-legal .rect-legal-yes-element").animate({

					opacity:"1",

				},100);

				$("#rect-legal-no").attr("class", "timeline-element timeline-button rect-legal-no-element");

				$("#phase-2-legal .rect-legal-no-element").animate({

					opacity:".4",

				},100);

				$("#section-3-legal .line-2").animate({

					height:"60px",

					borderLeftWidth:"2px"

				},100);

				$("#section-3-legal .line-1").animate({

					height:"0px",

				},100);



				

				$("#section-3-case-type").animate({

					height:"150px",

					width:"510px",

					left: "0px",

					borderWidth:"2px",

				},100,function() {

					$("#section-3-case-type .box-text").html('<h4>Case Signed & Lawsuit Filed:</h4><p>A summons and complaint is filed with the court, and the defendant is served. The defendant must answer within 30 days, otherwise they run the risk of a default judgement being made in the plaintiff\'s favor. </p>');

				}).queue(function() {

					$(this).addClass("overflow").dequeue();

				});



				

				$("#section-4-legal .box-text-question a").attr("data-content", "<p>One of the main delays in resolving a personal injury lawsuit is waiting for the legal process. The defendent must first be served a summons, court dates must be set, and any challenges to dismiss the case must be addressed. Personal injury lawsuits can take upwards of 2-3 years to be complete.</p><br><p>Bisnar Chase will update our clients at least every 30 days even if there are no updates with the court, just to let them know that we are still on the case and are working towards getting our clients their compensation.</p>");



			} else if ($(this).attr('id') == "rect-legal-no") {

				// We will pursue a settlement from insurance.

				casePath = "settlement";



				$(this).attr("class", "timeline-element timeline-button timelinebuttnselected rect-legal-no-element");

				$("#phase-2-legal .rect-legal-no-element").animate({

					opacity:"1",

				},100);

				$("#rect-legal-yes").attr("class", "timeline-element timeline-button rect-legal-yes-element");

				$("#phase-2-legal .rect-legal-yes-element").animate({

					opacity:".4",

				},100);

				$("#section-3-legal .line-1").animate({

					height:"60px",

					borderLeftWidth:"2px"

				},100);

				$("#section-3-legal .line-2").animate({

					height:"0px",

				},100);



				$("#section-3-case-type .box-text").html('<h4>Case Signed:</h4> <p>Issues regarding insurance coverage, policy limits, vehicle damage, and benefits available are investigated.</p>');

				$("#section-3-case-type").animate({

					height:"95px",

					width:"510px",

					left: "0px",

					borderWidth:"2px",

				},100,function() {

					$("#section-4-legal .box-text-question a").attr("data-content", "<p>One of the main delays in finishing a personal injury case is waiting for the defendant's insurance company to respond, once they have received all of the information about the claim. Evidence and reports must be reviewed to determine what an appropriate settlement would be. We update our clients whenever there is progress on their case, but we also call every 30 days to get updates from the client on their medical treatment and healing process, and to let the client know that we are still working on their case. </p><br><p>Occasionally, some insurance companies will wait before responding in an effort to entice our clients to accept their first offer, which is generally lower than it should be. <a href='https://www.bestattorney.com/resources/negotiation-tips.html' target='_blank'>More Information Here</a></p>");

				}).queue(function() {

					$(this).addClass("overflow").dequeue();

				});

				

				

			}



			// If this is the first time clicking one of these buttons, do the following. 

			if (phase2LegalClicked == false) {



				phase2LegalClicked = true;

				$("html, body").animate({scrollTop: "+=300"}, 600);







				stopbounce = false;

				bounce(2);



				$("#expand-arrow-2 i ").css("display","inline").animate({

						opacity:1,

					},200);





				$("#section-3-case-type .box-text-question i").delay(100).animate({

					opacity:1,

				},100);



				//$("#section-3-case-type").addClass("overflow");



				

				

			}

		}

	});

	

	//  PHASE 6   (Clicking on expand arrow on the left side.)

	$("#expand-arrow-2").on("click", "i", function() {

		//Once CaseTypeSet is true, the case type can no longer be changed. 

		caseTypeSet = true;

		stopbounce = true;



		s4lAnimate1 = function() {

			$("#expand-arrow-2 i").slideUp(50);



			$("html, body").animate({scrollTop: "+=300"}, 600);



			

			return $("#section-4-legal .timeline-element");

		}



		s4lAnimate2 = function() {



			$("#section-4-legal .line-1").animate({

				borderLeftWidth:"2px",

			},50);

			$("#section-4-legal .line-1").animate({

				height:"80px",

			},150);



			return $("#section-4-legal .timeline-element");

		}



		s4lAnimate3 = function() {

			$("#"+casePath+"-1").animate({

				height:"125px",

				width:"300px",

				borderWidth:"2px",

				left:"107px"

			},200);

			return $("#section-4-legal .timeline-element");

		}



		s4lAnimate4 = function() {

			$("#section-4-legal .line-2").css("border-top-width","2px").animate({

				width: "140px",

			},100).css("border-right-width","2px").animate({

				height: "129px",

			},100);

			return $("#section-4-legal .timeline-element");

		}



		s4lAnimate5 = function() {



			$("#constants").animate({

				height:"46px",

				width:"236px",

				borderWidth:"2px",

				left:"376px"

			},200);



			return $("#section-4-legal .timeline-element");

		}







		s4lAnimate6 = function() {



			$("#section-4-legal .line-3").animate({

				height:"60px",

				borderLeftWidth:"2px",

			},200);



			return $("#section-4-legal .timeline-element");

		}



		s4lAnimate7 = function() {



			$("#section-4-legal .box-text-question i").delay(100).animate({

					opacity:1,

			},100);



			return $("#section-4-legal .timeline-element");

		}





		s4lAnimate8 = function() {



			$("#section-5-legal .line-1").animate({

				borderLeftWidth:"2px",

			},50);

			$("#section-5-legal .line-1").animate({

				height:"100px",

			},150);



			$("#section-5-legal .line-2").animate({

				borderLeftWidth:"2px",

			},50);

			$("#section-5-legal .line-2").animate({

				height:"40px",

			},150);



			return $("#section-5-legal .timeline-element");

		}



		s4lAnimate9 = function() {



			$("#"+casePath+"-2").animate({

				height:"131px",

				width:"354px",

				borderWidth:"2px",

				left: "79px"

			},100);



			setTimeout(function() {

				$("#"+casePath+"-2").addClass("overflow");

				$("#"+casePath+"-2 .box-text-question i").animate({

					opacity:1,

				},100);

			},200);



			return $("#section-5-legal .timeline-element");

		}



		s4lAnimate10 = function() {

			stopbounce = false;

			bounce(3);



			$("#expand-arrow-3 i ").css("display","inline").animate({

					opacity:1,

				},200);



			return $("#section-4-legal .timeline-element");

		}

		

    	runAnimations([s4lAnimate1,s4lAnimate2,s4lAnimate3,s4lAnimate4,s4lAnimate5,s4lAnimate6,s4lAnimate7,s4lAnimate8,s4lAnimate9,s4lAnimate10]);

    });



	// Phase 7

	$("#expand-arrow-3 i").click(function() {

		

		stopbounce = true;

		$("#expand-arrow-3 i").slideUp(50);



		s6lAnimate1 = function() {



			$("html, body").animate({scrollTop: "+=300"}, 600);



			return $(".timeline-element");

		}





		s6lAnimate2 = function() {



			$("#section-6-legal").css({minHeight:"165px",marginTop:"-80px"});



			return $(".timeline-element");

		}



		s6lAnimate3 = function() {



			$("#section-6-legal .line-1").css("border-left-width","2px").animate({

				height:"100px",

			},200);



			return $(".timeline-element");

		}



		s6lAnimate4 = function() {

			$("#"+casePath+"-3").animate({

				height:"100px",

				width:"530px",

				borderWidth: "2px",

				left:"-15px",

			},200);



			return $(".timeline-element");

		}



		s6lAnimate5 = function() {



			$("#section-7 .line-2").css("border-left-width","2px").animate({

				height:"50px",

			},200);

			

			return $(".timeline-element");

		}



		s6lAnimate6 = function() {

			

			if (casePath == "lawsuit") {

				var dimensions = ["152px","292px"];

			} else if (casePath == "settlement") {

				var dimensions = ["110px","270px"];

			}



			$("#"+casePath+"-4").animate({

				height:dimensions[0],

				width:dimensions[1],

				borderWidth: "2px",

				left:"30px",

			},200);



			$("html, body").animate({scrollTop: "+=100"}, 600);



			return $(".timeline-element");

		}



		s6lAnimate7 = function() {



			$("#section-7 .line-3").css("border-left-width","2px").animate({

				height:"50px",

			},200);



			var longLineHeight;



			if ($(".timeline-checked").length > 4) {

				longLineHeight = "1330px";

			} else if ($(".timeline-checked").length > 3) {

				longLineHeight = "1230px";

			} else {

				longLineHeight = "1150px";

			}



			$("#constant-line-long").css("border-left-width","2px").animate({

				height:longLineHeight

			},500);



			return $(".timeline-element");

		}



		s6lAnimate8 = function() {

			

			$("#"+casePath+"-5").animate({

				height:"80px",

				width:"270px",

				borderWidth: "2px",

				left:"30px",

			},200);



			$("#constant-calls-"+casePath).animate({

				height:"110px",

				width:"400px",

				borderWidth:"2px",

			},200);

			setTimeout(function() {

				$("#constant-calls-"+casePath).addClass("overflow");

			},200);



			$("#constant-calls-"+casePath+" .box-text-question i").delay(100).animate({

					opacity:1,

			},100);



			$("html, body").animate({scrollTop: "+=100"}, 600);



			return $(".timeline-element");

		}



		s6lAnimate9 = function() {



			$("#section-7 .line-4").css("border-left-width","2px").animate({

				height:"50px",

			},200);



			$("#phase-4-medical .line-2").css("border-left-width","2px").animate({

				height:"36px",

			},200);





			return $(".timeline-element");

		}



		s6lAnimate10 = function() {



			$("#"+casePath+"-6").animate({

				height:"100px",

				width:"270px",

				borderWidth: "2px",

				left:"30px",

			},200);



			$("#constant-law-changes").animate({

				height:"110px",

				width:"400px",

				borderWidth:"2px",

			},100);

			setTimeout(function() {

				$("#constant-law-changes").addClass("overflow");

			},200);



			$("#constant-law-changes .box-text-question i").delay(100).animate({

					opacity:1,

			},100);



			$("html, body").animate({scrollTop: "+=200"}, 600);

			



			return $(".timeline-element");

		}



		s6lAnimate11 = function() {

			

			stopbounce = false;

			bounce(4);

			

			$("#expand-arrow-4 i ").css("display","inline").animate({

				opacity:1,

			},200);



			return $(".timeline-element");

		}



    	runAnimations([s6lAnimate1, s6lAnimate2, s6lAnimate3, s6lAnimate4, s6lAnimate5, s6lAnimate6, s6lAnimate7, s6lAnimate8,s6lAnimate9,s6lAnimate10, s6lAnimate11]);

    });



	$("#expand-arrow-4 i").click(function() {

		

		stopbounce = true;



		s7lSettlement1 = function() {

			$("#expand-arrow-4 i").slideUp();



			return $(".timeline-element");

		}

		

		// ANIMATIONS FOR THE SETTLEMENT SIDE OF THINGS. 

		s7lSettlement2 = function() {



			$("#section-8 .line-1").css("border-left-width","2px").animate({

				height:"90px",

			},200);



			return $(".timeline-element");

		}



		s7lSettlement3 = function() {

			

			$("#"+casePath+"-7").animate({

				height:"100px",

				width:"700px",

				borderWidth: "2px",

				left:"175px",

			},200);



			setTimeout(function() {

				$("#"+casePath+"-7").addClass("overflow");

			},200);



			$("#"+casePath+"-7 .box-text-question i").delay(100).animate({

					opacity:1,

			},100);



			$("html, body").animate({scrollTop: "+=400"}, 600);



			return $(".timeline-element");

		}



		s7lSettlement4 = function() {



			$("#section-8 .line-2").css("border-left-width","2px").animate({

				height:"50px",

			},200);



			return $(".timeline-element");

		}





		s7lSettlement5 = function() {



			$("#settlement-8").animate({

				height:"46px",

				width: "200px",

				borderWidth: "2px",

			},200);

			

			return $(".timeline-element");

		}



		s7lSettlement6 = function() {



			stopbounce = false;

			bounce(5);

			

			$("#expand-arrow-5 i ").css("display","inline").animate({

				opacity:1,

			},200);





			return $(".timeline-element");

		}



		// ANIMATIONS FOR THE LAWSUIT SIDE OF THINGS. 



		s7lLawsuit1 = function() {

			

			$("#expand-arrow-4 i").slideUp();



			return $(".timeline-element");

		}



		s7lLawsuit2 = function() {



			$("#section-8 .line-1").css("border-left-width","2px").animate({

				height:"100px",

			},200);



			return $(".timeline-element");

		}



		s7lLawsuit3 = function() {



			$("#lawsuit-7").css("border-width","2px").animate({

				height:"70px",

				width:"1000px",

				left:"0px",

			},200);



			$("html, body").animate({scrollTop: "+=400"}, 600);



			return $(".timeline-element");

		}



		s7lLawsuit4 = function() {



			$("#section-9 .line-1-lawsuit").css("border-left-width","2px").animate({

				height:"70px",

			},200);



			return $(".timeline-element");

		}



		s7lLawsuit5 = function() {



			$("#lawsuit-8-1").css("border-width","2px").animate({

				height:"100px",

				width:"130%",

			},200);



			$("#section-9 .line-2-lawsuit").css("border-left-width","2px").animate({

				height:"190px",

			},200);



			return $(".timeline-element");

		}



		s7lLawsuit6 = function() {



			$("#lawsuit-8-2").css("border-width","2px").animate({

				height:"100px",

				width:"130%",

				right:"15%",

			},200);



			$("#section-9 .line-3-lawsuit").css("border-left-width","2px").animate({

				height:"70px",

			},200);



			$("#lawsuit-8-1 .box-text-question i").animate({

				opacity:1,

			},200);



			$("#lawsuit-8-1").addClass("overflow");



			return $(".timeline-element");

		}



		s7lLawsuit7 = function() {



			$("#lawsuit-8-3").css("border-width","2px").animate({

				height:"100px",

				width:"130%",

				right:"15%",

			},200);



			$("#section-9 .line-4-lawsuit").css("border-left-width","2px").animate({

				height:"190px",

			},200);



			$("#lawsuit-8-2 .box-text-question i").animate({

				opacity:1,

			},200);



			$("#lawsuit-8-2").addClass("overflow");



			return $(".timeline-element");

		}



		s7lLawsuit8 = function() {



			$("#lawsuit-8-4").css("border-width","2px").animate({

				height:"100px",

				width:"130%",

				right:"15%",

			},200);



			$("#section-9 .line-5-lawsuit").css("border-left-width","2px").animate({

				height:"70px",

			},200);



			$("#lawsuit-8-3 .box-text-question i").animate({

				opacity:1,

			},200);



			$("#lawsuit-8-3").addClass("overflow");



			return $(".timeline-element");

		}



		s7lLawsuit9 = function() {



			$("#lawsuit-8-5").css("border-width","2px").animate({

				height:"100px",

				width:"130%",

				right:"30%",

			},200);



			$("#lawsuit-8-4 .box-text-question i").animate({

				opacity:1,

			},200);



			$("#lawsuit-8-4").addClass("overflow");



			return $(".timeline-element");

		}



		s7lLawsuit10 = function() {



			$("#lawsuit-8-5 .box-text-question i").animate({

				opacity:1,

			},200);



			$("#lawsuit-8-5").addClass("overflow");



			stopbounce = false;

			bounce(6);

				

			$("#expand-arrow-6 i").css("display","inline").animate({

				opacity:1,

			},200);



			return $(".timeline-element");

		}







		if (casePath == "settlement") {

			runAnimations([s7lSettlement1, s7lSettlement2, s7lSettlement3, s7lSettlement4, s7lSettlement5, s7lSettlement6]);

		} else if ( casePath == "lawsuit") {

			runAnimations([s7lLawsuit1, s7lLawsuit2, s7lLawsuit3, s7lLawsuit4, s7lLawsuit5, s7lLawsuit6, s7lLawsuit7, s7lLawsuit8, s7lLawsuit9, s7lLawsuit10]);

		}		

	});



	//Phase 8

	$("#expand-arrow-5 i").click(function() {

		

		damagesExpanded = true;

		stopbounce = true;



		s9lAnimate1 = function() {



			$("#expand-arrow-5 i ").slideUp(50);

			return $("#section-9 .timeline-element");

		}



		s9lAnimate2 = function() {

			$("#section-9 .line-1").css("border-right-width", "2px").animate({

				height:"30px"

			},400);



			listDamages();



			return $("#section-9 .timeline-element");

		}

		s9lAnimate3 = function() {

			$("html, body").animate({scrollTop: "+=300"}, 600);



			$("#settlement-9").animate({

				width:"750px",

				borderWidth:"2px"

			},400);



			$("#settlement-9 .box-text-question i").animate({

				opacity:1

			},500, function() {

				$("#settlement-9").addClass("overflow");

			});



			return $("#section-9 .timeline-element");

		}



		s9lAnimate4 = function() {

			afterDemand();

			return $("#section-9 .timeline-element");

		}



    	if (damagesExpanded == true && damagesDisplayed == true) {

    		runAnimations([s9lAnimate1,s9lAnimate2,s9lAnimate3,s9lAnimate4]);

    	} else {

    		runAnimations([s9lAnimate1,s9lAnimate2,s9lAnimate3]);

    	}});



	// Phase 8 Lawsuit

	$("#expand-arrow-6 i").click(function() {



		stopbounce = true;

		$("#expand-arrow-6 i").slideUp();



		s8lLawsuit1 = function() {

			

			return $(".timeline-element");

		}



		s8lLawsuit2 = function() {

			

			$(".line-6-lawsuit").css("border-left-width","2px").animate({

				height:"230px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit3 = function() {



			$(".line-7-lawsuit").css("border-left-width","2px").animate({

				height:"110px",

			},100);



			return $(".timeline-element");

		}



		s8lLawsuit4 = function() {



			$(".line-8-lawsuit").css("border-left-width","2px").animate({

				height:"230px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit5 = function() {



			$(".line-9-lawsuit").css("border-left-width","2px").animate({

				height:"110px",

			},100);



			return $(".timeline-element");

		}



		s8lLawsuit6 = function() {



			$(".line-10-lawsuit").css("border-left-width","2px").animate({

				height:"230px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit7 = function() {



			$("#lawsuit-9").css({"border-width":"2px","display":"block"}).animate({

				height:"170px",

				width:"1000px",



			},200);



			$("html, body").animate({scrollTop: "+=250"}, 600);



			return $(".timeline-element");

		}



		s8lLawsuit8 = function() {



			$("#section-9 .line-2").css({

				borderLeftWidth:"2px",

				width:"0",

				float:"none",

				margin:"auto",

				left:"0",

				borderStyle:"solid",

			}).animate({

				height:"100px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit9 = function() {



			$("#lawsuit-10").css("border-width","2px").animate({

				height:"130px",

				width:"700px",

			},200);



			$("html, body").animate({scrollTop: "+=300"}, 600);



			return $(".timeline-element");

		}



		s8lLawsuit10 = function() {



			$("#section-10-lawsuit .line-1").css("border-left-width","2px").animate({

				height:"100px",

			},200);



			$("#lawsuit-10 .box-text-question i").animate({

				opacity:1

			},400, function() {

				$("#lawsuit-10").addClass("overflow");

			});



			return $(".timeline-element");

		}



		s8lLawsuit11 = function() {



			$("#section-10-lawsuit .line-2").css("border-left-width","2px").animate({

				height:"100px",

			},200);



			$("#lawsuit-11").css("border-width","2px").animate({

				height:"180px",

				width:"450px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit12 = function() {



			$("#lawsuit-12").css("border-width","2px").animate({

				height:"160px",

				width:"400px",

			},200);



			$("html, body").animate({scrollTop: "+=100"}, 600);



			$("#lawsuit-11 .box-text-question i").animate({

				opacity:1

			},400, function() {

				$("#lawsuit-11").addClass("overflow");

			});



			return $(".timeline-element");

		}



		s8lLawsuit13 = function() {



			$("#section-10-lawsuit .line-4").css("border-left-width","2px").animate({

				height:"100px",

			},200);



			return $(".timeline-element");

		}



		s8lLawsuit14 = function() {



			$("#section-10-lawsuit #lawsuit-finished").css("border-width","2px").animate({

				height:"120px",

				width:"400px",

			},200);



			stopbounce = false;

			bounce(7);



			$("#expand-arrow-7 i").css({

				display:"block"

			}).animate({

				opacity:1,

			},200);



			

			return $(".timeline-element");

		}



		runAnimations([s8lLawsuit1,s8lLawsuit2,s8lLawsuit3,s8lLawsuit4,s8lLawsuit5, s8lLawsuit6, s8lLawsuit7, s8lLawsuit8, s8lLawsuit9, s8lLawsuit10, s8lLawsuit11, s8lLawsuit12, s8lLawsuit13, s8lLawsuit14]);

	});



	// Phase 9 Lawsuit

	$("#expand-arrow-7 i").click(function() {



		stopbounce = true;



		s10lLawsuit1 = function() {

			

			$("#expand-arrow-7 i").slideUp(100);



			return $(".timeline-element");

		}



		s10lLawsuit2 = function() {

			

			$("#section-10-lawsuit .line-3").css("border-left-width","2px").animate({

					height:"120px",

				},200);



			return $(".timeline-element");

		}



		s10lLawsuit3 = function() {



			$("#case-trial").css("border-width","2px").animate({

				height:"105px",

				width:"500px",

			},200);



			$("html, body").animate({scrollTop: "+=200"}, 600);



			return $(".timeline-element");

		}



		s10lLawsuit4 = function() {

			

			$("#section-10-lawsuit .line-5").css({

				"border-left-width":"2px",

				"margin": "0 auto",

			}).animate({

				height:"100px",

			},200, function() {

				$(this).css({

					"border-bottom-width":"2px",

				}).animate({

					width: "50%",

					left: "139px",

				},200);

			});



			return $(".timeline-element");

		}



		s10lLawsuit5 = function() {

			

			$("#section-10-lawsuit .line-6").css("border-left-width","2px").animate({

					height:"100px",

				},200);



			return $(".timeline-element");

		}



		s10lLawsuit6 = function() {



			$("#verdict").css("border-width","2px").animate({

				height:"190px",

				width:"500px",

			},200);



			$("html, body").animate({scrollTop: "+=300"}, 600);



			return $(".timeline-element");

		}



		s10lLawsuit7 = function() {



			$("#part-4").delay(1500).slideDown(1000, function() {

				$("html, body").animate({scrollTop: "+=100"}, 500);

			});





			return $(".timeline-element");

		}



		runAnimations([s10lLawsuit1, s10lLawsuit2, s10lLawsuit3, s10lLawsuit4, s10lLawsuit5, s10lLawsuit6, s10lLawsuit7]);

	});



	$(".reset-button").click(function() {

		resetTimeline();

	});



	$("#bottom-consultation").click(function() {

		$("#part-5").slideDown();

		$("html, body").animate({scrollTop: "+=450"}, 500);

	});



	$("#contact-injuries-box").on("click", "span", function() {

		var number = $(this).parents(".evaluation-injury").attr('id');

		$(this).parents(".evaluation-injury").remove();

		delete injuriesObject[number];

		var damages;

		switch (number.substring(7)) {

			case "1":

				damages = "<i class='fa fa-hospital-o'></i> Hospital/ICU Bills";

				$( "i[data-checkbox='icu']" ).parents("li").trigger( "click" );

				break;

			case "2":

				damages = "<i class='fa fa-heart'></i> Surgery Payments";

				$( "i[data-checkbox='surgery']" ).parents("li").trigger( "click" );

				break;

			case "3":

				damages = "<i class='fa fa-male'></i> Physical Therapy Bills";

				$( "i[data-checkbox='physical-therapy']" ).parents("li").trigger( "click" );

				break;

			case "4":

				damages = "<i class='fa fa-medkit'></i> Cost of Medications";

				$( "i[data-checkbox='medication']" ).parents("li").trigger( "click" );

				break;

			case "5":

				damages = "<i class='fa fa-user-md'></i> Cost of Specialist Visits";

				$( ".timeline-checked[data-checkbox='practitioner']" ).parents("li").trigger( "click" );

				break;	

		}



		for (var i=damagesArray.length-1; i>=0; i--) {

		    if (damagesArray[i] === damages) {

		        damagesArray.splice(i, 1);

		    }

		}

	});

	

	var form = $("#timeline-evaluation-form");

	form.submit(function(event) {

		var formMessages = $('#contact-form-messages');
		// Stop the browser from submitting the form.
		    event.preventDefault();
		    var formData = form.serialize();
		    var injuryData = JSON.stringify(injuriesObject);

			//Submitting the form via ajax.
			$.ajax({
				type: 'POST',
				url: "https://www.bestattorney.com/template-files/contact-files/timeline-contact.php",
				data: (formData+"&injuryData="+injuryData),
			})
			.done(function(response) {
				$(formMessages).text("Thank You for submitting your case for a free evaluation! We will get back to you as soon as possible.");
			})
			.fail(function(data) {
			    if (data.responseText !== '') {
			        $(formMessages).text(data.responseText);
			    } else {
			        $(formMessages).text('We\'re sorry, An error occured and your message could not be sent.');
			    }
			});

	});



	$(window).scroll(function(e){

		parallax();

		if ($(window).scrollTop() >= 750 && $(window).scrollTop() < $("#timeline").innerHeight() && $(window).scrollTop() < 3500) {

		  	$("#fixed-reset-bar").stop().slideDown();

		} else {

		  	$("#fixed-reset-bar").stop().slideUp();

		}

	});



	$("#view-static").click(function() {
		window.location = "https://www.bestattorney.com/resources/case-timeline-static.html";
	});

	





});