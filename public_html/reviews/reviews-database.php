<?php



$reviews = array(



	array(

			"id" => 1,

			"reviewer" => "Maria Vasquez",

			"reviewExcerpts" => array("Colleen... emailed me, called me, sent me words of encouragement almost every week! When I [would] least expect it and I had a horrible day or had many questions and concerns she was there!", "I will always remember the team of professionals and I would recommended Bisnar Chase to anyone."),

			"fullReview" => "I don’t even know where to start, because I really wanted to write this recommendation even if there was no star or rating to be given!

							My daughter and I were in a bad car accident in 2011. She received a broke jaw, broken pelvic, broken arm, and a ruptured ear drum. I prayed to the lord while I was in ICU to guide me and send me to the right attorney that would help me and my daughter and not just look at us as a monetary opportunity. After much research I choose Bisnar Chase. <br>

							After spending 2 months in the ICU, we were in a long recovery process. Learning to walk, to eat due to wire shut for months! Back to the basic all over again…

							4 years later it just came to an end! I tell you I have 2 different paralegals assisting me but Colleen Cadogan was the best I had this past year…

							This lady, emailed me, called me, send me words of encouragement almost every week! When I least expect it and I had a horrible day or had many questions and concerns she was there! As the paralegal doing her job, but also as a human being, a mother, a wife….just genuinely concern for my daughter and I.

							As the case came to a close….I will always remember the team of professionals and I would recommended Bisnar Chase to anyone….and be patient, patience",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-maria-vasquez.png",

			"caseType" => array(),

	),

	array(

			"id" => 2,

			"reviewer" => "Vanessa Sampson",

			"reviewExcerpts" => array("After a bad accident... we contacted Bisnar Chase and they took care of everything.  It gave us the peace of mind we needed to heal.", "I am very impressed with their level of professionalism and their compassion.  We definitely recommend Bisnar Chase."),

			"fullReview" => "After a bad accident, we were lost on how to handle the insurance agents, the paperwork involved and the claims representatives.  We contacted Bisnar Chase and they took care of everything.  It gave us the peace of mind we needed to heal.  I am very impressed with their level of professionalism and their compassion.  We definitely recommend Bisnar Chase.",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-vanessa-sampson.png",

			"caseType" => array(),

	),

	array(

			"id" => 3,

			"reviewer" => "Eva Shaffer",

			"reviewExcerpts" => array("I have used Bisnar and Chase twice now, both for car accidents. Each time I received a fair settlement and honestly received more than I expected. The entire team of Bisnar and Chase were professional, courteous and friendly. The paralegals there are top notch!", "I cannot express enough how much I appreciated the constant communication from them and how they explained things I didn&#39;t understand in such a way that I could.", "I highly recommend Bisnar and Chase if you are considering hiring a lawyer for an automobile accident."),

			"fullReview" => "I have used Bisnar and Chase twice now, both for car accidents. Each time I received a fair settlement and honestly received more than I expected. The entire team of Bisnar and Chase were professional, courteous and friendly. The paralegals there are top notch! I cannot express enough how much I appreciated the constant communication from them and how they explained things I didn&#39;t understand in such a way that I could. <br> Will I ever use them again? I only hope not to get into another car accident ever so that I don&#39;t have to, but I will definitely use them if I do (knock on wood I don&#39;t). I highly recommend Bisnar and Chase if you are considering hiring a lawyer for an automobile accident.",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-eva-shaffer.png",

			"caseType" => array(),

	),

	array(

			"id" => 4,

			"reviewer" => "Paige Montgomery",

			"reviewExcerpts" => array("No problems and very professional. I was kept in the loop through the whole process and was able to get a better settlement than my insurance company said I would."),

			"fullReview" => "Great people in this office, everyone was really helpful explaining everything. I was referred by my aunt for my car accident in October and the case went pretty fast. No problems and very professional. I was kept in the loop thru [<em>sic</em>] the whole process and was able to get a better settlement than my insurance company said I would. I cant really compare them to other law firms because it was the first time I had to use an attorney, but my bad experience with the car accident was handled as good as I could have hoped.",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-paige-montgomery.png",

			"caseType" => array(),

	),

	array(

			"id" => 5,

			"reviewer" => "Maddy King",

			"reviewExcerpts" => array("Bisnar Chase is an outstanding firm... I highly recommend this firm."),

			"fullReview" => "Bisnar Chase is an outstanding firm. After working with them for several years, I have been consistently impressed by their expertise and willingness to help. I highly recommend this firm.",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-maddy-king.png",

			"caseType" => array(),

	),

	array(

			"id" => 6,

			"reviewer" => "Harley Howard",

			"reviewExcerpts" => array("I have nothing but high praise for Bisnar & Chase and would highly recommend them without question."),

			"fullReview" => "September 15, 2012, I was T-Boned by a lady at 45 miles an hour, who was busy \"adjusting her GPS\" instead of watching where she was going. Needless to say the next few moments were very important. After thanking God that I was not paralyzed, I made sure that after I called my wife that the second call was going to be to Bisnar & Chase.  I am familiar with this fine law firm because both my wife and daughter were in a car accident in 2008 and Bisnar & Chase came to our aid. <br>From all the staff to Mr Bisnar himself, they came through for me as they did for my family 5 years ago. The outcome of my case was more than I expected.  I have nothing but high praise for Bisnar & Chase and would highly recommend them without question.<br> Dr. Harley Howard",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-harley-howard.png",

			"caseType" => array(),

	),

	array(

			"id" => 7,

			"reviewer" => "[Anonymous]",

			"reviewExcerpts" => array("I received a great settlement, things moved quickly, and they still keep in contact with me to see how I am doing. Really great law firm."),

			"fullReview" => "John Bisnar and Brian chase were a gift from god. I had a lot of questions and no answers after my car accident and they were able to satisfy all of my needs throughout the duration of my case. I received a great settlement, things moved quickly, and they still keep in contact with me to see how I am doing. Really great law firm.",

			"stars" => 5,

			"reviewedAt" => "Google",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	),

	array(

			"id" => 8,

			"reviewer" => "Chris O.",

			"reviewExcerpts" => array("I... experienced a friendly staff (especially the receptionist Loraine) that treats every one of their clients like they are movie stars."),

			"fullReview" => "Like Danielle, I have been waiting a long time to review Bisnar  Chase too.  My review is a little different, however.  I have only experienced a friendly staff (especially the receptionist Loraine) that treats everyone of their clients like they are movie stars.  If I had a friend that was in an accident these guys would be the first people I would send them to.",

			"stars" => 4,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-06-05", "June 5th, 2011"],

	),

	array(

			"id" => 9,

			"reviewer" => "Bruce Anguiano",

			"reviewExcerpts" => array("I would like to thank Bisnar and Chase... They were very professional and extremely effective."),

			"fullReview" => "I would like to thank Bisnar and Chase for the wonderful job that they accomplished on my personal injury case. They were very professional and extremely effective. Thanks again.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2016-01-26", "January 26th, 2016"],

	),

	array(

			"id" => 10,

			"reviewer" => "Kathy E.",

			"reviewExcerpts" => array("Bisnar/Chase was there to inform me every step of the way as to what was going on. They answer calls and emails right away, and I was never left wondering what was going on.", "The whole staff is caring and friendly and everyone there treated us like family.", "I had nothing but a positive experience with them and I would recommend Bisnar/Chase to all of my friends and family or to anyone who is in need of legal help."),

			"fullReview" => "My grandson and I were involved in a roll-over accident in which he broke his ankle, and I broke my neck. Unfortunately it was caused by a hit and run driver who got away. One call to Bisnar/Chase and they came right away. Of course, my case took about 3 years to settle, but Bisnar/Chase was there to inform me every step of the way as to what was going on. They answer calls and emails right away, and I was never left wondering what was going on. The whole staff is caring and friendly and everyone there treated us like family. Not to mention the fact that they never seemed to forget you, by sending birthday cards with BaskinRobbin gift cards and on New Years they always sent the fancy fortune cookies! I always thought that was very thoughtful and caring of them. I had nothing but a positive experience with them and I would recommend Bisnar/Chase to all of my friends and family or to anyone who is in need of legal help.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-kathy-e.png",

			"caseType" => array(),

			"dateReviewed" => ["2015-01-08", "January 8th, 2015"],

	),

	array(

			"id" => 11,

			"reviewer" => "Brady M.",

			"reviewExcerpts" => array("John Bisnar was so professional and caring... His staff was amazing, friendly and seemed to really care about me as a person.", "I couldn&#39;t be happier with the level of service I received.", "I was constantly updated on my case status and received far more from my settlement than I expected."),

			"fullReview" => "John Bisnar was so professional and caring. When I found myself in this situation and never having a car accident before I didn&#39;t know what steps to take. Mr. Bisnar explained every single detail to me and what to expect. His staff was amazing, friendly and seemed to really care about me as a person. I lost all doubt I may have had about dealing with personal injury lawyers and couldn&#39;t be happier with the level of service I received.  I was constantly updated on my case status and received far more from my settlement than I expected.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-brady-m.png",

			"caseType" => array(),

			"dateReviewed" => ["2014-05-30", "May 30th, 2014"],

	),

	array(

			"id" => 12,

			"reviewer" => "Shannon F.",

			"reviewExcerpts" => array("I fully knew what to expect and was informed every step of the way.", "In addition to my settlement which I was really happy with, I couldn&#39;t believe how great their whole staff treated me.", "I really felt like &#39;me&#39; as a person mattered a lot to them. Would highly recommend!"),

			"fullReview" => "My sister in law referred me to Bisnar Chase after I was rear ended on the 405 in January.  They went over everything with me for best and worst case scenarios of my car accident claim. I fully knew what to expect and was informed every step of the way.  That was a major factor to me!  In addition to my settlement which I was really happy with, I couldn&#39;t believe how great their whole staff treated me.  I really felt like \"me\" as a person mattered a lot to them. Would highly recommend!",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-shannon-f.png",

			"caseType" => array(),

			"dateReviewed" => ["2014-04-16", "April 16th, 2014"],

	),

	array(

			"id" => 13,

			"reviewer" => "Kim E.",

			"reviewExcerpts" => array("They listened to me, answered my questions, advised me and kept me informed.", "Starting with the receptionist, my paralegal, the paralegal supervisor, Mrs. De La Torre, Mrs. Barker and Mr. Bisnar himself, everyone was helpful, friendly and professional.", "Four of my family members have used these guys and had equally good experiences."),

			"fullReview" => "I am very pleased with the results this law firm obtained for me. Their professionalism, attentiveness and friendliness was outstanding. They listened to me, answered my questions, advised me and kept me informed. A truck hit my car causing me serious injuries, medical bills, lost time from work and damaged to my car. They took care of everything wonderfully. They helped with getting my car repaired and a rental car. They helped me get medical help. They got my doctor bills paid, kept me from having to go to court and obtained a settlement for me beyond my expectations. Four of my family members have used these guys and had equally good experiences.  One of my relatives consulted with them and got great advise about handling her matter. Mr. Bisnar told her she didn&#39;t need an attorney and told her how to handle it. She couldn&#39;t believe the compassion and great advise, especially without a fee. Mr. Bisnar helped my brother-in-law get a multiple six-figure recovery in an employment law dispute. Starting with the receptionist, my paralegal, the paralegal supervisor, Mrs. De La Torre, Mrs. Barker and Mr. Bisnar himself, everyone was helpful, friendly and professional. It was a refreshing experience after a bad accident. They send me a birthday card with a Baskin Robins gift certificate every year. In January they send me chocolate dipped fortune cookies. They have even offered me Lakers&#39; tickets. They certainly didn&#39;t forget me after my case was done.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-kim-e.png",

			"caseType" => array(),

			"dateReviewed" => ["2014-03-20", "March 20th, 2014"],

	),

	array(

			"id" => 14,

			"reviewer" => "Ailsie C.",

			"reviewExcerpts" => array("The communication is fantastic, my case isn&#39;t worth a lot, but it would  appear like they enjoy their work, rather than trying to leech out my money.", "I would recommend them as well, very professional and efficient."),

			"fullReview" => "I went to them from a recommendation from my tax attorney. They have a good rap, and they talk the talk, so far they have walked the walk. I will update after the court case happens. The communication is fantastic, my case isn&#39;t worth a lot, but it would  appear like they enjoy their work, rather than trying to leech out my money. They don&#39;t bill me for every phone call, which is nice!! I would recommend them as well, very professional and efficient.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-ailsie-c.png",

			"caseType" => array(),

			"dateReviewed" => ["2012-12-07", "December 7th, 2012"],

	),

	array(

			"id" => 15,

			"reviewer" => "Clark G.",

			"reviewExcerpts" => array("Undoubtedly the best firm I&#39;ve ever encountered... it felt like a real place where work gets done.", "These guys bring the word classy back to law. I definitely can recommend these guys."),

			"fullReview" => "Undoubtedly the best firm I&#39;ve ever encountered. It&#39;s not a huge firm, as I only worked with a few people, but it felt like a real place where work gets done. I never felt like a \"little person,\" as my case wasn&#39;t small, but it wasn&#39;t huge either. I know lawyers have gotten a bad rap, but these guys bring the word classy back to law. I definitely can recommend these guys.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-clark-g.png",

			"caseType" => array(),

			"dateReviewed" => ["2012-12-04", "December 4th, 2012"],

	),

	array(

			"id" => 16,

			"reviewer" => "John P.",

			"reviewExcerpts" => array("I had a really good experience with this firm... I had a really good experience with this firm.", "I didn&#39;t have a huge case, but they still called me on a regular basis and wrapped things up pretty quickly after I finished treating."),

			"fullReview" => "I put up something about these guys before, but its gone now...

							I had a really good experience with this firm. I didn&#39;t have a huge case, but they still called me on a regular basis and wrapped things up pretty quickly after I finished treating.

							I had a really good experience with this firm.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-john-p.png",

			"caseType" => array(),

			"dateReviewed" => ["2012-06-29", "June 29th, 2012"],

	),

	array(

			"id" => 17,

			"reviewer" => "Stephanie C.",

			"reviewExcerpts" => array("The fact they took the time out of their busy day to respond so thoroughly to a cry for help (in which they had absolutely nothing to gain), speaks volumns about the integrity of this firm."),

			"fullReview" => "INTEGRITY, CONFIDENCE AND THOROUGHNESS-THIS FIRM IS AMAZING!

							when my German Shepherd was falsely accused of biting a child, I thought, who better than the best personal injury attorney out there to ask for advise?

							Researched best attorneys in OC for dog bites, and was directed to the law firm of Bisnar and Chase.

							To say I was astounded at the depth of the response I received to my online enquiry is an understatement. <br>Upfront they said they weren&#39;t defense attorneys, but (after winning numerous severe dog bite cases over the years) they took the time to line item their opinion of how events would unfold (putting my mind at ease).

							In this day and age, the fact they took the time out of their busy day to respond so thoroughly to a cry for help ( inwhich they had absolutely nothing to gain), speaks volumns about the integrity of this firm. I wouldn&#39;t hesitate to use them, or refer them.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2012-05-11", "May 11th, 2012"],

	),

	array(

			"id" => 18,

			"reviewer" => "Amanda P.",

			"reviewExcerpts" => array("These people were so unbelievably nice. When you go to a personal injury law firm, you dont expect to like the people that are representing you.", "They did a great job with my case and kept me informed throughout the whole process. Definite 5 stars."),

			"fullReview" => "These people were so unbelievably nice. When you go to a personal injury law firm, you dont expect to like the people that are representing you. They did a great job with my case and kept me informed throughout the whole process. Definite 5 stars.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-amanda-p.png",

			"caseType" => array(),

			"dateReviewed" => ["2012-12-22", "December 22nd, 2012"],

	),

	array(

			"id" => 19,

			"reviewer" => "James G.",

			"reviewExcerpts" => array("I had a great experience with this firm. My attorney did a great job and I had a great result."),

			"fullReview" => "I wanted to wait to put this up until I receive my check. I had a great experience with this firm. My attorney did a great job and I had a great result. I dont know of they are different than they used to be, but they were exactly what I was looking for.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-12-15", "December 15th, 2011"],

	),

	array(

			"id" => 20,

			"reviewer" => "Rachel R.",

			"reviewExcerpts" => array("Their staff was friendly and helpful throughout the entire process."),

			"fullReview" => "Great experience all around. Their staff was friendly and helpful throughout the entire process. They forward all of your fees till your case gets resolved.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-rachel-r.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-12-09", "December 9th, 2011"],

	),

	array(

			"id" => 21,

			"reviewer" => "Frank D.",

			"reviewExcerpts" => array("Bisnar Chase rocked my case"),

			"fullReview" => "Bisnar chase rocked my case. I got a nice chunk of change an [<em>sic</em>] they were pretty cool to me.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-frank-d.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-11-29", "November 29th, 2011"],

	),

	array(

			"id" => 22,

			"reviewer" => "Jacob C.",

			"reviewExcerpts" => array("They were quick, efficient and friendly. Ended up with a great settlement."),

			"fullReview" => "I did a car accident case with these guys. They were quick, efficient and friendly. Ended up with a great settlement. Why did I get the royal treatment? No idea.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-jacob-c.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-11-29", "November 29th, 2011"],

	),

	array(

			"id" => 23,

			"reviewer" => "Denisse S.",

			"reviewExcerpts" => array("Sent them some questions by email of my case and were very fast on getting back to me with sound advice."),

			"fullReview" => "Were very helpful, even if you don&#39;t hire them. Sent them some questions by email of my case and were very fast on getting back to me with sound advice. Didn&#39;t end up needing to get a lawyer, and it was very comendable on their part to be honest with their advice.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2011-11-18", "November 18th, 2011"],

	),

	array(

			"id" => 24,

			"reviewer" => "Gloria C.",

			"reviewExcerpts" => array("Jerusalem Beligan and the entire staff impressed me every step of the way.", "From the moment I walked through the door I knew I was going to be taken care of, and I was."),

			"fullReview" => "BISNAR | CHASE did a great job in representing me. Jerusalem Beligan and the entire staff impressed me every step of the way. From the moment I walked through the door I knew I was going to be taken care of, and I was.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2010-12-22", "December 22nd, 2010"],

	),

	array(

			"id" => 25,

			"reviewer" => "M E.",

			"reviewExcerpts" => array("[Your clients] both felt cared for and taken care of every step of the way!"),

			"fullReview" => "I just wanted everyone to know how grateful Miguel and Luga were to your office and everyone that participated in their case. I met with them this morning to give them their settlement checks - Miguel stated from the first time he walked into our office and from walking out the door today was one of the most heartwarming experiences he has ever had. They both felt cared for and taken care of every step of the way! I just wanted to pass that on to each of you and to applaud you for such a fine job!",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2009-09-17", "September 17th, 2019"],

	),

	array(

			"id" => 26,

			"reviewer" => "K N.",

			"reviewExcerpts" => array("I cannot begin to articulate our gratitude to the Bisnar Chase staff - we are so very thankful for all you have done for [our son].", "You and your staff have been an integral part of Josh&#39;s recovery, and we will be forever grateful."),

			"fullReview" => "We received an email from A.G. Edwards stating they have received the settlement check from Nissan. Once again, I cannot begin to articulate our gratitude to the Bisnar Chase staff - we are so very thankful for all you have done for Josh. As parents, Charlotte and I can rest in peace, when that day comes, knowing that Josh&#39;s needs have been taken care of, and his financial future is in good hands with sound financial planners at A.G. Edwards.

							On December 17, 2003 we received a call from the paramedics stating that Josh and Lisa had been in a severe accident, and we needed to meet them at the emergency room. I remember telling God that I can deal with missing limbs because they can be replaced, but please do not take him from us. A month later we met with members of your staff for the first time, Josh had a traumatic brain injury and his future was looking pretty bleak; we had no idea where he would be eighteen months later. All we could think of at the time was that our son&#39;s future, his goals, and dreams had been taken from him so abruptly.

							As time passed, Josh continued to improve and he began his journey of recovery. Many have been involved in this process, and you and your staff have been an integral part of Josh&#39;s recovery, and we will be forever grateful. God has brought so many people into our lives, and we are so very blessed. Numerous emails were sent to Personal Injury Attorney&#39;s. John...you were on a ski trip at Big Bear with your family, and you were the only Personal Injury Attorney to respond - I believe this was divine intervention. Josh has a professor from Mt. San Jacinto College who has taken him \"under his wing,\" and is showing him other career possibilities.

							John, the future looks good for Josh - thank you!",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

			"dateReviewed" => ["2009-09-17", "September 17th, 2009"],

	),

	array(

			"id" => 27,

			"reviewer" => "Melissa",

			"reviewExcerpts" => array("Everyone has been great. I really don&#39;t think anything could have been done better."),

			"fullReview" => "Great Lawfirm – My sister Kristen in Chicago made the recommendation. Everyone has been great. I really don&#39;t think anything could have been done better.",

			"stars" => 5,

			"reviewedAt" => "City Search",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	),

	array(

			"id" => 28,

			"reviewer" => "Kona Kim",

			"reviewExcerpts" => array("[Bisnar Chase] has a great reputation as being hardworkers for their clients. It was a pleasure to deal with everyone."),

			"fullReview" => "Everyone at Bisnar | Chase was so nice. They have a great reputation as being hardworkers for their clients. It was a pleasure to deal with everyone.",

			"stars" => 5,

			"reviewedAt" => "Yahoo!",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	),

	array(

			"id" => 29,

			"reviewer" => "Morgana R",

			"reviewExcerpts" => array("I will definitely not hesitate to contact these attorneys for any of my legal needs."),

			"fullReview" => "Recently, I had to find a personal injury attorney on a case that had special circumstances. I had a car accident that is almost due to expire and unfortunately, in the beginning,  I decided no [<em>sic</em>] to get an attorney. And oh boy am I regretting it.

							I didn&#39;t have a lot of respect for lawyers before this. And because of that, I made the decision not to represent myself two years ago.

							Altho [<em>sic</em>] they were not able to help me, Mr. Bisnar was so helpful that he directed me in a manner that I didn&#39;t need an attorney at all. He was funny and charming. It was my fault for not going the standard route, but next time, I will definitely not hesitate to contact these attorneys for any of my legal needs.

							I was VERY impressed. I don&#39;t use those words lightly (especially about attorneys!) so this is no bull! neither was Mr. Bisnar of Bisnar and Chase.",

			"stars" => 4,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-morgana-r.png",

			"caseType" => array(),

	),

	array(

			"id" => 30,

			"reviewer" => "Chelsea C.",

			"reviewExcerpts" => array("[My mom&#39;s] client experience with the associates at Bisnar & Chase was so impressive that she raved to me about every time she heard anything from them.", "The Bisnar & Chase team really went above and beyond my family&#39;s expectations.  And for that I am extremely grateful.", "If you live in the... General Orange County area I would highly recommend Bisnar & Chase to both friends, and family."),

			"fullReview" => "I could not recommend Bisnar & Chase highly enough!  Now, I must be forthcoming and tell you that I am writing this review on behalf of my wonderful mother.  You see, she&#39;s a mom, and therefor [<em>sic</em>] by nature is completely unable to do even the most simple of tasks on the internet.  Like writing a a Yelp review.  But we all love our moms don&#39;t we?

							Yes.  We all love our moms (except Charles Manson), and would do anything to make sure they are protected and well taken care of.  Upon referral from my mother&#39;s coworker, we contacted Bisnar & Chase.  They had done a great job in this previous case and were located extremely close to both UCI and home, which made stopping by their office very convenient for my Mom.

							My Mom had been in an auto accident, nothing crazy but not a mere fender bender, and suffered what we thought to be some minor whiplash.  Months later this was definitely not the case. Her client experience with the associates at Bisnar & Chase was so impressive that she raved to me about every time she heard anything from them.  How thorough they were. How they listened better than I did (or something to that effect). How she liked Shannon and Mr. Bisnar and felt that they really had her best interests at heart. I honestly couldn&#39;t get her to shut up about them.

							But as a daughter that last part was all that I needed to hear to know that I we had found the right people and I could sleep easy at night knowing my Mom was in good hands.  The Bisnar & Chase team really went above and beyond my family&#39;s expectations.  And for that I am extremely grateful.

							If you live in the Newport Beach/Irvine/Costa Mesa/General Orange County area I would highly recommend Bisnar & Chase to both friends, and family.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-chelsea-c.png",

			"caseType" => array(),

	),

	array(

			"id" => 31,

			"reviewer" => "Cynthia",

			"reviewExcerpts" => array("I knew Bisnar Chase would help me. Your law firm is excellent and makes me wish I could work there!"),

			"fullReview" => "I knew Bisnar Chase would help me. Your law firm is excellent and makes me wish I could work there!",

			"stars" => 5,

			"reviewedAt" => "Super Pages",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	),

	array(

			"id" => 32,

			"reviewer" => "Jen",

			"reviewExcerpts" => array("Anytime I had a question I understood the response and would be responded to in a timely fashion."),

			"fullReview" => "Anytime I had a question I understood the response and would be responded to in a timely fashion. I really enjoyed talking to Lorraine. She was so bubbly and made me feel appreciated as a client. All the ladies I spoke to were wonderful.",

			"stars" => 5,

			"reviewedAt" => "Super Pages",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	),

	array(

			"id" => 33,

			"reviewer" => "Karla Hawley",

			"reviewExcerpts" => array("I really love how this firm cares for the people they represent."),

			"fullReview" => "While my wrongful death case is still being decided I really love how this firm cares for the people they represent I feel so much better after talking with Doug on a monthly basis he reassures me things will be okay. Looking for to ending this real soon but I am very thankful to Bisnar and Chase for all they do!",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-karla-hawley.png",

			"caseType" => array("wrongful-death"),

	),

	array(

			"id" => 34,

			"reviewer" => "Thomas Arcala",

			"reviewExcerpts" => array("Not only did they do a fantastic job during the whole process, they got me a little more money too."),

			"fullReview" => "I was in an accident and it was the other persons fault. I call [sic] Bisnar Chase and they came to my door and took down all the information to get the case going. Not only did they do a fantastic job during the whole process, they got me a little more money too. ;) Thank you Bisnar Chase and Staff for your help. It truly has made a difference.",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-thomas-arcala.png",

			"caseType" => array("accident"),

	),

	array(

			"id" => 35,

			"reviewer" => "Alexis Ashburn",

			"reviewExcerpts" => array("I cannot speak highly enough of Bisnar and Chase.", "Bisnar and Chase... stayed diligent throughout our long federal case. They handled everything start to finish and held our hands the whole way!"),

			"fullReview" => "I cannot speak highly enough of Bisnar and Chase. They represented us when we were hit by a government drunk driver on base while my husband was enlisted in April 2010 and stayed diligent throughout our long federal case. They handled everything start to finish and held our hands the whole way!",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-alexis-ashburn.png",

			"caseType" => array("accident"),

	),

	array(

			"id" => 36,

			"reviewer" => "Amy Leone",

			"reviewExcerpts" => array("I called Bisnar Chase, and was able to get information that helped me deal with the insurance on my own."),

			"fullReview" => "My car accident happened in April. I called Bisnar Chase, and was able to get information that helped me deal with the insurance on my own. They didn&#39;t take my case, but Mary took my call both times I called; she was so very helpful!! I hope I don&#39;t need them again, but if I do, I know they&#39;ll help me. FIVE STARS!!",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-amy-leone.png",

			"caseType" => array("accident"),

	),

	array(

			"id" => 37,

			"reviewer" => "Jen Hefler",

			"reviewExcerpts" => array("I shouldn&#39;t have waited so long to call an attorney. Thank you isn&#39;t enough."),

			"fullReview" => "Happy the way my case went. The only thing that got the other insurance company to hear me was \"here is the number to my attorney\", I shouldn&#39;t have waited so long to call an attorney. Thank you isn&#39;t enough.",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-jen-hefler.png",

			"caseType" => array(""),

	),

	array(

			"id" => 38,

			"reviewer" => "Krystal Mead",

			"reviewExcerpts" => array("Thank you for your professionalism, and for getting my case settled.", "This was not my first accident, Bisnar Chase handled what I definitely didn&#39;t want to deal with."),

			"fullReview" => "This was not my first accident, Bisnar Chase handled what I definitely didn&#39;t want to deal with. Thank you for your professionalism, and for getting my case settled.",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-krystal-mead.png",

			"caseType" => array(""),

	),
	array(

			"id" => 39,

			"reviewer" => "Walter Temple",

			"reviewExcerpts" => array("Thank you for your professionalism, and for getting my case settled.", "This was not my first accident, Bisnar Chase handled what I definitely didn&#39;t want to deal with."),

			"fullReview" => "John and Brian handled a pi case over 10 years ago. I fill [sic] real lucky to have had them on my side. They are the best. No doubt about it thanks. Walter temple",

			"stars" => 5,

			"reviewedAt" => "Facebook",

			"thumbnailPath" => "/images/review-thumbs/thumb-walter-t.png",

			"caseType" => array(""),

	),
	array(

			"id" => 40,

			"reviewer" => "Kirsti O.",

			"reviewExcerpts" => array("[John Bisnar] is a genuine, helpful and kind man who I can promise has your best interests at heart."),

			"fullReview" => "I live the UK. I recently had fertlity treatment in California and encountered some problems with the clinic. I contacted Mr Bisnar and although my case was small he talked me through negotiations with the clinic I finally managed to get all my fees back from the clinic and would have not been able to do it without Mr Bisnar as I knew nothing about California law. He is a genuine, helpful and kind man who I can promise has your best interests at heart.",

			"stars" => 5,

			"reviewedAt" => "Yelp",

			"thumbnailPath" => "/images/review-thumbs/thumb-anonymous.png",

			"caseType" => array(),

	)

);







?>
