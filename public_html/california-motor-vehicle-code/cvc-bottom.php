<div class="flex-container">
	<div class="border-box cvc-resources">
		<div>
			<a href="https://www.bestattorney.com/giving-back/"><img data-src="/images/cvc-raffle.jpg" class="" alt="Client Appreciation"></a>
		</div>
		<h4 class="centered">Giving back!</h4>
		<p>Please visit our Giving back page</p>
	</div>
	<!-- <div class="border-box cvc-resources">
		<div>
			<a href="https://www.bestattorney.com/faqs/"><img data-src="/images/cvc-faqs.jpg"  class="" alt="FAQs about personal injury"></a>
		</div>
		<h4 class="centered">FAQs About Car Accident Injuries</h4>
		<p>Some of the most frequently asked questions about car accidents and the experience of hiring a lawyer.</p>
	</div> -->
	<div class="border-box cvc-resources">
		<div>
			<a href="https://www.bestattorney.com/faqs/"><img data-src="/images/cvc-faqs.jpg" class="" alt="FAQs about personal injury"></a>
		</div>
		<h4 class="centered">FAQs About Car Accident Injuries</h4>
		<p>Some of the most frequently asked questions about car accidents and the experience of hiring a lawyer.</p>
	</div>
	<div class="border-box cvc-resources">
		<div>
			<a href="https://www.bestattorney.com/resources/"><img data-src="/images/cvc-resources.jpg" class="" alt="Personal Injury Resources"></a>
		</div>
		<h4 class="centered">Resources for Personal Injury</h4>
		<p>A compiled list of resources for those who need to know more about personal injury law.</p>
	</div>
	<div class="border-box cvc-resources">
		<div>
			<a href="https://www.bestattorney.com/resources/book-order-form.html"><img data-src="/images/cvc-books.jpg" class="" alt="Bisnar Chase books on personal injury"></a>
		</div>
		<h4 class="centered">Personal Injury Books</h4>
		<p>Books by Bisnar Chase to keep people informed about personal injury. Accident victims can get a free copy!</p>
	</div>
</div>