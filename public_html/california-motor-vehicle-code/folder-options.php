<?php
// Folder Options for /california-motor-vehicle-code

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false, 


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 4,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "accident", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"spanishPageEquivalent" => "",

	"noLiveChat" => true,

	// =====================================
	// Defining Sidebar Content
	// =====================================
	// "sidebarLinksTitle" => "Client Stories",

	// "sidebarLinks" => array(
	 //    ),

	// "showSidebarContactForm" => true,

	// "sidebarContentTitle" => "Our Case Results",

	// "sidebarContentHtml" => "
		
	// ",

    "extraSidebar" => "", 

	"caseResultsArray" => [], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	"headerImageClass" => "cvc", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => true, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>