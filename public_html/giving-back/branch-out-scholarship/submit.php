<?php

include $_SERVER['DOCUMENT_ROOT'].'/template-files/contact-files/contact-submission-scholarship.php';


$contact = new ContactSubmissionScholarship();




// $contact->checkValidations();

// $contact->checkForSpam();

$contact->setUpSendgrid();
$contact->prepareIntakeMessage();
$contact->sendIntakeEmail();
$contact->prepareAutoResponseMessage();
$contact->sendAutoResponseEmail();

if ($contact->inquirySent) {
	header("HTTP/1.0 200 OK");
	exit("Thank You, Your Scholarship Has Been Submitted!");
}




//////////////////////////////////////////////////////
// I GUESS THIS IS BACKUP?
//////////////////////////////////////////////////////



// Workaround Sucuri to get the real IP Address
// if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {
// 	$_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
// }

// //
// // SendGrid PHP Library Example
// //
// // This example shows how to send email through SendGrid
// // using the SendGrid PHP Library.  For more information
// // on the SendGrid PHP Library, visit:
// //
// //     https://github.com/sendgrid/sendgrid-php
// //

// require $_SERVER['DOCUMENT_ROOT'].'/sendgrid-php/vendor/autoload.php';
// require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';


// /* CREATE THE SENDGRID MAIL OBJECT
// ====================================================*/
// $sendgrid = new SendGrid( $sg_username, $sg_password );
// $mail = new SendGrid\Email();
// $mail2 = new SendGrid\Email();

// /* SEND MAIL
// /  Replace the the address(es) in the setTo/setTos
// /  function with the address(es) you're sending to.
// ====================================================*/

// if ($_SERVER["REQUEST_METHOD"] == "POST") {

// 	if ($_POST['form-hp'] != "") {
// 		header("HTTP/1.0 403 Forbidden");
// 		mail($_POST['email'], "Contact Form Error", "This email has triggered several warnings and is considered to have been spamming contact forms. If you have received this message in error, please call us and let us know. Thank you for your help. ");
// 		exit("There was a problem with your submission related to spammy behavior, please try again.");
// 	}

// 	if (trim($_POST['terms']) != "true") {
// 		header("HTTP/1.0 403 Forbidden");
// 		exit("Please Read the Terms and Conditions and Check the Box!");
// 	}

// 	$name = $_POST['name'];
// 	$school = $_POST['school'];
// 	$grade = $_POST['grade'];
// 	$phone = trim($_POST['phone']);
// 	$validphone = true;
//   $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
// 	$q1 = trim($_POST['q1']);
// 	$q2 = trim($_POST['q2']);
// 	$q3 = trim($_POST['q3']);
// 	$q4 = trim($_POST['q4']);
// 	$q5 = trim($_POST['q5']);
// 	$terms = trim($_POST['terms']);

// 	$essay = "\n\nIntroduction: \n";
// 	$essay .= "$q1 \n\n";
// 	$essay .= "Why are you committed to education? \n";
// 	$essay .= "$q2 \n\n";
// 	$essay .= "What are the struggles your community goes through? \n";
// 	$essay .= "$q3 \n\n";
// 	$essay .= "What is your plan to solve or deal with these issues in your community? \n";
// 	$essay .= "$q4 \n\n";
// 	$essay .= "How will your continued education help you make your plan a reality? \n";
// 	$essay .= "$q5 \n";

// 	$phonevalidate = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
// 	if (!preg_match( $phonevalidate, $phone )) {
// 		$validphone = false;
// 	}

// 	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
// 			header("HTTP/1.0 400 Bad Request");
//             echo "Please provide a valid email";
// 			exit;
// 	}
// 	if (!$validphone) {
// 			header("HTTP/1.0 400 Bad Request");
//             echo "Please provide a valid phone number";
// 			exit;
// 	}

// 	$subject = "Scholarship Submission from: ".$name." - Due 12/15/18";
// 	$EmailBody = "
// $name has submitted a scholarship application. \n\n
// Name: $name
// School: $school
// Grade: $grade
// Email Address: $email
// Phone Number: $phone
// Application: $essay\n
// 	";
// 	$EmailFooter="\nSenders IP address: ".$_SERVER['REMOTE_ADDR']."\n\n".$historys."\n\n";
// 	$message = $EmailBody.$EmailFooter;

// 	$EmailResponse = "
// Dear $name,
// Thank you for submitting your scholarship application to us! We will review all of our applicants and choose the best one at the end of the scholarship period. When we choose a winner, we will use the email or phone number to contact our winner. Please ensure that the information you provided is correct.\n\n
// Name: $name
// School: $school
// Grade: $grade
// Email Address: $email
// Phone Number: $phone
// Application: \n$essay\n\n

// Thank you, have a great day!\n\n
// - Bisnar Chase Personal Injury Attorneys\n
// www.bestattorney.com\n
// 	";


// 	try {
// 	    $mail->
// 	    setFrom( "marketing@bestattorney.com" )->
// 	    addTo( "marketing@bestattorney.com" )->
// 	    setSubject($subject)->
// 	    setText($message);

// 	    $response = $sendgrid->send($mail);

// 	    if (!$response) {
// 	        throw new Exception("Did not receive response.");
// 	    } else if ($response->message && $response->message == "error") {
// 	        throw new Exception("Received error: ".join(", ", $response->errors));
// 	    }
// 	} catch ( Exception $e ) {
// 	    var_export($e);
// 	    exit;
// 	}

// 	try {
// 	    $mail2->
// 	    setFrom( "marketing@bestattorney.com" )->
// 	    addTo( $email )->
// 	    setSubject("Thanks for Submitting Your Scholarship - Bisnar Chase")->
// 	    setText($EmailResponse);

// 	    $response2 = $sendgrid->send($mail2);

// 	    if (!$response2) {
// 	        throw new Exception("Did not receive response.");
// 	    } else if ($response2->message && $response2->message == "error") {
// 	        throw new Exception("Received error: ".join(", ", $response2->errors));
// 	    } else {
// 	    	header("HTTP/1.0 200 OK");
// 	        exit("Thank you for your scholarship application!");

// 	    }
// 	} catch ( Exception $e ) {
// 	    var_export($e);
// 	    exit;
// 	}

// }
// else {
// 	header("HTTP/1.0 403 Forbidden");
// 	echo "There was a problem with your submission, please try again.";
// 	exit;
// }

// ?>