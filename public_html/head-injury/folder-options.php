<?php
// Folder Options for /head-injury

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Head Injury Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Brain Trauma" => "/head-injury/brain-trauma.html",
	    "Child Injuries" => "/head-injury/child-injuries.html",
	    "Concussion Symptoms by Gender" => "/head-injury/concussion-symptoms-by-gender.html",
	    "Effects of Concussions" => "/head-injury/effects-of-concussions.html",
	    "Elderly Injuries" => "/head-injury/elderly-injuries.html",
	    "Second Impact Syndrome" => "/head-injury/second-impact-syndrome.html",
	    "Traumatic Brain Injury" => "/head-injury/traumatic-brain-injury.html",
	    "Traumatic Brain Injury FAQ's" => "/head-injury/traumatic-brain-injury-tips.html",
	    "Treatments for Head Injuries" => "/head-injury/treatments.html",
	    "Head Injury Home" => "/head-injury/"
	),

	"sidebarContentTitle" => "Types of Head Injuries",

	"sidebarContentHtml" => "<ul class='bpoint'>
                            <li><b><a href='https://www.bestattorney.com/head-injury/effects-of-concussions.html'>Concussions</a></b>: This is when an individual suffers a blow to the head and experiences an instant loss of awareness or alertness. Some victims of concussions may lose consciousness for seconds, minutes or even hours. Recent studies show that those who suffer more than one concussion, especially as a result of sports injuries, may face long-term consequences such as victims of more severe traumatic brain injuries.</li>
                            <li><b>Skull fractures</b>: A skull fracture is a break or crack in the skull bone. Individuals may suffer a linear skull fracture (a break that does not result in bone movement), depressed skull fracture (part of the skull is sunken in from the trauma), a diastatic skull fracture (breaks along the suture lines in the skull) or basilar skull fracture (break in the bone from the base of the skull).</li>
                            <li><b>Intracranial hematoma</b>: This is when the victim suffers a blood clot in or around the brain. Depending on the type of hematoma injury, the victim may experience minor to life-threatening symptoms.</li>
                            <li><b>Diffuse axonal injury</b>: This is when the brain is injured due to a shaking movement. Diffuse axonal injuries can occur as the result of a car crash, a fall-related accident or due to other causes such as shaken baby syndrome. </li>
                            <li>If you or a loved one have suffered from any of the above injuries as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                        </ul>",

	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["Orange County Car Accident Brain Injury Client Testimonial", "/images/sb-video-lVUUCtM3Ebg.jpg", "lVUUCtM3Ebg"],
		["Short and Long Term Effects of a Concussion", "/images/sb-video-gny8lZ1Ds3g.jpg", "gny8lZ1Ds3g"]
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>
