<?php



// Workaround Sucuri to get the real IP Address

if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {

	$_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];

}



//

// SendGrid PHP Library Example

//

// This example shows how to send email through SendGrid

// using the SendGrid PHP Library.  For more information

// on the SendGrid PHP Library, visit:

//

//     https://github.com/sendgrid/sendgrid-php

//


error_log("THIS CONTACT FORM SHOULD NOT BE USED - BISNAR-CHASE-CLIENT-SURVEY-MAIL.PHP + ".$page);
require 'sendgrid-php/vendor/autoload.php';

require $_SERVER['DOCUMENT_ROOT'].'/../misc/sendgrid-creds.php';





/* CREATE THE SENDGRID MAIL OBJECT

====================================================*/

$sendgrid = new SendGrid( $sg_username, $sg_password );

$mail = new SendGrid\Email();

$mail2 = new SendGrid\Email();





/*====================================================*/



if ($_SERVER["REQUEST_METHOD"] == "POST") {



	$name = $_POST['name'];

	if (!$name) {

		header("HTTP/1.0 400 Bad Request");

        exit("Please Provide Your First and Last Name!");

	}



	$recommend = $_POST['recommend'];

	$recommendText = trim($_POST['recommend-text']);



	$quality = $_POST['quality'];

	$qualityText = trim($_POST['quality-text']);



	$satisfied = $_POST['satisfied'];

	$satisfiedText = trim($_POST['satisfied-text']);



	$needs = $_POST['needs'];

	$needsText = trim($_POST['needs-text']);



	$responsive = $_POST['responsive'];

	$responsiveText = trim($_POST['responsive-text']);



	$appreciate = trim($_POST['appreciate']);

	$process = trim($_POST['process']);

	$improve = trim($_POST['improve']);

	$assist = trim($_POST['assist']);



	$marketing = $_POST['marketing'];





	$subject = "Bisnar Chase Client Survey Response - From: ".$name;



	$emailBody =  "Name: $name \n\n";

	$emailBody .= "How likely is it that you would recommend Bisnar Chase: $recommend \n";

	if ($recommendText) {

		$emailBody .= "Explain: $recommendText \n";

	}



	$emailBody .= "\nHow would you rate the quality of our service: $quality \n";

	if ($qualityText) {

		$emailBody .= "Explain: $qualityText \n";

	}



	$emailBody .= "\nHow satisfied are you with our firm: $satisfied \n";

	if ($satisfiedText) {

		$emailBody .= "Explain: $satisfiedText \n";

	}



	$emailBody .= "\nHow well did our staff meet your needs: $needs \n";

	if ($needsText) {

		$emailBody .= "Explain: $needsText \n";

	}



	$emailBody .= "\nHow responsive have we been: $responsive \n";

	if ($responsiveText) {

		$emailBody .= "Explain: $responsiveText \n";

	}



	$emailBody .= "\nWhich member of our team did you most appreciate: \n $appreciate \n\n";



	$emailBody .= "What part of the process do you feel we handled well: \n $process \n\n";



	$emailBody .= "What part of the process do you feel we could improve on?: \n $improve \n\n";



	$emailBody .= "Was there any member of the team who could have done a better job: \n $assist \n\n";



	$emailBody .= "May we use this review for marketing purposes?: $marketing \n\n";



	$emailBody .= "Final Client Score: ". ($recommend+$quality+$satisfied+$needs+$responsive) . "/25";



	$emailFooter="\n\n Senders IP address: ".$_SERVER['REMOTE_ADDR'];

	$message = $emailBody.$emailFooter;





	try {

	    $mail->

	    setFrom( "marketing@bestattorney.com" )->

	    setReplyTo("marketing@bestattorney.com")->

	    setFromName($name)->

	    addTo("ccadogan@bisnarchase.com")->

	    addTo("marketing@bestattorney.com")->

	    setSubject($subject)->

	    setText($message);

	    

	    $response = $sendgrid->send($mail);



	    if (!$response) {

	        throw new Exception("Did not receive response.");

	    } else if ($response->message && $response->message == "error") {

	        throw new Exception("Received error: ".join(", ", $response->errors));

	    } else {

	    	header("HTTP/1.0 200 OK");

		    exit("Thank You, your survey response has been sent to us! We will use your input to ensure that we are constantly improving and giving our clients a better experience.");

	    }

	} catch ( Exception $e ) {

	    var_export($e);

	    exit;

	}

}

else {

	header("HTTP/1.0 403 Forbidden");

	echo "There was a problem with your submission, please try again.";

	exit;

}



?>