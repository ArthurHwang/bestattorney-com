<?php
// Folder Options for /newport-beach
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 3,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "orange-county", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"nav" => "newport-beach",

	"sidebarLinksTitle" => "Newport Beach Injury Information",

	"spanishPageEquivalent" => "",

	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
        "Bicycle Accidents" => "/newport-beach/bicycle-accidents.html",
        "Brain Injury" => "/newport-beach/brain-injury.html",
        "Bus Accidents" => "/newport-beach/bus-accidents.html",
        "Car Accidents" => "/newport-beach/car-accidents.html",
        "Dog Bites" => "/newport-beach/dog-bites.html",
        "DUI Accidents" => "/newport-beach/dui-accidents.html",
        "Employment Law" => "/newport-beach/employment-lawyers.html",
        "Hit and Run Accidents" => "/newport-beach/hit-and-run-accidents.html",
        "Motorcycle Accidents" => "/newport-beach/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/newport-beach/nursing-home-abuse.html",
        "Pedestrian Accidents" => "/newport-beach/pedestrian-accidents.html",
				"Personal Injury" => "/newport-beach/",
				"Premises Liability" => "/newport-beach/premises-liability.html",
        "Slip and Fall Accidents" => "/newport-beach/slip-and-fall-accidents.html",
        "Truck Accidents" => "/newport-beach/truck-accidents.html",
        "Worst City for Bicyclists" => "/newport-beach/worst-city-for-bicyclists.html",
        "Wrongful Death" => "/newport-beach/wrongful-death.html",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];

?>