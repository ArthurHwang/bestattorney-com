<?php
// Folder Options for /car-accidents/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "accident";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Car Accident Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
        "Who Is At Fault?" => "/car-accidents/who-is-at-fault.html",
        "Hazardous Roads" => "/car-accidents/hazardous-roads.html",
        "Injury Compensation" => "/car-accidents/injury-compensation.html",
        "Insurance Bad Faith" => "/car-accidents/insurance-bad-faith.html",
        "Insurance Hardball" => "/car-accidents/insurance-hardball.html",
		"Bodily Injury" => "/car-accidents/bodily-injury-lawyers.html",
        "Lemon Laws" => "/car-accidents/lemon-laws.html",
        "Lower Extremity Injuries" => "/car-accidents/lower-extremity-injuries.html",
        "Premature Insurance Settlements" => 
		"/car-accidents/premature-insurance-settlements.html",
        "Rental Car Safety" => "/car-accidents/rental-car-safety.html",
        "Rights of Passengers" => "/car-accidents/rights-of-passengers.html",
        "Road Debris" => "/car-accidents/road-debris.html",
        "Predatory Towing" => "/car-accidents/predatory-towing.html",
        "SUV Rollovers" => "/car-accidents/suv-rollovers.html",
        "SUV Roof Crush" => "/car-accidents/suv-roof-crush.html",
        "T-Bone Accidents" => "/car-accidents/t-bone-car-accident-lawyer.html",
        "Rear-End Accidents" => "/car-accidents/rear-end-accident-lawyer.html",
        "Head-On Car Accidents" => "/car-accidents/head-on-car-accident-lawyer.html",
        "Rideshare Accident Lawyer" => "/car-accidents/rideshare-accident-lawyer.html",
        "Lyft Accidents" => "/car-accidents/lyft-car-accident-lawyers.html",
        "Uber Accidents" => "/car-accidents/uber-car-accident-lawyers.html",
        "Car Accidents Home" => "/car-accidents/"
    ),

	"sidebarContentTitle" => "Car Accident Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'><li>Serving California since 1978.</li>
                            <li>Recovered over $650 Million in verdicts and settlements</li>
                            <li>Rated top attorneys year after year.</li>
                            <li>No recovery, no fee promise.</li>
                            <li>If you or a loved one have suffered in a car accident as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                            </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category','auto'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [0, array(13)], // see getReviews() in functions.php

	"videosArray" => [
		['Are the Insurance Companies on My Side?', '', 'DkKmXYasbr4'],
		['Can My Lawyer Help Me Get My Car Repaired?', '', 'WSuOEz40ZPA']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
