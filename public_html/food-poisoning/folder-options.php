<?php
// Folder Options for /food-poisoning

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Food Poisoning Injury Information",

	"spanishPageEquivalent" => "",

	"noLiveChat" => true,


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Botulism" => "/food-poisoning/botulism.html",
        "E.coli" => "/food-poisoning/ecoli.html",
        "Foodborne Illnesses" => "/food-poisoning/foodborne-illness.html",
        "Hepatitis A" => "/food-poisoning/hepatitis-a.html",
        "Listeria" => "/food-poisoning/listeria.html",
        "New CDC Reports" => "/food-poisoning/new-cdc-reports.html",
        "Raw Milk Sickness" => "/food-poisoning/raw-milk-sickness.html",
        "Salmonella" => "/food-poisoning/salmonella.html",
        "Shigella" => "/food-poisoning/shigella.html",
        "Symptoms of Food Poisoning" => "/food-poisoning/symptoms.html",
        "Tainted Eggs" => "/food-poisoning/tainted-eggs.html",
        "Types of Food Poisoning" => "/food-poisoning/types.html",
        "Food Poisoning Home" => "/food-poisoning/"
	),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
	['Stomach Flu vs. Food Poisoning Medical Course', '../images/text-header-images/flu-food-poisoning-dr-oz-video-thumbnail.jpg', 'EC7UaLIAEP4'],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>