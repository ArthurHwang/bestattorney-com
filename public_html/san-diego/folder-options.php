<?php
// Folder Options for /san-diego

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "San Diego Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Amusement Park Accidents" => "/san-diego/amusement-park-accidents.html",
        "Auto Accidents" => "/san-diego/auto-accidents.html",
        "Aviation Accidents" => "/san-diego/aviation-accidents.html",
        "Boat Accidents" => "/san-diego/boat-accidents.html",
        "Bus Accidents" => "/san-diego/bus-accidents.html",
        "Dog Bites" => "/san-diego/dog-bites.html",
        "Motorcycle Accidents" => "/san-diego/motorcycle-accidents.html",
        "Nursing Home Abuse" => "/san-diego/nursing-home-abuse.html",
        "Pedestrian Accidents" => "/san-diego/pedestrian-accidents.html",
        "Premises Liability" => "/san-diego/premises-liability.html",
        "Product Liability" => "/san-diego/product-liability.html",
        "Slip and Fall Accidents" => "/san-diego/slip-and-fall-accidents.html",
        "Swimming Pool Accidents" => "/san-diego/swimming-pool-accidents.html",
        "Train Accidents" => "/san-diego/train-accidents.html",
        "Wrongful Death" => "/san-diego/wrongful-death.html",
        "San Diego Home" => "/san-diego/",
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",

	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>
