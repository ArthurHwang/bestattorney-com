<?php
// Folder Options for /auto-defects/ford-carbon-monoxide

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 1,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Carbon Monoxide Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
	"Ford Carbon Monoxide" => "/auto-defects/ford-carbon-monoxide/",
"Ford CO Complete History" => "/auto-defects/ford-carbon-monoxide/complete-history-carbon-monoxide-ford-explorer.html",
"Ford CO FAQ" => "/auto-defects/ford-carbon-monoxide/ford-explorer-carbon-monoxide-faq.html",
    ),

	"sidebarContentTitle" => "Auto Defect Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
    	<li>Ford Explorer vehicles from 2011 can have a defect where the car allows poisonous fumes from the exhaust to enter the cabin where the drivers and passengers are.</li>
	    <li>Newport Beach Police Officer Brian McDowell was driving in his 2014 Ford Explorer Police Cruiser when he lost consciousness due to the overwhelming amount of Carbon Monoxide in the car</li>
	    <li>McDowell suffered serious injuries and is bringing a lawsuit against Ford for allowing a defective vehicle to be sold.</li>
	    <li>If you or a loved one have suffered as a result of carbon monoxide poisoning inside a Ford vehicle, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Brian Chase Discusses CO Leaks on CBS', '/images/video-images/brian-on-cbs.jpg', 'fTyLKA-IhSI'],
		['How Does The Auto Recall Process Work?', '', '-wv3MjBd6pg'],
		['Why Focus on Auto Defect Cases?', 'https://i.ytimg.com/vi_webp/hpi1tBCvEtw/mqdefault.webp', 'hpi1tBCvEtw']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => true,


	"loadFontAwesome" => true //This should usually be true

];



?>