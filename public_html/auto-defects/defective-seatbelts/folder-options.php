<?php
// Folder Options for /auto-defects/defective-seatbelts

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,
	
	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Defective Seatbelt Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"History of Seatbelts" => "/auto-defects/defective-seatbelts/history-of-seat-belts.html",
        "Seatbelt Safety" => "/auto-defects/defective-seatbelts/seat-belt-safety.html",
        "Types of Seatbelt Injuries" => "/auto-defects/defective-seatbelts/types-of-seat-belt-injuries.html",
        "False Seat Belt Latching" => "/auto-defects/defective-seatbelts/false-seat-belt-latching.html",
        "Seatbelt Unlatching Cover Up" => "/auto-defects/defective-seatbelts/seat-belt-unlatching-cover-up.html",
        "GM Seatbelt Failure" => "/auto-defects/defective-seatbelts/gm-seat-belt-failure.html",
        "Back to Auto Defects" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Defective Seatbelt Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
		<li>The most common seatbelt issue is with false latching - when the belt clicks into place but it isn't secured well, and it comes undone when jolted or under pressure.</li>
    	<li>Seatbelt usage drastically decreases the user's chance of injury during a car crash, and a defective seatbelt can provide little to no benefit in a crash.</li>
    	<li>If you or a loved one have suffered as a result of a seatbelt defect, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['How Does The Auto Recall Process Work?', '', '-wv3MjBd6pg'],
		['Why Focus on Auto Defect Cases?', 'https://i.ytimg.com/vi_webp/hpi1tBCvEtw/mqdefault.webp', 'hpi1tBCvEtw']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>