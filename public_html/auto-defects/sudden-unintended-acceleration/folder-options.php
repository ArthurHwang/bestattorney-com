<?php
// Folder Options for /auto-defects/sudden-unintended-acceleration/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Unintended Acceleration Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Toyota SUA Timeline" => "/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration.html",
        "Toyota Whistleblower Statement" => "/auto-defects/sudden-unintended-acceleration/toyota-sua-whistleblower-personal-statement.html",
        "Sudden Acceleration Claims Lives" => "/auto-defects/sudden-unintended-acceleration/sua-claims-lives.html",
        "Sudden Acceleration Complaints" => "/auto-defects/sudden-unintended-acceleration/sua-complaints.html",
        "Ford Sudden Unintended Acceleration" => "/auto-defects/sudden-unintended-acceleration/ford-sudden-unintended-acceleration.html",
        "Toyota Floor Mat Recalls" => "/auto-defects/sudden-unintended-acceleration/toyota-floor-mat-recalls.html",
        "Sudden Unintended Acceleration Home" => "/auto-defects/sudden-unintended-acceleration/",
        "Back to Auto Defects" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Unintended Acceleration Lawyers",

	"sidebarContentHtml" => "<ul class='bpoint'>
		<li>Sudden unintended acceleration (SUA) is an auto defect that makes a car start accelerating without pressing the gas pedal. It is often accompanied by loss of braking power and can usually only be stopped by putting the car in neutral.</li>
        <li>The SUA defects have caused hundreds of car crashes, and as many as 89 deaths and 52 injuries, according to <a href='http://www.cbsnews.com/news/toyota-unintended-acceleration-has-killed-89/'>CBS News.</a></li>
        <li>Toyota has recalled over 10 million cars due to SUA, and it still remains an issue. Nissan recalled 300,000 cars as recently as 8/12/15 for acceleration issues caused by the trim panel.</li>
        <li>Toyota has been fined over 1.27 Billion dollars for their negligence regarding sudden acceleration, and has had to pay more in confidential settlements and verdicts to victims of SUA. </li>
    	<li>If you or a loved one have suffered in a bicycle accident as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['What is Sudden Unintended Acceleration?', '', 'K0vYdW7h9-A'],
		['What are the Most Common Causes of Sudden Acceleration?', '', 'M6mzQXFv4T0']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>