<?php
// Folder Options for /auto-defects/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================

	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject,

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Auto Defect Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Rollovers" => "/auto-defects/rollovers/",
        "Seat Back Failures" => "/auto-defects/seatback-failure/",
        "Ford Carbon Monoxide Poisoning" => "/auto-defects/ford-carbon-monoxide/",
        "Sudden Unintended Acceleration" => "/auto-defects/sudden-unintended-acceleration/",
        "Airbag Failures" => "/auto-defects/airbag-failures.html",
        "Defective Seatbelts" => "/auto-defects/defective-seatbelts/",
        "Tesla Autopilot Crashes" => "/auto-defects/tesla-autopilot-crash-lawyer.html",
        "Emergency Response Vehicles" => "/auto-defects/emergency-response-vehicles.html",
        "Self-Driving Car Crashes" => "/auto-defects/autonomous-car-accident-lawyer.html",
        "Car Seat Injuries" => "/auto-defects/car-seat-injuries.html",
        "Auto Defects Home" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Related Auto Defects",

	"sidebarContentHtml" => "<ul class='bpoint'>
	    <li>Automakers often negligently sell cars that have defects that can lead to injuries or death:</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/sudden-unintended-acceleration/'><strong>Sudden Acceleration:</strong></a> A car can accelerate to full speed without pressing the gas pedal and may not be able to be stopped by the brakes</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/rollovers/'><strong>Rollovers:</strong></a> Taller cars like 15-passenger vans are much more likely to rollover under dangerous driving conditions and cause injuries.</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/roof-crush/'><strong>Roof Crush:</strong></a> Rollover accidents are especially dangerous when the vehicle roof is not enforced, leading to major injuries or death.</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/ford-carbon-monoxide/'><strong>Ford Carbon Monoxide Poisoning: </strong></a>Some Ford Explorer drivers have been injured after passing out while driving because of car exhaust leaking into the passenger cabin and poisioning the drivers.</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/airbag-failures.html'><strong>Airbag Failure:</strong></a> Consumers expect their airbag to protect them in a crash, and passengers can be injured terribly if the airbag does not go off in the event of a crash.</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/defective-seatbelts/'><strong>Seatbelt Failure:</strong></a> Defective seatbelts can click into position but then release when jolted suddenly, offering no protection in a car crash.</li>
	    <li><a href='https://www.bestattorney.com/auto-defects/seatback-failure/'><strong>Seat Back Failure:</strong></a> Many car seats are made cheaply and can break in the event of a rear-end accident, leading to head injuries for the driver or a passenger behind them.</li>
	    <li>If you or a loved one have suffered as a result of an auto defect, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
   </ul>",

	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['How Does The Auto Recall Process Work?', '', '-wv3MjBd6pg'],
		['Why Focus on Auto Defect Cases?', '/images/2019-assets/mqdefault.webp', 'hpi1tBCvEtw']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false,

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.


	// =====================================
	// Defining Page Resources
	// =====================================

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>