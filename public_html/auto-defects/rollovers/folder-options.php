<?php
// Folder Options for /auto-defects/rollovers/

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Rollover Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"After a Rollover Accident" => "/auto-defects/rollovers/after-a-rollover-accident.html",
        "15 Passenger Van Advisory" => "/auto-defects/rollovers/15-passenger-van-advisory.html",
        "Avoiding a 15 Passenger Van Rollover" => "/auto-defects/rollovers/avoiding-a-rollover.html",
        "SUV Rollovers" => "/auto-defects/rollovers/suv-rollovers.html",
        "SUV Rollover Victims" => "/auto-defects/rollovers/suv-rollover-victim.html",
        "Van Rollovers" => "/auto-defects/rollovers/van-rollovers.html",
        "Rollovers Home" => "/auto-defects/rollovers/",
        "Back to Auto Defects" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Auto Defect Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
	    <li>Several different factors can lead to being injured in a rollover accident:</li>
	    <li><strong>Type of Car: </strong>Evidence shows that 15 passenger vans are by far the most likely vehicles to roll over. SUVs and other tall cars also have a higher likelihood of rolling over.</li>
	    <li><strong>Luggage on the Roof: </strong>If a lot of heavy luggage is on the roof, the car will be top-heavy and more likely to roll over on a sharp turn.</li>
	    <li><strong>Tire inflation: </strong>If the Tires are over or under-inflated, cars can handle differently and it can be easier to tip over. </li>
	    <li><strong>ESC: </strong>If the car has electronic stability control (ESC), the car is more likely to avoid loss of traction, which can lead to a rollover</li>
	    <li><strong>Roof Crush: </strong>If the car roof is not reinforced, a rollover accident can result in a roof crush, which can injure or kill passengers inside.</li>
	    <li>If you or a loved one have suffered as a result of a rollover or roof crush, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['How Does The Auto Recall Process Work?', '', '-wv3MjBd6pg'],
		['Why Focus on Auto Defect Cases?', 'https://i.ytimg.com/vi_webp/hpi1tBCvEtw/mqdefault.webp', 'hpi1tBCvEtw']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>