<?php
// Folder Options for /auto-defects/roof-crush

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Roof Crush Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"New Car Roof Standards" => "/auto-defects/roof-crush/new-car-roof-standards.html",
        "Roof Crush Home" => "/auto-defects/roof-crush/",
        "Back to Auto Defects" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Roof Crush Defect Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
		<li>Cars without re-enforced roofs can be much more dangerous in a rollover accident. The top of the car can crush inwards, leading to major injury or death.</li>
        <li><a href='https://www.bestattorney.com/auto-defects/rollovers/avoiding-a-rollover.html'>15 Passenger Vans</a> are some of the most likely vehicles to roll over and experience roof crush. Be careful when driving these dangerous vehicles!</li>
        <li>If you or a loved one have suffered as a result of a rollover or roof crush, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
        <li>Over $650 Million in verdicts and settlements.</li>
        <li>Firm commitment to public safety and holding corporate giants accountable.</li>
        <li>A team of auto defect attorneys who are dedicated and compassionate.</li>
        <li>Serving California since 1978.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['How Does The Auto Recall Process Work?', '', '-wv3MjBd6pg'],
		['Why Focus on Auto Defect Cases?', 'https://i.ytimg.com/vi_webp/hpi1tBCvEtw/mqdefault.webp', 'hpi1tBCvEtw']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];
