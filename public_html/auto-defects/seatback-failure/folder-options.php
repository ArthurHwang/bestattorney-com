<?php
// Folder Options for /auto-defects/seatback-failure

if (!isset($options['pageSubject'])) {
	$options['pageSubject'] = "autodefect";
}
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, 

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Seatback Failure Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Defective Car Seat Backs" => "/auto-defects/seatback-failure/defective-car-seat-backs.html",
		// Touched by Arthur Hwang 10/23/18.  Changed to redirect to /seatback-failure/
		// "Seat Back Injuries" => "/auto-defects/seatback-failure/seat-back-injuries.html",
		"Seat Back Injuries" => "/auto-defects/seatback-failure/",

        "Car Seat Back Testing" => "/auto-defects/seatback-failure/car-seat-back-testing.html",
        "Back to Auto Defects" => "/auto-defects/",
    ),

	"sidebarContentTitle" => "Auto Defect Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'>
    	<li>Seatback Failure happens when a car is rear-ended and a car seat breaks at the hinge, sending the passengers backwards into the seat behind them with no restraint. This forceful push into the back seat can cause brain damage to the passenger in the broken seat, or damage to the passenger in the back seat.</li>
    	<li>Most broken seatbacks are a result of flimsy construction and insufficient standards, but some incidents can happen when incorrect materials have been used to create the seat, or a retaining pin that keeps the seat upright is sheared off. All of these are dangerous defects that should not happen under normal rear-end accidents.</li> 
    	<li>If you or a loved one have suffered as a result of a seatback failure, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
    </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ['category', 'autodefect'], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [
		['Brian Chase on SeatBacks', 'https://img.youtube.com/vi/Gbv85ZZ4qjg/mqdefault.jpg', 'Gbv85ZZ4qjg'],
		['Brian Chase Explains Liability', 'https://img.youtube.com/vi/Vw4O5R68zsU/mqdefault.jpg', 'Vw4O5R68zsU']
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>