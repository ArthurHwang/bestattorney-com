<?php
// Folder Options for /pharmaceutical-litigation

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "drugdefect", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Defective Drug Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Accutane" => "/pharmaceutical-litigation/accutane.html",
        "Talcum Powder / Ovarian Cancer" => "/pharmaceutical-litigation/talcum-powder.html",
        "List of Talcum Powder Studies" => "/pharmaceutical-litigation/list-of-talcum-powder-studies.html",
				"Xarelto" => "/pharmaceutical-litigation/xarelto.html",
				"Zantac" => "/pharmaceutical-litigation/zantac-cancer-lawyers.html",
        "Zofran" => "/pharmaceutical-litigation/zofran-lawyers.html",
        "Low Testosterone Therapy" => "/pharmaceutical-litigation/low-t-lawyers.html",
        "Pharmaceutical Litigation Home" => "/pharmaceutical-litigation/",
    ),

	"sidebarContentTitle" => "Defective Drug Attorneys",

	"sidebarContentHtml" => "<ul class='bpoint'><li>If you or a loved one have suffered from a defective drug, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li></ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["The Dangers of Talcum Powder and Ovarian Cancer", "/images/video-images/video-image-3.jpg", "81fmiEyrsRg"],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>