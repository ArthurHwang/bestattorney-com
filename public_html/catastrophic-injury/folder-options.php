<?php
// Folder Options for /catastrophic-injury/

if (!isset($options['pageSubject'])) {
  $options['pageSubject'] = "";
}
$pageSubject = $options['pageSubject'];

$options = [

  // =====================================
  // Defining the page type
  // =====================================

  "isHome" => false,

  "isBlog" => false,

  "isGeo" => false,

  "isPa" => true,

  "isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.

  "isContact" => false,


  // =====================================
  // Defining Page Meta Information
  // =====================================

  "canonical" => "", //defaults to the page URI without parameters

  "noindex" => false,

  "searchWeight" => 2,

  // =====================================
  // Defining Template Content
  // =====================================

  "pageSubject" => $pageSubject,

  "location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

  "sidebarLinksTitle" => "Catastrophic Injury Information",

  "spanishPageEquivalent" => "",


  // =====================================
  // Defining Sidebar Content
  // =====================================

  "sidebarLinks" => array(
    "About Serious Injuries" => "/catastrophic-injury/about-serious-injuries.html",
    "Amputations" => "/catastrophic-injury/amputations.html",
    "Auto Products Liability" => "/auto-defects/",
    "Burn Injuries" => "/catastrophic-injury/burn-injuries.html",
    "Cal Trans Negligence" => "/catastrophic-injury/cal-trans-negligence.html",
    "Catastrophic Injury Client Testimonials" => "/catastrophic-injury/client-testimonials.html",
    "Complex Regional Pain Syndrome" => "/catastrophic-injury/complex-regional-pain-syndrome-lawyers.html",
    "Disc Herniation" => "/catastrophic-injury/disc-herniation.html",
    "Eye Injuries" => "/catastrophic-injury/eye-injuries.html",
    // "Hearing Loss" => "/catastrophic-injury/hearing-loss.html",
    "Nerve Damage" => "/catastrophic-injury/nerve-damage.html",
    "Neck Injury" => "/catastrophic-injury/neck-injury-lawyer.html",
    "Back Injury" => "/catastrophic-injury/back-injury-lawyer.html",
    "Post Traumatic Stress Disorder (PTSD)" => "/catastrophic-injury/post-traumatic-stress-disorder.html",
    "Product Liability" => "/catastrophic-injury/product-liability.html",
    "Rotator Cuff Injuries" => "/catastrophic-injury/rotator-cuff-injuries.html",
    "Scarring and Disfigurement" => "/catastrophic-injury/scarring-and-disfigurement-lawyers.html",
    "Spinal Cord Injury" => "/catastrophic-injury/spinal-cord-injury.html",
    "Torn Meniscus" => "/catastrophic-injury/torn-meniscus.html",
    // "Traffic Accidents" => "/catastrophic-injury/traffic-accidents.html",
    "Workplace Accidents" => "/catastrophic-injury/workplace-accidents.html",
    "Catastrophic Injury Home" => "/catastrophic-injury/",
  ),

  "sidebarContentTitle" => "Catastrophic Injury Attorneys",

  "sidebarContentHtml" => "<ul class='bpoint'><li>If you or a loved one have suffered as a result of someone else’s negligence, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li></ul>",

  // "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

  "caseResultsArray" => ["top"], // see printCaseResults() in functions.php

  "rewriteCaseResults" => true,

  "reviewsArray" => [2], // see getReviews() in functions.php

  "videosArray" => [
    ['Brian Chase\'s Greatest Accomplishment is Helping People', '', 'jKKvVphfbzg'],
    ['Brian Chase Fights for Consumer Rights', '', 'ZixMhFMI3UQ']
  ], //array of video information that will show on the sidebar.

  "loadBlog" => true,

  "allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


  // =====================================
  // Defining Page Layout
  // =====================================

  // "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

  "fullWidth" => false,

  "showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

  // "loadTemplate" => "", //Needed? we can attain this info from the page type.


  // =====================================
  // Defining Page Resources
  // =====================================

  "loadJquery" => true,
  "loadJqueryUi" => false,
  "loadLegacyBootstrap" => false,


  "loadFontAwesome" => true //This should usually be true

];
