<?php
// Folder Options for /nursing-home-abuse

$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => true,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 5,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Nursing Home Abuse Injury Information",

	"spanishPageEquivalent" => "",

	"noLiveChat" => true,


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Abuse &amp; Neglect in California" => "/nursing-home-abuse/california-abuse-neglect.html",
		"Bed Rail Deaths" => "/nursing-home-abuse/bed-rail-death.html",
		"Bedsores" => "/nursing-home-abuse/bedsores.html",
		"Elder Abuse" => "/nursing-home-abuse/elder-abuse.html",
		"Elder Neglect" => "/nursing-home-abuse/elder-neglect.html",
		"Falsified Medical Records" => "/nursing-home-abuse/falsified-medical-records.html",
		"FAQ's" => "/nursing-home-abuse/faqs.html",
		"Forced Arbitration" => "/nursing-home-abuse/forced-arbitration.html",
		"Fraud" => "/nursing-home-abuse/fraud.html",
		"Insurance Denials" => "/nursing-home-abuse/insurance.html",
		"Signs of Abuse" => "/nursing-home-abuse/signs.html",
		"Nursing Home Abuse Lawyers" => "/nursing-home-abuse/",
    ),

	"sidebarContentTitle" => "Look for These Signs of Neglect:",

	"sidebarContentHtml" => "<ul class='bpoint'>
                            <li>Broken or fractured bones</li>
                            <li>Head injuries</li>
                            <li>Open wounds, cuts, punctures, untreated injuries in various stages of healing</li>
                            <li>Broken eyeglasses, dentures, hearing aids</li>
                            <li>Signs of punishment or physical restraint</li>
                            <li>Your loved one tells you they have been mistreated</li>
                            <li>A sudden change in behavior</li>
                            <li>The nursing home refuses to allow unsupervised visits</li>
                            <li>If your loved one exhibits any of the above signs, know your rights and <a href='https://www.bestattorney.com/contact.html'>contact an attorney</a> today.</li>
                        </ul>",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [3], // see getReviews() in functions.php

	"videosArray" => [
		["Spotting Signs of Elder Abuse", "", "520_pSDAx_s"],
		["What Should I Do If I Suspect Elder Abuse?", "", "qwlcADHtfvY"],
	], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>