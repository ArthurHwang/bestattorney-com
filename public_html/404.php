<?php
include_once($_SERVER['DOCUMENT_ROOT']."/template-files/preload.php");
include($_SERVER['DOCUMENT_ROOT']."/template-files/site-options/default-folder-options.php");

header("HTTP/1.1 404 Not Found");


?>
<!DOCTYPE html>
<html>
<head>
<title>404 Error: <?php echo $_SERVER['REQUEST_URI']; ?></title>
<meta name="description" content="Error Page - Not Found.">
<meta name="robots" content="noindex">

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.5.1/dist/instantsearch.min.css">
<script src="https://cdn.jsdelivr.net/npm/instantsearch.js@2.5.1"></script>

<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/header.php"); ?>


<h1 id="h1title">We've Moved Things Around!</h1>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/template-files/partials/_breadcrumbs.php");?>

The requested URL
<?php echo $_SERVER['REQUEST_URI']; ?> was not found on this server. We've done a little remodeling and the page may have moved.
<hr>
<?php //echo $_SERVER['HTTP_HOST']; ?>


<a href="https://www.bestattorney.com">Return to our homepage</a> and find what you were looking for.
<p>Need help with a personal injury case? <a href="https://www.bestattorney.com/contact.html">Contact us</a> for a free case review.

<hr>
<p>Need to find something else? Search our site here:</p>
<div class="clear"></div>

<div class="input-group">
	<input type="text" class="form-control" id="q" />
	<button class="btn btn-default"><i class="fa fa-search"></i></button>
	<div class="clearfix"></div>
</div>
<div class="ais-search-box--powered-by">
  Search by
  <a class="ais-search-box--powered-by-link" href="https://www.algolia.com/?utm_source=instantsearch.js&amp;utm_medium=website&amp;utm_content=localhost&amp;utm_campaign=poweredby" target="_blank">Algolia</a>
</div>

<div id="hits">
</div>


<div class="clear"></div>



<?php include($_SERVER['DOCUMENT_ROOT']."/template-files/side-footer.php"); ?>

<style>

#content .ais-search-box--input {
	height:38px;
	font-size:22px;
	padding-left:8px;
	float: left;
    width: 90%;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
	border-right: 0px;
	box-sizing:border-box;
}

#content .input-group .btn {
	float: left;
    height: 38px;
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
    width: 10%;
	box-sizing:border-box;
}


.search-result {
    margin: 15px 0;
    border-bottom: 1px solid #666;
    padding: 0 0 21px;
}

.search-result-title {
	font-size:18px;
}

.search-result-url {
	font-size: 12px;
    line-height: 10px;
}

.search-result-desc {
	font-size: 14px;
	line-height: 16px;
	margin-top:10px
}

.search-result em {
	font-style:normal;
    background-color: #00a1f130;
    color: #000000;
}

</style>

<script>
	let prevSearch = "";
	const search = instantsearch({
		appId: 'VX0VUN8JFN',
		apiKey: 'b5b50f91d64317f9afab3fb57f461de2',
		indexName: 'site_search',
		urlSync: {
			mapping: {q: 'query'}
		}
	});

	search.addWidget(
		instantsearch.widgets.hits({
			container: "#hits",
			templates: {
				empty: 'No results found',
				item: '<div class="search-result"><div class="search-result-title"><a href="{{url}}">{{{_highlightResult.title.value}}}</a></div><div class="search-result-url">{{url}}</div><div class="search-result-desc">{{{_highlightResult.description.value}}}</div></div>'
			}
		})
	);

	/**
	// Sends a analytics event to google:
	// Category: Search
	// Action: [Search Query]
	// Label: [All previous searches from this session]
	*/
	search.addWidget(
		instantsearch.widgets.analytics({
			pushFunction: (qs, state, results) => {
				ga('send', 'event', 'Search', state.query, prevSearch);
				prevSearch = `${state.query}, ${prevSearch}`;
			},
			delay: 2000
		})
	);

// initialize SearchBox
	search.addWidget(
		instantsearch.widgets.searchBox({
			container: '#q',
			placeholder: 'Search our website:',
			poweredBy: false,
			reset: false,
			wrapInput: false,
			magnifier: false,
			cssClasses: "search-box"
		})
	);


	search.start();


</script>