<?php
// Folder Options for /abogados/sobre-nosotros


$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => false,

	"isPa" => false,

	"isSpanish" => true, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false, 


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 3,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => "", //Don't change this unless you want this to be true for the whole folder.

	"location" => "", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================
	"sidebarLinksTitle" => "Sobre Nosotros",

	"sidebarLinks" => array(
		"Lesiones Personales" => "/abogados/lesiones-personales.html",
		"Accidentes de Auto" => "/abogados/accidentes-de-auto.html",
		"Defectos Automovilísticos" => "/abogados/defectos-automovilisticos.html",
		"Productos Defectuosos" => "/abogados/productos-defectuosos.html",
		"Depositivos Medicos" => "/abogados/depositivos-medicos.html",
		"Drogas Peligrosas" => "/abogados/drogas-peligrosas.html",
		"Mordeduras de Perro" => "/abogados/mordeduras-de-perro.html",
		"Responsibilidad Civil" => "/abogados/responsibilidad-civil.html",
		"Página de inicio española" => "/abogados/",
		"Elegir un Abogado" => "/abogados/recursos/elegir-un-abogado.html",
		"Resultos Destacados" => "/abogados/resultos-destacados.html",
		"Contactenos" => "/abogados/contactenos.html",
    ),

	// "showSidebarContactForm" => false,

    "extraSidebar" => "", 

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => false,

	"reviewsArray" => [], // see getReviews() in functions.php

	// "videosArray" => [
	// 	['Bisnar Chase Brand Video', '/images/video-images/brian-brand-video.jpg', 'wmv1HKnhW7U'],
	// 	['Brian Chase on American Law TV', '/images/featured-homepage-images/brian-american-law-television.jpg', 'BhVPzkYsLuE']
	// ], //array of video information that will show on the sidebar.

	"loadBlog" => false,

	"allowSidebarTruncate" => false, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,

	"loadFontAwesome" => true //This should usually be true

];



?>