<?php
// Folder Options for /los-angeles
$pageSubject = $options['pageSubject'];

$options = [

	// =====================================
	// Defining the page type
	// =====================================
		
	"isHome" => false,

	"isBlog" => false,

	"isGeo" => true,

	"isPa" => false,

	"isSpanish" => false, //sets options for when a contact form is submitted, or other dynamic things that need to be changed based on the langage.  

	"isContact" => false,


	// =====================================
	// Defining Page Meta Information
	// =====================================

	"canonical" => "", //defaults to the page URI without parameters

	"noindex" => false,

	"searchWeight" => 2,

	// =====================================
	// Defining Template Content
	// =====================================

	"pageSubject" => $pageSubject, //Don't change this unless you want this to be true for the whole folder.

	"location" => "los-angeles", //Defaults to orange-county, but if it's los-angeles or riverside then the address and phone number are changed on the page and the Schema is updated.

	"sidebarLinksTitle" => "Los Angeles Injury Information",

	"spanishPageEquivalent" => "",


	// =====================================
	// Defining Sidebar Content
	// =====================================

	"sidebarLinks" => array(
		"Car Accidents" => "/los-angeles/car-accidents.html",
        "Auto Defects" => "/los-angeles/auto-defects.html",
        "Dog Bites" => "/los-angeles/dog-bites.html",
        "Motorcycle Accidents" => "/los-angeles/motorcycle-accidents.html",
        "Bicycle Accidents" => "/los-angeles/bicycle-accidents.html",
        "Premises Liability" => "/los-angeles/premises-liability.html",
        "Product Liability" => "/los-angeles/product-liability.html",
		"Brain Injury" => "/los-angeles/brain-injury.html",
		"Freeway Accidents" => "/los-angeles/freeway-accident-lawyers.html",
		"Parking Lot Accidents" => "/los-angeles/parking-lot-accidents.html",
		"Amusement Park Accidents" => "/los-angeles/amusement-park-accidents.html",
        "Truck Accidents" => "/los-angeles/truck-accidents.html",
        "Bus Accidents" => "/los-angeles/bus-accidents.html",
		"Road Debris Accidents" => "/los-angeles/road-debris-car-accidents.html",
		"Tour Bus Accidents" => "/los-angeles/celebrity-tour-bus-lawyer.html",
		"Common Car Accident Causes" => "/los-angeles/common-causes-car-accidents.html",
		"Whiplash Injuries" => "/los-angeles/whiplash-injuries.html",
        "Employment Law" => "/los-angeles/employment-lawyers.html",
        "Workplace Harassment" => "/los-angeles/workplace-harassment.html",
        "Hostile Work Environments" => "/los-angeles/hostile-work-environment.html",
        "Hoverboard Injuries" => "/los-angeles/hoverboard-injury-lawyers.html",
		"Hazardous Roadways" => "/los-angeles/hazardous-roadways.html",
        "Failure to Warn" => "/los-angeles/failure-to-warn.html",
        "Hit and Run Accidents" => "/los-angeles/hit-and-run-accidents.html",
        "Nursing Home Abuse" => "/los-angeles/nursing-home-abuse.html",
        "Pedestrian Accidents" => "/los-angeles/pedestrian-accidents.html",
        "Slip and Fall Accidents" => "/los-angeles/slip-and-fall-accidents.html",
        "Swimming Pool Accidents" => "/los-angeles/swimming-pool-accidents.html",
        "Train Accidents" => "/los-angeles/train-accidents.html",
        "Boating Accidents" => "/los-angeles/boating-accidents.html",
        "Wrongful Death" => "/los-angeles/wrongful-death.html",
		"Aviation Accidents" => "/los-angeles/aviation-accidents.html",
		"Overserving Alcohol Liability" => "/los-angeles/overserving-alcohol-injuries.html",
		"Scooter Accidents" => "/los-angeles/scooter-injury-lawyers.html",
        "Contact LA Office" => "/los-angeles/contact.html",
        "Los Angeles Home" => "/los-angeles/",
		
    ),

	"sidebarContentTitle" => "",

	"sidebarContentHtml" => "",
	
	// "extraSidebar" => "", // If you want an extra section on the sidebar, put the FULL HTML of the sidebar box here.

	"caseResultsArray" => ["top"], // see printCaseResults() in functions.php

	"rewriteCaseResults" => true,

	"reviewsArray" => [2], // see getReviews() in functions.php

	"videosArray" => [], //array of video information that will show on the sidebar.

	"loadBlog" => true,

	"allowSidebarTruncate" => true, // sidebar will remove blog, then, videos, then reviews if the sidebar is way longer than the content. If this is set to false, it prevents that.


	// =====================================
	// Defining Page Layout
	// =====================================

	// "headerImageClass" => "", //uncomment this and add a classname for your new header image which you can change in the CSS

	"fullWidth" => false, 

	"showFooterVideos" => false, //This will load Jquery and UI, and will have some adverse affects on the page if it's a PA/GEO page.

	// "loadTemplate" => "", //Needed? we can attain this info from the page type.

	
	// =====================================
	// Defining Page Resources
	// =====================================	

	"loadJquery" => true,
	"loadJqueryUi" => false,
	"loadLegacyBootstrap" => false,


	"loadFontAwesome" => true //This should usually be true

];



?>