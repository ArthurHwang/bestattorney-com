<script>

/**
 * Change every link so it loads in localhost.
 * On every '<a>' link click, check if it's an internal link, and redirect to localhost instead.  
 */

$("a").click(function(e) {
	e.preventDefault();
	
	var link = $(this).attr('href');
	if (link) {
		if (link.substring(0, 28) == "https://www.bestattorney.com" ) {
			link = link.substring(29);
			window.location = "http://localhost/"+link;
		} else {
			window.location = link;
		}
	}
});

// Change every ajax call to call a file on the local server instead of on bestattorney.com
$.ajaxSetup({
    beforeSend: function (xhr,settings) {
		if (settings.url.substr(0, 28) == "https://www.bestattorney.com") {
			settings.url = "http://localhost"+settings.url.slice(28);
		}
    }
});

</script>