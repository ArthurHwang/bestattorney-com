var fs = require('graceful-fs')
var path = require('path')
var readFile

const gitChangesPath = path.resolve(__dirname, 'git-changes.txt')

//create txt file from variable above in current working directory
fs.open(gitChangesPath, 'r', (err, fd) => {
  if (err) {
    // console.log("Hello1");
    if (err.code === 'ENOENT') {
      console.error('Error: myfile does not exist')
    }
    throw err
  }
})

//try to update global variable above if file exists
try {
  readFile = fs.readFileSync(gitChangesPath, 'utf8')
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error('Error: git-changes.txt does not exist')
    console.log(err)
  } else {
    console.log('Unknown Error code: ' + err.code)
  }
  process.exit()
}

//Takes the txt file, replaces all new-line and carriage returns, strips them and splits into an array at every new line
var changedFiles = readFile
  .replace('\r\n', '\n')
  .replace('\r', '\n')
  .split('\n')
// console.log(changedFiles);
var testFiles = []
var pngImages = []
var jpgImages = []
for (i in changedFiles) {
  /**
   *  If the file is an HTML file and is stored in public_html:
   *  Convert it into a real URL
   *  If it is a removed file, ignore.
   *  If it is an index file, remove the index.html from the url.
   *  Heads up: This will not work on files in /root-files/
   *  Otherwise if it's a PNG or JPG put it to the appropriate array
   */

  // For some reason, "A" here means deleted even though it should mean Added.
  if (changedFiles[i].substr(0, 1) != 'A') {
    changedFiles[i] = changedFiles[i].substr(2)

    const extension = changedFiles[i].substr(-4, 4).toLowerCase()

    //if file is HTML file, cut the beginning of the localhost URL and replace with https://www.bestattorney.com
    if (extension == 'html' && changedFiles[i].substr(0, 11) == 'public_html') {
      changedFiles[i] = 'https://www.bestattorney.com' + changedFiles[i].substr(11)
      if (changedFiles[i].substr(-11, 10) == 'index.html') {
        changedFiles[i] = changedFiles[i].substr(0, changedFiles[i].length - 11)
      }
      testFiles.push(changedFiles[i])
    } else if (extension == '.png') {
      pngImages.push(changedFiles[i])
    } else if (extension == '.jpg') {
      jpgImages.push(changedFiles[i])
    }
  }
}

module.exports.testFiles = testFiles
module.exports.newImages = [jpgImages, pngImages]
