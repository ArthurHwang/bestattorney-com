const request = require('request');
const changedFiles = require('./helpers/ReadGitChanges.js');

const defaultTests = [
    "https://www.bestattorney.com/?test=true",
    "https://www.bestattorney.com/abogados/?test=true",
    "https://www.bestattorney.com/about-us/?test=true",
    "https://www.bestattorney.com/case-results/?test=true",
    "https://www.bestattorney.com/contact.html?test=true",
    "https://www.bestattorney.com/los-angeles/contact.html?test=true",
    "https://www.bestattorney.com/car-accidents/?test=true",
    "https://www.bestattorney.com/orange-county/?test=true",
    "https://www.bestattorney.com/los-angeles/?test=true",
    // "https://www.bestattorney.com/toxic-substance/test-page.html?test=true",
    "https://www.bestattorney.com/blog/?test=true",
    "https://www.bestattorney.com/blog/newport-beach-law-firm-represents-victim-of-las-vegas-shooting-who-suffers-from-ptsd?test=true"
]
//If there are too many pages to test, only test 50. 
if (changedFiles.testFiles.length > 50) {
    changedFiles.testFiles = changedFiles.testFiles.slice(0, 50);
}
// Add query parameter to the end of the URL so that we can test it properly. 
for (let i = 0; i < changedFiles.testFiles.length; i++) {
    changedFiles.testFiles[i] = changedFiles.testFiles[i]+"?test=true";
}

//Merge default tests with changedFiles.
const testFiles = defaultTests.concat(changedFiles.testFiles);

console.log("==================================================");
console.log("Tests for recently changed files: "+testFiles.length+ " test(s) to run.");
console.log("==================================================");

jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;
describe("All Custom Tests", function () {

    var testResults;
    beforeAll(function (done) {

        var requestPromises = [];
        for (i in testFiles) {
            requestPromises.push(createRequestPromise(testFiles[i]));
        }

        Promise.all(requestPromises).then(function (responses) {
            // getPhpLog().then(function (phpErrors) {
                // console.log("=======================");
                // console.log(responses);
                // console.log("=======================");
            //     testResults = testUrlsForPhpErrors(responses, phpErrors);
                testResults = responses;
                done();
            // }).catch(function (err) {
            //     console.log("Error from nested: " + err);
            // });
        }).catch(function (err) {
            console.log("Error from outer: " + err);
        });
    });

    for (var l = 0; l < testFiles.length; l++) {
        (function() {
            var i = l;
            it("custom url: should return 200", function () {
                testFor200(testResults, i);
            })
        })();
        (function() {
            var i = l;
            it("custom url: should not have PHP errors", function () {
                testForErrors(testResults, i);
            });
        })();
    }
});

function testFor200(testResults, i) {
    console.log("Testing: "+testResults[i].url);
    if (testResults[i].statusCode != 200) {
        console.log("==== ERROR: " + testResults[i].url + " returns status " + testResults[i].statusCode + " ====");
    }
    expect(testResults[i].statusCode).toEqual(200);
} 

function testForErrors(testResults, i) {
    if (testResults[i].error != false) {
        console.log("==== ERROR: " + testResults[i].url + " triggered PHP errors! " + testResults[i].error + "====");
    }
    expect(testResults[i].error).toEqual(false);
}

/**
 * Run a simple url test with an expect() statment.  
 * @param {string} url - url to test
 * @param {function} done - passed in function to tell jasmine when async operation is complete
 */
function getReq(url, done) {
    var statusCode;
    request.get(url, (error, response, body) => {
        statusCode = response.statusCode;
        expect(statusCode).toEqual(200);
        done();
    });
}

/**
 * Returns a Promise that gets items in the public PHP log
 * Resolves to an array of each line item
 */
// function getPhpLog() {
//     return new Promise(function (resolve, reject) {
//         request.get("https://www.bestattorney.com/template-files/log/public_error_log.txt", (error, response, body) => {
//             if (error) reject("error from nested Request Promise: " + error);
//             resolve(body.split("\n"));
//         });
//     });
// }

/**
 * Checks if the url in the phpError line matches the url that we are querying 
 * @param {string} phpError - The line of the PHP error in the public error doc.
 * @param {string} responseUrl - The url of the changed file from github that we're checking.
 */
// function checkPhpError(phpError, responseUrl) {
//     var phpErrorUrl = phpError.substr(0, phpError.indexOf(" --- "));
//     // if (responseUrl == "https://www.bestattorney.com/toxic-substance/mercury-poisoning.html") {
//     //     console.log("ResponseURL is mercury poisoning.");
//     //     console.log("phpErrorUrl is '"+phpErrorUrl+"'");
//     //     console.log("They are equal? " + (phpErrorUrl == responseUrl));
//     // }
//     return phpErrorUrl == responseUrl;
// }

/**
 * Create a promise for each changed file that we got from github.
 * The promise resolves after we GET that link and set a timestamp.
 * Resolves an object with properties for URL, StatusCode, and Timestamp
 * @param {string} url - The URL that we're GETing.
 */
function createRequestPromise(url) {
    var timestamp = Math.round((new Date()).getTime() / 1000);

    return new Promise(function (resolve, reject) {
        let options = {
            url: url,
            headers: {
                'User-Agent': "CircleCI-testing"
            }
        };
        request.get(options, (error, response, body) => {
            if (error) reject("error from createRequestPromise: " + error);

            var error = (response.headers['x-php-error'] != undefined) ? response.headers['x-php-error'] : false;

            resolve({
                url: url,
                statusCode: response.statusCode,
                timestamp: timestamp,
                error: error
            });
        });
    });
}
/**
 * Iterate through each url to check and each line in the PHP error document
 * First Check if the error timestamp happens after our response timestamp, starting from the most recent errors.
 * If it happened before, we know there's no errors from the most recent commit. (break.)
 * Else, check if the URL is the same. If it's the same, there was an important error/warning that happened as a result of our commit. 
 * @param {Array} responses - Array of response objects that we already queried.
 * @param {Array} phpErrors - Array of PHP errors from the public error log.
 */
// function testUrlsForPhpErrors(responses, phpErrors) {
//     for (var i = 0; i < responses.length; i++) {
//         // console.log("Comparing response Timestamp: "+responses[i].timestamp+" to: ");
//         for (var j = phpErrors.length-2; j > 0; j -= 2) {
//             // console.log("phpErrors[" + j +"] timestamp: "+ phpErrors[j]);

//             if (parseInt(phpErrors[j]) >= responses[i].timestamp) {
//                 if( checkPhpError(phpErrors[j + 1], responses[i].url) ) {
//                     console.log("");
//                     console.log("!!!!============ There is an error ==========!!!!");
//                     console.log("Tested url: " + responses[i].url + " appears in phpErrors on line " + j);
//                     console.log("");
//                     responses[i].error = true;
//                     break; 
//                 }
//             } else {
//                 responses[i].error = false;
//                 // console.log("PHP Error timestamp is older than request timestamp. Breaking. ")
//                 break;
//             }
//         }
//     }
//     return responses;
// }