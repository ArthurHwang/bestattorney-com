const ImageCompressor = require("./ImageCompressor");

const imagesPath = "../../public_html/images/";
const blogImagesPath = "../../public_html/blog/wp-content/uploads/";

let a = new ImageCompressor(blogImagesPath);

// Calls git diff and spits out the changed files from the last two commits into a git-changes.txt file
let prom = a.getChangedFiles();

// Calls the JS readGitChanges, which is also used for testing html files that have been changed.
// Assigns changed images to this.changedImages;
a.readChangedFiles(prom).then(() => {
  console.log("read changed files. changed files are: ");
  console.log(a.changedImages[0]);
  console.log(a.changedImages[1]);
  a.runGitChangesAndBlog();
});
