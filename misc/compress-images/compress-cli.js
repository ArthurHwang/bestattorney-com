const ImageCompressor = require("./ImageCompressor");
const CompressOne = require("./CompressOne");
const minimist = require("minimist");

// CLI to run image compression on images or directories you choose.
// Defaults to adding all arguments to an array of directories to search for files that need compression.
// If the --onefile flag is used, compresses just the file that is passed in.
// If the --forceAllImages flag is used, re-compresses EVERYTHING. (default false)
// If the --webp flag is used, create webp files too (default true)

const args = minimist(process.argv.slice(2), {
  alias: {
    o: "onefile",
    f: "forceAllImages", //Doesn't work yet.
    w: "webp"
  }
});
console.log("args");
console.log(args);

const imagesPath = args._;

// Cannot short circuit eval assign automatic boolean.
args.w = args.w !== undefined ? args.w : true;
args.f = args.f !== undefined ? args.f : false;

if (imagesPath.length > 0) {
  let a = new ImageCompressor(imagesPath, {
    createWebp: args.w,
    force: args.f
  });
  a.runPaths();
}
if (args.o && args.o.length > 0) {
  let a = new CompressOne(args.o, {
    createWebp: args.w
  });
  a.minifyOne(args.o);
}
