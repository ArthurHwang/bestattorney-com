const ImageCompressor = require('./ImageCompressor');

const imagesPath = "../../public_html/images/";
const blogImagesPath = "../../public_html/blog/wp-content/uploads/";

let a = new ImageCompressor([imagesPath, blogImagesPath]);
a.runPaths();