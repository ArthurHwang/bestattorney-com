module.exports = {
    quicksort: function(array, left, right) {
        if (left < right) {
            let pivot = right;
            let partitionIndex = module.exports.partition(array, pivot, left, right);
            module.exports.quicksort(array, left, partitionIndex - 1);
            module.exports.quicksort(array, partitionIndex + 1, right);
        }
        return array;
    },
    partition: function(array, pivot, left, right) {
        const pivotValue = array[pivot].path;
        let partitionIndex = left;

        for (let i = left; i < right; i++) {
            if (array[i].path < pivotValue) {
                module.exports.swap(array, i, partitionIndex);
                partitionIndex++;
            }
        }
        module.exports.swap(array, right, partitionIndex);
        return partitionIndex;
    },
    swap: function(array, i, j) {
        const swap = array[i];
        array[i] = array[j];
        array[j] = swap;
    }
};
