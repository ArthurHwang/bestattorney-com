let ImageCompressor = require('./ImageCompressor');
const imageminJpegtran = require('imagemin-jpegtran');
// const imageminmozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const webp = require('imagemin-webp');
const imagemin = require('imagemin-keep-folder');


module.exports = class CompressOne extends ImageCompressor {

  // constructor(path, options) {
  //     super(path, options);
  // }

  minifyOne(file) {
    this.minImages(
      [file],
      [
        webp({
          quality: 100,
          method: 6
        }),
        imageminJpegtran({
          progressive: true
        }),
        imageminPngquant({
          quality: 100,
          speed: 1,
          verbose: true,
          strip: true
        })
      ],
      file + "has been minified!"
    ).then((data) => {
      console.log("One Image minified!");
      console.log(data);
    });
  }
}