const fs = require("graceful-fs");
const readdirp = require("readdirp");
const path = require("path");
const exec = require("child_process").exec;
const imageminJpegtran = require("imagemin-jpegtran");
// const imageminmozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require("imagemin-pngquant");
const webp = require("imagemin-webp");
const imagemin = require("imagemin-keep-folder");

const qs = require("./quicksort");

module.exports = class ImageCompressor {
  constructor(
    paths,
    options = {
      compressJpg: true,
      compressPng: true,
      createWebp: true,
      force: false //Doesn't work yet.
    }
  ) {
    this.paths = paths;
    this.options = options;
  }

  readFiles(relativeFilepath) {
    let self = this;
    return new Promise(function (resolve, reject) {
      let filesArray = [];
      let fileTypes = [];

      if (self.options.compressJpg) {
        fileTypes.push("*.jpg", "*.jpeg", "*.JPG");
      }
      if (self.options.compressPng) {
        fileTypes.push("*.png", "*.PNG");
      }
      if (self.options.createWebp) {
        fileTypes.push("*.webp");
      }

      // reads all files in a directory
      readdirp({
        root: path.resolve(__dirname, relativeFilepath),
        fileFilter: fileTypes
      })
        .on("warn", function (err) {
          reject(err);
        })
        .on("error", function (err) {
          reject(err);
        })
        .on("data", function (entry) {
          filesArray.push({
            path: entry.fullPath,
            modified: entry.stat.mtimeMs
          });
        })
        .on("end", function () {
          resolve(filesArray);
        });
    });
  }

  getChangedFiles() {
    const gitChangesPath = path.resolve(__dirname, "../../spec/helpers", "git-changes.txt");
    console.log("gitChangesPath:");
    console.log(gitChangesPath);
    return new Promise((res, rej) => {
      const child = exec("git diff-tree --name-status -r --no-commit-id HEAD HEAD~2 > " + gitChangesPath, (error, stdout, stderr) => {
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
        if (error !== null) {
          console.log(`exec error: ${error}`);
          rej();
        }
        res();
      });
    });
  }

  readChangedFiles(filesPromise) {
    return filesPromise.then(() => {
      const changedFiles = require("../../spec/helpers/ReadGitChanges");
      this.changedImages = changedFiles.newImages;
      console.log("In readChangedFiles() - Changed Images are:");
      console.log(this.changedImages);
    });
  }

  checkFiles(filesArray) {
    const minFiles = [];

    for (let i = 0; i < filesArray.length - 1; i++) {
      const dot = filesArray[i].path.indexOf(".");

      let path1 = filesArray[i].path;
      let path2 = filesArray[i + 1].path;

      const string1 = path1.substring(0, path1.length - 4);
      const string2 = path2.substring(0, path1.length - 4);

      if (string1 !== string2) {
        //If the strings are not equal, then there is no webp file and it needs to be created.
        minFiles.push(path1);
        if (i == filesArray.length - 2) {
          //If we're at the second to last path, and the last 2 paths don't match, we need to push the next one too
          minFiles.push(path2);
        }
      } else {
        // If the strings are equal, we need to check that the first image is the jpg/png file. (it should be if we sorted correctly)
        // Then check if the jpg/png files are newer than the webp files.
        // If so, we need to add that file to the list to be compressed and transformed into webp.
        let baseModified = filesArray[i].modified;
        let webpModified = filesArray[i + 1].modified;

        if (baseModified > webpModified) {
          minFiles.push(path1);
        }
        i++;
      }
    }
    let jpgs = [];
    let pngs = [];
    for (let j = 0; j < minFiles.length; j++) {
      if (minFiles[j].substring(minFiles[j].length - 3) == "jpg") {
        jpgs.push(minFiles[j]);
      } else if (minFiles[j].substring(minFiles[j].length - 3) == "png") {
        pngs.push(minFiles[j]);
      }
    }
    return [jpgs, pngs];
  }

  minImages(imageArray, plugins, doneString = "Finished Compression!") {
    return new Promise(function (resolve, reject) {
      console.log("starting minImages async function with: (" + imageArray[0] + ")");

      try {
        imagemin(imageArray, {
          plugins: plugins
        }).then(data => {
          console.log("=================");
          console.log(doneString);
          console.log("=================");
          resolve();
        });
      } catch (error) {
        console.log("error with imagemin:");
        console.log(error);
        reject(error);
      }
    });
  }

  createMinifyPromises(minFiles) {
    console.log(minFiles);
    const self = this;
    // Create array of promises that will resolve when minifying and such is done.
    let minPromises = [];
    if (self.options.compressJpg && minFiles[0].length > 0) {
      console.log(`There are ${minFiles[0].length} jpg files to be compressed`);

      // minPromises.push(self.minImages(minFiles[0], imageminmozjpeg({
      //     progressive: true,
      //     quality: 70
      // }), "jpgs compressed"));

      minPromises.push(
        self.minImages(
          minFiles[0],
          [
            imageminJpegtran({
              progressive: true
            })
          ],
          "jpgs compressed"
        )
      );
      if (self.options.createWebp) {
        minPromises.push(
          self.minImages(
            minFiles[0],
            [
              webp({
                quality: 100,
                method: 6
              })
            ],
            "webp created from jpg"
          )
        );
      }
    }
    if (self.options.compressPng && minFiles[1].length > 0) {
      console.log(`There are ${minFiles[1].length} png files to be compressed`);

      minPromises.push(
        self.minImages(
          minFiles[1],
          [
            imageminPngquant({
              quality: 60,
              speed: 1,
              verbose: true,
              strip: true
            })
          ],
          "pngs compressed"
        )
      );
      if (self.options.createWebp) {
        minPromises.push(
          self.minImages(
            minFiles[1],
            [
              webp({
                quality: 100,
                method: 6
              })
            ],
            "webp created from png"
          )
        );
      }
    }
    Promise.all(minPromises)
      .then(() => {
        console.log("Done Minifiying and Creating new images!");
      })
      .catch(err => {
        console.log("Error within self.minify promise.all");
      });
  }

  runPaths() {
    let self = this;
    let promiseArray = [];
    for (let i = 0; i < this.paths.length; i++) {
      promiseArray.push(this.readFiles(this.paths[i]));
    }

    Promise.all(promiseArray)
      .then(filesArray => {
        //Flatten array, all files are together in one array.
        filesArray = [].concat.apply([], filesArray);

        // Sort all files by their pathname
        qs.quicksort(filesArray, 0, filesArray.length - 1);

        // Check if files need to be compressed / created.
        const minFiles = self.checkFiles(filesArray);
        self.createMinifyPromises(minFiles);
      })
      .catch(err => {
        console.log("");
        console.log("==================");
        console.log("Error:");
        console.log(err);
        console.log("==================");
        console.log("");
      });
  }

  runGitChangesAndBlog() {
    let self = this;

    // Read all image files in the blog.
    // There should be only one string in paths.
    self
      .readFiles(self.paths)
      .then(filesArray => {
        //Sort by path name.
        qs.quicksort(filesArray, 0, filesArray.length - 1);

        // Check all blog files to see if they have need to be minified/webp'd
        // Returns an array [[jpgs], [pngs]]
        const minFiles = self.checkFiles(filesArray);

        // Concatenate all of the images together that need to be minified/webP'd
        minFiles[0] = minFiles[0].concat(self.changedImages[0]);
        minFiles[1] = minFiles[1].concat(self.changedImages[1]);

        // Minify those images.
        self.createMinifyPromises(minFiles);
      })
      .catch(err => {
        console.log("");
        console.log("==================");
        console.log("Error:");
        console.log(err);
        console.log("==================");
        console.log("");
      });
  }
};
