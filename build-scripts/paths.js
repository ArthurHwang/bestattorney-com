const path = require('path')

module.exports = {
  SRC: path.resolve(__dirname, '../public_html/'),
  DIST: path.resolve(__dirname, '../public_html/', 'dist'),

  ASSETS: '/dist'
}
