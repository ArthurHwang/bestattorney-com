const path = require('path')
const { SRC, DIST, ASSETS } = require('./paths')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
//const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const WatchTimePlugin = require('webpack-watch-time-plugin')
const webpack = require('webpack')

/* is there a better way to check these?
console.log('              SRC:', SRC);
console.log('             DIST:', DIST);
console.log('           ASSETS:', ASSETS);
console.log();
console.log('output.publicPath:', path.resolve(ASSETS, "js"));
console.log('      output.path:', path.resolve(DIST, "js"));
*/

module.exports = {
  //Creates a source map that allows you to debug code even when it's combined into 1 file.
  devtool: 'source-map',
  // Get the javascript from here:
  entry: {
    home: path.resolve(SRC, 'src', 'home-scripts.js'),
    'pa-geo': path.resolve(SRC, 'src', 'pa-geo-scripts.js'),
    default: path.resolve(SRC, 'src', 'default-scripts.js'),
    blog: path.resolve(SRC, 'src', 'blog-scripts.js'),
    contact: path.resolve(SRC, 'src', 'contact-scripts.js')
  },
  // Output JS to here:
  output: {
    path: path.resolve(DIST, 'js'),
    filename: '[name]-scripts.js',
    //sourceMapFilename: '[file].map', // this is the default anyways
    publicPath: path.resolve(ASSETS, 'js')
    //publicPath: "https://www.bestattorney.com/dist/js/" // doesn't make a difference
  },
  plugins: [
    // Display time when watcher rebuild happens
    WatchTimePlugin,
    // Minifies JS
    // new UglifyJsPlugin({
    // 	parallel: true,
    // 	sourceMap: true
    // }),
    // Outputs CSS to a css file.
    new ExtractTextPlugin({ filename: '../css/[name]-styles.css' }),

    /*
		// Minifies CSS (this breaks css source maps!)
		new OptimizeCssAssetsPlugin({
	      assetNameRegExp: /\.css$/g,
	      cssProcessor: require('cssnano'),
	      cssProcessorOptions: { discardComments: {removeAll: true }, sourceMap: true },
	      canPrint: true
	    }),
		*/

    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                //url: false,
                minimize: true,
                sourceMap: true // this doesn't actually do anything
              }
            }
          ]
        })
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '../css/'
            }
          }
        ]
      }
    ]
  }
}
