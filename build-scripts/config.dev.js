const merge = require('webpack-merge')

module.exports = merge(require('./config.base.js'), {
  watch: true,
  watchOptions: {
    aggregateTimeout: 1000,
    ignored: /node_modules/
  }
})
